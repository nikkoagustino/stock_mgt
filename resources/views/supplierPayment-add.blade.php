@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Add New Supplier Payment</h1>
    </div>
</div>

<form action="{{ url('supplierPayment/add') }}" method="post" enctype="multipart/form-data">
    @csrf


    <div class="row">
        <div class="col-2">
            Supplier Code<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code" required="required" class="form-control fill_supplier">
                        <option value="" disabled="disabled" selected="selected"></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>


    <div class="row">
        <div class="col-2">
            Tanggal <span class="required">*</span>
        </div>
        <div class="col-4">
            <?php date_default_timezone_set("Asia/Jakarta"); ?>
            <input type="datetime-local" value="{{date('Y-m-d H:i:s')}}" name="tanggal" class="form-control">
        </div>
    </div>
    <div class="row">
        <div class="col-2">
           Nominal Pembayaran <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="total" type="number" required="required" class="form-control" placeholder="Angka Saja Tanpa Titik dan Koma"></input>
        </div>
    </div>
    

    <div class="row">
        <div class="col-2">
            Total Pcs <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="pcs" type="number" required="required" class="form-control" placeholder="Angka Saja Tanpa Titik dan Koma"></input>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            Detail Produk
        </div>
        <div class="col-4">
            <textarea name="detail" required="required" placeholder="Semua Yang Di bayar" rows="5" class="form-control"></textarea>
            </div>
    </div>

        
    <div class="row">
        <div class="col-2">
            <input type="submit" value="Save" class="form-control btn btn-success">
        </div>
    </div>
</form>
@endsection