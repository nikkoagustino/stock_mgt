@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Tambah Produk / Stok Masuk</h1>
    </div>
</div>
<div class="row" style="border: 2px solid #aaa; margin: 10px">
    <div class="row" style="font-size: 12px">
        <div class="col-2">
            Supplier
            <select name="supplier_code" id="" class="form-control">
                <option value="" disabled="disabled" selected="selected"></option>
                @foreach ($supplier as $row)
                <option value="{{ $row->supplier_code }}">{{ $row->supplier_code }} - {{ $row->supplier_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-1">
            Kode Internal
            <input type="text" name="internal_code" class="form-control">
        </div>
        <div class="col-1">
            Kode Eksternal
            <input type="text" name="external_code" class="form-control">
        </div>
        <div class="col-2">
            Nama Produk
            <input type="text" name="product_name" class="form-control">
        </div>
        <div class="col-1">
            Harga Supplier
            <input type="number" name="supplier_price" class="form-control">
        </div>
        <div class="col-1">
            Harga Jual
            <input type="number" name="selling_price" class="form-control">
        </div>
    </div>
    <div class="row" style="font-size: 12px">
        <div class="col-2">
            Variant
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>
                            <button style="width: 100%" class="btn btn-primary btn-sm" id="btnTambahVariant">Tambah Variant</button>
                        </th>
                    </tr>
                </thead>
                <tbody class="rowVariant">
                    <tr>
                        <td>
                            <input type="text" name="variant[]" class="form-control">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-10" style="overflow: auto">
            Qty Stok Masuk
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>XS</th>
                        <th>S</th>
                        <th>M</th>
                        <th>L</th>
                        <th>XL</th>
                        <th>2XL</th>
                        <th>3XL</th>
                        <th>4XL</th>
                        <th>5XL</th>
                        <th>6XL</th>
                        @for ($i = 25; $i <= 45; $i++)
                        <th>{{ $i }}</th>
                        @endfor
                    </tr>
                </thead>
                <tbody class="rowSize">
                    
                    <tr>
                        <td><input type="number" name="size_xs[]" class="form-control"></td>
                        <td><input type="number" name="size_s[]" class="form-control"></td>
                        <td><input type="number" name="size_m[]" class="form-control"></td>
                        <td><input type="number" name="size_l[]" class="form-control"></td>
                        <td><input type="number" name="size_xl[]" class="form-control"></td>
                        <td><input type="number" name="size_2xl[]" class="form-control"></td>
                        <td><input type="number" name="size_3xl[]" class="form-control"></td>
                        <td><input type="number" name="size_4xl[]" class="form-control"></td>
                        <td><input type="number" name="size_5xl[]" class="form-control"></td>
                        <td><input type="number" name="size_6xl[]" class="form-control"></td>
                        @for ($i = 25; $i <= 45; $i++)
                        <td><input type="number" name="size_{{ $i }}" class="form-control"></td>
                        @endfor
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div style="display: none">
    <table>
        
    <tr class="copyRowVariant">
            <td>
                <input type="text" name="variant[]" class="form-control">
            </td>
        </tr>
    <tr class="copyRowSize">
            <td><input type="number" name="size_xs[]" class="form-control"></td>
            <td><input type="number" name="size_s[]" class="form-control"></td>
            <td><input type="number" name="size_m[]" class="form-control"></td>
            <td><input type="number" name="size_l[]" class="form-control"></td>
            <td><input type="number" name="size_xl[]" class="form-control"></td>
            <td><input type="number" name="size_2xl[]" class="form-control"></td>
            <td><input type="number" name="size_3xl[]" class="form-control"></td>
            <td><input type="number" name="size_4xl[]" class="form-control"></td>
            <td><input type="number" name="size_5xl[]" class="form-control"></td>
            <td><input type="number" name="size_6xl[]" class="form-control"></td>
            @for ($i = 25; $i <= 45; $i++)
            <td><input type="number" name="size_{{ $i }}" class="form-control"></td>
            @endfor
        </tr>
    </table>
</div>
@endsection
@section('scripts')
<style type="text/css">
    table th {
        height: 50px;
        font-size: 20px;
        text-align: center;
    }
    table input.form-control {
        padding: 5px;
    }
    table input[type=number].form-control {
        width: 60px;
    }
</style>
<script>
$(document).ready(function(){
    $('select[name=supplier_code]').on('change', function(){
        $('input[name=internal_code]').val($('select[name=supplier_code]').val());
    });

    $('input[name=internal_code]').on('change paste keyup', function(){
        var ajax_url = "{{ url('api/product') }}/" + $('input[name=internal_code]').val();
        console.log('GET ' + ajax_url);
        $.ajax({
            method: 'get',
            url: ajax_url,
            datatype: 'json',
            success: function(result) {
                var data = $.parseJSON(result);
                console.log(data);
                $('input[name=external_code]').val(data[0].external_code);
                $('input[name=product_name').val(data[0].product_name);
                $('input[name=supplier_price').val(data[0].supplier_price);
                $('input[name=selling_price').val(data[0].selling_price);
            }
        });
    });

    $('#btnTambahVariant').on('click', function(){
        event.preventDefault();
        $('.rowVariant').append($('.copyRowVariant').clone());
        $('.rowSize').append($('.copyRowSize').clone());
    })
});
</script>
@endsection