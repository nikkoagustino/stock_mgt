@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Produk</h1>
        <a href="{{ url('products/add') }}" class="btn btn-primary">+ Tambah Produk</a>
    </div>
</div>
<div class="row">
    <div class="col-2">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Produk</th>
                </tr>
                <tr>
                    <th>Size</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach($variants as $row)
                <?php
                $array_variant[$i] = $row->variant;
                $i++;
                ?>
                <tr>
                    <td>{{ $row->variant }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <?php
    foreach ($products as $row):
    $total_product[$row->internal_code]['size_xs'] = 0;
    $total_product[$row->internal_code]['size_s'] = 0;
    $total_product[$row->internal_code]['size_m'] = 0;
    $total_product[$row->internal_code]['size_l'] = 0;
    $total_product[$row->internal_code]['size_xl'] = 0;
    $total_product[$row->internal_code]['size_2xl'] = 0;
    $total_product[$row->internal_code]['size_3xl'] = 0;
    $total_product[$row->internal_code]['size_4xl'] = 0;
    $total_product[$row->internal_code]['size_5xl'] = 0;
    $total_product[$row->internal_code]['size_6xl'] = 0;
    for ($i=25; $i <= 45 ; $i++) {
    $key = 'size_'.$i;
    $total_product[$row->internal_code][$key] = 0;
    }
    endforeach;
    foreach ($products as $row):
    $array_product[$row->internal_code][$row->variant]['size_xs'] = $row->size_xs;
    $array_product[$row->internal_code][$row->variant]['size_s'] = $row->size_s;
    $array_product[$row->internal_code][$row->variant]['size_m'] = $row->size_m;
    $array_product[$row->internal_code][$row->variant]['size_l'] = $row->size_l;
    $array_product[$row->internal_code][$row->variant]['size_xl'] = $row->size_xl;
    $array_product[$row->internal_code][$row->variant]['size_2xl'] = $row->size_2xl;
    $array_product[$row->internal_code][$row->variant]['size_3xl'] = $row->size_3xl;
    $array_product[$row->internal_code][$row->variant]['size_4xl'] = $row->size_4xl;
    $array_product[$row->internal_code][$row->variant]['size_5xl'] = $row->size_5xl;
    $array_product[$row->internal_code][$row->variant]['size_6xl'] = $row->size_6xl;
    $total_product[$row->internal_code]['size_xs'] = $total_product[$row->internal_code]['size_xs'] + $row->size_xs;
    $total_product[$row->internal_code]['size_s'] = $total_product[$row->internal_code]['size_s'] + $row->size_s;
    $total_product[$row->internal_code]['size_m'] = $total_product[$row->internal_code]['size_m'] + $row->size_m;
    $total_product[$row->internal_code]['size_l'] = $total_product[$row->internal_code]['size_l'] + $row->size_l;
    $total_product[$row->internal_code]['size_xl'] = $total_product[$row->internal_code]['size_xl'] + $row->size_xl;
    $total_product[$row->internal_code]['size_2xl'] = $total_product[$row->internal_code]['size_2xl'] + $row->size_2xl;
    $total_product[$row->internal_code]['size_3xl'] = $total_product[$row->internal_code]['size_3xl'] + $row->size_3xl;
    $total_product[$row->internal_code]['size_4xl'] = $total_product[$row->internal_code]['size_4xl'] + $row->size_4xl;
    $total_product[$row->internal_code]['size_5xl'] = $total_product[$row->internal_code]['size_5xl'] + $row->size_5xl;
    $total_product[$row->internal_code]['size_6xl'] = $total_product[$row->internal_code]['size_6xl'] + $row->size_6xl;
    for ($i=25; $i <= 45 ; $i++) {
    $key = 'size_'.$i;
    $array_product[$row->internal_code][$row->variant][$key] = $row->$key;
    $total_product[$row->internal_code][$key] = $total_product[$row->internal_code][$key] + $row->$key;
    }
    endforeach;
    ?>
    <div class="col-10" style="overflow: auto; display: inline-flex;">
        @foreach ($product_code as $rowx)
        <div style="display: inline;">
            
            <table class="table table-bordered table-striped" style="border: 1px solid black; display: table-cell;">
                <thead>
                    <tr>
                        <th colspan="10" style="text-align: center; white-space: nowrap;">{{ $rowx->internal_code }} - {{ $rowx->product_name }}</th>
                    </tr>
                    <tr>
                        @if ($total_product[$rowx->internal_code]['size_xs'] > 0)
                        <th width="5%">XS</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_s'] > 0)
                        <th width="5%">S</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_m'] > 0)
                        <th width="5%">M</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_l'] > 0)
                        <th width="5%">L</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_xl'] > 0)
                        <th width="5%">XL</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_2xl'] > 0)
                        <th width="5%">2XL</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_3xl'] > 0)
                        <th width="5%">3XL</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_4xl'] > 0)
                        <th width="5%">4XL</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_5xl'] > 0)
                        <th width="5%">5XL</th>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_6xl'] > 0)
                        <th width="5%">6XL</th>
                        @endif
                        <?php
                        for ($i=25; $i <= 45 ; $i++) {
                            $key = 'size_'.$i;
                            if ($total_product[$rowx->internal_code][$key] > 0) {
                                echo '<th width="5%">'.$i.'</th>';
                            }
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < count($array_variant); $i++)
                    <tr>
                        @if ($total_product[$rowx->internal_code]['size_xs'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_xs']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_xs'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_s'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_s']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_s'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_m'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_m']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_m'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_l'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_l']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_l'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_xl'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_xl']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_xl'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_2xl'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_2xl']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_2xl'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_3xl'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_3xl']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_3xl'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_4xl'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_4xl']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_4xl'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_5xl'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_5xl']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_5xl'] : '&nbsp;'; !!}</td>
                        @endif
                        @if ($total_product[$rowx->internal_code]['size_6xl'] > 0)
                        <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]]['size_6xl']) ? $array_product[$rowx->internal_code][$array_variant[$i]]['size_6xl'] : '&nbsp;'; !!}</td>
                        @endif

                        <?php
                        for ($x=25; $x <= 45 ; $x++) {
                            $key = 'size_'.$x;
                            if ($total_product[$rowx->internal_code][$key] > 0) { ?>
                                <td>{!! isset($array_product[$rowx->internal_code][$array_variant[$i]][$key]) ? $array_product[$rowx->internal_code][$array_variant[$i]][$key] : '&nbsp;'; !!}</td>
                        <?php
                            }
                        }
                        ?>
                    </tr>
                    @endfor
                </tbody>
            </table>
        </div>
        @endforeach
    </div>
</div>
@endsection