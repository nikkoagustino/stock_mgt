<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
  <meta name="author" content="AdminKit">
  <meta name="keywords" content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link rel="shortcut icon" href="{{ url('public/assets/img/icons/icon-48x48.png') }}" />
  <title>AdminKit Demo - Bootstrap 5 Admin Template</title>
  <link href="{{ url('public/assets/css/app.css') }}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/b71ce7388c.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <style type="text/css">
    .alert {
      position: fixed;
      z-index: 999;
      top: 20px;
      left: 40vw;
    }
    .required {
      color: red;
    }
    .form-control {
      margin-bottom: 10px;
    }
    .coming-soon h1 {
      text-align: center;
      vertical-align: middle;
      line-height: 100vh;
    }
    .table-api td {
      border: 1px solid #ddd;
    }

    .table-api {
      border: 5px solid lightblue;
    }
    .table-api tr:first-child td {
      background: lightblue;
      font-weight: bold;
      font-size: 18px;
    }

    .table-api tr:last-child td {
      border-top: 3px solid lightblue;
    }
    header .fas {
      font-size: 25px;
      margin-bottom: 5px;
      text-align: center;
      display: block;
    }
    </style>
</head>
<body>
  <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
    </symbol>
    <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
    </symbol>
    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </symbol>
  </svg>
  <header>
    <div class="px-3 py-2 bg-dark text-white">
      <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
          <a href="/" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
            MyAdmin
          </a>

          <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
            <li>
              <a href="{{ url('/') }}" class="nav-link text-white">
                <i class="fas fa-chart-area"></i>
                Dashboard
              </a>
            </li>
            <li>
              <a href="#" class="nav-link text-white">
                <i class="fas fa-dolly"></i>
                Pesanan
              </a>
            </li>
            <li>
              <a href="{{ url('products') }}" class="nav-link text-white">
                <i class="fas fa-cubes"></i>
                Produk
              </a>
            </li>
            <li>
              <a href="{{ url('supplier') }}" class="nav-link text-white">
                <i class="fas fa-truck"></i>
                Supplier
              </a>
            </li>
            <li class="sidebar-item">
                <a class="nav-link text-white" href="{{ url('api/documentation') }}">
                    <i class="fas fa-plug"></i> API Docs
                </a>
            </li>
            <li>
              <a href="{{ url('system-log') }}" class="nav-link text-white">
                <i class="fas fa-list-ul"></i>
                Log Sistem
              </a>
            </li>
            <li>
              <a href="#" class="nav-link text-white">
                <i class="fas fa-users"></i>
                Akun
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>
  <div class="container-fluid" style="padding-top: 10px">
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
          <strong>{{ $message }}</strong>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
          <strong>{{ $message }}</strong>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif
    @yield('content')
  </div>
  @yield('scripts')
</body>
</html>