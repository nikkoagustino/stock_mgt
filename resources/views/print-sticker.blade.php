<?php
$array_lines = explode('|', $param);
foreach ($array_lines as $row) {
    if (!empty($row)) {
        $array_data = explode('_', $row);
        
        for ($i=0; $i < $array_data[3]; $i++) { 
            echo "<div class='sticker'>";
            echo "<img src='".url('public/logo_square.jpg')."'>";
            echo "<div class='text'>";
            echo "<span style='font-size: 14px'><b>".$array_data[0]."</b></span>";
            echo "<span>".$array_data[5]."</span>";
            echo "<span>IDR ".number_format($array_data[4])."</span>";
            echo "<span>".$array_data[1]."</span>";
            echo "<span style='font-size:16px'><b>".$array_data[2]."</b></span>";
            echo "</div>";
            echo "</div>";
        }
    }
}
?>

<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 10px;  /* this affects the margin in the printer settings */
    }
</style>
<style>
    body {
        font-family: Arial;
        font-size: 10px;
    }
    .sticker {
        border: 1px solid blue;
        width: 150px;
        margin: 5px;
        display: inline-block;
        padding: 2px 10px;
        text-align: center;
    }
    img {
        width: 65px;
        display: inline-block;
        margin: 5px;
    }
    .text {
        display: inline-block;
    }
    span {
        margin: 2px;
        display: block;
    }
</style>
<script type="text/javascript">
    window.onload = function () {
        window.print();
    }
</script>