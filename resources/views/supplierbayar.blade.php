@extends('template')
@section('content')
<div class="row">
    <div class="col-14">
        <h1>Supplier</h1>
        <a href="{{ url('supplierPayment/add') }}" class="btn btn-primary">+ Add New Supplier Payment</a>
    </div>
</div>
<div class="row">
    <div class="col-14 table-scroll-x">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Login</th>
                    <th>Nama</th>
                    <th>Item Code</th>
                    <th>Ttl Item Terjual</th>
                    <th>Ttl Hrg Terjual</th>
                    <th>Item Sdh Byr Bayar</th>
                    <th>Sudah Di bayar</th>
                    
                    <th>Item Blm Byr</th>
                    
                    <th>Est Belum Byr</th>
                    
                    <th>Terakhir</th>
                    <th>Sisa Item</th>
                    <th>Valuasi Stok</th>
                </tr>
            </thead>
            <tbody>
                <?php $paymentarray=(array) $paymentarray;?>
                @foreach($paymentarray as $key => $value)
                <tr>
                    <td><a href="supplierlogin/editlogin/{{ $key }}">{{ $key }}</a></td>
                    <td>{{ $paymentarray[$key]['nama'] }}</td>
                    <td><a href="rekapstokadmin/{{ $paymentarray[$key]['kode'] }}">{{ $paymentarray[$key]['kode'] }}</a></td>
                    <td>{{ number_format($paymentarray[$key]['penjualan']->itemsales-5) }}</td>
                    <td>{{ number_format($paymentarray[$key]['penjualan']->totalsales) }}</td>
                    <td>{{ number_format($paymentarray[$key]['pembayaran']->pcs ??0) }}</td>
                    <td>{{ number_format($paymentarray[$key]['pembayaran']->total ??0)}}</td>
                    <td><strong><p style="color:red">{{ (number_format($paymentarray[$key]['penjualan']->itemsales-5) - ($paymentarray[$key]['pembayaran']->pcs ??0)) }}</p></strong></td>
                    <td><strong><p style="color:red">{{ number_format($paymentarray[$key]['penjualan']->totalsales - ($paymentarray[$key]['pembayaran']->total ??0)) }}</p></strong></td>

                    <td>{{ $paymentarray[$key]['pembayaran']->tanggalakhir ??0 }}</td>
                    <td>{{ number_format($paymentarray[$key]['stok']->total_item_stock ??0) }}</td>
                    <td>{{ number_format($paymentarray[$key]['stok']->total_valuasi_stock ??0)}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
<script>

function deleteOrder(id) {
    if (confirm("Yakin Hapus Data?")) {
        window.location.href = "{{ url('supplierlogin/delete/') }}/"+id;
    }
}

</script>
@endsection