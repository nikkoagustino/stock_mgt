@extends('template')
@section('content')
<?php 
$data_stock = [];
foreach ($stock as $row) :
    $data_stock[$row->internal_code][$row->variant][$row->size]['qty'] = $row->qty;
    $data_stock[$row->internal_code][$row->variant][$row->size]['supplier'] = $row->supplier_code;
    $data_stock[$row->internal_code][$row->variant][$row->size]['external_code'] = $row->external_code;
    if (!empty($row->images)) {
        $data_stock[$row->internal_code][$row->variant][$row->size]['image'] = $row->images;
    } else {
        $data_stock[$row->internal_code][$row->variant][$row->size]['image'] = 'public/no_image.png';
    }
endforeach;
?>
<div class="row">
    <div class="col-12">
        <h2>Cek Stok : {{ ($type == "all") ? "Semua Data" : "Hanya Stok Kurang" }}</h2>
{{--         <div class="filter noprint"> 
            <b>Filter Supplier</b>
            <select class="form-control filter-supplier">
                <option value="" selected="selected">Semua Supplier</option>
                @foreach ($supplier as $row)
                <option value="{{ $row->supplier_code }}">{{ $row->supplier_code }} - {{ $row->supplier_name }}</option>
                @endforeach
            </select>
        </div> --}}
        <button class="btn btn-primary noprint" data-bs-toggle="modal" data-bs-target="#filterModal"><i class="fas fa-filter"></i> Filter Supplier</button>
        <button class="btn btn-success noprint" onclick="window.print()"><i class="fas fa-print"></i> Print Laporan</button>    
        <div class="table-scroll-x">
        <table class="table table-striped table-stock">
            <thead>
                <tr>
                    <th>Supplier</th>
                    <th>External Code</th>
                    <th>Kode Internal</th>
                    <th>Product Name</th>
                    <th>Variant</th>
                    <th>Size</th>
                    <th>Selisih</th>
                    <th>Qty Order</th>
                    <th>Qty Stock</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($order as $row)
                @if (isset($data_stock[$row->internal_code][$row->variant][$row->size]['qty']))
                <?php $stock_qty = $data_stock[$row->internal_code][$row->variant][$row->size]['qty']; ?>
                @else
                <?php $stock_qty = 0; ?>
                @endif

                @if ($type == 'minus')
                    @if ($stock_qty - $row->qty < 0)
                        <?php $showrow = true; ?>
                        @if (isset($_GET['filter']))
                            <?php $filter_supplier = explode("|", $_GET['filter']); ?>
                            @if ((isset($master[$row->internal_code]['supplier_code'])) && (in_array($master[$row->internal_code]['supplier_code'], $filter_supplier)))
                                <?php $showrow = true; ?>
                            @else
                                <?php $showrow = false; ?>
                            @endif
                        @endif
                    @else
                        <?php $showrow = false; ?>
                    @endif
                @else
                    <?php $showrow = true; ?>
                    @if (isset($_GET['filter']))
                        <?php $filter_supplier = explode("|", $_GET['filter']); ?>
                        @if ((isset($master[$row->internal_code]['supplier_code'])) && (in_array($master[$row->internal_code]['supplier_code'], $filter_supplier)))
                            <?php $showrow = true; ?>
                        @else
                            <?php $showrow = false; ?>
                        @endif
                    @endif
                @endif 


                @if ($showrow == true)
                <tr>
                    <td>{{ (isset($master[$row->internal_code]['supplier_code'])) ? $master[$row->internal_code]['supplier_code'] : 'N/A' }}</td>
                    <td>{{ (isset($master[$row->internal_code]['external_code'])) ? $master[$row->internal_code]['external_code'] : 'N/A' }}</td>
                    <td>{{ $row->internal_code }}</td>
                    <td>{{ (isset($master[$row->internal_code]['product_name'])) ? $master[$row->internal_code]['product_name'] : 'N/A' }}</td>
                    <td class="nowrap">
                        <div class="thumbnail">
                            @if (isset($data_stock[$row->internal_code][$row->variant][$row->size]['image']))
                            <img onclick="changeSrc(this)" data-bs-toggle="modal" data-bs-target="#imageModal" src="{{ url('public').Storage::url( $data_stock[$row->internal_code][$row->variant][$row->size]['image']); }}">
                            @else 
                            <img onclick="changeSrc(this)" data-bs-toggle="modal" data-bs-target="#imageModal" src="{{ url('public').Storage::url('public/no_image.png');}}">
                            @endif
                        </div>
                        {{ $row->variant }}
                    </td>
                    <td>{{ $row->size }}</td>
                    @if ( $stock_qty - $row->qty < 0)
                    <td style="font-weight:bold; color: red">{{ $stock_qty - $row->qty }}
                    @elseif ($stock_qty - $row->qty > 0)
                    <td style="font-weight:bold; color: green">+{{ $stock_qty - $row->qty }}
                    @else
                    <td>{{ $stock_qty - $row->qty }}
                    @endif
                     &nbsp; <a class="btn btn-sm btn-warning noprint" href="{{ url('stock-check/detail') }}/{{ $row->internal_code }}/{{ $row->variant }}/{{ $row->size }}" target="_blank"><i class="fas fa-eye"></i></a>
                    </td>
                    <td>{{ $row->qty }}</td>
                    <td>{{ $stock_qty }}</td>
                </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="filterModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="filterModalLabel">Filter Supplier</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body overflow-auto" style="height: 50vh">
        @foreach ($supplier as $row)
        <div class="form-check">
          <input class="form-check-input" name="filter_check" type="checkbox" value="{{ $row->supplier_code }}"
          <?php if ((isset($_GET['filter'])) && (in_array($row->supplier_code, $filter_supplier))) {
            echo "checked";
          }
          ?>
          >
          <label class="form-check-label" for="flexCheckChecked">
            {{ $row->supplier_code }} - {{ $row->supplier_name }}
          </label>
        </div>
        @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="window.location.href='{{ Request::url() }}'">Lihat Semua Data</button>
        <button type="button" class="btn btn-primary" onclick="filterCompareStock()">Terapkan Filter</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('off-canvas')
    
    <!-- Pushable content along with off-canvas opener. -->
    <div class="bs-offset-main bs-canvas-anim noprint" id="btn-right-menu">
        <button class="btn btn-primary" type="button" data-toggle="canvas" data-target="#bs-canvas-right" aria-expanded="false" aria-controls="bs-canvas-right">&#9776; Tampil Supplier</button> 
    </div>
    
        
    <div id="bs-canvas-right" class="bs-canvas bs-canvas-anim bs-canvas-right position-fixed bg-light h-100" data-width="600px">
        <header class="bs-canvas-header p-3 bg-primary overflow-auto">
            <button type="button" class="bs-canvas-close float-left close" aria-label="Close"><i class="fas fa-times"></i></button>
            <h4 class="d-inline-block text-light mb-0 float-right">Supplier List</h4>
        </header>
        <div class="bs-canvas-content px-3 py-5">
            <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Supplier</th>
                    <th>Alamat</th>
                    <th>Kontak</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($supplier as $row): 
                echo "<tr><td>".$row->supplier_code."<br>".$row->supplier_name."</td>";
                echo "<td>".$row->location."</td>";
                echo "<td>".$row->phone."<br>".$row->email."</td></tr>";
            endforeach;
            ?>
            </tbody>
        </table>
        </div>    
    </div>
@endsection
@section('script')
<style>
.filter-supplier {
    width: 300px;
}
.filter,
.filter .form-control {
    display: inline-block;
    margin-bottom: 20px;
}
</style>
<script>
    var table = $('.table-stock').DataTable({
        pageLength: 25
    });

    // $('.filter-supplier').on('change', function() {
    //     table.draw();
    // } );

    // $.fn.dataTable.ext.search.push(
    //     function( settings, data, dataIndex ) {
    //         var supplier_code = $('.filter-supplier option:selected').val();
    //         var column = data[0];
    //         console.log(supplier_code);

    //         if (supplier_code == '') {
    //             return true;
    //         }
    //         if (column == supplier_code)
    //         {
    //             return true;
    //         }
    //         return false;
    //     }
    // );

    function filterCompareStock() {
        var checkbox_filter = "";
        $('input[name="filter_check"]:checked').each(function() {
           console.log(this.value);
           checkbox_filter += this.value + "|";
        });
        window.location.href = "{{ Request::url() }}?filter=" + checkbox_filter;
    }

    @if ((isset($_GET['trigger'])) && ($_GET['trigger'] == 'print'))
    $(document).ready(function(){
        window.print();
    });
    @endif

</script>
@endsection