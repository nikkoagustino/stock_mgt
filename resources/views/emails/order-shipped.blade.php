<html>
    <body>
        
Dear {{ '@'.$data->instagram }},<br>

<br>Terima kasih telah berbelanja bersama MomAvel.id.<br>

<br>Berikut informasi tracking pengiriman ID Express:<br>

<br>Pesanan anda telah terkirim. Pengiriman kami menggunakan ID Express. Nomor Resi Pengiriman : {{ $data->awb }}<br>

<br>Berikut link tracking(Apabila link traking tidak bekerja bisa kunjungi web IDEXPRESS):<br>
<br>https://idexpress.com/lacak-paket/?actionType=delivery&waybillNo={{ $data->awb }}<br>

<br>Terima kasih<br>

<br>Hormat Kami,
<br>MomAvel.id
    </body>
</html>