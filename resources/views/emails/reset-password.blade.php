<html>
    <body>
        
Dear {{ $userdata->firstname }} {{ $userdata->lastname }}<br>{{ '@'.$userdata->username }},<br>

<br>Anda telah melakukan permintaan reset password.<br>

<br>Silahkan klik link di bawah ini untuk melanjutkan reset password : 
<br><a href="{{ url('user/reset-password')."/".base64_encode($userdata->username.'|'.$userdata->password) }}">{{ url('user/reset-password')."/".base64_encode($userdata->username.'|'.$userdata->password) }}</a><br>

<br>Terima kasih<br>

<br>Hormat Kami,
<br>MomAvel.id
    </body>
</html>