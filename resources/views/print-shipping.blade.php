<div class="sticker">
    <table>
        <tr>
            <td width="120px">
                @if (isset($customer))
                <img style="height: 100px" src="{{ url('public').\Storage::url($customer->logo_toko) }}" alt="">
                @else
                <img src="{{ url('public/momavel.jpg') }}" alt="">
                @endif
                
            </td>
            <td width="300px">
                <b>Penerima :</b><br>
                <?php echo strtoupper(nl2br($shipping->shipping_address)); ?>
            </td>
            <td rowspan="2">
                
    <table class="item">
	    <tr>
            <th colspan="5" color="red">PERHATIAN!! MOHON VIDEOKAN UNBOXING!! KAMI TIDAK TERIMA KOMPLEN TANPA VIDEO UNBOXING!!</th>
        </tr>
        <tr>
            @if (isset($customer))
            @else
                <th colspan="5">Instagram: {{ '@'.$shipping->instagram }}</th>
            @endif
        </tr>
        <tr>
            <th>Kode</th>
            <th>Variant</th>
            <th>Size</th>
            <th>Qty</th>
            <th>&#10004;</th>
        </tr>
        @foreach($item as $row)
        <tr>
            <td>{{ $row->internal_code }}</td>
            <td>{{ $row->variant }}</td>
            <td>{{ $row->size }}</td>
            <td>{{ $row->qty }}</td>
            <td></td>
        </tr>
        @endforeach
    </table>
            </td>
        </tr>
        <tr>
            @if (isset($customer))
            <td>
                <b>Pengirim :</b><br>
                {{$customer->nama_toko}}<br>({{$customer->phone}})
            </td>
            @else
            <td>
                <b>Pengirim :</b><br>
                MomAvel.id<br>(+62 812-8344-3452)
            </td>
            @endif
            <td>
                <b>Deskripsi :</b><br>
                PAKAIAN<br>(No. Order {{ $shipping->id }})
            </td>

        </tr>

    </table>
	
</div>
<style type="text/css" media="print">
@page {
size: auto;   /* auto is the initial value */
margin: 10px;  /* this affects the margin in the printer settings */
}
</style>
<style>
table {
font-family: Arial;
        font-size: 12px !important;
line-height: 18px;
}
td {
    padding: 5px;
}
.sticker {
border: 1px solid blue;
width: 200mm;
margin: 5px;
display: inline-block;
padding: 2px 10px;
}
img {
width: 100px;
}
.item {
margin: 15px 10px;
border-collapse: collapse;
}
.item td, .item th {
border: 1px solid grey;
padding: 0px 10px;
text-align: center;
}
.item th {
background: lightgreen;
color-adjust: exact;
-webkit-print-color-adjust: exact;
}
</style>
<script type="text/javascript">
window.onload = function () {
window.print();
}
</script>