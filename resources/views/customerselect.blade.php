@extends('template')
@section('content')

<form action="{{ url('customerselect') }}" method="post">
@csrf
<div class="row p-5">
    <div >
        <div  >
            <h3>Message Blast</h3>
            <?php date_default_timezone_set('Asia/Jakarta');?>
            
        </div>
    </div>
    <?php $ttlstok=0;
          $ttlvaluasi=0;?>
        <div class="col-12">
        <br>
        Pilih List Customer :
        <br>
        <select name="pilihan" onchange="" class="l" required="required">
                            
                            <option value="Vincent">Vincent</option>
                            <option value="Paid Customer with Phone & Email">Paid Customer with Phone & Email</option>
                            <option value="Paid Customer with Email">Paid Customer with Email</option>
                            <option value="Paid Customer with Phone">Paid Customer with Phone</option>
                            
                            <option value="Customer No Order with Phone & Email">Customer No Order with Phone & Email</option>
                            <option value="Customer No Order with Email">Customer No Order with Email</option>
                            <option value="Customer No Order with Phone">Customer No Order with Phone</option>
                            <option value="WA OTO Registered ALL">WA OTO Registered ALL</option>
                            <option value="WA OTO Registered Paid">WA OTO Registered Paid</option>
                            <option value="WA OTO Registered No Transaction">WA OTO Registered No Transaction</option>
                            
        </select>
        <br>
        <br>
        <button type="submit" id="submitButton" class=" btn btn-success">
            &nbsp; Buka Data
        </button>
        </div>
    
</div>       
</form>
@endsection
@section('script')
<script>

    $(document).ready(function(){
        $('input[name=totitem]').val($ttlstok);
        $('input[name=totvaluasi]').val($ttlvaluasi);
    });
</script>
@endsection