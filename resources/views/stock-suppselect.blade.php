@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Cek Stok Berdasarkan Supplier</h2>
    </div>
</div>
<div class="row">
    <form action="{{ url('selectbysupp') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="col-12">
        
        <table class="table table-striped tableFixHead">
            <thead>
                <tr>

                    <th width="10%">Kode Supplier</th>
                    
                    <th width="3%" style="white-space: nowrap;"></th>
                </tr>
            </thead>
            <tbody class="table-fill">
                <tr data-seq="1">
                    
                    <td>
                        <select name="supplier_code" required="required" class="form-control fill_supplier">
                            <option value="" disabled="disabled" selected="selected"></option>
                            @foreach ($supplier as $row)
                            <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                            @endforeach
                        </select>
                    </td>
                    
                    <td>
                    <button class="btn btn-sm btn-primary btn-save" data-seq='0' onclick="Buka()"><i class="fas fa-save">Buka</i></button></td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</div>



@endsection

@section('script')
<script>
   function Buka(){
        if ($('select[name=internal_code]').val()=="" || $('select[name=internal_code]').val()==null){
        } else {
            window.location.href='{{ url('stock/editkode').'/' }}' + $('select[name=internal_code]').val();
        }
    }
</script>
@endsection