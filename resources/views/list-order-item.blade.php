@extends('template')
@section('content')
<?php foreach ($stock as $row) {
    $product_name[$row->internal_code] = $row->product_name;    
} ?>
<div class="row">
    <div class="col-12">
        <h2>List Order {{ "@".$instagram }}</h2>
        @if (isset($userdata))
        <h4>(Contact: {{ $userdata->phone }} / {{ $userdata->email }})</h4>
        @else
        <h4>Customer Belum Terdaftar</h4>
        @endif
    </div>
    <div class="col-12 table-scroll-x">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Kode Internal</th>
                    <th>Nama Produk</th>
                    <th style="width: 150px">Variant</th>
                    <th style="width: 100px">Size</th>
                    <th style="width:60px">Qty</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ($item_order as $row)
                <tr data-seq='{{$row->id}}'>
                    <td>{{ $row->internal_code }}</td>
                    <td>{{ (isset($product_name[$row->internal_code])) ? $product_name[$row->internal_code] : '' }}</td>
                    <td><input class="form-control uppercase" name="variant" type="text" value="{{ $row->variant }}" disabled="disabled"></td>
                    <td><input class="form-control uppercase" name="size" type="text" value="{{ $row->size }}" disabled="disabled"></td>
                    <td><input class="form-control" name="qty" type="number" value="{{ $row->qty }}" disabled="disabled"></td>
                    @if ($row->order_status == 'new')
                    <td style="white-space: nowrap;">
                        <a id="edit_{{$row->id}}" href="javascript:void(0)" onclick="editThis({{$row->id}})" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                        <a  style="display: none" id="save_{{$row->id}}" href="javascript:void(0)" onclick="saveEdit({{$row->id}})" class="btn btn-success btn-sm"><i class="fas fa-check"></i></a>
                        <a href="javascript:void(0)" onclick="deleteOrder({{ $row->id }})" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                    </td>
                    @else
                    <td></td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        <table class="table">
            <form action="{{ url('selling/add') }}" method="post">
                @csrf
                <input type="hidden" name="batch_no" value="{{ $item_order[0]->batch_no }}">
                <input type="hidden" name="return_url" value="{{ url()->current() }}">
                <input type="hidden" name="comment" value="Order manual by {{ \Session::get('fullname') }}">
                <tr>
                    <td colspan="5"><b>Tambah Order</b> <i>Hanya berlaku untuk barang ready stock</i></td>
                </tr>
                <tr>
                    <td width="15%">
                        Instagram
                        <input type="text" readonly="readonly" name="instagram" class="form-control" value="{{ $instagram }}">
                    </td>
                    <td width="15%">
                        Tanggal
                        <input type="text" readonly="readonly" name="tanggal" class="form-control" value="{{ $tanggal }}">
                    </td>
                    <td width="20%">
                        Pilih Produk
                        {{-- <select name="internal_code" onchange="updateVariant()" class="form-control" required="required"> --}}
                        <select name="internal_code" class="form-control" onchange="updateVariant()" required="required">
                            <option value="" disabled="disabled" selected="selected"></option>
                            @foreach($product_main as $row)
                            <option value="{{$row->internal_code}}">{{ $row->internal_code }} - {{ $row->product_name }}</option>
                            @endforeach
                        </select>
                    </td>
                    {{-- <td width="20%">
                        Pilih Variant
                        <select name="variant" onchange="updateSize()" class="uppercase form-control" required="required"></select>
                    </td>
                    <td width="10%">
                        Pilih Size
                        <select name="size" onchange="getStock()" class="uppercase form-control" required="required"></input>
                    </td> --}}
                    <td width="20%">
                        Pilih Variant
                        <select name="variant" onchange="updateSize()" class="uppercase form-control" required="required"></select>
                    </td>
                    <td width="10%">
                        Size
                        <select name="size" onchange="getStock()" class="form-control" required="required">
                            <option value="NOSIZE">NOSIZE</option>
                            <option value="ALLSIZE">ALLSIZE</option>
                            <option disabled="disabled">-------------</option>
                            <option value="3XS">3XS</option>
                            <option value="2XS">2XS</option>
                            <option value="XS">XS</option>
                            <option value="S">S</option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                            <option value="XL">XL</option>
                            <option value="2XL">2XL</option>
                            <option value="3XL">3XL</option>
                            <option value="4XL">4XL</option>
                            <option value="5XL">5XL</option>
                            <option value="6XL">6XL</option>
                            <option disabled="disabled">-------------</option>
                            @for ($i = 25; $i < 45; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </td>
                    <td>
                        Stok    
                        <span class="max-qty">0</span>
                    </td>
                    <td width="20%">
                        Masukkan Qty {{-- (Max: <span class="max-qty">0</span>) --}}
                        <input type="number" class="form-control" name="qty" required="required">
                    </td>
                    <td width="10%">
                        <button type="submit" class="form-control btn btn-success"><i class="fas fa-save"></i> &nbsp; Simpan</button>
                    </td>
                </tr>
            </form>
        </table>
    </div>
</div>
@endsection
@section('script')
<script>
function deleteOrder(id) {
    if (confirm("Yakin Hapus Data?")) {
        window.location.href = "{{ url('selling/order/delete') }}/"+id;
    }
}

function updateVariant() {
    var internal_code = $('select[name=internal_code] option:selected').val();
    var ajax_url = "{{ url('api/get-variant') }}/"+internal_code;
    console.log(ajax_url);
    $.ajax({
        url: ajax_url,
        method: "get",
        datatype: "json",
        success: function(result) {
            result = JSON.parse(result);
            console.log(result);
            var option = "<option value='' disabled='disabled' selected='selected'>-- Pilih Variant</option>";
            $.each(result, function(index, value){
                option += "<option value='"+value.variant+"'>"+value.variant+"</option>";
            });
            $('select[name=variant]').html(option);
        }
    });
}

function updateSize() {
    var internal_code = $('select[name=internal_code] option:selected').val();
    var variant = $('select[name=variant] option:selected').val();
    var ajax_url = "{{ url('api/get-size') }}/"+internal_code+"/"+variant;
    console.log(ajax_url);
    $.ajax({
        url: ajax_url,
        method: "get",
        datatype: "json",
        success: function(result) {
            result = JSON.parse(result);
            console.log(result);
            var option = "<option value='' disabled='disabled' selected='selected'>-- Pilih Variant</option>";
            $.each(result, function(index, value){
                option += "<option value='"+value.size+"'>"+value.size+"</option>";
            });
            $('select[name=size]').html(option);
        }
    });
}

function getStock() {
    var internal_code = $('select[name=internal_code] option:selected').val();
    var variant = $('select[name=variant] option:selected').val();
    var size = $('select[name=size] option:selected').val();
    var ajax_url = "{{ url('api/product') }}/"+internal_code+"/"+variant+"/"+size;
    console.log(ajax_url);
    $.ajax({
        url: ajax_url,
        method: "get",
        datatype: "json",
        success: function(result) {
            result = JSON.parse(result);
            console.log(result);
            // $('input[name=qty]').attr('max', result[0].qty);
            $('.max-qty').text(result[0].qty);
        }
    });

}

function editThis(rowid) {
    $('tr[data-seq='+rowid+'] input').removeAttr('disabled');
    $('#edit_'+rowid).hide();
    $('#save_'+rowid).show();
}

function saveEdit(rowid) {
    var qty = $('tr[data-seq='+rowid+'] input[name=qty]').val();
    var variant = $('tr[data-seq='+rowid+'] input[name=variant]').val();
    var size = $('tr[data-seq='+rowid+'] input[name=size]').val();

    var fd = new FormData();
    fd.append('id', rowid);
    fd.append('qty', qty);
    fd.append('variant', variant.toUpperCase());
    fd.append('size', size.toUpperCase());


    $.ajax({
        url: '{{url("api/update-order")}}',
        method: 'post',
        processData: false,
        contentType: false,
        cache: false,
        data: fd,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
            if (result == 'ok') {
                $('tr[data-seq='+rowid+'] input').attr('disabled', 'disabled');
                $('#edit_'+rowid).show();
                $('#save_'+rowid).hide();
            }
        }
    });
}

function addNewOrder() {
    var qty = $('tr[data-seq='+rowid+'] input[name=qty]').val();
    var variant = $('tr[data-seq='+rowid+'] input[name=variant]').val();
    var size = $('tr[data-seq='+rowid+'] input[name=size]').val();

    var fd = new FormData();
    fd.append('id', rowid);
    fd.append('qty', qty);
    fd.append('variant', variant.toUpperCase());
    fd.append('size', size.toUpperCase());


    $.ajax({
        url: '{{url("api/update-order")}}',
        method: 'post',
        processData: false,
        contentType: false,
        cache: false,
        data: fd,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
            if (result == 'ok') {
                $('tr[data-seq='+rowid+'] input').attr('disabled', 'disabled');
                $('#edit_'+rowid).show();
                $('#save_'+rowid).hide();
            }
        }
    });
}
</script> 
@endsection