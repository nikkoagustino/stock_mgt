@extends('template')
@section('content')
<?php 
foreach ($stock as $row) :
    $array_product[$row->internal_code]['internal_code'] = $row->internal_code;
    $array_product[$row->internal_code]['external_code'] = $row->external_code;
    $array_product[$row->internal_code]['product_name'] = $row->product_name;
    $array_product[$row->internal_code]['pin_to_top'] = $row->pin_to_top;
    $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['supplier_price'] = $row->supplier_price;
    $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['selling_price'] = $row->selling_price;
    $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['register_time'] = $row->register_time;
    $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['qty'] = $row->qty;
    
    $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['id'] = $row->id;
    if (!empty($row->keterangan)) {
        $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['keterangan'] = $row->keterangan;
    } else {
        $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['keterangan'] = '';
    }
    if (!empty($row->images)) {
        $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['image'] = $row->images;
    } else {
        $array_product[$row->internal_code]['variant'][$row->variant][$row->size]['image'] = 'public/no_image.png';
    }
endforeach;
?>
<div class="row">
    <div class="col-12">
        <h1>Stock Report</h1>
        <button class="btn btn-primary btn-reprint noprint" onclick="reprintSticker()"><i class="fas fa-tags"></i> Re-Print Stiker</button>
        <button class="btn btn-success noprint" onclick="window.print()"><i class="fas fa-print"></i> Print Laporan</button>
    </div>
</div>
<div class="row">
    <div class="col-12 table-scroll-x">
        <table class="table table-striped datatable display">
            <thead>
                <tr>
                    <th width="5%">Kode Internal</th>
                    <th width="10%">Kode External</th>
                    <th width="20%">Nama Produk</th>
                    <th width="15%">Harga Supplier</th>
                    <th width="15%">Harga Jual</th>
                    <th width="15%">Harga Asli</th>
                    <th width="10%">Variant / Size</th>
                    <th width="10%">Keterangan</th>
                    <th width="5%">Reg Time</th>
                    <th width="5%">Qty</th>
                    <th width="5%">LDP</th>
                    <th width="5%">BRD</th>
                    <th width="5%" class="no-print"></th>
                </tr>
            </thead>
            <tbody>
                @if (isset($array_product))
                @foreach ($array_product as $row)
                <tr>
                    <td>
                        @if ($row['pin_to_top'] > 0)
                        <a style="color: orange;" href="{{ url('stock-report/pin') }}/{{ $row['internal_code'] }}/0">
                            <i style="color: orange" class="fas fa-thumbtack" data-bs-toggle="tooltip" data-bs-placement="top" title="Lepaskan Sematan"></i>
                        </a>
                        @else
                        <a style="color: inherit;" href="{{ url('stock-report/pin') }}/{{ $row['internal_code'] }}/1">
                            <i class="fas fa-thumbtack" data-bs-toggle="tooltip" data-bs-placement="top" title="Sematkan Produk Ini Di Atas"></i>
                        </a>
                        @endif
                        &nbsp;
                        {{ $row['internal_code'] }}
                    </td>
                    <td>
                        {{ $master[$row['internal_code']]['external_code'] }}<br>
                        Kategori: {{ $master[$row['internal_code']]['category'] }}
                    </td>
                    <td><h5>{{ $master[$row['internal_code']]['product_name'] }}</h5>
                        <img style="cursor: pointer;" src="{{ (isset($master[$row['internal_code']]['master_image'])) ? url('public').Storage::url($master[$row['internal_code']]['master_image']) : url('public').Storage::url('public/no_image.png') }}"  onclick="changeSrc(this)" data-bs-toggle="modal" data-bs-target="#imageModal" width="100px">
                    </td>
                    <td>
                        {{ number_format($master[$row['internal_code']]['supplier_price']) }}
                    </td>
                    <td>{{ number_format($master[$row['internal_code']]['selling_price']) }}</td>
                    <td>{{ number_format((int)$master[$row['internal_code']]['hargaasli']) }}</td>
                    
                    <td class="nowrap">
                        @foreach ($array_product[$row['internal_code']]['variant'] as $rowx => $value)
                            @foreach ($array_product[$row['internal_code']]['variant'][$rowx] as $rowy => $value)
                            <div class="thumbnail">
                                <img onclick="changeSrc(this)" data-bs-toggle="modal" data-bs-target="#imageModal" src="{{ url('public').Storage::url($value['image']); }}">
                            </div>
                            {{ $rowx }} / {{ $rowy }}<br>
                            @endforeach
                        @endforeach
                    </td>
                    <td class="nowrap">
                        @foreach ($array_product[$row['internal_code']]['variant'] as $rowx => $value)
                            @foreach ($array_product[$row['internal_code']]['variant'][$rowx] as $rowy => $value)
                            {{ $value['keterangan'] }}<br>
                            @endforeach
                        @endforeach
                    </td>
                    <td>
                        
                        {{ $master[$row['internal_code']]['register_time'] }}
                    </td>
                    <td class="nowrap">
                        @foreach ($array_product[$row['internal_code']]['variant'] as $rowx => $value)
                            @foreach ($array_product[$row['internal_code']]['variant'][$rowx] as $rowy => $value)
                            {{ $value['qty'] }}<br>
                            @endforeach
                        @endforeach
                    </td>
                    <td>
                        
                        {{ $master[$row['internal_code']]['diskon'] }}
                    </td>
                    <td>
                        
                        {{ $master[$row['internal_code']]['branded'] }}
                    </td>
                    <td class="nowrap noprint">
                        @foreach ($array_product[$row['internal_code']]['variant'] as $rowx => $value)
                            @foreach ($array_product[$row['internal_code']]['variant'][$rowx] as $rowy => $value)
                            <input type="checkbox" name="reprint_sticker" class="noprint form-check-input" value="{{ $row['internal_code'] }}_{{ $rowx }}_{{ $rowy }}_{{ $value['qty'] }}_{{ $value['selling_price'] }}_{{ $row['external_code'] }}">
                            <button class="btn btn-sm btn-warning" data-bs-toggle="tooltip" data-bs-placement="right" title="Edit" onclick="editProduct({{ $value['id'] }})"><i class="fas fa-edit"></i></button>
                            <button class="btn btn-sm btn-danger" data-bs-toggle="tooltip" data-bs-placement="right" title="Delete" onclick="deleteProduct({{ $value['id'] }})"><i class="fas fa-times-circle"></i></button> <br>
                            @endforeach
                        @endforeach
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>

<div class="modal" id="modalSticker" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reprint Sticker</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body table-scroll-x">
        <table class="table table-striped" style="width: 100%">
            <thead>
                <tr>
                    <th>Kode Internal</th>
                    <th>Kode Eksternal</th>
                    <th>Variant</th>
                    <th>Size</th>
                    <th>Qty Stock</th>
                    <th>Banyak Print</th>
                </tr>
            </thead>
            <tbody id="reprintTable"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="printSticker()">Print</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('script')
<script>
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
});

function editProduct(id) {
    window.open('{{ url('stock/edit') }}/'+id,"_blank")
}

function deleteProduct(id) {
    if (confirm('Yakin Hapus Data?')) {
        window.location.href = '{{ url('stock/delete') }}/'+id;
    }
}

function changeSrc(thisid) {
    var src_new = $(thisid).attr('src');
    $('.image_modal').attr("src", src_new);
}

function reprintSticker() {
    $('#reprintTable').empty();
    var tr = ''; var seq = 1;
    $('input[name=reprint_sticker]:checked').each(function(){
        var cbo_value = $(this).val();
        cbo_value = cbo_value.split('_');
        tr += '<tr data-seq="'+seq+'">' + 
                '<td><input type="text" style="width: 150px" readonly="readonly" name="internal_code" value="'+cbo_value[0]+'">' + 
                '<input type="hidden" name="selling_price" value="'+cbo_value[4]+'"></td>' + 
                '<td><input type="text" style="width: 200px" readonly="readonly" name="external_code" value="'+cbo_value[5]+'"></td>' + 
                '<td><input type="text" style="width: 150px" readonly="readonly" name="variant" value="'+cbo_value[1]+'"></td>' + 
                '<td><input type="text" style="width: 60px" readonly="readonly" name="size" value="'+cbo_value[2]+'"></td>' + 
                '<td><input type="number" readonly="readonly" style="width: 60px" name="qty" value="'+cbo_value[3]+'"></td>' + 
                '<td><input type="number" style="width: 60px" step="1" min="1" name="qty_print" value="'+cbo_value[3]+'"></td>' + 
                '</tr>';
        seq++;
        $('#reprintTable').html(tr);
    });
    $('#modalSticker').modal("show");
}

function printSticker() {
    event.preventDefault();
    var print_parameter = '';
    console.log('print triggered');
    $.each($('input[name=internal_code]'), function(){
            var sequence = $(this).closest('tr').attr('data-seq');
            var internal_code = $('tr[data-seq='+sequence+'] input[name=internal_code]').val().toUpperCase();
            var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val().toUpperCase();
            var size = $('tr[data-seq='+sequence+'] input[name=size]').val();
            var qty_in = parseInt($('tr[data-seq='+sequence+'] input[name=qty_print]').val());
            var selling_price = parseInt($('tr[data-seq='+sequence+'] input[name=selling_price]').val());
            var external_code =($('tr[data-seq='+sequence+'] input[name=external_code]').val()).toUpperCase();
            
            print_parameter += internal_code + '_' + variant + '_' + size + '_' + qty_in + '_' + selling_price + '_' + external_code + '|';
    });
    window.open('{{ url('print/sticker') }}/'+print_parameter);
}
</script>

<style type="text/css">
    table {
        line-height: 30px;
    }
</style>
@endsection