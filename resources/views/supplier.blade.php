@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Supplier</h1>
        <a href="{{ url('supplier/add') }}" class="btn btn-primary">+ Add New Supplier</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Location</th>
                    <th>Email</th>
                    <th width="20%">Phone</th>
                    <th width="5%">Repeat Order</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($supplier as $row)
                <tr>
                    <td>{{ $row->supplier_code }}</td>
                    <td>{{ $row->supplier_name }}</td>
                    <td>{!! nl2br($row->location) !!}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->phone }}</td>
                    <td>{{ ($row->repeat_order == 1) ? 'Ya' : 'Tidak'; }}</td>
                    <td>
                        <a href="{{ url('supplier/edit/'.$row->id) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                        <a href="{{ url('supplier/delete/'.$row->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection