@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Manage Kategori</h1>
    </div>
</div>
<div class="row">
    <div class="col-12 col-md-4">
        <h3>Tambah Kategori</h3>
        <form action="{{ url('category') }}" method="post">
            @csrf
            Kategori
            <input type="text" name="category_name" class="form-control" required="required">
            <input type="submit" class="btn btn-primary" value="Simpan Kategori">
        </form>
    </div>
    <div class="col-12 col-md-8">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Kategori</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($category as $row)
                <tr>
                    <td>{{ $row->category_name }}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" onclick="deleteKategori('{{ $row->category_name }}')"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>                
               @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script>
    function deleteKategori(category_name) {
        if (confirm("Yakin hapus data?")) {
            window.location.href = '{{ url('category/delete') }}/'+category_name;
        }
    }
</script>
@endsection