@extends('template')
@section('content')
<div class="row">
    <div class="col-13">
        <h2>List Reseller</h2>
        <br>
        Total: {{ number_format(count($list_order)) }}
        <br>
        <br>
    </div>
    <div class="col-13 table-scroll-x">
        <table class="table table-striped display nowrap datatable-list">
            <thead>
                <tr>
                    <th>Nama Toko</th>
                    <th>Telpon</th>
                    <th>Pembelian</th>
                    <th>Nama Depan</th>
                    
                    <th>Email</th>
                    <th>Reffer</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($list_order as $row)
                <tr>
                    <td><a href="{{ url('reseller-edit').'/'.$row->username }}">{{ strtolower($row->username) }}</a></td>
                    <td>
                        <a href="http://wa.me/{{ $row->phone }}" target="_blank">{{ $row->phone }}</a>
                    </td>
                    <td>{{ number_format($row->paidamount) }}</td>
                    <td>{{ $row->firstname }}</td>
                    
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->reffer }}</td>
                    <td>
                        <button onclick="window.location.href='{{ url('reseller-edit').'/'.$row->username }}'" class="btn btn-sm btn-warning" data-bs-toggle="tooltip" data-bs-placement="top" title="Ubah Data"><i class="fas fa-edit"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- {{ $list_order->links('pagination::bootstrap-4') }} --}}
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form action="{{ url('order/save-awb') }}" method="post">
      <div class="modal-header">
        <h5 class="modal-title">Masukkan No. Resi Pengiriman</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
            @csrf
            <input type="text" name="payment_id" readonly="readonly" class="form-control">
            <input type="text" placeholder="No. Resi / AWB" name="awb" class="form-control">
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Simpan">
      </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
    function changeID(new_id) {
        $('input[name="payment_id"]').val(new_id);
    }

    function deleteListOrder(instagram, tanggal) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = '{{ url('selling/delete') }}/'+instagram+'/'+tanggal;
        }
    }

    $(document).ready(function() {
        $('.datatable-list').DataTable( {
            'pageLength': 25,
            "order": [[ 0, "desc" ]]
        } );
    } );
</script> 
@endsection