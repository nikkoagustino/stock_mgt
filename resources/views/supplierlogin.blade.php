@extends('template')
@section('content')
<div class="row">
    <div class="col-14">
        <h1>Supplier</h1>
        <a href="{{ url('supplierlogin/add') }}" class="btn btn-primary">+ Add New Supplier Login</a>
    </div>
</div>
<div class="row">
    <div class="col-14">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Login</th>
                    <th>Nama</th>
                    <th>Acces 1</th>
                    <th>Acces 2</th>
                    <th>Acces 3</th>
                    <th>Acces 4</th>
                    <th>Acces 5</th>
                    <th>Acces 6</th>
                    <th>Bank Acc</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach($supplier as $row)
                <tr>
                    <td><a href="supplierlogin/editlogin/{{ $row->username }}">{{ $row->username }}</a></td>
                    <td>{{ $row->nama }}</td>
                    <td>{{ $row->supplier1 }}</td>
                    <td>{{ $row->supplier2 }}</td>
                    <td>{{ $row->supplier3 }}</td>
                    <td>{{ $row->supplier4 }}</td>
                    <td>{{ $row->supplier5 }}</td>
                    <td>{{ $row->supplier6 }}</td>
                    <td>{{ $row->demodemode }}</td>
                    <td>
                        <a href="supplierlogin/editlogin/{{ $row->username }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                        
                        <a href="javascript:void(0)" onclick="deleteOrder({{$row->id}})" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
<script>

function deleteOrder(id) {
    if (confirm("Yakin Hapus Data?")) {
        window.location.href = "{{ url('supplierlogin/delete/') }}/"+id;
    }
}

</script>
@endsection