@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Stock Edit Form</h1>
    </div>
</div>
<form action="{{ url('stock/save-edit') }}" enctype="multipart/form-data" method="post">
    @csrf
    <input type="hidden" name="id" value="{{ $stock->id }}">
    <div class="row">
        <div class="col-xs-12 col-md-7 form">
            <div class="row">
                <div class="col-4">
                    Supplier
                </div>
                <div class="col-8">
                    <select readonly="readonly" name="supplier_code" class="form-control fill_supplier">
                        <option value="{{ $master_data->supplier_code }}">[{{ $master_data->supplier_code }}] {{ $supplier[$master_data->supplier_code] }}</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Kode External <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input readonly="readonly" name="external_code" value="{{ $master_data->external_code }}" required="required" type="text" class="form-control fill_external_code" placeholder="Code"></input>
                    <i class="msg_external_green" style="color:green; display: none;">External Code Sudah Terdaftar</i>
                    <i class="msg_external_red" style="color:red; display: none;">External Code Belum Terdaftar</i>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Kode Internal <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input readonly="readonly" name="internal_code" readonly="readonly" value="{{ $master_data->internal_code }}" type="text" required="required" class="form-control" placeholder="Code"></input>
                    <i class="msg_internal_green" style="color:green; display: none;">Internal Code Sudah Terdaftar</i>
                    <i class="msg_internal_red" style="color:red; display: none;">Internal Code Belum Terdaftar</i>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Nama Produk <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input name="product_name" value="{{ $master_data->product_name }}" type="text" required="required" class="form-control fill_product_name" placeholder="Product Name"></input>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    Kategori <span class="required">*</span>
                </div>
                <div class="col-8">
                    <select name="category" required="required" class="form-select form-control">
                        @foreach ($category as $row)
                        <option <?= ($master_data->category == $row->category_name) ? "selected='selected'" : ""; ?> value="{{ $row->category_name }}">{{ $row->category_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Harga Supplier <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input type="number"  min="1000" step="100" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="{{ $master_data->supplier_price }}" name="supplier_price" class="form-control fill_supplier_price">
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Harga Jual <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input type="number"  min="1000" step="100" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="{{ $master_data->selling_price }}" name="selling_price" class="form-control fill_selling_price">
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Deskripsi
                </div>
                <div class="col-8">
                <textarea name="deskripsi" placeholder="Deskripsi Produk Harap Di Isi Detil"  class="form-control">{{ $master_data->deskripsi }}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Branded(Ga masuk tiktok) :
                </div>
                <div class="col-8">
                    <select name="branded" class="form-control">
                        <option value="{{ $master_data->branded }}">{{ $master_data->branded }}</option>
                        <option value="Ya">Ya</option>
                        <option value="Tdk">Tdk</option>
                        
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Masuk Landing Page :
                </div>
                <div class="col-8">
                    <select name="diskon" class="form-control">
                        <option value="{{ $master_data->diskon }}">{{ $master_data->diskon }}</option>
                        <option value="Ya">Ya</option>
                        <option value="Tidak">Tidak</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Harga Asli (Jgn di isi kl ga Promo) 
                </div>
                <div class="col-8">
                    <input name="hargaasli" type="number"  step="100" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="{{ $master_data->hargaasli }}"  class="form-control"></input>
                </div>
            </div>
            
            <div class="row">
                <div class="col-4">
                    Tampilkan di Store? <span class="required">*</span>
                </div>
                <div class="col-8">
                    <select name="visibility" required="required" class="form-control">
                        <option value="1" <?= ($master_data->visibility == "1") ? "selected='selected'" : ""; ?>>Ya</option>
                        <option value="0" <?= ($master_data->visibility == "0") ? "selected='selected'" : ""; ?>>Tidak</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Foto Master
                </div>
                <div class="col-8">
                <input type="file" accept="image/*" onchange="uploadMasterImage()" name="master_image" class="form-control">
                </div>
            </div>
        </div>
        <div class="d-none d-md-block col-md-1"></div>
        <div class="col-12 col-md-4">
            <img style="max-height: 250px" src="{{ (isset($master_data->master_image)) ? url('public').'/'.Storage::url($master_data->master_image) : url('public/storage/no_image.png'); }}" class="master_image_frame" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-scroll-x">
            <table class="table table-striped table-bordered" style="min-width: 1000px">
                <thead>
                    <tr>
                        <th width="15%">Variant</th>
                        <th width="10%">Size</th>
                        <th width="10%">Keterangan</th>
                        <th width="7%">Qty Saat Ini</th>
                        <th width="7%">Qty Masuk</th>
                        <th colspan="2" width="10%">Gambar</th>
                        <th width="2%">Print Sticker</th>
                        <th width="5%"></th>
                    </tr>
                </thead>
                <tbody class="variant_table">
                    <tr data-seq='1'>
                        <td>
                            <input type="text" name="variant" value="{{ $stock->variant }}" class="form-control">
                            <i class="variant_avail" style="display: none; color:red">Variant produk sudah ada</i>
                        </td>
                        <td>
                            <input type="text" name="size" value="{{ $stock->size }}" class="form-control" readonly="readonly">
                        </td>
                        <td>
                            <textarea name="keterangan" rows="3" class="form-control">{{ $stock->keterangan }}</textarea>
                        </td>
                        <td>
                            <input class="form-control fill_qty" type="number" name="current_qty" value="{{ $stock->qty }}" readonly="readonly">
                        </td>
                        <td>
                            <input class="form-control" type="number" name="qty_in" value="0" required="required">
                        </td>
                        <td width="5%">
                            <img src="{{ (isset($stock->images)) ? $stock->images : url('public/storage/no_image.png') }}" height="60px" class="fill_img" alt="">
                        </td>
                        <td width="5%">
                            <input class="form-control" type="file" name="product_photo">
                        </td>
                        <td style="text-align: center">
                            <input type="checkbox" name="print_sticker" value="Y">
                        </td>
                        <td>
                            <input type="submit" class="btn btn-primary btn-save" value="Simpan">
                        </td>
                    </tr>
                </tbody>
                <tr>
                    <td>
                        <button class="form-control btn btn-danger" onclick="deleteVariant()"><i class="fas fa-trash"></i> Hapus Variant Produk</button>
                    </td>
                    <td colspan="6"></td>
                    <td colspan="2">
                        <button class="form-control btn btn-warning" onclick="printSticker()"><i class="fas fa-print"></i> Print Sticker</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>
@endsection
@section('script')
<style>
    input[type=checkbox] {
        width: 20px;
        height: 20px;
    }
    .form i {
    margin-top: -40px;
    float: right;
    font-size: 12px;
    margin-right: 10px;
    }
</style>
<script>
$(document).ready(function(){
    var current_variant = $('input[name=variant]').val();
    $('input[name=variant]').on('change paste keyup', function(){
        if ($('input[name=variant]').val() == current_variant) { return; }
        var internal_code = $('input[name=internal_code]').val();
        var variant = $('input[name=variant]').val();
        var size = $('input[name=size]').val();
        var ajax_url = "{{ url('api/product') }}/" + internal_code + '/' + variant + '/' + size;
        console.log(ajax_url);
        $.ajax({
            method: 'get',
            url: ajax_url,
            success: function(result) {
              if (result.length > 10) {
                $('.variant_avail').show();
                $('.btn-save').attr('disabled', 'disabled');
              } else {
                $('.variant_avail').hide();
                $('.btn-save').removeAttr('disabled');
              }
            }
        });
    });
});

function printSticker() {
    event.preventDefault();
    var print_parameter = '';
    console.log('print triggered');
    $.each($('input[name=print_sticker]'), function(){
        var val = (this.checked ? "1" : "0");
        if (val == 1) {
            var sequence = $(this).closest('tr').attr('data-seq');
            var internal_code = $('input[name=internal_code]').val().toUpperCase();
            var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val().toUpperCase();
            var size = $('tr[data-seq='+sequence+'] input[name=size]').val();
            var qty_in = parseInt($('tr[data-seq='+sequence+'] input[name=qty_in]').val());
            var selling_price = parseInt($('input[name=selling_price]').val());
            var external_code =($('input[name=external_code]').val()).toUpperCase();
            
            print_parameter += internal_code + '_' + variant + '_' + size + '_' + qty_in + '_' + selling_price + '_' + external_code + '|';
        }
    });
    window.open('{{ url('print/sticker') }}/'+print_parameter);
}


function uploadMasterImage() {
    event.preventDefault();
    var external_code = $('input[name=external_code]').val();

    var files = $('input[name=master_image]').prop('files')[0]; 
    console.log(files);
    var ajax_url = "{{ url('api/save_master_image') }}";

    var fd = new FormData();
    fd.append('external_code', external_code);
    fd.append('master_image', files);


    $.ajax({
        method: 'post',
        url: ajax_url,
        processData: false,
        contentType: false,
        cache: false,
        enctype: 'multipart/form-data',
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        data: fd,
        success: function(result) {
            console.log(result);
            var new_src = "{{ url('/') }}/" + result.replace("public", "public/storage") + "";
            $('.master_image_frame').attr('src', new_src);
        }
    });
}

function deleteVariant() {
    event.preventDefault();
    if (confirm("Yakin Hapus Data?")) {
        var id = $('input[name=id]').val();
        var url = '{{ url("stock/delete") }}/'+id;
        window.location.href = url;
    }
}
</script>
@endsection