@extends('template')
@section('content')
<?php $qty_total = 0; $price_total = 0; ?>
<div class="row">
    <div class="col-12">
        <h1>Laporan Penjualan Supplier</h1>
    </div>
</div>
    <form action="{{ url('laporan-penjualan') }}" method="POST">
<div class="row">
        @csrf
        <div class="col-12 col-md-3">
            <input type="date" value="{{ $report_date }}" class="form-control form-control-normal" name="report_date">
        </div>
        <div class="col-12 col-md-3">
            <select name="supplier_code" class="form-select">
                <option value="">ALL SUPPLIER</option>
            @foreach ($supplier_list as $row)
                <option <?= ($supplier_filter == $row->supplier_code) ? 'selected="selected"' : ''; ?> value="{{ $row->supplier_code }}">{{ $row->supplier_name }}</option>
            @endforeach
            </select>
        </div>
        <div class="col-12 col-md-3">
            <button class="btn  btn-primary noprint">Submit</button>
        </div>
        <div class="col-12 col-md-3" style="text-align: right">
            <button class="btn btn-success noprint" onclick="window.print()"><i class="fas fa-print"></i> Print</button>
        </div>
</div>
    </form>

@if (isset($rekap))
<div class="row">
    <div class="col-12">
        <h3>Rekap Penjualan</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Supplier Code</th>
                    <th>Supplier Price</th>
                    <th>Qty Total</th>
                </tr>
            </thead>
            @foreach ($rekap as $row)
            <tr>
                <td>{{ $row->supplier_code }}</td>
                <td>{{ $row->supplier_price }}</td>
                <td>{{ $row->qty_total }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endif

@if (isset($report))
<div class="row">
    <div class="col-12">
        <h3>Detail Penjualan</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Kode Supplier</td>
                    <td>Kode Internal</td>
                    <td>Nama Barang</td>
                    <td>Variant</td>
                    <td>Size</td>
                    <td>Qty</td>
                    <td>Harga Supplier</td>
                </tr>
            </thead>
            <tbody>
                @foreach($report as $row)
                <tr>
                    <td>{{ $row->supplier_code }} - {{ $row->supplier_name }}</td>
                    <td>{{ $row->internal_code }}</td>
                    <td>{{ $row->product_name }}</td>
                    <td>{{ $row->variant }}</td>
                    <td>{{ $row->size }}</td>
                    <td>{{ $row->qty }}</td>
                    <td>{{ number_format($row->supplier_price, 0) }}</td>
                </tr>
                    <?php $qty_total = $qty_total + $row->qty;
                    $price_total = $price_total + ($row->qty * $row->supplier_price); ?>
                @endforeach  
            </tbody>
        </table>
    </div>
</div>
@endif
@endsection