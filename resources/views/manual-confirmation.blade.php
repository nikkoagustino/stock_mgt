@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Konfirmasi Pembayaran Manual</h2>
    </div>
    <div class="col-12 col-md-8">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Kode Internal</th>
                    <th>Variant</th>
                    <th>Size</th>
                    <th>Harga Jual</th>
                    <th>Qty</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order as $row)
                <tr>
                    <td>{{ $row->internal_code }}</td>
                    <td>{{ $row->variant }}</td>
                    <td>{{ $row->size }}</td>
                    <td>{{ $row->selling_price }}</td>
                    <td>{{ $row->qty }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="card">
          <div class="card-body">
            {!! nl2br($payment->shipping_address) !!}
          </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <form action="{{ url('manual-confirmation') }}" method="post">
            @csrf
            Payment ID
            <input type="text" readonly="readonly" class="form-control" name="payment_id" value="{{ $payment->id }}">
            Instagram
            <input type="text" readonly="readonly" class="form-control" name="instagram" value="{{ $payment->instagram }}">
            Jumlah Pembayaran Terdaftar
            <input type="text" readonly="readonly" class="form-control" name="amount_db" value="{{ $payment->amount }}">
            Jumlah Pembayaran Diterima
            <input type="text" class="form-control" name="amount" value="{{ $payment->amount }}">
            Cara Pembayaran
            <select name="payment_type" class="form-control">
                <option value="Midtrans">Midtrans</option>
                <option value="Transfer Bank">Transfer Bank Tanpa Midtrans</option>
            </select>
            <input type="hidden" name="transaction_status" value="Manual Confirm">
            <input type="submit" class="btn btn-success" value="Simpan Pembayaran">
        </form>
    </div>
</div>
@endsection
