@extends('userpage/template')
@section('content')
<?php $cartdata = \Session::get('cartdata');
$total = 0; ?>
<div class="row">
    <div class="col"></div>
    <div class="col-12 col-md-8 p-4 mt-5 border bg-light">
        <h5>Cart Sementara</h5>
        <div class="col-12">
            <div class="tableFixHead">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama Barang</th>
                            <th>Variant</th>
                            <th>Size</th>
                            <th>Harga Satuan</th>
                            <th>Qty</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (isset($cartdata))
                        @foreach ($cartdata as $row)
                        <?php $subtotal = $row['qty'] * $master_data[$row['internal_code']]['selling_price'];
                        $total += $subtotal; ?>
                        <tr>
                            <td>{{ $row['internal_code'] }}</td>
                            <td>{{ $master_data[$row['internal_code']]['product_name'] }}</td>
                            <td>{{ $row['variant'] }}</td>
                            <td>{{ $row['size'] }}</td>
                            <td>{{ number_format($master_data[$row['internal_code']]['selling_price']) }}</td>
                            <td>{{ $row['qty'] }}</td>
                            <td>{{ number_format($subtotal) }}</td>
                        </tr>
                        @endforeach
                        @else 
                        <tr>
                            <td colspan="7"><center>Keranjang Belanja Anda Kosong</center></td>
                        </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">Total</td>
                            <td>{{ number_format($total) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        @if (isset($cartdata))
        <form action="{{ url('save-cart') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-12 col-md-4">Instagram</div>
                <div class="col-12 col-md-4">
                    <div class="input-group">
                        <span class="input-group-text" id="basic-addon1">@</span>
                        <input type="text" name="instagram" placeholder="instagram" value="<?= (!empty(\Session::get('instagram'))) ? \Session::get('instagram') : ''; ?>" required="required" class="form-control" aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <button type="submit" class="form-control btn btn-warning"><i class="fas fa-shopping-cart"></i> &nbsp; Simpan Cart</button>
                </div>
            </div>
        </form>
        @endif
    </div>
    <div class="col"></div>
</div>
@endsection