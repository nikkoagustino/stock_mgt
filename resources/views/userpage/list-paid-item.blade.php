@extends('userpage/template')
@section('content')
<?php foreach ($stock as $row) {
    $product_name[$row->internal_code] = $row->product_name;    
} 
$totalord=0;
?>
<div class="tableFixHead">
<div class="row">
    <div class="col-13">
        <h2>List Order</h2>
    </div>
</div>
<div class="row">
    <div class="col-13 col-sm-8">
        <div class="table-scroll-x">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Kode Internal</th>
                    <th>Nama Produk</th>
                    <th style="width: 150px">Variant</th>
                    <th style="width: 100px">Size</th>
                    <th style="width:60px">Qty</th>
                    <th style="width:60px">Price</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($item_order as $row)
                <tr data-seq='{{$row->id}}'>
                    <td>{{ $row->internal_code }}</td>
                    <td>{{ (isset($product_name[$row->internal_code])) ? $product_name[$row->internal_code] : '' }}</td>
                    <td>{{ $row->variant }}</td>
                    <td>{{ $row->size }}</td>
                    <td>{{ $row->qty }}</td>
                    <td>{{ number_format($row->selling_price) }}</td>
                    <?php $totalord = $totalord + ($row->qty * $row->selling_price); ?>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
    <div class="col-12 col-sm-4">
    
        <h4>Data Pembayaran</h4>
        <div class="row">
            <div class="col-4">
                Tanggal Order
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ $payment->order_date }}">
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-4">
                Jumlah Bayar
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ number_format($payment->amount) }}">
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Metode Pembayaran
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ $payment->payment_type }}">
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Status Pembayaran
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ $payment->transaction_status }}">
            </div>
        </div>
        
        
    </div>
</div>
</div>
@endsection
@section('script')
<style>
.tableFixHead          { overflow: auto; height: 80vh; }
@media only screen and (max-width: 768px) {
.tableFixHead th, .tableFixHead td {
font-size: 0.49rem;
}
}
.tableFixHead thead th { position: sticky; top: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.tableFixHead tfoot, .tableFixHead tfoot td, .tableFixHead tfoot th { position: -webkit-sticky; position: sticky; bottom: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.btn-success:disabled {
    background-color: #aaa;
    border: black;
}

</style>
@endsection