@extends('userpage/template')
@section('meta')
@if (isset($page_title))
    <title>{{ $page_title }} - MomAvel.id</title>
    {{-- <meta name="description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    <meta name="keywords" content="cart">
    <meta name="robots" content="noindex, nofollow">
    <link rel="canonical" href="{{ url()->current() }}">
    <meta property="og:title" content="{{ $page_title }} - MomAvel.id">
    {{-- <meta property="og:description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    {{-- <meta property="og:type" content="product"> --}}
    {{-- <meta property="og:url" content="{{ url()->current() }}"> --}}
    {{-- <meta property="og:image" content="{{ url('public').\Storage::url($slides[0]->image_url) }}"> --}}
@else
    <meta name="title" content="MomAvel.id">
    <title>MomAvel.id</title>
@endif
@endsection
@section('content')
<div class="row mt-3">
    <h5>Hi, {{ '@'.\Session::get('instagram') }}</h5>
    <div class="tableFixHead">
    <ul class="nav nav-tabs" id="myTab" role="tablist" >
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="list-order-tab" data-bs-toggle="tab" data-bs-target="#list-order" type="button" role="tab" aria-controls="list-order" aria-selected="true">List Order</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="addresses-tab" data-bs-toggle="tab" data-bs-target="#addresses" type="button" role="tab" aria-controls="addresses" aria-selected="false">Alamat & Telp</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="password-tab" data-bs-toggle="tab" data-bs-target="#password" type="button" role="tab" aria-controls="password" aria-selected="false">Password</button>
        </li>
    </ul>

    <div class="col-12 p-4 border bg-light mx-auto">
    
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="list-order" role="tabpanel" aria-labelledby="list-order-tab">
                <h4>List Order</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nomor Order</th>
                            <th>Tanggal</th>
                            <th>Jumlah Pembayaran</th>
                            <th>Status Order</th>
                            <th>No. Resi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($paid_order as $row)
                        <tr>
                            <td><a href="selling/user-paid/{{ $row->id }}" class="border border-warning">{{ $row->id }}</a></td>
                            <td><a href="selling/user-paid/{{ $row->id }}" class="border border-warning">{{ $row->order_date }}</a></td>
                            <td><a href="selling/user-paid/{{ $row->id }}" class="border border-warning">{{ number_format($row->amount) }}</a></td>
                            @if ($row->order_status == 'pending')
                            <td>
                                Pembayaran Pending<br>
                                @if ($row->payment_type == 'Midtrans')
                                <button class="btn btn-warning" onclick="window.location.href='{{ url('pay-now').'/'.$row->id }}'">Bayar Sekarang</button>
                                @endif
                                <button onclick="window.location.href='{{ url('user-cancel-payment').'/'.$row->id }}';this.disabled=true;this.form.submit()" class="btn btn-sm btn-danger" data-bs-toggle="tooltip" data-bs-placement="top" title="Pembayaran Sering Kali Gagal karena tidak ada bank yg di pilih, Bisa di ulang kembali. Kembali ke cek pesanan untuk lakukan pembayaran ulang.">Ulangi Pembayaran (Jika Gagal)<i class="fas fa-times"></i></button>
                            </td>
                            @elseif ($row->order_status == 'paid')
                            <td>Standby Pengiriman</td>
                            @elseif ($row->order_status == 'delivery')
                            <td>Dikirim</td>
                            @endif
                            <td>
                                @if (isset($row->awb))
                                <a href="https://idexpress.com/lacak-paket/?actionType=delivery&waybillNo={{ $row->awb }}" target="_blank" class="border border-warning">{{ $row->awb }}</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="addresses" role="tabpanel" aria-labelledby="addresses-tab">
            <form action="{{ url('my-account/savephone') }}" method="post">
            Username / Instagram
                <input type="text" required="required" readonly="readonly" value="{{ $userdata->username }}" name="instagram1" class="form-control mb-2">
                
                No Telpon (Jangan Gunakan + dan Spasi) Contoh = 628123456175
                <input type="text" required="required" value="{{ $userdata->phone }}" name="phone" class="form-control mb-2" >
                
                Nama Depan
                <input type="text" required="required" value="{{ $userdata->firstname }}" name="firstname" class="form-control mb-2" >
                Nama Belakang
                <input type="text" required="required" value="{{ $userdata->lastname }}" name="lastname" class="form-control mb-2" >
                Email
                <input type="text" required="required" value="{{ $userdata->email }}" name="email" class="form-control mb-2" >

                <a href="javascript:void(0)" onclick="saveAdd()" class="form-control btn btn-success"><i class="fas fa-edit">Simpan Data Pribadi</i></a>
                </form>
            
             
                <br>
                <br>
                <button class="btn btn-success"  data-bs-toggle="modal" data-bs-target="#addAddressModal"><i class="fas fa-plus"></i> &nbsp; Tambah Alamat</button>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Alamat Pengiriman</th>
                            <th>Ongkos Kirim per kg</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($addresses as $row)
                        <tr>
                            <td>{!! nl2br($row->shipping_address) !!}</td>
                            <td>{{ number_format($row->shipping_cost) }}</td>
                            <td><button onclick="deleteAddress('{{ $row->id }}')" class="btn btn-danger">
                                <i class="fas fa-times"></i>
                            </button></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
                <form action="{{ url('user/reset-password') }}" method="post">
                @csrf
                Username / Instagram
                <input type="text" required="required" readonly="readonly" value="{{ $userdata->username }}" name="instagram" class="form-control mb-2">
                <input type="hidden" name="oldpass" value="{{ $userdata->password }}">
                Password Baru
                <input type="password" required="required" name="password" class="form-control mb-2" onkeyup="matchingPassword()">
                Konfirmasi Password Baru
                <input type="password" required="required" name="confirm_password" class="form-control mb-2" onkeyup="matchingPassword()">
                <i class="text-danger passnomatch" style="display: none">Password tidak cocok<br></i>
                <input type="submit" name="passw" disabled="disabled" value="Simpan Password" class="btn btn-warning"><br>
            </form>
            </div>
        </div>
    </div>
    </div>
</div>
@include('userpage/add-address-modal')
@endsection
@section('script')
<style>
.tableFixHead          { overflow: auto; height: 80vh; }
@media only screen and (max-width: 768px) {
.tableFixHead th, .tableFixHead td {
font-size: 0.49rem;
}
}
.tableFixHead thead th { position: sticky; top: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.tableFixHead tfoot, .tableFixHead tfoot td, .tableFixHead tfoot th { position: -webkit-sticky; position: sticky; bottom: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.btn-success:disabled {
    background-color: #aaa;
    border: black;
}

</style>
<script>

    $(document).ready(function(){
        loadProvince();
        
		$('input[name=phone]').bind('keypress', function (e) {  
            if (e.keyCode == 43 || e.keyCode == 32 || e.keyCode == 45|| e.keyCode == 46 || e.keyCode == 40 || e.keyCode == 41) {  
            return false;  
            } else {
                var newValu1 = $('input[name=phone]').val().replace(' ','');
                $('input[name=phone]').val(newValue1);
                newValue1 = $('input[name=phone]').val().replace('0','');
                $('input[name=phone]').val(newValue1);
            } 
            

        }); 
    });



    function matchingPassword() {
        var pass1 = $('input[name=password]').val();
        var pass2 = $('input[name=confirm_password]').val();
        if (pass1 == pass2) {
            $('input[name=passw]').removeAttr('disabled');
            $('.passnomatch').hide();
        } else {
            $('input[type=passw]').attr('disabled', 'disabled');
            $('.passnomatch').show();
        }
    }
    function deleteAddress(address_id) {
        if (confirm('Yakin hapus alamat ini?')) {
            window.location.href="{{ url('user/delete-address') }}/"+address_id;
        }
    }
    function saveAdd(rowid) {
        
    var instagram1 = $('input[name=instagram]').val();
    var phone = $('input[name=phone]').val();
    var firstname = $('input[name=firstname]').val();
    var lastname = $('input[name=lastname]').val();
    var email = $('input[name=email]').val();
    
    var fd = new FormData();
    fd.append('instagram', instagram1.toLowerCase());
    fd.append('phone', phone);
    fd.append('firstname', firstname);
    fd.append('lastname', lastname);
    fd.append('email', email);

    

    $.ajax({
        url: '{{url("my-account/savephone")}}',
        method: 'post',
        processData: false,
        contentType: false,
        cache: false,
        data: fd,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
            
            if (result == 'ok') {

                location.reload();
            }
        }
    });

}
</script>
@endsection