@extends('userpage/ldtemplate')
	@section('pixel')
<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '850553866052582');
fbq('track', 'PageView');
fbq('track', 'ViewContent');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=850553866052582&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->
	@endsection
@section('content')
<div class="row mt-5">
<input type="hidden" name="reffer" value="{{ Session::get('reffer'); }}">
    <div class="col-12 col-md-10">
        <div class="row">
            <div class="col-12 p-2">
                <div class="card h-100 bg-light stock-card">
                  <div class="card-body">
                    <div class="row">
                        
                        <div class="col-12 col-md-8">
                            <p style="color:red"><h1>PILIH VARIANT:</h1></p>
                        </div>
                        
                            <table class="table table-sm table-striped table-stock">
                                <thead>
                                    <tr>
                                        <th style="width:100px">Photo</th>
                                        <th>Variant</th>
                                        <th>Size</th>
                                        <th>Info</th>
                                        <th>Stock</th>
                                        <th colspan="2">Qty Beli</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $x = 1 @endphp
                                    @foreach ($product as $row) 
                                    @if ($row->qty > 0)
                                    <tr data-seq='{{ $x }}'>
                                        <td >
                                            <img style="width: 33%; margin-top: 5px; cursor: pointer;" src="{{ url('public').\Storage::url($row->images) }}" onclick="changeSrc(this)"  data-bs-toggle="modal" data-bs-target="#imageModal" alt="">
                                        </td>
                                        <td>
                                            <input type="hidden" name="internal_code" value="{{ $master->internal_code }}">
                                            <input type="hidden" name="variant" value="{{ $row->variant }}">
                                            <input type="hidden" name="size" value="{{ $row->size }}">
                                            {{ $row->variant }}
                                        </td>
                                        <td>{{ $row->size }}</td>
                                        <td>{{ ucwords($row->keterangan) }}</td>
                                        <td>{{ $row->qty }}</td>
                                        <td>
                                            <input type="number" name="qty" value="1" style="width: 50px" min="1" max="{{ $row->qty }}">
                                        </td>
                                        <td>
                                            <button class="btn btn-sm btn-danger" onclick="addToCart(this)"><i class="fas fa-cart-plus">&nbsp;Pilih Ini</i></button>
                                        </td>
                                    </tr>
                                    @php $x++ @endphp
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-12 col-md-4">
                          
                          <div class="cont">
                            
                            @if($master->hargaasli != null && $master->hargaasli > $master->selling_price && !empty($master->master_image))
          					<div class="ribbon ribbon-top-left">
                              <span>SALE!!!</span>
          					</div>
                            @endif
                            
                            <div class="product-img-wrapper">
                            @if (!empty($master->master_image))
                            <img style="max-width: 100%; cursor: pointer;" src="{{ url('public').\Storage::url($master->master_image) }}" onclick="changeSrc(this)"  data-bs-toggle="modal" data-bs-target="#imageModal"  alt="">
                            @else
                            <img style="max-width: 100%; cursor: pointer;" src="{{ url('public').\Storage::url('no_image.png') }}" alt="">
                            @endif
                              
                            @if($master->hargaasli != null && $master->hargaasli > $master->selling_price && !empty($master->master_image))
                                <div class="sale-price">
                                  <s>{{ number_format($master->hargaasli) }}</s>

                                    @if (Session::get('reffer')=='WEB'  || Session::get('reffer')=='WA OTO')
                                
                                        <b><s>{{ number_format($master->selling_price) }}</s> {{number_format($master->selling_price * 0.9)}}</b>
                                    @else
                                        <b>{{ number_format($master->selling_price) }}</b>
                                    @endif
                                  
                                </div>
                              @endif
                            </div>                      
                            </div>                      
                        </div>
                        <div class="col-12 col-md-8">
                            @if($master->hargaasli != null && $master->hargaasli > $master->selling_price)
                            <h3>{{ $master->internal_code }} <p style="color:red">(SALE!!)</p></h3>
                            @else
                            <h3>{{ $master->internal_code }}</h3></h4>
                            @endif
                            
                            <h4>{{ ucwords($master->product_name) }}</h4>
                            @if($master->hargaasli != null && $master->hargaasli > $master->selling_price)
                                <h4><p style="color:red">Harga Asli : <s>{{ number_format($master->hargaasli) }}</s></p></h4>
                            @endif
                            @if (Session::get('reffer')=='WEB'  || Session::get('reffer')=='WA OTO')
                                
                                <h4>Harga Jual : <s>{{ number_format($master->selling_price) }}</s> {{number_format($master->selling_price * 0.9)}}</h4>
                            @else
                                <h4>Harga Jual : {{ number_format($master->selling_price) }}</h4>
                            @endif
                            
                            <h4>Deskripsi :</h4>  {!! nl2br(ucwords($master->deskripsi)) !!}
                            </br></br>
                            
                        </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    function addToCart(thisid) {
        var reffer = $('input[name=reffer]').val();
        var sequence = $(thisid).closest('tr').attr('data-seq');
        var internal_code = $('tr[data-seq='+sequence+'] input[name=internal_code]').val();
        var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val();
        var size = $('tr[data-seq='+sequence+'] input[name=size]').val();
        var qty = $('tr[data-seq='+sequence+'] input[name=qty]').val();
        {{-- var ajax_url = "{{ url('api/add-to-session-cart') }}"; --}}
        var ajax_url = "{{ url('api/add-to-cart') }}";


        $.ajax({
            url: ajax_url,
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                internal_code: internal_code,
                variant: variant,
                size: size,
                qty: qty,
                _token: '{{ csrf_token() }}',
                reffer: reffer,
            },
            success: function (myresponse) {
                console.log(myresponse);
                window.location.href = '../../rekap/ldcheck';
            },
            error: function(xhr, status, error) {
                var err = JSON.parse(xhr.responseText);
                console.log(err);
                alert(err.message);
            }
        });
        
    }
</script>
@endsection