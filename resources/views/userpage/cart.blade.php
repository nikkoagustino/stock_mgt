@extends('userpage/template')
@section('meta')
@if (isset($page_title))
    <title>{{ $page_title }} - MomAvel.id</title>
    {{-- <meta name="description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    <meta name="keywords" content="cart">
    <meta name="robots" content="noindex, nofollow">
    <link rel="canonical" href="{{ url()->current() }}">
    <meta property="og:title" content="{{ $page_title }} - MomAvel.id">
    {{-- <meta property="og:description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    {{-- <meta property="og:type" content="product"> --}}
    {{-- <meta property="og:url" content="{{ url()->current() }}"> --}}
    {{-- <meta property="og:image" content="{{ url('public').\Storage::url($slides[0]->image_url) }}"> --}}
@else
    <meta name="title" content="MomAvel.id">
    <title>MomAvel.id</title>
@endif
@endsection
@section('content')
<?php
$stock_array = [];
$repeat = [];
foreach ($stock as $row) {
    $stock_array[$row->internal_code][$row->variant][$row->size]['qty'] = $row->qty;
    $stock_array[$row->internal_code][$row->variant][$row->size]['selling_price'] = $row->selling_price;
}
foreach ($supplier as $row) {
    $repeat[$row->supplier_code] = $row->repeat_order;
}
?>

    <form action=" {{url('rekap/checkout') }}" method="POST">
    @csrf
<div class="row mt-5">
    <div class="col"></div>
    <div class="col-12 col-md-8 p-4 border bg-light">
        <div class="col-12">
        <?php date_default_timezone_set("Asia/Jakarta"); ?>
        <input type="hidden" value="{{date('Y-m-d H:i:s')}}" name="tglskrg">
        <input type="hidden" value="{{date('Y-m-d H:i:s')}}" name="tanggal">
        <input type="hidden" value="{{ \Session::get('reffer') }}" name="reffer">
            <h5>Halo {{ '@'.$custdata->username }}, periksa kembali pesanan kamu ya.</h5>
            Tanggal Pesanan Hingga : {{ date('d/m/Y', strtotime($tanggal)) }}
            <div class="tableFixHead">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama Barang</th>
                            <th>Variant</th>
                            <th>Size</th>
                            <th>Harga Satuan</th>
                            <th>Qty</th>
                            <th>Subtotal</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total_price = 0; $total_qty = 0;
                        $status_cart = true; $qty_ready = 0;
                        ?>
                        @foreach ($cart as $row)
                    <?php 
                    $total_qty += $row->qty;
                    if (isset($master[$row->internal_code]['supplier_code'])) {
                        // jika supplier terdaftar, cek apakah supplier restock?
                        if ($repeat[$master[$row->internal_code]['supplier_code']] == 1) {
                            // jika supplier repeat order, cek apakah qty terdaftar untuk variant dan size tsb
                            if (isset($stock_array[$row->internal_code][$row->variant][$row->size]['qty'])) {
                                if ($row->qty <= $stock_array[$row->internal_code][$row->variant][$row->size]['qty']) {
                                    $item_stock = "Ready";
                                    $selling_price = $stock_array[$row->internal_code][$row->variant][$row->size]['selling_price'];
                                } else {
                                    $item_stock = "Waiting";
                                    $selling_price = 0;
                                }
                            } else {
                                $item_stock = "Waiting";
                                $selling_price = 0;
                            }
                        } else {
                            // jika supplier tidak repeat order
                            if (isset($stock_array[$row->internal_code][$row->variant][$row->size]['qty'])) {
                                if ($row->qty <= $stock_array[$row->internal_code][$row->variant][$row->size]['qty']) {
                                    $item_stock = "Ready";
                                    $selling_price = $stock_array[$row->internal_code][$row->variant][$row->size]['selling_price'];
                                } else {
                                    $item_stock = "Not Available";
                                    $selling_price = 0;
                                }
                            } else {
                                $item_stock = "Not Available";
                                $selling_price = 0;
                            }
                        }
                    } else {
                        // jika supplier tidak terdaftar
                        $item_stock = "Not Available";
                        $selling_price = 0;
                    }

                    $subtotal = (int) $selling_price * (int) $row->qty;
                    $total_price = (int) $total_price + (int) $subtotal;
                        if ($item_stock == "Ready") {
                            $qty_ready += $row->qty;
                            echo "<tr style='color:green'>";
                        } else if ($item_stock == "Not Available") {
                            echo "<tr style='color:red'>";
                        } else if ($item_stock == "Waiting") {
                            echo "<tr style='color:orange'>";
                        }
                        ?>
                            <td>
                                <a href="{{ url('product').'/'.$row->internal_code }}">{{ $row->internal_code }}</a>
                                @if ($item_stock == 'Ready')
                                <input type="hidden" name="internal_code[]" value="{{ $row->internal_code }}">
                                <input type="hidden" name="variant[]" value="{{ $row->variant }}">
                                <input type="hidden" name="size[]" value="{{ $row->size }}">
                                <input type="hidden" name="qty[]" value="{{ $row->qty }}">
                                <input type="hidden" name="selling_price[]" value="{{ $selling_price }}">
                                <input type="hidden" name="comment[]" value="Order WEB {{ \Session::get('reffer') . ' IG: '. \Session::get('instagram') }}">
                                @endif
                            </td>
                            <td><a href="{{ url('product').'/'.$row->internal_code }}">{{ (isset($master[$row->internal_code]['product_name'])) ? $master[$row->internal_code]['product_name'] : 'N/A' }}</a></td>
                            <td><a href="{{ url('product').'/'.$row->internal_code }}">{{ $row->variant }}</a></td>
                            <td><a href="{{ url('product').'/'.$row->internal_code }}">{{ $row->size }}</a></td>
                            <td>{{ number_format($selling_price) }}</td>
                            <td>{{ $row->qty }} ({{ $item_stock }})</td>
                            <td>{{ number_format($subtotal) }}</td>
                            <td>
                                <a href="{{ url('rekap/cancel') }}/{{ $row->internal_code }}">
                                    <i class="fas fa-times"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="6">Total Belanja</td>
                            <td><span class="total-belanja">0</span></td>
                            <td></td>
                        </tr>
                        @if (\Session::get('reffer')=='WEB' || \Session::get('reffer')=='WA OTO')
                        <tr>
                            <th colspan="6">Diskon Reseller (Min 1jt) {{ $discount->percent }}%</td>
                            <td>-<span class="total-diskon">0</span></td>
                            <td></td>
                        </tr>
                        @endif
                        <tr>
                            <th colspan="4">ID Express Standard Shipping</td>
                            <td><span class="ongkir">0</span> /kg</td>
                            <td>{{ ceil($qty_ready / 5) }} kg</td>
                            <td><span class="total-ongkir">0</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th colspan="6">Free Ongkir / Subsidi Ongkir </td>
                            <td>-<span class="gratis-ongkir">0</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th colspan="6">TOTAL PEMBAYARAN</td>
                            <td><b class="total-text">0</b></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <br>
        </div>
        <div class="row">
            <div class="col-12 mb-2">
            <i style="font-size: 12px">Jumlah pembayaran yang tertera hanya untuk barang yang sudah READY. <br>Jika melanjutkan pembayaran dengan beberapa barang yang masih WAITING, barang tersebut tidak akan termasuk ke dalam daftar pembayaran. <br>Pembayaran untuk barang yang WAITING dapat dilakukan di lain waktu jika barang sudah READY.</i>
                
            </div>
            
        </div>
        <div class="row">
            <div class="col-8">
                <h4>Alamat Pengiriman:</h4>
                @foreach ($addresses as $row)
                <div class="card mb-2">
                  <div class="card-body">
                    <input type="radio" name="shipping_address" data-ongkir="{{ $row->shipping_cost }}" onclick="calculateCost()" required="required" value="{{ $row->shipping_address }}">
                    {!! nl2br($row->shipping_address) !!}
                  </div>
                </div>
                @endforeach
                <a class="btn btn-success" href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#addAddressModal"><i class="fas fa-plus"></i> &nbsp; Tambah Alamat</a>

            </div>
            <div class="col-4">
                    <input type="hidden" name="order_id" value="{{ strtotime(now()) }}">
                    <input type="hidden" name="transaction_status" value="Midtrans - Pending">
                    <input type="hidden" name="nama_depan" value="{{ $custdata->firstname }}">
                    <input type="hidden" name="nama_belakang" value="{{ $custdata->lastname }}">
                    <input type="hidden" name="telepon" value="{{ $custdata->phone }}">
                    <input type="hidden" name="email" value="{{ $custdata->email }}">
                    <input type="hidden" name="amount" value="{{ $total_price }}">
                    
                    <input type="hidden" name="instagram" value="{{ $custdata->username }}">
                    <input type="hidden" name="reffer" value="{{ \Session::get('reffer') }}">
                    <input type="hidden" name="fs" value="{{ \Session::get('fs') }}">
                    <button class="form-control btn btn-success" name="btnsub" onclick ="this.disabled=true;this.form.submit()" <?= ($total_price == 0) ? 'disabled="disabled"' : ''; ?>><i class="fas fa-money-bill-wave"></i> &nbsp; Lanjutkan Pembayaran</button>
                    <br><i style="font-size: 12px;color: red">Anda akan diarahkan ke halaman pembayaran. Mohon selesaikan pembayaran Anda dalam waktu 1 jam atau pesanan akan otomatis dibatalkan.</i>
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>
    </form>
    @include('userpage/add-address-modal')
@endsection
@section('script')
<style>
.tableFixHead          { overflow: auto; height: 80vh; }
@media only screen and (max-width: 768px) {
.tableFixHead th, .tableFixHead td {
font-size: 0.49rem;
}
}
.tableFixHead thead th { position: sticky; top: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.tableFixHead tfoot, .tableFixHead tfoot td, .tableFixHead tfoot th { position: -webkit-sticky; position: sticky; bottom: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.btn-success:disabled {
    background-color: #aaa;
    border: black;
}
</style>
<script>

    function calculateCost() {
        var qty_ready = {{ $qty_ready }};
        var subtotal = {{ $total_price }};
        var percent_diskon = 0;
        var total_diskon = 0;
        if(subtotal >=1000000){
            percent_diskon = {{ $discount->percent }};
            total_diskon = parseInt(subtotal * (percent_diskon / 100));
        }
        
        var shipping_cost = $('input[name=shipping_address]:checked').data("ongkir");
        //console.log(shipping_cost);
        var total_ongkir = Math.ceil(qty_ready / 4) * shipping_cost;
        var gratis_ongkir = (Math.ceil(qty_ready / 3.5)-1) * 10000;
        var total_price = subtotal  + total_ongkir - gratis_ongkir;
        if ($('input[name=reffer]').val()=='WEB'  || $('input[name=reffer]').val()=='WA OTO'){
            total_price = subtotal - total_diskon + total_ongkir - gratis_ongkir;
        } 
        if($('input[name=fs]').val() == 'y'){
            total_price=1000;
        }

        $('input[name=amount]').val(total_price);
        $('.total-belanja').html(parseInt(subtotal).toLocaleString());
        $('.total-diskon').html(parseInt(total_diskon).toLocaleString());
        $('.total-text').html(parseInt(total_price).toLocaleString());
        $('.total-ongkir').html(parseInt(total_ongkir).toLocaleString());
        $('.gratis-ongkir').html(parseInt(gratis_ongkir).toLocaleString());
        $('.ongkir').html(parseInt(shipping_cost).toLocaleString());
        
    }
    $(document).ready(function(){
        $('input[name=shipping_address]:first').attr('checked', 'checked').trigger('click');
    });
</script>
@endsection