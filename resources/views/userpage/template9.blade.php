<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ Session::token() }}"> 
        <link rel="shortcut icon" href="{{ url('public/favicon.webp') }}">
        <link rel="icon" href="{{ url('public/favicon.webp') }}">
        @if (isset($page_title))
        <meta name="title" content="{{ $page_title }} - MomAvel.id">
        <title>{{ $page_title }} - MomAvel.id</title>
        @else
        <meta name="title" content="MomAvel.id">
        <title>MomAvel.id</title>
        @endif
        
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-V3D7Y00Z19"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-V3D7Y00Z19');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-254771568-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-254771568-1');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '557283602775095'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=557283602775095&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->      
      

<script src="https://kit.fontawesome.com/b71ce7388c.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <style>
 .tooltip.bs-tooltip-top .tooltip-arrow::before {
      border-top-color: red;
    }

    .tooltip.bs-tooltip-bottom .tooltip-arrow::before {
      border-bottom-color: red;
    }

    .tooltip.bs-tooltip-start .tooltip-arrow::before {
      border-left-color: red;
    }

    .tooltip.bs-tooltip-end .tooltip-arrow::before {
      border-right-color: red;
    }
    .tooltip-inner {
      background: red;
    }
@keyframes lds-spinner {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}
/* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 99999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}


.cont {
  position: relative;
  text-align: center;
  color: white;
}
.centered {
  position: absolute;
  top: 53%;
  left: 50%;
  transform: translate(-50%, -50%);
}


/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
    background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));
  background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 150ms infinite linear;
  -moz-animation: spinner 150ms infinite linear;
  -ms-animation: spinner 150ms infinite linear;
  -o-animation: spinner 150ms infinite linear;
  animation: spinner 150ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}


  .header-1 {
    background: #ff7b7e;
    color: white;
  }
  .phone-gold {
    color: black;
    cursor: pointer;
    background: yellow;
    font-size: 12px;
    display: inline-block;
    padding: 5px 15px;
  }
  .header-2 {
    background: #202020;
  }
  .header-2 img {
    height: 100px;
    padding: 10px;
  }
  .header-2 .form-control {
    margin: 0;
  }
  .header-2 form {
    margin-bottom: 0;
  }
  .header-3 {
    background: #ff7b7e;
  }
  .header-3 a {
    color: black;
    text-decoration: none;
    padding: 5px 15px !important;
  }
  .btn-my-account {
    background: #ff7b7e;
    border: 1px solid black;
    color: black;
    font-size: 16px;
  }
  .btn-my-account i {
    color: black;
  }
  h4 {
    font-size: 18px;
  }
  .bg-light {
    background-color: #f5f1eb !important;
}
.pagination li a {
  background: #ff7b7e !important;
  color: black !important;
}
.footer {
  background: #ff7b7e;
  color: black;
  font-size: 12px;
}
.alert {
    position: fixed;
    top: 50px;
    left: 20vw;
    z-index: 999;
    right: 20vw;
    box-shadow: 0px 0px 20px 10px grey;
}
.nav-tabs .nav-link {
  background: hotpink !important;
  color: white !important;
    border: 1px solid white;
}
.nav-tabs .nav-link.active {
  background: antiquewhite !important;
  color: black !important;
}
.stock-card,
.stock-card th,
.stock-card td {
  font-size: .9rem;
}
.stock-card .btn-warning {
    font-size: .9rem;
    padding: 3px 6px;

}
.dropdown-menu {
  background: antiquewhite;
}
.btn-cart {
  background: #ff7b7e;
  height: 50px;
  position: relative;
  font-size: 26px;
}
.btn-cart .cart-qty {
    position: absolute;
    left: 33px;
    top: 4px;
    background: red;
    padding: 1px 7px;
    font-size: 12px;
    color: white;
    border-radius: 15px;
}
.btn-warning {
  background-color: #ff7b7e;
  color: white;
  border-color: #ff7b7e;
}
.btn-warning:hover {
  background: hotpink;
  color: black;
  border-color: hotpink;
}
.nav-link:hover {
  color: maroon;
}
a {
  text-decoration: none;
}
.nav-pills .nav-link {
  border: 1px solid lightgrey;
  border-radius: 0;
  border-bottom: 0;
}
.nav-pills li:last-child {
  border-bottom: 1px solid lightgrey;
}
.product-img-wrapper {
 	position: relative;
  }
.sale-price {
    position: absolute;
    bottom: 0;
    text-align: center;
    width: 100%;
  background: rgba(0,0,0,0.5);
}
  .sale-price s {
    color: red;
    display: block;
  }
  .sale-price b {
  	color: white;
    font-size: 1.2rem;
    line-height: 10px;
    padding-bottom: 10px;
  }
  /* common */
.ribbon {
  width: 100px;
  height: 100px;
  overflow: hidden;
  position: absolute;
  z-index: 2;
}
.ribbon span {
  position: absolute;
  display: block;
  width: 165px;
  padding: 5px 0;
  font-weight: 700;
  font-size: 1.2rem;
  background-color: red;
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
}
  /* top left*/
.ribbon-top-left {
  top: -3px;
  left: -3px;
}
.ribbon-top-left::before,
.ribbon-top-left::after {
  border-top-color: transparent;
  border-left-color: transparent;
}
.ribbon-top-left::before {
  top: 0;
  right: 0;
}
.ribbon-top-left::after {
  bottom: 0;
  left: 0;
}
.ribbon-top-left span {
    right: -15px;
    top: 15px;
  transform: rotate(-45deg);
}
        </style>
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2JKC0M0XDP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2JKC0M0XDP');
</script>
    </head>
    <body>
        <div class="loading">
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
          <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
          </symbol>
          <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
          </symbol>
          <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
          </symbol>
        </svg>
        <div class="container-fluid header-1">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="phone-gold" onclick="window.open('https://wa.me/6281283443452')">
                  <i class="fas fa-phone"></i> &nbsp;  Contact Us: +62 812 8344 3452
                </div>
                @if (Session::get('instagram'))
              <span class="position-absolute top-0 pt-1" style="color:black;font-size:70%;">
              &nbsp; Welcome {{ '@'.Session::get('instagram') }}&nbsp;&nbsp;
              </span>
              @endif
              </div>
            </div>
          </div>
        </div>

        <div class="container-fluid header-2">
          <div class="container">
              <div class="row header-wrapper">
                <div class="col-12 col-md-5">
                  <a href="{{ url('/') }}">
                    <img src="{{ url('public/momavel.webp') }}" alt="">
                  </a>
                    <img src="{{ url('public/header-photo.webp') }}" class="p-0" alt="">
                </div>
                
                {{-- <div class="col-2 d-block px-0 d-md-none my-auto">
                  <button class="btn form-control btn-lg btn-cart" data-bs-toggle="tooltip" data-bs-placement="top" title="Keranjang Belanja" onclick="window.location.href = '{{ url('cart') }}'">
                    <i class="fas fa-shopping-cart"></i>
                    <span class="cart-qty">0</span>
                  </button>
                </div> --}}
                <div class="col-12 col-md-5 my-auto">
                  <form action="{{ url('product-search') }}" method="GET">
                    @csrf
                    <div class="input-group">
                      <input type="text" placeholder="Cari Kode Produk atau Nama Produk" class="form-control" name="search">
                      <button role="submit" type="submit" class="btn btn-warning"><i class="fas fa-search"></i></button>
                    </div>
                  </form>
                </div>
                
               {{--  <div class="d-none d-md-block col-md-1 my-auto">
                  <button class="btn form-control btn-lg btn-cart" data-bs-toggle="tooltip" data-bs-placement="top" title="Keranjang Belanja" onclick="window.location.href = '{{ url('cart') }}'">
                    <i class="fas fa-shopping-cart"></i>
                    <span class="cart-qty">0</span>
                  </button>
                </div> --}}
                <div class="d-block d-md-none col-12 mb-2"></div>
              </div>
            </div>
        </div>

        <nav class="navbar navbar-expand-md header-3">
          <div class="container position-relative">
              
            
            <div class="navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav mx-auto">
                {{-- <a class="nav-link" aria-current="page" href="#">Home</a> --}}
                {{-- <a class="nav-link" aria-current="page" href="#">Produk</a> --}}
                <a class="nav-link" aria-current="page" href="{{url('/')}}"><i class="fas fa-tags"></i> &nbsp; Etalase</a>
                
                <a class="nav-link" aria-current="page" href="{{url('rekap/check')}}"><i class="fas fa-shopping-cart"></i> &nbsp; Keranjang</a>
                
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarCategory" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  <i class="fas fa-user"></i> &nbsp; Akun Saya
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarCategory">
                          
                      <li><a class="dropdown-item" href="{{ url('my-account') }}"><i class="fas fa-user"></i> &nbsp; Semua Orderan</a></li>
                    @if (!empty(\Session::get('instagram')))
                      <li><a class="nav-link" aria-current="page" href="{{url('user/logout')}}"><i class="fas fa-sign-out-alt"></i> &nbsp; Logout</a></li>
                    @endif
                  </ul>
                </li>
                {{-- <a class="nav-link" aria-current="page" href="{{url('cara-berbelanja')}}">Cara Berbelanja</a> --}}
              </div>
            </div>
          </div>
        </nav>

        <div class="container">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                  <strong>{{ session('success') }}</strong>
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if (session('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <strong>{{ session('error') }}</strong>
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif
            @yield('content')
        </div>

        <div class="container-fluid footer mt-5">
          <div class="container py-2">
            <center><a href="supps/login">Supplier Login</a> &nbsp; &nbsp;&nbsp;&nbsp;            <a href="resellerlogin">Reseller/Dropshipper</a></center>
          
            <center>Copyright &copy; 2021 <?= (date('Y') > 2021) ? "- ".date('Y') : ''; ?> MomAvel.id</center>
          </div>
        </div>

        <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-body">
                      <img class="image_modal" width="100%" src="" />
                      <br>
                      <br>
                      <button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal" aria-label="Close">Close</button>
                  </div>
              </div>
          </div>
      </div>
        @yield('script')
        <script>
          $(document).ready(function(){
              updateCartQty();

            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
              return new bootstrap.Tooltip(tooltipTriggerEl);
            });
          });

            $( document ).ajaxStart(function() {
                $( ".loading" ).show();
            });
            $( document ).ajaxStop(function() {
                $( ".loading" ).hide();
            });
            $(document).ready(function(){
                $( ".loading" ).hide();
            });

            function changeSrc(thisid) {
                var src_new = $(thisid).attr('src');
                $('.image_modal').attr("src", src_new);
            }

            function updateCartQty() {
              $.ajax({
                  method: 'get',
                  url: "{{ url('api/cart-qty') }}",
                  cache: false,
                  success: function(result) {
                    $('.cart-qty').html(result);                      
                  }
              });
            }
        </script>
    </body>
</html>