@extends('userpage/template')
@section('content')
<div class="row mt-5">
    <div class="col-12 col-md-2">
        <h4>Kategori</h4>
        <ul class="nav flex-column nav-pills">
        @foreach ($product_category as $row)
        <li><a class="nav-link" href="{{ url('category')."/".$row->category }}">{{ $row->category }}</a></li>
        @endforeach
        </ul>
    </div>
    <div class="col-12 col-md-10">
        <div class="row">
            <div class="col-12 p-2">
                <div class="card h-100 bg-light stock-card">
                  <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            @if (!empty($master->master_image))
                            <img style="max-width: 100%; cursor: pointer;" src="{{ url('public').\Storage::url($master->master_image) }}" onclick="changeSrc(this)"  data-bs-toggle="modal" data-bs-target="#imageModal" alt="">
                            @else
                            <img style="max-width: 100%; cursor: pointer;" src="{{ url('public').\Storage::url('no_image.png') }}" onclick="changeSrc(this)"  data-bs-toggle="modal" data-bs-target="#imageModal" alt="">
                            @endif
                            @foreach ($product as $row)
                            @if ($row->images)
                            <img style="width: 33%; margin-top: 5px; cursor: pointer;" src="{{ url('public').\Storage::url($row->images) }}" onclick="changeSrc(this)"  data-bs-toggle="modal" data-bs-target="#imageModal" alt="">
                            @endif
                            @endforeach                            
                        </div>
                        <div class="col-12 col-md-8">
                            <h3>{{ $master->internal_code }}</h3>
                            <h4>{{ $master->product_name }}</h4>
                            <h4>Harga : {{ number_format($master->selling_price) }}</h4>
                            Stok : 
                            <table class="table table-sm table-striped table-stock">
                                <thead>
                                    <tr>
                                        <th>Variant</th>
                                        <th>Size</th>
                                        <th>Info</th>
                                        <th>Stock</th>
                                        <th colspan="2">Qty Beli</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $x = 1 @endphp
                                    @foreach ($product as $row) 
                                    @if ($row->qty > 0)
                                    <tr data-seq='{{ $x }}'>
                                        <td>
                                            <input type="hidden" name="internal_code" value="{{ $master->internal_code }}">
                                            <input type="hidden" name="variant" value="{{ $row->variant }}">
                                            <input type="hidden" name="size" value="{{ $row->size }}">
                                            {{ $row->variant }}
                                        </td>
                                        <td>{{ $row->size }}</td>
                                        <td>{{ $row->keterangan }}</td>
                                        <td>{{ $row->qty }}</td>
                                        <td>
                                            <input type="number" name="qty" value="1" style="width: 50px" min="1" max="{{ $row->qty }}">
                                        </td>
                                        <td>
                                            <button class="btn btn-sm btn-warning" onclick="addToCart(this)"><i class="fas fa-cart-plus"></i></button>
                                        </td>
                                    </tr>
                                    @php $x++ @endphp
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    function addToCart(thisid) {
        var sequence = $(thisid).closest('tr').attr('data-seq');
        var internal_code = $('tr[data-seq='+sequence+'] input[name=internal_code]').val();
        var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val();
        var size = $('tr[data-seq='+sequence+'] input[name=size]').val();
        var qty = $('tr[data-seq='+sequence+'] input[name=qty]').val();
        {{-- var ajax_url = "{{ url('api/add-to-session-cart') }}"; --}}
        var ajax_url = "{{ url('api/add-to-cart') }}";


        $.ajax({
            url: ajax_url,
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                internal_code: internal_code,
                variant: variant,
                size: size,
                qty: qty,
                _token: '{{ csrf_token() }}',
            },
            success: function (myresponse) {
                console.log(myresponse);
                alert(myresponse.message);  
            },
            error: function(xhr, status, error) {
                var err = JSON.parse(xhr.responseText);
                console.log(err);
                alert(err.message);
            }
        });
        
    }
</script>
@endsection