@extends('userpage/template')
@section('meta')
@if (isset($page_title))
    <title>{{ $page_title }} - MomAvel.id</title>
    {{-- <meta name="description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    <meta name="keywords" content="cart">
    <meta name="robots" content="noindex, nofollow">
    <link rel="canonical" href="{{ url()->current() }}">
    <meta property="og:title" content="{{ $page_title }} - MomAvel.id">
    {{-- <meta property="og:description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    {{-- <meta property="og:type" content="product"> --}}
    {{-- <meta property="og:url" content="{{ url()->current() }}"> --}}
    {{-- <meta property="og:image" content="{{ url('public').\Storage::url($slides[0]->image_url) }}"> --}}
@else
    <meta name="title" content="MomAvel.id">
    <title>MomAvel.id</title>
@endif
@endsection
@section('content')
<div class="row mt-5">
    <div class="col"></div>
    <div class="col-4 p-4 border bg-light ">
        <div class="alert alert-warning" role="alert">
            Pembayaran Untuk Nomor Pesanan <b>{{ session()->get('order_id') }}</b> Pending. Silahkan Melakukan Pembayaran Dan Hubungi Admin.
        </div>
    </div>
    <div class="col"></div>
</div>
@endsection