@extends('userpage/template')
@section('meta')
@php $category_list = '';
foreach ($product_category as $row) {
    if ($category_list == '') {
        $category_list = $row->category;
    } else {
        $category_list .= ', '.$row->category;
    }
}
@endphp
    <title>{{ $page_title }} - MomAvel.id</title>
    <meta name="description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish.">
    <meta name="keywords" content="{{ $category_list }}">
    <meta name="robots" content="index, follow">
    <link rel="canonical" href="{{ url()->current() }}">
    <meta property="og:title" content="{{ $page_title }} - MomAvel.id">
    <meta property="og:description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish.">
    <meta property="og:type" content="product">
    <meta property="og:url" content="{{ url()->current() }}">
    @if (!empty($slides[0]->image_url))
    <meta property="og:image" content="{{ url('public').\Storage::url($slides[0]->image_url) }}">
    @endif
@endsection
@section('content')
<?php
$stock_list = [];
foreach ($stock as $row) {
    $stock_list[$row->internal_code][$row->variant][$row->size]['qty'] = $row->qty;
    $stock_list[$row->internal_code][$row->variant][$row->size]['keterangan'] = $row->keterangan;
}
 ?>
<div class="row mt-3">
    <div class="col-12 col-md-2">
        <h4>Kategori</h4>
        <ul class="nav flex-column nav-pills">
        <li><a class="nav-link" href="{{ url('category')."/promo" }}">PROMO / SALE</a></li>
        @foreach ($product_category as $row)
        <li><a class="nav-link" href="{{ url('category')."/".$row->category }}">{{ $row->category }}</a></li>
        @endforeach
        </ul>
        </br>
        </br>
        <ul class="nav flex-column nav-pills">
        
        <li><a class="nav-link" >
            
            <h6>Cek Ongkir</h6>
            
            </br>Provinsi
            <select name="province_id" required="required" class="form-control" onchange="loadCity()"></select>
            <input type="hidden" name="province">
            Kabupaten / Kota
            <select name="city_id" required="required" class="form-control" onchange="loadSubdistrict()"></select>
            Kecamatan
            <input type="hidden" name="city">
            
            <select name="subdistrict_id" required="required" class="form-control" onchange="loadOngkir()"></select>
            <input type="hidden" name="subdistrict">
            
            Ongkos Kirim /kg
            <input type="number" readonly="readonly" value="" class="form-control" name="shipping_cost">
            
        </a></li>
        
        </ul>
    </div>
	</br>
			</br>
    <div class="col-12 col-md-10">

            @php
            $x = 0;
            $slide_count = count($slides);
            @endphp
            @if ($slide_count == 1)
                  <img src="{{ url('public').\Storage::url($slides[0]->image_url) }}" class="d-block w-100" alt="...">
            @else

            <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
              <div class="carousel-indicators">
                @for ($i=0; $i < $slide_count; $i++)
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{ $i }}" {{ (($i == 0) ? 'class=active aria-current=true' : '') }} aria-label="Slide {{ $i }}"></button>
                @endfor
              </div>
              <div class="carousel-inner">
                @foreach ($slides as $row)
                <div class="carousel-item{{ (($x == 0) ? ' active' : '') }}" data-bs-interval="3000">
                  <img src="{{ url('public').\Storage::url($row->image_url) }}" class="d-block w-100" alt="...">
                </div>
                @php $x++; @endphp
                @endforeach
              </div>
              <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
            @endif
        <div class="row mt-3">
            <div class="col-12 col-md-8">
                @if (isset($search_query))
                <h5>Menampilkan hasil pencarian untuk: "{{ $search_query }}"</h5>
                @elseif (isset($category_query))
                <h5>Menampilkan produk dalam kategori: "{{ $category_query }}"</h5>
                @else
                <h5>Menampilkan semua stok tersedia</h5>
                @endif
            </div>
            <div class="col-12 col-md-4">
                <select name="sort" id="changeSort" class="form-select">
                    <option value="newest" selected="selected">Terbaru</option>
                    <option value="code_ASC">Kode Barang A-Z</option>
                    <option value="code_DESC">Kode Barang Z-A</option>
                    <option value="name_ASC">Nama Barang A-Z</option>
                    <option value="name_DESC">Nama Barang Z-A</option>
                    <option value="price_ASC">Harga Terendah</option>
                    <option value="price_DESC">Harga Tertinggi</option>
                </select>
            </div>
            <?php $x = 1; ?>
        @foreach ($master as $row)
            @if ($row->selling_price > 0)
                
            <div class="col-6 col-md-3 p-2">
            <a style="color: inherit;" href="{{ url('product').'/'.$row->internal_code }}">
                <div class="card h-100 bg-light stock-card">
                  <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                          <div class="cont">
                            
                            @if($row->hargaasli != null && $row->hargaasli > $row->selling_price && !empty($row->master_image))
          					<div class="ribbon ribbon-top-left">
                              <span>SALE!!!</span>
          					</div>
                            @endif
                            
                            <div class="product-img-wrapper">
                            @if (!empty($row->master_image))
                            <img style="max-width: 100%; cursor: pointer;" src="{{ url('public').\Storage::url($row->master_image) }}" alt="">
                            @else
                            <img style="max-width: 100%; cursor: pointer;" src="{{ url('public').\Storage::url('no_image.png') }}" alt="">
                            @endif
                              
                            @if($row->hargaasli != null && $row->hargaasli > $row->selling_price && !empty($row->master_image))
                                <div class="sale-price">
                                  <s>{{ number_format($row->hargaasli) }}</s>
                                  <b>{{ number_format($row->selling_price) }}</b>
                                </div>
                              @endif
                            </div>
                          </div>
                            <h3>{{ $row->internal_code }}</h3>
                            @if($row->hargaasli != null && $row->hargaasli > $row->selling_price)
                            <h6>{{ ucwords($row->product_name) }} (SALE!!)</h6>
                            @else
                            <h6>{{ ucwords($row->product_name) }}</h6>
                            @endif


                            
                            @if($row->hargaasli != null && $row->hargaasli > $row->selling_price)
                                <h6><p style="color:red; line-height: 0.7 ;">Dari : <s>{{ number_format($row->hargaasli) }}</s></p>
                                <p style="color:black; line-height: 0.2;">Jadi : {{ number_format($row->selling_price) }}</p></h6>
                            @else
                                <h6>Harga : {{ number_format($row->selling_price) }}</h6>
                            @endif
                        </div>
                    </div>
                  </div>
              </div>
            </a>
            </div>
            @endif
        @endforeach
        {{ $master->links() }}
        
    </div>
    <center></br><img style="max-width: 80%; cursor: pointer;" src="{{ url('public').\Storage::url('freeongkir1.jpg') }}" alt=""></br></center>
    </div>
    


    
</div>

@endsection
@section('script')
<script>
    $(document).ready(function(){
        loadProvince();
        var sortbysel = '{{ request()->sort }}';
        if (sortbysel == '') {
            sortbysel = 'newest';
        }
        console.log(sortbysel);
        $('#changeSort').val(sortbysel);
    });


    function loadProvince() {
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/province',
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var province = '<option disabled="disabled" selected="selected">-- Pilih Provinsi</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    province += '<option value="'+item.province_id+'">'+item.province+'</option>';
                });
                $('select[name=province_id]').html(province);
            }
        });
    }

    function loadCity() {
        var province_id = $('select[name=province_id]').find(':selected').val();
        console.log(province_id);
        var province_name = $('select[name=province_id]').find(':selected').text();
        $('input[name=province]').val(province_name);
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/city?province='+province_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var city = '<option disabled="disabled" selected="selected">-- Pilih Kota</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    city += '<option value="'+item.city_id+'">'+item.type+' '+item.city_name+'</option>';
                });
                $('select[name=city_id]').html(city);
            }
        });
    }

    function loadSubdistrict() {
        var city_id = $('select[name=city_id]').find(':selected').val();
        var city_name = $('select[name=city_id]').find(':selected').text();
        $('input[name=city]').val(city_name);
        $.ajax({
            method: 'POST',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/subdistrict?city='+city_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var subdistrict = '<option disabled="disabled" selected="selected">-- Pilih Kecamatan</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    subdistrict += '<option value="'+item.subdistrict_id+'">'+item.subdistrict_name+'</option>';
                });
                $('select[name=subdistrict_id]').html(subdistrict);
            }
        });
    }

    function loadOngkir() {

        var subdistrict_id = $('select[name=subdistrict_id]').find(':selected').val();
        var subdistrict_name = $('select[name=subdistrict_id]').find(':selected').text();
        $('input[name=subdistrict]').val(subdistrict_name);
        var origin_city = 155; // jakarta utara
        var postdata = 'origin='+origin_city+'&originType=city&destination='+subdistrict_id+'&destinationType=subdistrict&weight=1000&courier=ide';
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/cost',
                'method': 'post',
                'postdata' : postdata,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                 var tr = '';
                var response = JSON.parse(response);
                var shipping_cost = response.rajaongkir.results[0].costs[0].cost[0].value;
                var shipping = response.rajaongkir.results[0].costs[0].service+' '+response.rajaongkir.results[0].costs[0].description;
                 console.log(response);
                 // $.each(response.rajaongkir.results[0].costs, function(index, item){
                    tr += '<tr>'+
                    '<td>'+shipping+'</td>'+
                    '<td>'+shipping_cost+' /kg</td>'+
                    '</tr>'; 
                 // });
               
                 $('input[name=shipping_cost]').val(shipping_cost);
                 
            }
        });
    }






    $('#changeSort').on('change', function() {
        var currentURI = window.location.href;
        var sortBy = $('#changeSort').find(':selected').val();
        if (currentURI.indexOf('sort=') >= 0) {
            var baseURI = currentURI.split('?')[0];
            var getParameter = currentURI.split('?')[1];
            var breakParameter = getParameter.split('&');
            var regenerateURI = baseURI;
            var x = 0;
            if (breakParameter.length == 1) {
                regenerateURI = baseURI + '?sort=' + sortBy;
            } else {
                $.each(breakParameter, function(index, val) {
                    var paramArray = val.split('=');
                    if (paramArray[0] == 'sort') {
                        regenerateURI = regenerateURI + '&' + 'sort=' + sortBy;
                    } else {
                        if (x > 0) {
                            regenerateURI = regenerateURI + '&' + val;
                        } else {
                            regenerateURI = regenerateURI + '?' + val;
                        }
                    }
                    x++;
                });
            }
            window.location.href = regenerateURI;
        } else {
            if (currentURI.indexOf('?') >= 0) {
                window.location.href = currentURI+'&sort='+sortBy;
            } else {
                window.location.href = currentURI+'?sort='+sortBy;
            }
        }
    });






</script>
@endsection