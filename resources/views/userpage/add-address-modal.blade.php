        <div class="modal fade" id="addAddressModal" tabindex="-1" aria-labelledby="addAddressModal" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-body">
                      <form action="{{ url('user/add-address') }}" method="post">
                        @csrf
                        <input type="hidden" name="callbackURL" value="{{ \Request::path() }}">
                        <input type="hidden" name="instagram" value="{{ \Session::get('instagram') }}">
                      <div class="row">
                          <div class="col-6">
                              <h4>Masukkan Detail Penerima</h4>
                              Nama Depan
                              <input type="text" class="form-control mb-2" required="required" name="nama_depan">
                              Nama Belakang
                              <input type="text" class="form-control mb-2" name="nama_belakang">
                              Telepon
                              <input type="text" class="form-control mb-2" required="required" name="telepon">
                              Email
                              <input type="email" class="form-control mb-2" required="required" name="email">
                          </div>
                          <div class="col-6">
                              <h4>Masukkan Alamat Pengiriman</h4>
                              Provinsi
                              <select name="province_id" class="form-control mb-2" required="required" onchange="loadCity()"></select>
                              <input type="hidden" name="province">
                              Kota / Kabupaten
                              <select name="city_id" class="form-control mb-2" required="required" onchange="loadSubdistrict()"></select>
                              <input type="hidden" name="city">
                              Kecamatan
                              <select name="subdistrict_id" class="form-control mb-2" required="required" onchange="loadOngkir()"></select>
                              <input type="hidden" name="subdistrict">
                              Alamat
                              <textarea name="address" required="required" placeholder="Alamat Lengkap" rows="5" class="form-control mb-2"></textarea>
                              <h4>Tarif Pengiriman ID Express</h4>
                              <table class="table table-striped">
                                  <thead>
                                      <tr>
                                          <th>Jasa Pengiriman</th>
                                          <th>Ongkir / kg</th>
                                      </tr>
                                  </thead>
                                  <tbody class="choice_ongkir">
                                  </tbody>
                              </table>
                              <input type="hidden" name="shipping">
                              <input type="hidden" name="shipping_cost">
                              <div class="row">
                                  <div class="col-6">
                                      <button type="submit" id="saveAddressButton" disabled="disabled" class="btn form-control mb-2 btn-warning">Simpan</button>
                                  </div>
                                  <div class="col-6">
                                    <button type="button" class="btn form-control btn-danger" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      <script type="text/javascript">
        
        $(document).ready(function(){
          loadProvince();
        });
    function loadProvince() {
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/province',
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var province = '<option disabled="disabled" selected="selected">-- Pilih Provinsi</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    province += '<option value="'+item.province_id+'">'+item.province+'</option>';
                });
                $('select[name=province_id]').html(province);
            }
        });
    }

    function loadCity() {
        var province_id = $('select[name=province_id]').find(':selected').val();
        console.log(province_id);
        var province_name = $('select[name=province_id]').find(':selected').text();
        $('input[name=province]').val(province_name);
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/city?province='+province_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var city = '<option disabled="disabled" selected="selected">-- Pilih Kota</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    city += '<option value="'+item.city_id+'">'+item.type+' '+item.city_name+'</option>';
                });
                $('select[name=city_id]').html(city);
            }
        });
    }

    function loadSubdistrict() {
        var city_id = $('select[name=city_id]').find(':selected').val();
        var city_name = $('select[name=city_id]').find(':selected').text();
        $('input[name=city]').val(city_name);
        $.ajax({
            method: 'POST',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/subdistrict?city='+city_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var subdistrict = '<option disabled="disabled" selected="selected">-- Pilih Kecamatan</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    subdistrict += '<option value="'+item.subdistrict_id+'">'+item.subdistrict_name+'</option>';
                });
                $('select[name=subdistrict_id]').html(subdistrict);
            }
        });
    }

    function loadOngkir() {

        var subdistrict_id = $('select[name=subdistrict_id]').find(':selected').val();
        var subdistrict_name = $('select[name=subdistrict_id]').find(':selected').text();
        $('input[name=subdistrict]').val(subdistrict_name);
        var origin_city = 155; // jakarta utara
        var postdata = 'origin='+origin_city+'&originType=city&destination='+subdistrict_id+'&destinationType=subdistrict&weight=1000&courier=ide';
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/cost',
                'method': 'post',
                'postdata' : postdata,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var tr = '';
                var response = JSON.parse(response);
                var shipping_cost = response.rajaongkir.results[0].costs[0].cost[0].value;
                var shipping = response.rajaongkir.results[0].costs[0].service+' '+response.rajaongkir.results[0].costs[0].description;
                 console.log(response);
                 // $.each(response.rajaongkir.results[0].costs, function(index, item){
                    tr += '<tr>'+
                    '<td>'+shipping+'</td>'+
                    '<td>'+shipping_cost+' /kg</td>'+
                    '</tr>'; 
                 // });
                 $('.choice_ongkir').html(tr);
                 $('input[name=shipping]').val(shipping);
                 $('input[name=shipping_cost]').val(shipping_cost);
                 $('#saveAddressButton').removeAttr('disabled');
            }
        });
    }

      </script>