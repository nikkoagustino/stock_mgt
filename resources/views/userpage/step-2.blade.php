@extends('userpage/template')
@section('content')
<div class="row mt-5">
    @if (isset($customer_data))
        @if (isset($customer_data->password))
        <div class="col-12 col-md-4 p-4 border bg-light mx-auto text-center">
            Customer telah terdaftar, silahkan login
            <form action="{{ url('rekap/check') }}" method="post">
                @csrf
                Username / Instagram
                    <input type="hidden" name="user_status" value="login">
                    <input type="hidden" name="tanggal" value="{{ $req->tanggal }}">
                <input type="text" required="required" name="instagram" value="{{ $req->instagram }}" class="form-control mb-2">
                Password
                <input type="password" required="required" name="password" class="form-control mb-2">
                <input type="submit" value="Login" class="btn btn-warning">
                <a href="{{ url('user/forgot-password') }}">Lupa Password?</a>
            </form>
        </div>
        @else
        <div class="col-12 col-md-4 p-4 border bg-light mx-auto text-center">
            Customer telah terdaftar tanpa password, silahkan buat password untuk melanjutkan
            <form action="{{ url('rekap/check') }}" method="post">
                @csrf
                Username / Instagram
                    <input type="hidden" name="user_status" value="newpass">
                    <input type="hidden" name="tanggal" value="{{ $req->tanggal }}">
                <input type="text" required="required" readonly="readonly" name="instagram" value="{{ $req->instagram }}" class="form-control mb-2">
                    Password
                    <input type="password" name="password" required="required" class="form-control mb-2" onkeyup="matchingPassword()">
                    Confirm Password
                    <input type="password" name="confirm_password" required="required" class="form-control mb-2" onkeyup="matchingPassword()">
                    <i class="passnomatch" style="display:none; color:red; font-size: 12px">Password Not Match</i>
                <input type="submit" id="passButton" disabled="disabled" value="Simpan Password" class="btn btn-warning">
            </form>
        </div>
        @endif
    @else

        <div class="col-12 col-md-8 p-4 border bg-light mx-auto">
           <center><h4>Customer belum terdaftar, silahkan melanjutkan Pendaftaran dan Checkout</h4></center>
           <hr>
        <form action="{{ url('rekap/check') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-6">
                    <h4>Masukkan Detail Penerima</h4>
                    Instagram
                    <input type="text" readonly="readonly" name="instagram" value="{{ $req->instagram }}" class="form-control mb-2">
                    <input type="hidden" name="tanggal" value="{{ $req->tanggal }}">
                    <input type="hidden" name="user_status" value="register">
                    Nama Depan
                    <input type="text" class="form-control mb-2" required="required" name="nama_depan">
                    Nama Belakang
                    <input type="text" class="form-control mb-2" name="nama_belakang">
                    Telepon
                    <input type="text" class="form-control mb-2" required="required" name="telepon">
                    Email
                    <input type="email" class="form-control mb-2" required="required" name="email">
                    Password
                    <input type="password" name="password" required="required" class="form-control mb-2" onkeyup="matchingPassword()">
                    Confirm Password
                    <input type="password" name="confirm_password" required="required" class="form-control mb-2" onkeyup="matchingPassword()">
                    <i class="passnomatch" style="display:none; color:red; font-size: 12px">Password Not Match</i>
                </div>
                <div class="col-6">
                    <h4>Masukkan Alamat Pengiriman</h4>
                    Provinsi
                    <select name="province_id" class="form-control mb-2" required="required" onchange="loadCity()"></select>
                    <input type="hidden" name="province">
                    Kota / Kabupaten
                    <select name="city_id" class="form-control mb-2" required="required" onchange="loadSubdistrict()"></select>
                    <input type="hidden" name="city">
                    Kecamatan
                    <select name="subdistrict_id" class="form-control mb-2" required="required" onchange="loadOngkir()"></select>
                    <input type="hidden" name="subdistrict">
                    Alamat
                    <textarea name="address" required="required" placeholder="Alamat Lengkap" rows="5" class="form-control mb-2"></textarea>
                    <h4>Tarif Pengiriman ID Express</h4>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Jasa Pengiriman</th>
                                <th>Ongkir / kg</th>
                            </tr>
                        </thead>
                        <tbody class="choice_ongkir">
                        </tbody>
                    </table>
                    <input type="hidden" name="shipping">
                    <input type="hidden" name="shipping_cost">
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" id="checkoutButton" disabled="disabled" class="btn form-control mb-2 btn-warning">Checkout</button>
                        </div>
                        <div class="col-6">
                            <button class="btn form-control mb-2 btn-danger" onclick="showChoiceForm()">Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        </div>
    @endif
</div>
@endsection
@section('script')
<script>

    $(document).ready(function(){
        loadProvince();
    });
    function loadProvince() {
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/province',
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var province = '<option disabled="disabled" selected="selected">-- Pilih Provinsi</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    province += '<option value="'+item.province_id+'">'+item.province+'</option>';
                });
                $('select[name=province_id]').html(province);
            }
        });
    }

    function loadCity() {
        var province_id = $('select[name=province_id]').find(':selected').val();
        console.log(province_id);
        var province_name = $('select[name=province_id]').find(':selected').text();
        $('input[name=province]').val(province_name);
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/city?province='+province_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var city = '<option disabled="disabled" selected="selected">-- Pilih Kota</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    city += '<option value="'+item.city_id+'">'+item.type+' '+item.city_name+'</option>';
                });
                $('select[name=city_id]').html(city);
            }
        });
    }

    function loadSubdistrict() {
        var city_id = $('select[name=city_id]').find(':selected').val();
        var city_name = $('select[name=city_id]').find(':selected').text();
        $('input[name=city]').val(city_name);
        $.ajax({
            method: 'POST',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/subdistrict?city='+city_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var subdistrict = '<option disabled="disabled" selected="selected">-- Pilih Kecamatan</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    subdistrict += '<option value="'+item.subdistrict_id+'">'+item.subdistrict_name+'</option>';
                });
                $('select[name=subdistrict_id]').html(subdistrict);
            }
        });
    }

    function loadOngkir() {

        var subdistrict_id = $('select[name=subdistrict_id]').find(':selected').val();
        var subdistrict_name = $('select[name=subdistrict_id]').find(':selected').text();
        $('input[name=subdistrict]').val(subdistrict_name);
        var origin_city = 155; // jakarta utara
        var postdata = 'origin='+origin_city+'&originType=city&destination='+subdistrict_id+'&destinationType=subdistrict&weight=1000&courier=ide';
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/cost',
                'method': 'post',
                'postdata' : postdata,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var tr = '';
                var response = JSON.parse(response);
                var shipping_cost = response.rajaongkir.results[0].costs[0].cost[0].value;
                var shipping = response.rajaongkir.results[0].costs[0].service+' '+response.rajaongkir.results[0].costs[0].description;
                 console.log(response);
                 // $.each(response.rajaongkir.results[0].costs, function(index, item){
                    tr += '<tr>'+
                    '<td>'+shipping+'</td>'+
                    '<td>'+shipping_cost+' /kg</td>'+
                    '</tr>'; 
                 // });
                 $('.choice_ongkir').html(tr);
                 $('input[name=shipping]').val(shipping);
                 $('input[name=shipping_cost]').val(shipping_cost);
                 $('#checkoutButton').removeAttr('disabled');
                 matchingPassword();
            }
        });
    }

    function matchingPassword() {
        var pass1 = $('input[name=password]').val();
        var pass2 = $('input[name=confirm_password]').val();
        if (pass1 == pass2) {
            $('#checkoutButton').removeAttr('disabled');
            $('#passButton').removeAttr('disabled');
            $('.passnomatch').hide();
        } else {
            $('#checkoutButton').attr('disabled', 'disabled');
            $('#passButton').attr('disabled', 'disabled');
            $('.passnomatch').show();
        }
    }
</script>
@endsection