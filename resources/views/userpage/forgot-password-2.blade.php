@extends('userpage/template')
@section('content')
<?php
function hide_mail($email) {
    $mail_part = explode("@", $email);
    $asterisk_length = strlen($mail_part[0]) - 4;
    $star = "";
    for ($i = 0; $i < $asterisk_length; $i++) {
        $star .= "*";
    }
    $front = substr($mail_part[0], 0, 3);
    $back = substr($mail_part[0], strlen($mail_part[0]) - 3, 3);
    $mail_part[0] = $front.$star.$back;
    return implode("@", $mail_part);
}

function hide_phone($phone) {
    
    $asterisk_length = strlen($phone) - 4;
    $star = "";
    for ($i = 0; $i < $asterisk_length; $i++) {
        $star .= "*";
    }
    $front = substr($phone, 0, 3);
    $back = substr($phone, strlen($phone) - 3, 3);
    $phone = $front.$star.$back;
    return $phone;
}
?>
<div class="row mt-5">
    <div class="col-12 col-md-6 p-4 border bg-light mx-auto text-center">
        @if (!isset($userdata))
        <h4>Pengguna {{ '@'.$instagram }} belum terdaftar. Silahkan mendaftar di <a href="{{ url('rekap') }}">halaman ini</a>.</h4>
        @else
        <h4>Kami telah mengirimkan link reset password ke Email : <span class="text-danger">{{ hide_mail($userdata->email) }}</span> <br> dan Telpon : <span class="text-danger">{{ hide_phone($userdata->phone) }}</span>  <br>untuk konfirmasi reset password. <br><br>Silahkan Klik link yang di berikan untuk melakukan reset password. Dan jangan berikan link tersebut kepada siapapun.</h4>
        <br><i>
        Cek folder spam / junk email jika Anda tidak menerima email dari kami.</i>
        @endif
    </div>
</div>
@endsection