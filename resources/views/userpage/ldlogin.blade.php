@extends('userpage/ldtemplate')
@section('meta')
@if (isset($page_title))
    <title>{{ $page_title }} - MomAvel.id</title>
    {{-- <meta name="description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    <meta name="keywords" content="cart">
    <meta name="robots" content="noindex, nofollow">
    <link rel="canonical" href="{{ url()->current() }}">
    <meta property="og:title" content="{{ $page_title }} - MomAvel.id">
    {{-- <meta property="og:description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    {{-- <meta property="og:type" content="product"> --}}
    {{-- <meta property="og:url" content="{{ url()->current() }}"> --}}
    {{-- <meta property="og:image" content="{{ url('public').\Storage::url($slides[0]->image_url) }}"> --}}
@else
    <meta name="title" content="MomAvel.id">
    <title>MomAvel.id</title>
@endif
@endsection
@section('content')
<div class="row mt-5">
    <div id="myDIV2" class="col-6 col-md-5 p-4 border bg-light mx-auto text-center">
        <a href="{{ url('user/ldregister/') }}/{{$internal_code}}" class="btn btn btn-danger"><i class="fas fa-user-plus"></i> &nbsp; Belum Pernah Berbelanja Disini</a>
        </br></br>Atau</br></br><button onclick="myFunction()" class="btn btn-success"><i class="fas fa-user-plus"></i> &nbsp; Sudah Pernah Belanja Disini <br>/ Punya MomAvel ID</button>
    </div>
        <div id="myDIV" style="display:block;" class="col-6 col-md-5 p-4 border bg-light mx-auto text-center">
            
            Silahkan Masukan User ID Anda
            <hr>
            <form action="{{ url('user/ldlogin') }}" method="post">
                @csrf
                <input type="hidden"  name="internal_code" value="{{$internal_code}}" class="form-control mb-2">
                Username / Instagram
                <input type="text" required="required" name="instagram" class="form-control mb-2">
                Password
                <input type="password" required="required" name="password" class="form-control mb-2">
                <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-sign-in-alt"></i> &nbsp; Login</button>
                <hr>
                
                <a href="{{ url('user/forgot-password') }}" target="_blank" class="btn btn-warning"><i class="fas fa-user-lock"></i> &nbsp; Lupa Password?</a>
            </form>
        </div>
</div>
@endsection
@section('script')
<script>

$(document).ready(function(){
  var x = document.getElementById("myDIV");
  
    x.style.display = "none";
  
})

function myFunction() {
    document.getElementById("myDIV").style.display="block";
    document.getElementById("myDIV2").style.display="none";
}

</script>


@endsection