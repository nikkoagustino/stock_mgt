@extends('userpage/template')
@section('meta')
@if (isset($page_title))
    <title>{{ $page_title }} - MomAvel.id</title>
    {{-- <meta name="description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    <meta name="keywords" content="cart">
    <meta name="robots" content="noindex, nofollow">
    <link rel="canonical" href="{{ url()->current() }}">
    <meta property="og:title" content="{{ $page_title }} - MomAvel.id">
    {{-- <meta property="og:description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    {{-- <meta property="og:type" content="product"> --}}
    {{-- <meta property="og:url" content="{{ url()->current() }}"> --}}
    {{-- <meta property="og:image" content="{{ url('public').\Storage::url($slides[0]->image_url) }}"> --}}
@else
    <meta name="title" content="MomAvel.id">
    <title>MomAvel.id</title>
@endif
@endsection
@section('content')
<div class="row mt-5">
        <div class="col-12 col-md-4 p-4 border bg-light mx-auto text-center">
            Verifikasi berhasil. Silahkan memasukkan password baru Anda.<hr>
            <form action="{{ url('user/reset-password') }}" method="post">
                @csrf
                Username / Instagram
                <input type="text" required="required" readonly="readonly" value="{{ $userdata->username }}" name="instagram" class="form-control mb-2">
                <input type="hidden" name="oldpass" value="{{ $userdata->password }}">
                Password Baru
                <input type="password" required="required" name="password" class="form-control mb-2" onkeyup="matchingPassword()">
                Konfirmasi Password Baru
                <input type="password" required="required" name="confirm_password" class="form-control mb-2" onkeyup="matchingPassword()">
                <i class="text-danger passnomatch" style="display: none">Password tidak cocok<br></i>
                <input type="submit" disabled="disabled" value="Simpan Password" class="btn btn-warning"><br>
            </form>
        </div>
</div>
@endsection
@section('script')
<script>
    function matchingPassword() {
        var pass1 = $('input[name=password]').val();
        var pass2 = $('input[name=confirm_password]').val();
        if (pass1 == pass2) {
            $('input[type=submit]').removeAttr('disabled');
            $('.passnomatch').hide();
        } else {
            $('input[type=submit]').attr('disabled', 'disabled');
            $('.passnomatch').show();
        }
    }
</script>
@endsection