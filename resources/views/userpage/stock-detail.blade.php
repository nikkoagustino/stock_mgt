@extends('userpage/template')
@section('meta')
    <title>{{ ucwords($master->product_name) }} - MomAvel.id</title>
    <meta name="description" content="{{ ucwords($master->deskripsi) }}">
    <meta name="keywords" content="{{ ucwords($master->product_name) }}, {{ ucwords($master->category) }}">
    <meta name="robots" content="index, follow">
    <link rel="canonical" href="{{ url()->current() }}">
    <meta property="og:title" content="{{ ucwords($master->product_name) }} - MomAvel.id">
    <meta property="og:description" content="{{ ucwords($master->deskripsi) }}">
    <meta property="og:type" content="product">
    <meta property="og:url" content="{{ url()->current() }}">
    @if (!empty($master->master_image))
    <meta property="og:image" content="{{ url('public').\Storage::url($master->master_image) }}">
    @endif
    <meta property="product:category" content="{{ $master->category }}">
    <meta property="product:availability" content="in stock">
    <meta property="product:condition" content="new">
    <meta property="product:price:amount" content="{{ $master->selling_price }}">
    <meta property="product:price:currency" content="IDR">
@endsection

@section('content')
<div class="row mt-5">
    <div class="col-12 col-md-2">
        <h4>Kategori</h4>
        <ul class="nav flex-column nav-pills">
        <li><a class="nav-link" href="{{ url('category')."/promo" }}">PROMO / SALE</a></li>
        @foreach ($product_category as $row)
        <li><a class="nav-link" href="{{ url('category')."/".$row->category }}">{{ $row->category }}</a></li>
        @endforeach
        </ul>
        <input type="hidden" name="reffer" value="{{ Session::get('reffer'); }}">
    </div>
    <div class="col-12 col-md-10">
        <div class="row">
            <div class="col-12 p-2">
                <div class="card h-100 bg-light stock-card">
                  <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-4">
                          
                          <div class="cont">
                            
                            @if($master->hargaasli != null && $master->hargaasli > $master->selling_price && !empty($master->master_image))
          					<div class="ribbon ribbon-top-left">
                              <span>SALE!!!</span>
          					</div>
                            @endif
                            
                            <div class="product-img-wrapper">
                            @if (!empty($master->master_image))
                            <img style="max-width: 100%; cursor: pointer;" src="{{ url('public').\Storage::url($master->master_image) }}" onclick="changeSrc(this)"  data-bs-toggle="modal" data-bs-target="#imageModal"  alt="">
                            @else
                            <img style="max-width: 100%; cursor: pointer;" src="{{ url('public').\Storage::url('no_image.png') }}" alt="">
                            @endif
                              
                            @if($master->hargaasli != null && $master->hargaasli > $master->selling_price && !empty($master->master_image))
                                <div class="sale-price">
                                  <s>{{ number_format($master->hargaasli) }}</s>
                                  <b>{{ number_format($master->selling_price) }}</b>
                                </div>
                              @endif
                            </div>                      
                            </div>                      
                        </div>
                        <div class="col-12 col-md-8">
                            @if($master->hargaasli != null && $master->hargaasli > $master->selling_price)
                            <h3>{{ $master->internal_code }} <p style="color:red">(SALE!!)</p></h3>
                            @else
                            <h3>{{ $master->internal_code }}</h3></h4>
                            @endif
                            
                            <h4>{{ ucwords($master->product_name) }}</h4>
                            @if($master->hargaasli != null && $master->hargaasli > $master->selling_price)
                                <h4><p style="color:red">Harga Asli : <s>{{ number_format($master->hargaasli) }}</s></p></h4>
                            @endif
                            <h4>Harga Jual : {{ number_format($master->selling_price) }}</h4>
                            <h4>Deskripsi :</h4>  {!! nl2br(ucwords($master->deskripsi)) !!}
                            </br></br>
                            
                        </div>
                        <h4>Stok :</h4> 
                            <table class="table table-sm table-striped table-stock">
                                <thead>
                                    <tr>
                                        <th>Photo</th>
                                        <th>Variant</th>
                                        <th>Size</th>
                                        <th>Info</th>
                                        <th>Stock</th>
                                        <th colspan="2">Qty Beli</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $x = 1 @endphp
                                    @foreach ($product as $row) 
                                    @if ($row->qty > 0)
                                    <tr data-seq='{{ $x }}'>
                                        <td>
                                            <img style="width: 33%; margin-top: 5px; cursor: pointer;" src="{{ url('public').\Storage::url($row->images) }}" onclick="changeSrc(this)"  data-bs-toggle="modal" data-bs-target="#imageModal" alt="">
                                        </td>
                                        <td>
                                            <input type="hidden" name="internal_code" value="{{ $master->internal_code }}">
                                            <input type="hidden" name="variant" value="{{ $row->variant }}">
                                            <input type="hidden" name="size" value="{{ $row->size }}">
                                            {{ $row->variant }}
                                        </td>
                                        <td>{{ $row->size }}</td>
                                        <td>{{ ucwords($row->keterangan) }}</td>
                                        <td>{{ $row->qty }}</td>
                                        <td>
                                            <input type="number" name="qty" value="1" style="width: 50px" min="1" max="{{ $row->qty }}">
                                        </td>
                                        <td>
                                            <button class="btn btn-sm btn-warning" onclick="addToCart(this)"><i class="fas fa-cart-plus"></i></button>
                                            @if (!empty(\Session::get('reseller')))
                                            <button class="btn btn-sm btn-success" onclick="addToCartReseller(this)"><i class="fas fa-cart-plus">&nbsp;Add Reseller</i></button>
                                            @endif
                                        </td>
                                    </tr>
                                    @php $x++ @endphp
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    function addToCart(thisid) {
        var reffer = $('input[name=reffer]').val();
        var sequence = $(thisid).closest('tr').attr('data-seq');
        var internal_code = $('tr[data-seq='+sequence+'] input[name=internal_code]').val();
        var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val();
        var size = $('tr[data-seq='+sequence+'] input[name=size]').val();
        var qty = $('tr[data-seq='+sequence+'] input[name=qty]').val();
        {{-- var ajax_url = "{{ url('api/add-to-session-cart') }}"; --}}
        var ajax_url = "{{ url('api/add-to-cart') }}";


        $.ajax({
            url: ajax_url,
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                internal_code: internal_code,
                variant: variant,
                size: size,
                qty: qty,
                _token: '{{ csrf_token() }}',
                reffer: reffer,
            },
            success: function (myresponse) {
                console.log(myresponse);
                alert(myresponse.message);  
            },
            error: function(xhr, status, error) {
                var err = JSON.parse(xhr.responseText);
                console.log(err);
                alert(err.message);
            }
        });
        
    }

    function addToCartReseller(thisid) {
        var reffer = $('input[name=reffer]').val();
        var sequence = $(thisid).closest('tr').attr('data-seq');
        var internal_code = $('tr[data-seq='+sequence+'] input[name=internal_code]').val();
        var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val();
        var size = $('tr[data-seq='+sequence+'] input[name=size]').val();
        var qty = $('tr[data-seq='+sequence+'] input[name=qty]').val();
        {{-- var ajax_url = "{{ url('api/add-to-session-cart') }}"; --}}
        var ajax_url = "{{ url('api/add-to-cartrs') }}";


        $.ajax({
            url: ajax_url,
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                internal_code: internal_code,
                variant: variant,
                size: size,
                qty: qty,
                _token: '{{ csrf_token() }}',
                reffer: reffer,
            },
            success: function (myresponse) {
                console.log(myresponse);
                alert(myresponse.message);  
            },
            error: function(xhr, status, error) {
                var err = JSON.parse(xhr.responseText);
                console.log(err);
                alert(err.message);
            }
        });
        
    }
</script>
@endsection