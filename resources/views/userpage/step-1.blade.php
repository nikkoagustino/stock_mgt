@extends('userpage/template')
@section('content')
<div class="row mt-5">
    <div class="col-12 col-md-4 p-4 border bg-light mx-auto">
        <form action="{{ url('rekap/user') }}" method="post">
            @csrf
            <h4>Masukkan Username Instagram</h4>
            <div class="input-group mb-2">
                <span class="input-group-text" id="basic-addon1">@</span>
                <input type="text" class="form-control" name="instagram" value="{{ (\Session::get('instagram')) ? \Session::get('instagram') : ''; }}" aria-describedby="basic-addon1">
            </div>
            {{-- <h4>Masukkan Tanggal Pembelian</h4> --}}
            <input type="hidden" class="form-control" value="{{ date('Y-m-d') }}" name="tanggal">
            <input type="submit" class="btn btn-warning" value="Cek Data">
        </form>
    </div>
</div>
@endsection