@extends('userpage/template')
@section('meta')
@if (isset($page_title))
    <title>{{ $page_title }} - MomAvel.id</title>
    {{-- <meta name="description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    <meta name="keywords" content="cart">
    <meta name="robots" content="noindex, nofollow">
    <link rel="canonical" href="{{ url()->current() }}">
    <meta property="og:title" content="{{ $page_title }} - MomAvel.id">
    {{-- <meta property="og:description" content="Temukan tren terbaru dari koleksi kami di website fashion terkemuka. Kami menyediakan pakaian, sepatu, dan aksesoris dengan berbagai gaya dan warna yang sesuai dengan selera Anda. Dapatkan penawaran eksklusif dan pengiriman gratis untuk pembelian Anda. Belanja sekarang di situs kami dan jadilah bagian dari gaya fashion yang trendi dan stylish."> --}}
    {{-- <meta property="og:type" content="product"> --}}
    {{-- <meta property="og:url" content="{{ url()->current() }}"> --}}
    {{-- <meta property="og:image" content="{{ url('public').\Storage::url($slides[0]->image_url) }}"> --}}
@else
    <meta name="title" content="MomAvel.id">
    <title>MomAvel.id</title>
@endif
@endsection
@section('content')
<div class="row mt-5">
        <div class="col-12 col-md-5 p-4 border bg-light mx-auto text-center">
            Silahkan login untuk melihat menu My Account
            <hr>
            <form name="myform" action="{{ url('usercart/login') }}" method="post">
                @csrf
                Username / Instagram
                <input type="text" required="required" name="instagram" value ="{{ $instagram }}" class="form-control mb-2">
                Password
                <input type="password" required="required" name="password" value ="{{ $password }}" class="form-control mb-2">
                <button type="submit" id="pencet" name="pencet" class="btn btn-warning"><i class="fas fa-sign-in-alt"></i> &nbsp; Login</button>
                <hr>
                <a href="{{ url('user/register') }}" class="btn btn-sm btn-success"><i class="fas fa-user-plus"></i> &nbsp; Register</a>
                <a href="{{ url('user/forgot-password') }}" class="btn btn-sm btn-danger"><i class="fas fa-user-lock"></i> &nbsp; Lupa Password?</a>
            </form>
        </div>
</div>
@endsection

@section('script')

<script>
    
        $(document).ready(function() {
            //document.getElementByName("pencet").submit();
            document.myform.submit();
        })

</script>
@endsection