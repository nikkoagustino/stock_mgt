<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ Session::token() }}"> 
        <link rel="shortcut icon" href="{{ url('public/favicon.webp') }}">
        <link rel="icon" href="{{ url('public/favicon.webp') }}">
        @if (isset($page_title))
        <meta name="title" content="{{ $page_title }} - MomAvel.id">
        <title>{{ $page_title }} - MomAvel.id</title>
        @else
        <meta name="title" content="MomAvel.id">
        <title>MomAvel.id</title>
        @endif
        
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-V3D7Y00Z19"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-V3D7Y00Z19');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-254771568-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-254771568-1');
</script>


<script src="https://kit.fontawesome.com/b71ce7388c.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <style>
 .tooltip.bs-tooltip-top .tooltip-arrow::before {
      border-top-color: red;
    }

    .tooltip.bs-tooltip-bottom .tooltip-arrow::before {
      border-bottom-color: red;
    }

    .tooltip.bs-tooltip-start .tooltip-arrow::before {
      border-left-color: red;
    }

    .tooltip.bs-tooltip-end .tooltip-arrow::before {
      border-right-color: red;
    }
    .tooltip-inner {
      background: red;
    }
@keyframes lds-spinner {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}
/* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 99999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}


.cont {
  position: relative;
  text-align: center;
  color: white;
}
.centered {
  position: absolute;
  top: 53%;
  left: 50%;
  transform: translate(-50%, -50%);
}


/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
    background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));
  background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 150ms infinite linear;
  -moz-animation: spinner 150ms infinite linear;
  -ms-animation: spinner 150ms infinite linear;
  -o-animation: spinner 150ms infinite linear;
  animation: spinner 150ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}


  .header-1 {
    background: #ff7b7e;
    color: white;
  }
  .phone-gold {
    color: black;
    cursor: pointer;
    background: yellow;
    font-size: 12px;
    display: inline-block;
    padding: 5px 15px;
  }
  .header-2 {
    background: #202020;
  }
  .header-2 img {
    height: 100px;
    padding: 10px;
  }
  .header-2 .form-control {
    margin: 0;
  }
  .header-2 form {
    margin-bottom: 0;
  }
  .header-3 {
    background: #ff7b7e;
  }
  .header-3 a {
    color: black;
    text-decoration: none;
    padding: 5px 15px !important;
  }
  .btn-my-account {
    background: #ff7b7e;
    border: 1px solid black;
    color: black;
    font-size: 16px;
  }
  .btn-my-account i {
    color: black;
  }
  h4 {
    font-size: 18px;
  }
  .bg-light {
    background-color: #f5f1eb !important;
}
.pagination li a {
  background: #ff7b7e !important;
  color: black !important;
}
.footer {
  background: #ff7b7e;
  color: black;
  font-size: 12px;
}
.alert {
    position: fixed;
    top: 50px;
    left: 20vw;
    z-index: 999;
    right: 20vw;
    box-shadow: 0px 0px 20px 10px grey;
}
.nav-tabs .nav-link {
  background: hotpink !important;
  color: white !important;
    border: 1px solid white;
}
.nav-tabs .nav-link.active {
  background: antiquewhite !important;
  color: black !important;
}
.stock-card,
.stock-card th,
.stock-card td {
  font-size: .9rem;
}
.stock-card .btn-warning {
    font-size: .9rem;
    padding: 3px 6px;

}
.dropdown-menu {
  background: antiquewhite;
}
.btn-cart {
  background: #ff7b7e;
  height: 50px;
  position: relative;
  font-size: 26px;
}
.btn-cart .cart-qty {
    position: absolute;
    left: 33px;
    top: 4px;
    background: red;
    padding: 1px 7px;
    font-size: 12px;
    color: white;
    border-radius: 15px;
}
.btn-warning {
  background-color: #ff7b7e;
  color: white;
  border-color: #ff7b7e;
}
.btn-warning:hover {
  background: hotpink;
  color: black;
  border-color: hotpink;
}
.nav-link:hover {
  color: maroon;
}
a {
  text-decoration: none;
}
.nav-pills .nav-link {
  border: 1px solid lightgrey;
  border-radius: 0;
  border-bottom: 0;
}
.nav-pills li:last-child {
  border-bottom: 1px solid lightgrey;
}
.product-img-wrapper {
 	position: relative;
  }
.sale-price {
    position: absolute;
    bottom: 0;
    text-align: center;
    width: 100%;
  background: rgba(0,0,0,0.5);
}
  .sale-price s {
    color: red;
    display: block;
  }
  .sale-price b {
  	color: white;
    font-size: 1.2rem;
    line-height: 10px;
    padding-bottom: 10px;
  }
  /* common */
.ribbon {
  width: 100px;
  height: 100px;
  overflow: hidden;
  position: absolute;
  z-index: 2;
}
.ribbon span {
  position: absolute;
  display: block;
  width: 165px;
  padding: 5px 0;
  font-weight: 700;
  font-size: 1.2rem;
  background-color: red;
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
}
  /* top left*/
.ribbon-top-left {
  top: -3px;
  left: -3px;
}
.ribbon-top-left::before,
.ribbon-top-left::after {
  border-top-color: transparent;
  border-left-color: transparent;
}
.ribbon-top-left::before {
  top: 0;
  right: 0;
}
.ribbon-top-left::after {
  bottom: 0;
  left: 0;
}
.ribbon-top-left span {
    right: -15px;
    top: 15px;
  transform: rotate(-45deg);
}
        </style>
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2JKC0M0XDP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2JKC0M0XDP');
</script>
      
      @yield('pixel')
 
    </head>
    <body>
        

        <div class="container">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                  <strong>{{ $message }}</strong>
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <strong>{{ $message }}</strong>
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif
            @yield('content')
        </div>



        <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-body">
                      <img class="image_modal" width="100%" src="" />
                      <br>
                      <br>
                      <button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal" aria-label="Close">Close</button>
                  </div>
              </div>
          </div>
      </div>
        @yield('script')
        <script>
          $(document).ready(function(){
              updateCartQty();

            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
              return new bootstrap.Tooltip(tooltipTriggerEl);
            });
          });

            $( document ).ajaxStart(function() {
                $( ".loading" ).show();
            });
            $( document ).ajaxStop(function() {
                $( ".loading" ).hide();
            });
            $(document).ready(function(){
                $( ".loading" ).hide();
            });

            function changeSrc(thisid) {
                var src_new = $(thisid).attr('src');
                $('.image_modal').attr("src", src_new);
            }

            function updateCartQty() {
              $.ajax({
                  method: 'get',
                  url: "{{ url('api/cart-qty') }}",
                  cache: false,
                  success: function(result) {
                    $('.cart-qty').html(result);                      
                  }
              });
            }
        </script>
    </body>
</html>