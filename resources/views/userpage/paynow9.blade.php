@extends('userpage/ldtemplate')
@section('pixel')

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '850553866052582');
fbq('track', 'PageView');
fbq('track', 'Purchase', {
    value: {{ $payment->amount }},
    currency: 'IDR'
});
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=850553866052582&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->
@endsection
@section('content')
<div class="row mt-5">
    <div id="myDIV2" class="col-8 col-md-8 p-4 border bg-light mx-auto text-center">
        <img src="{{ url('public/assetsarsha/img/fashion-logo.png') }}" width="90">
        <br><br>
        <H4>
            Total : Rp. {{number_format($payment->amount)}}.00
        </H4>
        <br><br>
        {{-- <form action="{{ url('bayar/qris') }}" method="post"> --}}
            {{-- @csrf --}}
            <input type="hidden" name="yyy" value="{{ $paymentsend }}">
            <img style="display: none; max-width: 100%; max-height: 400px;" src="" id="QRISImage" alt="">
            <button  name ="btnqris" class="btn btn-success" onclick="payWithQRIS();this.disabled=true" id="QRISButton"><i class="fas fa-cash-register"></i> &nbsp; Bayar Pakai QR / Qris</button>
        {{-- </form> --}}
        <br><small><i>Support BCA Mobile, BNI, BRI dan M-Banking Support QR,<br>Termasuk semua E Wallet Yg Support QR, Gopay, Ovo, Dana, ShopeePay, LinkAja, Dll. BCA tinggal Save dan load di QR</i></small>
        <div class="hideFromQRIS">
            
        <form action="{{ url('bayar/cc') }}" method="post">
            @csrf
            <input type="hidden" name="yyy" value="{{ $paymentsend }}">
            </br></br><h4>Atau</h4></br></br><button  name ="btncc" onclick="this.disabled=true;this.form.submit();$('button[name=btnqris]').attr('disabled', 'disabled');" class="btn btn-success"><i class="fas fa-cash-register"></i> &nbsp; Bayar Pakai CC / Transfer Bank</button>
        </form>
        <br><small><i>Support Kartu Kredit Visa, Master Card dan JCB<br>Virtual Account Bank Mandiri, BNI, BRI, Permata<br>Untuk BCA bisa gunakan Other Bank / BNI VA di kenakan biaya 2.500(office hr), 6.500(malam)<br>Pembayaran BCA di rekomendasikan menggunakan QR Qris di atas jk ada mbanking</i></small>
        </div>
    </div>
        
</div>
@endsection
@section('script')
<script>

$(document).ready(function(){
  var x = document.getElementById("myDIV");
  
    x.style.display = "none";
  
})

function myFunction() {
    document.getElementById("myDIV").style.display="block";
    document.getElementById("myDIV2").style.display="none";
}

function payWithQRIS() {
    $('button[name=btncc]').attr('disabled', 'disabled');
    var yyy = $('input[name=yyy]').val();
    $.ajax({
        url: '{{ url("bayar/qris") }}',
        type: 'POST',
        dataType: 'json',
        data: {
            yyy: yyy,
            _token: "{{ csrf_token() }}",
        },
    })
    .always(function(result) {
        var result = JSON.parse(result);
        $('#QRISImage').attr('src', result.actions[0].url).show();
        $('#QRISButton').hide();
        $('.hideFromQRIS').hide();
    });
    
}

</script>


@endsection