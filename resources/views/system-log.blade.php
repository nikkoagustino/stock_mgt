@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>System Log</h1>
    </div>
</div>
<div class="row">
    <div class="col-12">
        @foreach ($system_log as $row)
        {{ $row->time_log }} &nbsp; {{ $row->action }} <br>
        @endforeach
        {{ $system_log->links() }}
    </div>
</div>
@endsection