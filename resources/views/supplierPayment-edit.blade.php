@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Add New Supplier</h1>
    </div>
</div>

<form action="{{ url('supplierPayment/edit/'.$supplier_data->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-2">
            Supplier Code<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code" value="{{ $supplier_data->supplier_code }}" required="required" disabled = "disabled" class="form-control fill_supplier">
                        <option value="{{ $supplier_data->supplier_code }}" disabled="disabled" selected="selected">{{ $supplier_data->supplier_code }}</option>
                        
                    </select>
            </div>
    </div>


    <div class="row">
        <div class="col-2">
            Tanggal <span class="required">*</span>
        </div>
        <div class="col-4">
            <?php date_default_timezone_set("Asia/Jakarta"); ?>
            <input type="datetime-local" value="{{ $supplier_data->tanggal }}" name="tanggal" class="form-control">
        </div>
    </div>
    <div class="row">
        <div class="col-2">
           Nominal Pembayaran <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="total" type="number" value="{{ $supplier_data->total }}" required="required" class="form-control" placeholder="Angka Saja Tanpa Titik dan Koma"></input>
        </div>
    </div>
    

    <div class="row">
        <div class="col-2">
            Total Pcs <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="pcs" type="number" value="{{ $supplier_data->pcs }}" required="required" class="form-control" placeholder="Angka Saja Tanpa Titik dan Koma"></input>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            Detail Produk
        </div>
        <div class="col-4">
            <textarea name="detail" required="required" placeholder="Semua Yang Di bayar" rows="5" class="form-control">{{ $supplier_data->detail }}</textarea>
            </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <input type="submit" value="Save" class="btn btn-success">
        </div>
    </div>
</form>
@endsection