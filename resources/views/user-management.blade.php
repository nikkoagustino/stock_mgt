@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>User Management</h1>
    </div>
</div>
<div class="row">
    <div class="col-12 col-md-4">
        <h3>Tambah User</h3>
        <form action="{{ url('user-management') }}" method="post">
            @csrf
            Username
            <input type="text" name="username" class="form-control" required="required" placeholder="Username">
            Nama Lengkap
            <input type="text" name="fullname" class="form-control" required="required" placeholder="Nama Lengkap">
            Password
            <input type="password" name="password" class="form-control" required="required" placeholder="Password">
            Confirm Password
            <input type="password" name="password_confirm" class="form-control" required="required" placeholder="Password">
            <i style="color:red; display: none" class="password_no_match">Password tidak cocok</i>
            <br>
            Access Level
            <select name="access_level" class="form-control">
                <option value="staff">1 - Staff</option>
                <option value="supervisor">2 - Supervisor</option>
                <option value="superuser">3 - Superuser</option>
            </select>
            <input type="submit" class="btn btn-primary" value="Simpan User" disabled="disabled">
        </form>
    </div>
    <div class="col-12 col-md-8">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Nama Lengkap</th>
                    <th>Access Level</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user as $row)
                <tr>
                    <td>{{ $row->username }}</td>
                    <td>{{ $row->fullname }}</td>
                    <td>{{ $row->access_level }}</td>
                    <td>
                        @if ($row->access_level != 'superuser')
                        <button class="btn btn-danger btn-sm" onclick="deleteUser('{{ $row->username }}')"><i class="fas fa-trash"></i></button>
                        @endif
                    </td>
                </tr>                
               @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script>
    function deleteUser(username) {
        if (confirm("Yakin hapus data?")) {
            window.location.href = '{{ url('user-management/delete') }}/'+username;
        }
    }
    $(document).ready(function(){
        $('input[type=password]').on('change paste keyup', function(){
            if ($('input[name=password]').val() == $('input[name=password_confirm]').val()) {
                $('.password_no_match').hide();
                $('input[type=submit]').removeAttr('disabled');
            } else {
                $('.password_no_match').show();
                $('input[type=submit]').attr('disabled', 'disabled');
            }
        });
    });
</script>
@endsection