@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <table class="table table-api">
            <tr>
                <td width="10%">GET</td>
                <td>api/check-external/{external_code}</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{external_code}</td>
                <td>Product External Code</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Check if product with inputted external code is exist.
                    <br><br>Response:
                    <br>JSON first product data from database.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">GET</td>
                <td>api/product/{internal_code}</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{internal_code}</td>
                <td>Product Internal Code</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Retrieve product data with selected internal code. Be careful of using this since variant, size, and price are not filtered yet.
                    <br><br>Response:
                    <br>JSON list of product data with all variant and size.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">GET</td>
                <td>api/get-variant/{internal_code}</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{internal_code}</td>
                <td>Product Internal Code</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Retrieve all available product variant with selected internal code.
                    <br><br>Response:
                    <br>JSON list of variant.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">GET</td>
                <td>api/get-size/{internal_code}/{variant}</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{internal_code}</td>
                <td>Product Internal Code</td>
            </tr>
            <tr>
                <td>{variant}</td>
                <td>Product Variant</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Retrieve all available product size with selected internal code and variant.
                    <br><br>Response:
                    <br>JSON list of size.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">GET</td>
                <td>api/product/{internal_code}/{variant}/{size}</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{internal_code}</td>
                <td>Product Internal Code</td>
            </tr>
            <tr>
                <td>{variant}</td>
                <td>Product Variant</td>
            </tr>
            <tr>
                <td>{size}</td>
                <td>Product Size</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Retrieve product data with selected internal code, variant, and size.
                    <br><br>Response:
                    <br>JSON single product data.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">GET</td>
                <td>api/product-inline/{internal_code}</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{internal_code}</td>
                <td>Product Internal Code</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Retrieve product data with selected internal code.
                    <br><br>Response:
                    <br>STRING list of product data with all variant and size. To be used in table, first line is header. <br><i style="color:red">Be advised this response is NOT IN STANDARD API response JSON or XML format</i>
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">GET</td>
                <td>api/product-table/{internal_code}</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{internal_code}</td>
                <td>Product Internal Code</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Retrieve product data with selected internal code.
                    <br><br>Response:
                    <br>HTML TABLE of product data.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">GET</td>
                <td>api/generate/{supplier_code}</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{supplier_code}</td>
                <td>Supplier Code</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Retrieve next available internal code from selected supplier code.
                    <br><br>Response:
                    <br>STRING next available internal code generated using auto increment with format {supplier_code}{AI_integer} e.g. : CT3
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">POST</td>
                <td>api/save_product</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{supplier_code}</td>
                <td>Supplier Code</td>
            </tr>
            <tr>
                <td>{external_code}</td>
                <td>Product External Code</td>
            </tr>
            <tr>
                <td>{internal_code}</td>
                <td>Product Internal Code</td>
            </tr>
            <tr>
                <td>{variant}</td>
                <td>Product Variant</td>
            </tr>
            <tr>
                <td>{size}</td>
                <td>Product Size</td>
            </tr>
            <tr>
                <td>{qty_in}</td>
                <td>Input Quantity <i>(will be increment if product exist)</i></td>
            </tr>
            <tr>
                <td>{supplier_price}</td>
                <td>Supplier Price</td>
            </tr>
            <tr>
                <td>{selling_price}</td>
                <td>Selling Price</td>
            </tr>
            <tr>
                <td>{product_photo} <i>(optional)</i></td>
                <td>Image</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br> This function will insert new product if not exists and update product if already exist. To use {product_photo}, form data must be sent as multipart.
                    <br><br>Response:
                    <br>STRING response contains status of action.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">POST</td>
                <td>api/save_master_image</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{external_code}</td>
                <td>Product External Code</td>
            </tr>
            <tr>
                <td>{master_image}</td>
                <td>Image</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Insert or Update product master image.
                    <br><br>Response:
                    <br>URL of saved master image.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">POST</td>
                <td>api/save_selling</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{batch_no}</td>
                <td>Batch Number for Grouping</td>
            </tr>
            <tr>
                <td>{instagram}</td>
                <td>Instagram Username</td>
            </tr>
            <tr>
                <td>{tanggal}</td>
                <td>Date of Input (yyyy-mm-dd)</td>
            </tr>
            <tr>
                <td>{internal_code}</td>
                <td>Product Internal Code</td>
            </tr>
            <tr>
                <td>{variant}</td>
                <td>Product Variant</td>
            </tr>
            <tr>
                <td>{size}</td>
                <td>Product Size</td>
            </tr>
            <tr>
                <td>{qty}</td>
                <td>Quantity Order</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Insert new order
                    <br><br>Response:
                    <br>STRING status of action.
                </td>
            </tr>
        </table>
        <table class="table table-api">
            <tr>
                <td width="10%">POST</td>
                <td>api/update-order</td>
            </tr>
            <tr>
                <td colspan="2">
                    Input Parameter:
                </td>
            </tr>
            <tr>
                <td>{id}</td>
                <td>Order ID</td>
            </tr>
            <tr>
                <td>{variant}</td>
                <td>Product Variant</td>
            </tr>
            <tr>
                <td>{size}</td>
                <td>Product Size</td>
            </tr>
            <tr>
                <td>{qty}</td>
                <td>Quantity Order</td>
            </tr>
            <tr>
                <td colspan="2">
                    Function:
                    <br>Update current order. Can be used to change variant, size, or qty.
                    <br><br>Response:
                    <br>STRING status of action.
                </td>
            </tr>
        </table>
    </div>
</div>
@endsection