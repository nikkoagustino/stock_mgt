@extends('supplier/template')
@section('content')

<div class="p-5">
    <div class="col-11">
        <h1>List Pembayaran</h1>
        
    </div>
</div>
<div class="p-5">
    <div class="col-11">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Supplier</th>
                    <th>Nominal</th>
                    <th>Total Pcs</th>
                    <th>Detail</th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach($supplier as $row)
                    @if($row->supplier_code == \Session::get('supplier1') || $row->supplier_code == \Session::get('supplier2') || $row->supplier_code == \Session::get('supplier3') || $row->supplier_code == \Session::get('supplier4') || $row->supplier_code == \Session::get('supplier5') || $row->supplier_code == \Session::get('supplier6'))
                        <tr>
                            <td>{{ $row->tanggal }}</td>
                            <td>{{ $row->supplier_code }}</td>
                            <td>{{ number_format($row->total) }}</td>
                            <td>{{ number_format($row->pcs) }}</td>
                            <td>{!! nl2br($row->detail) !!}</td>

                            
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
<script>

function deleteOrder(id) {
    if (confirm("Yakin Hapus Data?")) {
        window.location.href = "{{ url('supplierPayment/delete/') }}/"+id;
    }
}

</script>
@endsection