@extends('supplier/template')
@section('content')

<form action="{{ url('supps/selectsales') }}" method="post">
@csrf
<div class="row p-5">
    <div >
        <div  >
            <h3>Daftar Penjualan Supplier</h3>
            <?php date_default_timezone_set('Asia/Jakarta');?>
            List Kode : {{ '(' . \Session::get('supplier1') . ' ' . \Session::get('supplier2'). ' ' . \Session::get('supplier3'). ' ' . \Session::get('supplier4'). ' ' . \Session::get('supplier5'). ' ' . \Session::get('supplier6') . ') '}}
        </div>
    </div>
    <?php $ttlstok=0;
          $ttlvaluasi=0;?>
        <div class="col-12">
        <br>
        <br>
        Dari Tanggal         :
        <br>
        <input type="date" value="{{date('Y-m-d')}}" name="tanggaldr" class="form-control">
          <b class="fs-4"></b>
          <br>
        Sampai Tanggal : 
        <br>
        <input type="date" value="{{date('Y-m-d')}}" name="tanggalsmp" class="form-control">
          <b class="fs-4"></b>
        <br>
            
        <button type="submit" id="submitButton" class="form-control btn btn-success">
            &nbsp; Buka Data
        </button>
        </div>
    
</div>       
</form>
@endsection
@section('script')
<script>

    $(document).ready(function(){
        $('input[name=totitem]').val($ttlstok);
        $('input[name=totvaluasi]').val($ttlvaluasi);
    });
</script>
@endsection