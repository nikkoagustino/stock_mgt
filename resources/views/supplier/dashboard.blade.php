@extends('supplier/template')
@section('content')

                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                        
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-area me-1"></i>
                                        Grafik Penjualan
                                    </div>
                                    <div class="card-body"><canvas id="sellingChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar me-1"></i>
                                        Penjualan Hari Ini
                                    </div>
                                    <div class="card-body">
                                        Total Item         :
                                        <br>
                                        <b class="fs-4">{{ number_format($valuasi->total_item_sold, 0) }}</b>
                                        <br>
                                        Valuasi Semua Item : 
                                        <br>
                                        <b class="fs-4">{{ number_format($valuasi->total_valuasi_sold, 0) }}</b>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
@endsection
@section('script')
<script>
var ctx = document.getElementById("sellingChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    // labels: ["Mar 1", "Mar 2", "Mar 3", "Mar 4", "Mar 5", "Mar 6", "Mar 7", "Mar 8", "Mar 9", "Mar 10", "Mar 11", "Mar 12", "Mar 13"],
    labels: [{!! $chart->tanggal !!}],
    datasets: [{
      label: "Qty Sold",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0.2)",
      borderColor: "rgba(2,117,216,1)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(2,117,216,1)",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(2,117,216,1)",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: [{{ $chart->total_jual }}],
      // data: [10000, 30162, 26263, 18394, 18287, 28682, 31274, 33259, 25849, 24159, 32651, 31984, 38451],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 30
        }
      }],
      yAxes: [{
        ticks: {
            min: 0,
          maxTicksLimit: 10
        },
        gridLines: {
          color: "rgba(0, 0, 0, .125)",
        }
      }],
    },
    legend: {
      display: false
    }
  }
});

</script>
@endsection