<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - Momavel Supplier</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="{{ url('public/assets/css/styles.css') }}" rel="stylesheet" />
        <link href="https://code.jquery.com/jquery-3.5.1.js">
        <link href="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js">
        <link href="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js">
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/b71ce7388c.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
        <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="dashboard">MomAvel.id Supplier</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            
            
            <!-- Navbar-->
          
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            




                            <div class="sb-sidenav-menu-heading">Home</div>
                            <a class="nav-link" href="dashboard">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>


                            <div class="sb-sidenav-menu-heading">Penjualan</div>
                            
                            <a class="nav-link" href="selectsales">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Penjualan Tanggal
                            </a>


                            <div class="sb-sidenav-menu-heading">Stok</div>
                            <a class="nav-link" href="rekapstoktotal">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Rekap Stok Total
                            </a>
                            <a class="nav-link" href="stockhariini">
                                <div class="sb-nav-link-icon"><i class="fas fa-angle-up"></i></div>
                                Stok Saat Ini
                            </a>
                            
                            <div class="sb-sidenav-menu-heading">Pembayaran</div>
                            <a class="nav-link" href="payment">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                List Pembayaran
                            </a>

                            <div class="sb-sidenav-menu-heading">Stok</div>
                            <a class="nav-link" href="logout">
                                <div class="sb-nav-link-icon"><i class="fas fa-angle-down"></i></div>
                                Logout
                            </a>

                            
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as: {{ \Session::get('supplierlogin') }}</div>
                        MomAvel.id Supplier
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    @yield('content')
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; MomAvel.id 2022</div>
                            <div>
                                
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script type="text/javascript">

        $(document).ready(function(){
            $('.datatable').dataTable({
              pageLength: 25,
            });
            $('.datatable-sm').dataTable({
              pageLength: 5,
            });
            
            $('#example').DataTable();

        });
        </script>
        

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ url('public/assets/js/scripts.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="{{ url('public/assets/demo/chart-area-demo.js') }}"></script>
        <script src="{{ url('public/assets/demo/chart-bar-demo.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="{{ url('public/assets/js/datatables-simple-demo.js') }}"></script>
              
        @yield('script')
    </body>
</html>
