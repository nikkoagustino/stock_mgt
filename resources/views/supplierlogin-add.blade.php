@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Add New Supplier Login</h1>
    </div>
</div>

<form action="{{ url('supplierlogin/add') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-2">
            Akses Supplier MomAvel <br> https://momavel.id/supps/login
        </div>
        
    </div>
    <br><br>
    <div class="row">
        <div class="col-2">
            Supplier Login <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="username" type="text" required="required" class="form-control" placeholder="username"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Nama <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="nama" type="text" required="required" class="form-control" placeholder="nama"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Password <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="password" type="text" required="required" class="form-control" placeholder="password"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Access 1<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code1" required="required" class="form-control fill_supplier">
                        <option value="" disabled="disabled" selected="selected"></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>

    <div class="row">
        <div class="col-2">
            Supplier Access 2
        </div>
        <div class="col-4">
                    <select name="supplier_code2" class="form-control fill_supplier">
                        <option value="" selected="selected"></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Access 3
        </div>
        <div class="col-4">
                    <select name="supplier_code3" class="form-control fill_supplier">
                        <option value="" selected="selected"></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>

    <div class="row">
        <div class="col-2">
            Supplier Access 4
        </div>
        <div class="col-4">
                    <select name="supplier_code4" class="form-control fill_supplier">
                        <option value="" selected="selected"></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>

    <div class="row">
        <div class="col-2">
            Supplier Access 5
        </div>
        <div class="col-4">
                    <select name="supplier_code5" class="form-control fill_supplier">
                        <option value="" selected="selected"></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>

    <div class="row">
        <div class="col-2">
            Supplier Access 6
        </div>
        <div class="col-4">
                    <select name="supplier_code6" class="form-control fill_supplier">
                        <option value="" selected="selected"></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>

    <div class="row">
        <div class="col-2">
            No Rek
        </div>
        <div class="col-4">
            <input name="demodemode" type="text" class="form-control" value="" placeholder="NoRek"></input>
        </div>
    </div>
    
    <div class="row">
        <div class="col-2">
            <input type="submit" value="Save" class="form-control btn btn-success">
        </div>
    </div>
</form>
@endsection