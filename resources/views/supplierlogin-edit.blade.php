@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Add New Supplier</h1>
    </div>
</div>

<form action="{{ url('supplierlogin/edit/'.$supplier_data->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-2">
            Akses Supplier MomAvel <br> https://momavel.id/supps/login
        </div>
        
    </div>
    <br><br>
    <div class="row">
        <div class="col-2">
            Username
        </div>
        <div class="col-4">
            <input name="username" type="text" readonly="readonly" class="form-control" value="{{ $supplier_data->username }}" placeholder="Code"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Nama
        </div>
        <div class="col-4">
            <input name="nama" type="text" class="form-control" value="{{ $supplier_data->nama }}" placeholder="nama"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Password <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="password" type="text" required="required" class="form-control" value="{{ $supplier_data->password}}" placeholder="Name"></input>
        </div>
    </div>

    <div class="row">
        <div class="col-2">
            Supplier Access 1<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code1" required="required" class="form-control fill_supplier">
                        <option value="{{ $supplier_data->supplier1 }}" selected="selected">{{ $supplier_data->supplier1 }}</option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Access 2<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code2" class="form-control fill_supplier">
                        <option value="{{ $supplier_data->supplier2 }}" selected="selected">{{ $supplier_data->supplier2 }}</option>
                        <option value=""></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Access 3<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code3" class="form-control fill_supplier">
                        <option value="{{ $supplier_data->supplier3 }}" selected="selected">{{ $supplier_data->supplier3 }}</option>
                        <option value=""></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Access 4<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code4" class="form-control fill_supplier">
                        <option value="{{ $supplier_data->supplier4 }}" selected="selected">{{ $supplier_data->supplier4 }}</option>
                        <option value=""></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Access 5<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code5" class="form-control fill_supplier">
                        <option value="{{ $supplier_data->supplier5 }}" selected="selected">{{ $supplier_data->supplier5 }}</option>
                        <option value=""></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Access 6<span class="required">*
        </div>
        <div class="col-4">
                    <select name="supplier_code6" class="form-control fill_supplier">
                        <option value="{{ $supplier_data->supplier6 }}" selected="selected">{{ $supplier_data->supplier6 }}</option>
                        <option value=""></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
            </div>
    </div>
    <div class="row">
        <div class="col-2">
            No Rek<span class="required">*
        </div>

        <div class="col-4">
            <input name="demodemode" type="text" class="form-control" value="{{ $supplier_data->demodemode }}" placeholder="NoRek"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <input type="submit" value="Save" class="btn btn-success">
        </div>
    </div>
</form>
@endsection