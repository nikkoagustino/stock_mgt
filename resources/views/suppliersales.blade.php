@extends('template')
@section('content')
<form action="{{ url('suppliersales') }}" method="post">
@csrf
<div class="row">
    <div class="col-14">
        <h1>Supplier Sales</h1>
        <div>
        Pembayaran Terakhir : {{ $pembayaran->tanggalakhir ??0 }}<br>
        <input type="hidden" value="{{ $kode }}" name="koder">
        Generate Dari Tanggal         :
        <input type="date" value="{{date('Y-m-d')}}" name="tanggaldr" class="form-control">
          <b class="fs-4"></b>
        Sampai Tanggal : 

        <input type="date" value="{{date('Y-m-d')}}" name="tanggalsmp" class="form-control">
          <b class="fs-4"></b>
        
            
        <button type="submit" id="submitButton" class="form-control btn btn-success">
            &nbsp; Buka Data
        </button>
        </div>
    </div>
</div>
</form>
<div class="row">
    <div class="col-14">
        <table class="table datatable">
            <thead>
                <tr>
                    
                    <th>Kode</th>
                    <th>Foto</th>
                    <th>Variant</th>
                    <th>Size</th>
                    <th>Harga</th>
                    <th>Quantity</th>
                    
                    

                </tr>
            </thead>
            <tbody>
                @foreach($sales as $row)
                <tr>
                    
                    <td>{{ $row->internal_code }}</td>
                    @if (!empty($row->master_image))
                            <td><center><a href="{{ url('public').\Storage::url( $row->master_image) }}" target="_blank"><img style="height: 70px;width:auto !important" src="{{ url('public').\Storage::url( $row->master_image) }}" class="d-block w-100" alt="..."></a></center></td>
                        @else

                            <td><center><a href="{{ url('public').\Storage::url( 'no_image.png') }}" target="_blank"><img style="height: 70px;width:auto !important" src="{{ url('public').\Storage::url( 'no_image.png') }}" class="d-block w-100" alt="..."></a></center></td>
                           
                        @endif
                        
                    
                    <td>{{ $row->variant }}</td>
                    <td>{{ $row->size }}</td>
                    <td>{{ $row->supplier_price }}</td>
                    <td>{{ $row->qty }}</td>

                    
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
<script>

function deleteOrder(id) {
    if (confirm("Yakin Hapus Data?")) {
        window.location.href = "{{ url('supplierlogin/delete/') }}/"+id;
    }
}

</script>
@endsection