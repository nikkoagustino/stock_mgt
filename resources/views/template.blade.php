<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
        <meta name="author" content="AdminKit">
        <meta name="keywords" content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">
        <meta name="csrf-token" content="{{ Session::token() }}"> 
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="shortcut icon" href="{{ url('public/favicon.webp') }}">
        <link rel="icon" href="{{ url('public/favicon.webp') }}">
        <title>Momavel Admin</title>
        <link href="{{ url('public/assets/css/app.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/b71ce7388c.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
        <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
        @yield('head')
		<style type="text/css">
          @media print{
            @page {
              size: landscape;
              margin: 0;
            }
            body {
              margin: 1cm;
            }
            .no-print, .no-print *, .noprint, .noprint *
            {
                display: none !important;
            }
          }
          .sidebar-header {
            background: #222e3c;
          }
          .content {
            max-width: 100%;
            width: 100%;
          }
          img {
            max-width: 100%;
          }
    .thumbnail {
        width: 45px;
        height: 25px;
        display: inline-block;
    }
    .thumbnail img {
        height: 100%;
        border: 1px solid #ddd;
        text-align: center;
        cursor: pointer;
    }
          .table-scroll-x {
            overflow-x: auto;
            width: 100%;
          }
          body {
            font-size: 12px;
          }
          .sidebar {
            max-width: 220px;
            min-width: 220px;
            font-size: 14px !important;
          }
          .form-control {
            padding: 0.2rem .5rem;
            font-size: inherit;
            margin-bottom: 10px;
          }
          .form-control-normal {
            padding: 0.375rem 2.25rem 0.375rem 0.75rem;
            -moz-padding-start: calc(0.75rem - 3px);
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
          }
          table {
    padding: .2rem .2rem;
  }
  .btn-sm {
    padding: 0.1rem .4em;
    font-size: 12px;
}
  }
            .alert {
                position: fixed;
                z-index: 999;
                top: 20px;
                left: 40vw;
            }
            .required {
                color: red;
            }
            .coming-soon h1 {
                text-align: center;
                vertical-align: middle;
                line-height: 100vh;
            }
            .table-api td {
                border: 1px solid #ddd;
            }

            .table-api {
                border: 5px solid lightblue;
            }
            .table-api tr:first-child td {
                background: lightblue;
                font-weight: bold;
                font-size: 18px;
            }

            .table-api tr:last-child td {
                border-top: 3px solid lightblue;
            }
            .buttons-print {
              background: green;
              color: white;
              border-radius: 7px;
              border: 1px solid green;
              padding: 5px 10px;
              font-size: 20px;
              margin-bottom: 5px;
              top: 78px;
              position: absolute;
              right: 50px;
            }
            @media (min-width: 1px) and (max-width: 991.98px) {
                .sidebar {
                     margin-left: 0 !important; 
                }
                .buttons-print {
                  top: 30px;
                }
            }
            a {
                text-decoration: none;
            }
            .uppercase {
                text-transform: uppercase;
            }
            .nowrap {
              white-space: nowrap;
            }

@keyframes lds-spinner {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}
/* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
    background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

  background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 150ms infinite linear;
  -moz-animation: spinner 150ms infinite linear;
  -ms-animation: spinner 150ms infinite linear;
  -o-animation: spinner 150ms infinite linear;
  animation: spinner 150ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}


    .bs-canvas-overlay {
   opacity: 0;
   z-index: -1;
}

.bs-canvas-overlay.show {
   opacity: 0.85;
   z-index: 1100;
}

.bs-canvas {
   top: 0;
   width: 0;
   z-index: 1110;
   overflow-x: hidden;
   overflow-y: auto;
}

.bs-canvas-left {
   left: 0;
}

.bs-canvas-right {
   right: 0;
}
/* Hide scrollbar for Chrome, Safari and Opera */
.bs-canvas-left::-webkit-scrollbar {
  display: none;
}

/* Hide scrollbar for IE, Edge and Firefox */
.bs-canvas-left {
  -ms-overflow-style: none;  /* IE and Edge */
  scrollbar-width: none;  /* Firefox */
}
.bs-canvas-anim {
   transition: all .4s ease-out;
   -webkit-transition: all .4s ease-out;
   -moz-transition: all .4s ease-out;
   -ms-transition: all .4s ease-out;
}
button.close {
    padding: 0;
    background-color: transparent;
    border: 2px solid white;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    font-weight: bold;
    font-size: 25px;
    color: white;
    margin-right: 16px;
    line-height: 26px;
    vertical-align: middle;
    height: 30px;
    width: 30px;
}
#btn-right-menu {
    position: fixed;
    top: 40%;
    right : -45px;
   transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -webkit-transform: rotate(-90deg);
    -o-transform: rotate(-90deg);
    z-index: 999;
}
#btn-left-menu {
    position: fixed;
    top: 40%;
    left: -30px;
   transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -webkit-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    z-index: 999;
}
.sidebar-nav .fas {
  width: 30px;
  text-align: center;
  margin: 0 0 0 -10px;
}
.btn-purple {
  background: purple;
  color: white;
}
.hit-and-run {
    border: 1px solid purple;
    color: purple;
    padding: 3px 5px;
    font-size: 10px;
    border-radius: 5px;
    margin-left: 5px;
}
span.counter {
    background: red;
    padding: 0px 3px;
    border-radius: 4px;
    position: absolute;
    right: 15px;
    bottom: 8px;
    color: white !important;
  }
        </style>
    </head>
    <body>
    <div class="bs-canvas-overlay bs-canvas-anim bg-dark position-fixed w-100 h-100"></div>    

    <div class="bs-offset-main bs-canvas-anim noprint" id="btn-left-menu">
        <button class="btn btn-danger" type="button" data-toggle="canvas" data-target="#bs-canvas-left" aria-expanded="false" aria-controls="bs-canvas-left">&#9776; Menu</button> 
    </div>
      @yield('off-canvas')

    
        
    <div id="bs-canvas-left" class="bs-canvas bs-canvas-anim bs-canvas-left position-fixed bg-light h-100" data-width="220px">
        <header class="bs-canvas-header p-3 bg-danger overflow-auto">
            <button type="button" class="bs-canvas-close float-left close" aria-label="Close"><i class="fas fa-times"></i></button>
            <h4 class="d-inline-block text-light mb-0 float-left">Menu</h4>
        </header>
        <div class="bs-canvas-content">
          
                    <a href="{{ url('dashboard') }}">
                        <img src="{{ url('public/logo_long.jpg') }}" alt="">
                    </a>
                    <ul class="sidebar-nav">
                        <li class="sidebar-header">
                            Fungsi Utama
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('dashboard') }}">
                                <i class="fas fa-chart-line"></i> Dashboard
                            </a>
                        </li>
                        
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('selling/manual') }}">
                                <i class="fas fa-cart-plus"></i> Penjualan (Manual)
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-in') }}">
                                <i class="fas fa-cubes"></i> Stok Masuk
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-kodeselect') }}">
                                <i class="fas fa-store"></i> Edit Stok By Code
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-report') }}">
                                <i class="fas fa-store"></i> Lap Stok
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('selectbysupp') }}">
                                <i class="fas fa-store"></i> Lap Stok By Supplier
                            </a>
                        </li>
                        
                        

                        
                        
                        

                        
                        <li class="sidebar-header">
                            List Pesanan
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('list-order/new') }}">
                                <i class="fas fa-clipboard-check"></i> Terdaftar
                                @if ($total_order_new > 0)
                                <span class="counter">{{ $total_order_new }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('list-order/newreseller') }}">
                                <i class="fas fa-clipboard-check"></i> Terdaftar Reseller
                                @if ($total_order_newreseller > 0)
                                <span class="counter">{{ $total_order_newreseller }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('hit-and-run') }}">
                                <i class="fas fa-flag"></i> Hit and Run
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('list-order/pending') }}">
                                <i class="fas fa-hourglass-half"></i> Pending Bayar
                                @if ($total_order_pending > 0)
                                <span class="counter">{{ $total_order_pending }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('list-order/paid') }}">
                                <i class="fas fa-dollar-sign"></i> Sudah Dibayar
                                @if ($total_order_paid > 0)
                                <span class="counter">{{ $total_order_paid }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('list-order/delivered') }}">
                                <i class="fas fa-shipping-fast"></i> Selesai Terkirim
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('refund/list') }}">
                                <i class="fas fa-history"></i> Masalah / Refund
                                @if ($total_order_refund > 0)
                                <span class="counter">{{ $total_order_refund }}</span>
                                @endif
                            </a>
                        </li>

                        <li class="sidebar-header">
                            Landing Page
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-reportld') }}">
                                <i class="fas fa-store"></i> List Produk
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('ld') }}">
                                <i class="fas fa-store"></i> Landing Page Trial
                            </a>
                        </li>



                        <li class="sidebar-header">
                            Customer
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('customer-list') }}">
                                <i class="fas fa-layer-group"></i> Customer List
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('customerselect') }}">
                                <i class="fas fa-layer-group"></i> Message Blast
                            </a>
                        </li>

                        <li class="sidebar-header">
                            Reseller
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('reseller-list') }}">
                                <i class="fas fa-layer-group"></i> Reseller List
                            </a>
                        </li>

                        
                        <li class="sidebar-header">
                            Stock Report(Awas Ngelag)
                        </li>
                        
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-reportav') }}">
                                <i class="fas fa-store"></i> Stok Ready
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-reportemp') }}">
                                <i class="fas fa-store"></i> Stok Kosong
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-photo-variant') }}">
                                <i class="fas fa-search-minus"></i> Stok Tidak Ada Foto
                            </a>
                        </li>

                        @if ((\Session::get('access_level') == 'superuser') || (\Session::get('access_level') == 'supervisor'))

                        <li class="sidebar-header">
                            Supplier Data
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('supplierbayar') }}">
                                <i class="fas fa-file-invoice-dollar"></i> Harus Di Bayar
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('supplierPayment') }}">
                                <i class="fas fa-file-invoice-dollar"></i> Input Pembayaran Supplier
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('laporan-penjualan') }}">
                                <i class="fas fa-layer-group"></i> Laporan Penjualan (Supplier)
                            </a>
                        </li>
                        
                        
                        <li class="sidebar-header">
                            Supplier Access
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('supplier') }}">
                                <i class="fas fa-warehouse"></i> Data Supplier
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('supplierlogin') }}">
                                <i class="fas fa-warehouse"></i> Info Login Supplier
                            </a>
                        </li>



                        

                        <li class="sidebar-header">
                            Basis Data
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('category') }}">
                                <i class="fas fa-boxes"></i> Kategori
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('discount') }}">
                                <i class="fas fa-percent"></i> Discount & Promotion
                            </a>
                        </li>

                        <li class="sidebar-header">
                            Pelaporan
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('selling-report') }}">
                                <i class="fas fa-layer-group"></i> Selling Order
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('payment-report') }}">
                                <i class="fas fa-file-invoice-dollar"></i> Laporan Pembayaran
                            </a>
                        </li>
                        
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('system-log') }}">
                                <i class="fas fa-list-ul"></i> Log Sistem
                            </a>
                        </li>

                        <li class="sidebar-header">
                            Branded
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-report-branded-instock') }}">
                                <i class="fas fa-store"></i> Produk Branded
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-report-nonbranded') }}">
                                <i class="fas fa-store"></i> Produk Non Branded
                            </a>
                        </li>


                        
                        @if ((\Session::get('access_level') == 'superuser') || (\Session::get('access_level') == 'supervisor'))
                        <li class="sidebar-header">
                            Tidak Di Gunakan
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-check/all') }}">
                                <i class="fas fa-search"></i> Cek Stok
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('selling') }}">
                                <i class="fas fa-dolly"></i> Penjualan (Import File)
                            </a>
                        </li>
                        @endif
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('stock-check/minus') }}">
                                <i class="fas fa-search-minus"></i> Cek Kekurangan Stok
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('user-management') }}">
                                <i class="fas fa-users"></i> Manajemen Pengguna
                            </a>
                        </li>
                        @endif
                        @if (\Session::get('access_level') == 'superuser')
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('api/documentation') }}">
                                <i class="fas fa-plug"></i> Dokumentasi API
                            </a>
                        </li>
                        @endif

                        <li class="sidebar-header">
                            User
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{ url('logout') }}">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        </li>

                    </ul>
        </div>    
    </div>
        <div class="loading">
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
          <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
          </symbol>
          <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
          </symbol>
          <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
          </symbol>
        </svg>
        <div class="wrapper">
            <div class="main">
                <main class="content">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                          <strong>{{ $message }}</strong>
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                      </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                          <strong>{{ $message }}</strong>
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                      </div>
                    @endif
                    @yield('content')
                </main>
            </div>
        </div>
        <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-body">
                      <img class="image_modal" width="100%" src="" />
                  </div>
              </div>
          </div>
      </div>
        
        <script type="text/javascript">

    jQuery(document).ready(function($) {
   var bsDefaults = {
         offset: false,
         overlay: true,
         width: '330px'
      },
      bsMain = $('.bs-offset-main'),
      bsOverlay = $('.bs-canvas-overlay');

   $('[data-toggle="canvas"][aria-expanded="false"]').on('click', function() {
      var canvas = $(this).data('target'),
         opts = $.extend({}, bsDefaults, $(canvas).data()),
         prop = $(canvas).hasClass('bs-canvas-right') ? 'margin-right' : 'margin-left';

      if (opts.width === '100%')
         opts.offset = false;
      
      $(canvas).css('width', opts.width);
      if (opts.offset && bsMain.length)
         bsMain.css(prop, opts.width);

      $(canvas + ' .bs-canvas-close').attr('aria-expanded', "true");
      $('[data-toggle="canvas"][data-target="' + canvas + '"]').attr('aria-expanded', "true");
      if (opts.overlay && bsOverlay.length)
         bsOverlay.addClass('show');
      return false;
   });

   $('.bs-canvas-close, .bs-canvas-overlay').on('click', function() {
      var canvas, aria;
      if ($(this).hasClass('bs-canvas-close')) {
         canvas = $(this).closest('.bs-canvas');
         aria = $(this).add($('[data-toggle="canvas"][data-target="#' + canvas.attr('id') + '"]'));
         if (bsMain.length)
            bsMain.css(($(canvas).hasClass('bs-canvas-right') ? 'margin-right' : 'margin-left'), '');
      } else {
         canvas = $('.bs-canvas');
         aria = $('.bs-canvas-close, [data-toggle="canvas"]');
         if (bsMain.length)
            bsMain.css({
               'margin-left': '',
               'margin-right': ''
            });
      }
      canvas.css('width', '');
      aria.attr('aria-expanded', "false");
      if (bsOverlay.length)
         bsOverlay.removeClass('show');
      return false;
   });
});


            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
              return new bootstrap.Tooltip(tooltipTriggerEl);
            });
            $( document ).ajaxStart(function() {
                $( ".loading" ).show();
            });
            $( document ).ajaxStop(function() {
                $( ".loading" ).hide();
            });
            $(document).ready(function(){
                $( ".loading" ).hide();
                $('.datatable').dataTable({
                  pageLength: 25,
                });
                $('.datatable-sm').dataTable({
                  pageLength: 5,
                });
            });

      function sizeValidation(size) {
        if (size == '?') { return true; }
        if (size == 'NOSIZE') { return true; }
        if (size == 'ALLSIZE') { return true; }
        if (size == '3XS') { return true; }
        if (size == '2XS') { return true; }
        if (size == 'XS') { return true; }
        if (size == 'S') { return true; }
        if (size == 'M') { return true; }
        if (size == 'L') { return true; }
        if (size == 'XL') { return true; }
        if (size == '2XL') { return true; }
        if (size == '3XL') { return true; }
        if (size == '4XL') { return true; }
        if (size == '5XL') { return true; }
        if (size == '6XL') { return true; }
        if ((size >= 25) && (size <= 45)) { return true; }
        return false;
      }
        </script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
		@yield('script')
    </body>
</html>