@extends('template')
@section('content')

<div class="row p-5">
    <div >
        <div  >
            <h3>Daftar Penjualan Supplier <a href="{{ url('supplierPayment/add') }}" class="btn btn-primary">+ Add New Supplier Payment</a>&nbsp;&nbsp;<a onclick="PrintNow()" class="btn btn-primary">Print Laporan</a></h3>
            Pembayaran Terakhir : {{ $pembayaran->tanggalakhir ??0 }}<br>
            <?php date_default_timezone_set('Asia/Jakarta');?>
            List Kode : {{ $kode }}
            <br>{{ $periode }}
        </div>
    </div>
        <div class="col-12">
        <table>
            <tr>
                <td>
                    Total Item         :
                    <br>
                    {{-- <input type="text" readonly="readonly" value="{{ $valuasi->total_item_sold }}" class="" name="totitem"> --}}
                    <b class="fs-4">{{ number_format($valuasi->total_item_sold, 0) }}</b>
                    <br>
                    Valuasi Semua Item : 
                    <br>
                    {{-- <input type="text" readonly="readonly" value="{{ $valuasi->total_valuasi_sold }}" class="" name="totval"> --}}
                    <b class="fs-4">{{ number_format($valuasi->total_valuasi_sold, 0) }}</b>
                    <br>
                </td>
                <td width="100">
                    
                </td>
                
                <td>
                    @foreach ($salesbyprice as $row)
                        {{ number_format($row->supplier_price) }} x {{ number_format($row->qty_sold) }} = {{ number_format($row->total_sold) }}<br>
                    @endforeach
                </td>
                <td width="100">
                    
                </td>
                <td>
                    No Rek: {{ $norek->demodemode }}
                </td>
            </tr>
        </table>



            
            <table id="example" class="table table-striped center table-bordered" style="text-align:center;font-size:11px;font-family:arial" border="1" cellspacing="0" >
                <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Photo</th>
                        
                        <th>Nama Barang</th>
                        <th>Variant</th>
                        <th>Size</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Sales</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sales as $row)
                    <tr>
                        <td>{{ $row->internal_code }}</td>
                        @if (!empty($row->images))
                            <td><center><a href="{{ url('public').\Storage::url( $row->images) }}" target="_blank"><img style="height: 70px;width:auto !important" src="{{ url('public').\Storage::url( $row->images) }}" class="d-block w-100" alt="..."></a></center></td>
                        @else
                            @if (!empty($row->master_image))
                                <td><center><a href="{{ url('public').\Storage::url( $row->master_image) }}" target="_blank"><img style="height: 70px;width:auto !important" src="{{ url('public').\Storage::url( $row->master_image) }}" class="d-block w-100" alt="..."></a></center></td>
                            @else
                            <td><center><a href="{{ url('public').\Storage::url( 'no_image.png') }}" target="_blank"><img style="height: 70px;width:auto !important" src="{{ url('public').\Storage::url( 'no_image.png') }}" class="d-block w-100" alt="..."></a></center></td>
                            @endif
                        @endif
                        
                        
                        <td>{{ $row->product_name }}</td>
                        <td>{{ $row->variant }}</td>
                        <td>{{ $row->size }}</td>
                        
                        <td>{{ $row->qty }}</td>
                        <td>{{ number_format($row->supplier_price) }}</td>
                        <td>{{ number_format($row->supplier_price * $row->qty) }}</td>
                        <td>{{ $row->order_date }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    
</div>       
@endsection
@section('script')


<script type="text/javascript">
function PrintNow() {
window.print();
}
</script>
@endsection