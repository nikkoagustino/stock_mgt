@extends('template')
@section('content')
<div class="container-fluid p-0">
    <h1 class="h3 mb-3"><strong>Analytics</strong> Dashboard</h1>
    <div class="row">
        <div class="col-md-4">
            <div class="w-100">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col mt-0">
                                        <h5 class="card-title">Order</h5>
                                    </div>
                                    <div class="col-auto">
                                        <div class="stat text-primary">
                                            <i class="align-middle" data-feather="shopping-cart"></i>
                                        </div>
                                    </div>
                                </div>
                                <h6><a href="{{ url('list-order/new') }}">Ada <span class="text-success">{{ $total_order_new }}</span> order baru yang belum dikonfirmasi</a></h6>
                                <h6><a href="{{ url('list-order/pending') }}">Ada <span class="text-success">{{ $total_order_pending}}</span> pending order yang siap dibayar</a></h6>
                                <h6><a href="{{ url('list-order/paid') }}">Ada <span class="text-success">{{ $total_order_paid }}</span> order yang siap dikirim</a></h6>
                                <h6><a href="{{ url('list-order/delivered') }}">Ada <span class="text-success">{{ $total_order_delivered }}</span> order yang sudah selesai</a></h6>
                                <h6><a href="{{ url('refund/list') }}">Ada <span class="text-success">{{ $total_order_refund }}</span> order yang sedang proses retur / refund</a></h6>
                                <br>
                                <table>
                                    <tr>
                                        <td>
                                            <br>Traffic Report Tanggal 
                                            <?php date_default_timezone_set('Asia/Jakarta'); ?>
                                            <br>{{ date('Y-m-d') }}

                                            @if ($webvisitor==null)
                                                <br>Normal Traffic : <span class="text-success">0</span> 
                                                <br>Ads Traffic : <span class="text-success">0</span> 
                                            @else
                                                <br>Normal Traffic : <span class="text-success">{{ $webvisitor->webvisitor }}</span> 
                                                <br>Ads Traffic : <span class="text-success">{{ $webvisitor->adsvisitor }}</span> 
                                                <br>
                                                <br>Yesterday Normal Traffic : <span class="text-success">{{ $webvisitoryesterday->webvisitor }}</span> 
                                                <br>Yesterday Ads Traffic : <span class="text-success">{{ $webvisitoryesterday->adsvisitor }}</span> 
                                                
                                            @endif
                                            @if ($totalregistered->counter != null)
                                                <br>
                                                <br>Total Registered : <span class="text-success">{{ $totalregistered->counter }}</span> 
                                            @endif
                                        </td>
                                        <td width="50"></td>
                                        <td>
                                            @if ($reguser != null)
                                            @foreach ($reguser as $key =>$value)
                                                @if ($key==null)
                                                    <br>Other {{ $reguser[$key]->reffer }} Reg : {{ $reguser[$key]->counter }} 
                                                @else
                                                    <br>{{ $reguser[$key]->reffer }} Reg : {{ $reguser[$key]->counter }} 
                                                @endif
                                            @endforeach
                                            @endif
                                            <br>
                                            @if ($reguseryesterday != null)
                                            @foreach ($reguseryesterday as $key =>$value)
                                                @if ($key==null)
                                                    <br>Ystd:Other {{ $reguseryesterday[$key]->reffer }} Reg : {{ $reguseryesterday[$key]->counter }} 
                                                @else
                                                    <br>Ystd:{{ $reguseryesterday[$key]->reffer }} Reg : {{ $reguseryesterday[$key]->counter }} 
                                                @endif
                                            @endforeach
                                            @endif
                                            <br>
                                            @if ($paymentrefer != null)
                                            @foreach ($paymentrefer as $key =>$value)
                                                
                                                    <br>{{ $paymentrefer[$key]->transaction_status }} Reg : {{ $paymentrefer[$key]->counter }} 
                                                
                                            @endforeach
                                            @endif
                                            @if ($total_order_refund != null)
                                            <br>Manual Payment : <span class="text-success">{{ $total_order_refund }}</span> 
                                            <br>Automated Payment : <span class="text-success">{{ $total_order_refund }}</span> 
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@if ((\Session::get('access_level') == 'superuser') || (\Session::get('access_level') == 'supervisor'))
        
    <div class="col-md-8">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-area me-1"></i>
                                        Grafik Penjualan
                                    </div>
                                    <div class="card-body"><canvas id="sellingChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col mt-0">
                            <h5 class="card-title">Total Penjualan</h5>
                        </div>
                        <div class="col-auto">
                            <div class="stat text-primary">
                                <i class="align-middle" data-feather="dollar-sign"></i>
                            </div>
                        </div>
                    </div>
                    <div class="table-scroll-x">
                        <table class="table" style="text-align: right; cursor: pointer;">
                            <tr>
                                <th>Periode</th>
                                <th>Qty Item</th>
                                <th>Qty Shipment</th>
                                <th>Omset</th>
                                
                                
                            </tr>
                            <tr style="font-weight: bold" onclick="window.location.href = '{{ url('sales-report').'/all' }}'">
                                <td style="text-align: left">Sejak Awal : </td>
                                <td>{{ number_format($sales_all['pcs']) }} pcs</td>
                                <td>{{ number_format($sales_all['shipment']) }} shipment</td>
                                <td>IDR {{ number_format($sales_all['total']) }}</td>
                                
                                
                            </tr>
                            <tr onclick="window.location.href = '{{ url('sales-report').'/yearly' }}'">
                                <td style="text-align: left">Tahun Ini : </td>
                                <td>{{ number_format($sales_yearly['pcs']) }} pcs</td>
                                <td>{{ number_format($sales_yearly['shipment']) }} shipment</td>
                                <td>IDR {{ number_format($sales_yearly['total']) }}</td>
                                
                                
                            </tr>
                            <tr onclick="window.location.href = '{{ url('sales-report').'/monthly' }}'">
                                <td style="text-align: left">Bulan Ini : </td>
                                <td>{{ number_format($sales_monthly['pcs']) }} pcs</td>
                                <td>{{ number_format($sales_monthly['shipment']) }} shipment</td>
                                <td>IDR {{ number_format($sales_monthly['total']) }}</td>
                                
                                
                            </tr>
                            <tr onclick="window.location.href = '{{ url('sales-report').'/weekly' }}'">
                                <td style="text-align: left">Minggu Ini : </td>
                                <td>{{ number_format($sales_weekly['pcs']) }} pcs</td>
                                <td>{{ number_format($sales_weekly['shipment']) }} shipment</td>
                                <td>IDR {{ number_format($sales_weekly['total']) }}</td>
                                
                                
                            </tr>
                            <tr onclick="window.location.href = '{{ url('sales-report').'/dayminustwo' }}'">
                                <td style="text-align: left">H-3 : </td>
                                <td>{{ number_format($sales_minustwo['pcs']) }} pcs</td>
                                <td>{{ number_format($sales_minustwo['shipment']) }} shipment</td>
                                <td>IDR {{ number_format($sales_minustwo['total']) }}</td>
                                
                                
                            </tr>
                            <tr onclick="window.location.href = '{{ url('sales-report').'/daybefore' }}'">
                                <td style="text-align: left">H-2 : </td>
                                <td>{{ number_format($sales_daybefore['pcs']) }} pcs</td>
                                <td>{{ number_format($sales_daybefore['shipment']) }} shipment</td>
                                <td>IDR {{ number_format($sales_daybefore['total']) }}</td>
                                
                                
                            </tr>
                            <tr onclick="window.location.href = '{{ url('sales-report').'/yesterday' }}'">
                                <td style="text-align: left">Kemarin : </td>
                                <td>{{ number_format($sales_yesterday['pcs']) }} pcs</td>
                                <td>{{ number_format($sales_yesterday['shipment']) }} shipment</td>
                                <td>IDR {{ number_format($sales_yesterday['total']) }}</td>
                                
                                
                            </tr>
                            <tr onclick="window.location.href = '{{ url('sales-report').'/daily' }}'">
                                <td style="text-align: left">Hari Ini : </td>
                                <td>{{ number_format($sales_daily['pcs']) }} pcs</td>
                                <td>{{ number_format($sales_daily['shipment']) }} shipment</td>
                                <td>IDR {{ number_format($sales_daily['total']) }}</td>
                                
                                
                            </tr>
                        </table>
                    </div>
                    <b>Pilih Periode Laporan :</b> <br>
                    <input type="date" name="date_start"> - 
                    <input type="date" name="date_end">
                    <button class="btn btn-sm btn-success" onclick="filterReport()"><i class="fas fa-search"></i> &nbsp; Lihat</button>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col mt-0">
                            <?php $total_valuation = 0;
                            foreach ($valuation as $row) {
                                $total_valuation += $row->qty_stock * $row->supplier_price; 
                            }
                            ?>
                            <h5 class="card-title">Total Valuasi Stok : <b>IDR {{ number_format($total_valuation) }}</b></h5>
                        </div>
                        <div class="col-auto">
                            <div class="stat text-primary">
                                <i class="align-middle" data-feather="dollar-sign"></i>
                            </div>
                        </div>
                    </div>
                    <div class="table-scroll-x">
                    <table class="table datatable-sm">
                        <thead>
                            <tr>
                                <th>Kode Internal</th>
                                <th>Qty</th>
                                <th>Harga Supplier</th>
                                <th>Subtotal Valuasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($valuation as $row)
                            <tr>
                                <td>{{ $row->internal_code }}</td>
                                <td>{{ $row->qty_stock }}</td>
                                <td>{{ number_format($row->supplier_price) }}</td>
                                <td>{{ number_format($row->qty_stock * $row->supplier_price) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    </div>
@endif

{{-- 
    <div class="row">
        <div class="col-12 col-md-6 col-xxl-3 d-flex order-2 order-xxl-3">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Browser Usage</h5>
                </div>
                <div class="card-body d-flex">
                    <div class="align-self-center w-100">
                        <div class="py-3">
                            <div class="chart chart-xs">
                                <canvas id="chartjs-dashboard-pie"></canvas>
                            </div>
                        </div>
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Chrome</td>
                                    <td class="text-end">4306</td>
                                </tr>
                                <tr>
                                    <td>Firefox</td>
                                    <td class="text-end">3801</td>
                                </tr>
                                <tr>
                                    <td>IE</td>
                                    <td class="text-end">1689</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-xxl-6 d-flex order-3 order-xxl-2">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Real-Time</h5>
                </div>
                <div class="card-body px-4">
                    <div id="world_map" style="height:350px;"></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-xxl-3 d-flex order-1 order-xxl-1">
            <div class="card flex-fill">
                <div class="card-header">
                    <h5 class="card-title mb-0">Calendar</h5>
                </div>
                <div class="card-body d-flex">
                    <div class="align-self-center w-100">
                        <div class="chart">
                            <div id="datetimepicker-dashboard"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-8 col-xxl-9 d-flex">
            <div class="card flex-fill">
                <div class="card-header">
                    <h5 class="card-title mb-0">Latest Projects</h5>
                </div>
                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="d-none d-xl-table-cell">Start Date</th>
                            <th class="d-none d-xl-table-cell">End Date</th>
                            <th>Status</th>
                            <th class="d-none d-md-table-cell">Assignee</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Project Apollo</td>
                            <td class="d-none d-xl-table-cell">01/01/2021</td>
                            <td class="d-none d-xl-table-cell">31/06/2021</td>
                            <td><span class="badge bg-success">Done</span></td>
                            <td class="d-none d-md-table-cell">Vanessa Tucker</td>
                        </tr>
                        <tr>
                            <td>Project Fireball</td>
                            <td class="d-none d-xl-table-cell">01/01/2021</td>
                            <td class="d-none d-xl-table-cell">31/06/2021</td>
                            <td><span class="badge bg-danger">Cancelled</span></td>
                            <td class="d-none d-md-table-cell">William Harris</td>
                        </tr>
                        <tr>
                            <td>Project Hades</td>
                            <td class="d-none d-xl-table-cell">01/01/2021</td>
                            <td class="d-none d-xl-table-cell">31/06/2021</td>
                            <td><span class="badge bg-success">Done</span></td>
                            <td class="d-none d-md-table-cell">Sharon Lessman</td>
                        </tr>
                        <tr>
                            <td>Project Nitro</td>
                            <td class="d-none d-xl-table-cell">01/01/2021</td>
                            <td class="d-none d-xl-table-cell">31/06/2021</td>
                            <td><span class="badge bg-warning">In progress</span></td>
                            <td class="d-none d-md-table-cell">Vanessa Tucker</td>
                        </tr>
                        <tr>
                            <td>Project Phoenix</td>
                            <td class="d-none d-xl-table-cell">01/01/2021</td>
                            <td class="d-none d-xl-table-cell">31/06/2021</td>
                            <td><span class="badge bg-success">Done</span></td>
                            <td class="d-none d-md-table-cell">William Harris</td>
                        </tr>
                        <tr>
                            <td>Project X</td>
                            <td class="d-none d-xl-table-cell">01/01/2021</td>
                            <td class="d-none d-xl-table-cell">31/06/2021</td>
                            <td><span class="badge bg-success">Done</span></td>
                            <td class="d-none d-md-table-cell">Sharon Lessman</td>
                        </tr>
                        <tr>
                            <td>Project Romeo</td>
                            <td class="d-none d-xl-table-cell">01/01/2021</td>
                            <td class="d-none d-xl-table-cell">31/06/2021</td>
                            <td><span class="badge bg-success">Done</span></td>
                            <td class="d-none d-md-table-cell">Christina Mason</td>
                        </tr>
                        <tr>
                            <td>Project Wombat</td>
                            <td class="d-none d-xl-table-cell">01/01/2021</td>
                            <td class="d-none d-xl-table-cell">31/06/2021</td>
                            <td><span class="badge bg-warning">In progress</span></td>
                            <td class="d-none d-md-table-cell">William Harris</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 col-lg-4 col-xxl-3 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Monthly Sales</h5>
                </div>
                <div class="card-body d-flex w-100">
                    <div class="align-self-center chart chart-lg">
                        <canvas id="chartjs-dashboard-bar"></canvas>
                    </div>
                </div>
            </div>
        </div>

         --}}
    </div>
</div>
@endsection

@section('script')
<script>
var ctx = document.getElementById("sellingChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [{!! $chart->tanggal !!}],
    datasets: [{
      label: "Qty Sold",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0.2)",
      borderColor: "rgba(2,117,216,1)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(2,117,216,1)",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(2,117,216,1)",
      pointHitRadius: 50000,
      pointBorderWidth: 2,
      data: [{{ $chart->total_jual }}],
    },{
      label: "Total Sold",
      lineTension: 0.3,
      backgroundColor: "rgba(200,200,0,0.2)",
      borderColor: "rgba(200,200,0,1)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(200,200,0,1)",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(200,200,0,1)",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: [{{ $chart->total_sold }}],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 30
        }
      }],
      yAxes: [{
        ticks: {
            min: 0,
          maxTicksLimit: 10
        },
        gridLines: {
          color: "rgba(0, 0, 0, .125)",
        }
      }],
    },
    legend: {
      display: false
    }
  }
});

</script>
<script src="{{ url('public/assets/js/app.js') }}"></script>
<script>
document.addEventListener("DOMContentLoaded", function() {
var ctx = document.getElementById("chartjs-dashboard-line").getContext("2d");
var gradient = ctx.createLinearGradient(0, 0, 0, 225);
gradient.addColorStop(0, "rgba(215, 227, 244, 1)");
gradient.addColorStop(1, "rgba(215, 227, 244, 0)");
// Line chart
new Chart(document.getElementById("chartjs-dashboard-line"), {
type: "line",
data: {
labels: [
@foreach($chart_data_sales as $row)
"{{ $row->order_month }}",
@endforeach
],
datasets: [{
label: "Penjualan (Rp)",
fill: true,
backgroundColor: gradient,
borderColor: window.theme.primary,
data: [
@foreach($chart_data_sales as $row)
{{ $row->total }},
@endforeach
]
}]
},
options: {
maintainAspectRatio: false,
legend: {
display: false
},
tooltips: {
intersect: false
},
hover: {
intersect: true
},
plugins: {
filler: {
propagate: false
}
},
scales: {
xAxes: [{
reverse: true,
gridLines: {
color: "rgba(0,0,0,0.0)"
}
}],
yAxes: [{
ticks: {
stepSize: 1000
},
display: true,
borderDash: [3, 3],
gridLines: {
color: "rgba(0,0,0,0.0)"
}
}]
}
}
});
});
</script>
<script>
document.addEventListener("DOMContentLoaded", function() {
// Pie chart
new Chart(document.getElementById("chartjs-dashboard-pie"), {
type: "pie",
data: {
labels: ["Chrome", "Firefox", "IE"],
datasets: [{
data: [4306, 3801, 1689],
backgroundColor: [
window.theme.primary,
window.theme.warning,
window.theme.danger
],
borderWidth: 5
}]
},
options: {
responsive: !window.MSInputMethodContext,
maintainAspectRatio: false,
legend: {
display: false
},
cutoutPercentage: 75
}
});
});
</script>
<script>
document.addEventListener("DOMContentLoaded", function() {
// Bar chart
new Chart(document.getElementById("chartjs-dashboard-bar"), {
type: "bar",
data: {
labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
datasets: [{
label: "This year",
backgroundColor: window.theme.primary,
borderColor: window.theme.primary,
hoverBackgroundColor: window.theme.primary,
hoverBorderColor: window.theme.primary,
data: [54, 67, 41, 55, 62, 45, 55, 73, 60, 76, 48, 79],
barPercentage: .75,
categoryPercentage: .5
}]
},
options: {
maintainAspectRatio: false,
legend: {
display: false
},
scales: {
yAxes: [{
gridLines: {
display: false
},
stacked: false,
ticks: {
stepSize: 20
}
}],
xAxes: [{
stacked: false,
gridLines: {
color: "transparent"
}
}]
}
}
});
});
</script>
<script>
document.addEventListener("DOMContentLoaded", function() {
var markers = [{
coords: [31.230391, 121.473701],
name: "Shanghai"
},
{
coords: [28.704060, 77.102493],
name: "Delhi"
},
{
coords: [6.524379, 3.379206],
name: "Lagos"
},
{
coords: [35.689487, 139.691711],
name: "Tokyo"
},
{
coords: [23.129110, 113.264381],
name: "Guangzhou"
},
{
coords: [40.7127837, -74.0059413],
name: "New York"
},
{
coords: [34.052235, -118.243683],
name: "Los Angeles"
},
{
coords: [41.878113, -87.629799],
name: "Chicago"
},
{
coords: [51.507351, -0.127758],
name: "London"
},
{
coords: [40.416775, -3.703790],
name: "Madrid "
}
];
var map = new jsVectorMap({
map: "world",
selector: "#world_map",
zoomButtons: true,
markers: markers,
markerStyle: {
initial: {
r: 9,
strokeWidth: 7,
stokeOpacity: .4,
fill: window.theme.primary
},
hover: {
fill: window.theme.primary,
stroke: window.theme.primary
}
},
zoomOnScroll: false
});
window.addEventListener("resize", () => {
map.updateSize();
});
});
</script>
<script>
document.addEventListener("DOMContentLoaded", function() {
var date = new Date(Date.now() - 5 * 24 * 60 * 60 * 1000);
var defaultDate = date.getUTCFullYear() + "-" + (date.getUTCMonth() + 1) + "-" + date.getUTCDate();
document.getElementById("datetimepicker-dashboard").flatpickr({
inline: true,
prevArrow: "<span title=\"Previous month\">&laquo;</span>",
nextArrow: "<span title=\"Next month\">&raquo;</span>",
defaultDate: defaultDate
});
});

function filterReport() {
    var start = $('input[name=date_start]').val();
    var end = $('input[name=date_end]').val();
    if (start > end) {
        alert('Tanggal Awal Tidak Boleh Lebih Besar Dari Tanggal Akhir');
    } else {
        window.location.href='{{ url('sales-report/filter') }}/'+start+'/'+end;
    }
}
</script>
@endsection