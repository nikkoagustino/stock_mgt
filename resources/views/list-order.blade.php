@extends('template')
@section('content')
<div class="row">
    <div class="col-14">
        <h2>List Order</h2>
    </div>
    <div class="col-14 table-scroll-x">
        <table class="table table-striped display nowrap datatable-list">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Instagram</th>
                    <th>Telpon</th>
                    <th>Nama</th>
                    <th>Action</th>
                    <th>Status</th>
                    <th>Total Order</th>
                    <th>Info</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($list_order as $row)
                <?php
                switch ($row->order_status) {
                    case 'paid':
                        $status = 'Standby Pengiriman';
                        break;

                    case 'delivery':
                        $status = 'Selesai';
                        break;

                    default:
                        $status = 'Menunggu Pembayaran';
                        break;
                }
                ?>
                <tr>
                    <td>
                        {{ $row->tanggal }}
                    </td>
                    <td>
                        @if ($row->order_status == 'paid')

                        @if ($row->packing_status == 0)
                            <button class="btn btn-sm btn-danger" onclick="window.location.href='{{ url('packing').'/'.$row->payment_id.'/1' }}'" data-bs-toggle="tooltip" data-bs-placement="top" title="Belum Packing"><i class="fas fa-box-open"></i></button>
                        @else
                            <button class="btn btn-sm btn-success" onclick="window.location.href='{{ url('packing').'/'.$row->payment_id.'/0' }}'" data-bs-toggle="tooltip" data-bs-placement="top" title="Sudah Packing"><i class="fas fa-box"></i></button>
                        @endif

                        @if ($row->shipping_status == 0)
                            <button class="btn btn-sm btn-danger" onclick="window.location.href='{{ url('shipping').'/'.$row->payment_id.'/1' }}'" data-bs-toggle="tooltip" data-bs-placement="top" title="Belum Kirim"><i class="fas fa-warehouse"></i></button>
                        @else
                            <button class="btn btn-sm btn-success" onclick="window.location.href='{{ url('shipping').'/'.$row->payment_id.'/0' }}'" data-bs-toggle="tooltip" data-bs-placement="top" title="Sudah Kirim"><i class="fas fa-truck"></i></button>
                        @endif

                        @endif
                        &nbsp;
                        <a href="{{ url('customer-edit').'/'.$row->instagram }}">{{ strtolower($row->instagram) }} </a>
                    </td>
                    <td>
                        <a href="http://wa.me/{{ $row->phone }}" target="_blank">{{ $row->phone }}</a>
                    </td>
                    <td>{{ (isset($row->firstname)) ? $row->firstname : ''; }} {{ (isset($row->lastname)) ? $row->lastname : ''; }}</td>
                    <td>
                        @if ($row->order_status == 'pending')
                            <button onclick="window.location.href='{{ url('selling/show-paid').'/'.$row->payment_id }}'" class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat Pesanan"><i class="fas fa-list"></i></button>
                            {{-- <button onclick="window.location.href='{{ url('recheck-payment').'/'.$row->payment_id }}'" class="btn btn-sm btn-success" data-bs-toggle="tooltip" data-bs-placement="top" title="Recheck Payment"><i class="fas fa-sync-alt"></i></button> --}}
                            <button onclick="window.location.href='{{ url('manual-confirmation').'/'.$row->payment_id }}'" class="btn btn-sm btn-warning" data-bs-toggle="tooltip" data-bs-placement="top" title="Checkout Manual"><i class="fas fa-cash-register"></i></button>
                            <button onclick="window.location.href='{{ url('cancel-payment').'/'.$row->payment_id }}'" class="btn btn-sm btn-danger" data-bs-toggle="tooltip" data-bs-placement="top" title="Cancel Payment"><i class="fas fa-times"></i></button>
                        @elseif ($row->order_status == 'paid')
                            <button onclick="window.location.href='{{ url('selling/show-paid').'/'.$row->payment_id }}'" class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat Pesanan"><i class="fas fa-list"></i></button>
                            <button class="btn btn-sm btn-success" onclick="window.open('{{ url('print/shipping/'.$row->payment_id) }}');" data-bs-toggle="tooltip" data-bs-placement="top" title="Print Label Pengiriman"><i class="fas fa-print"></i></button>
                            <span data-bs-toggle="tooltip" data-bs-placement="top" title="Input Resi">
                                <a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="changeID('{{$row->payment_id}}')" role="button" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="fas fa-shipping-fast"></i>
                                </a>
                            </span>
                            
                            <button class="btn btn-sm btn-danger" onclick="window.location.href='{{ url('refund/create').'/'.$row->payment_id }}'" data-bs-toggle="tooltip" data-bs-placement="top" title="Refund"><i class="fas fa-hand-holding-usd"></i></button>
                            <button onclick="deletePaidOrder('{{ $row->instagram }}', '{{ $row->tanggal }}','{{ $row->payment_id }}')" data-bs-toggle="tooltip" data-bs-placement="top" title="Batalkan Pembayaran" class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
                        @elseif ($row->order_status == 'delivery')
                            <button onclick="window.location.href='{{ url('selling/show-paid').'/'.$row->payment_id }}'" class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat Pesanan"><i class="fas fa-list"></i></button>
                            <button class="btn btn-sm btn-danger" onclick="window.location.href='{{ url('refund/create').'/'.$row->payment_id }}'" data-bs-toggle="tooltip" data-bs-placement="top" title="Refund"><i class="fas fa-hand-holding-usd"></i></button>
                            <button onclick="deleteDelOrder('{{ $row->payment_id }}')" data-bs-toggle="tooltip" data-bs-placement="top" title="Batalkan Pengiriman" class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
                        @else
                            <button onclick="window.location.href='{{ url('v2/manual-payment').'/'.$row->instagram.'/'.$row->tanggal }}'" class="btn btn-sm btn-success" data-bs-toggle="tooltip" data-bs-placement="top" title="Pembayaran Manual"><i class="fas fa-money-bill-wave"></i></button>
                            <button onclick="window.location.href='{{ url('selling/show').'/'.$row->instagram.'/'.$row->tanggal }}'" class="btn btn-sm btn-warning" data-bs-toggle="tooltip" data-bs-placement="top" title="Ubah Pesanan"><i class="fas fa-edit"></i></button>
                            @if (\Session::get('access_level') != 'staff')
                            <button onclick="deleteListOrder('{{ $row->instagram }}', '{{ $row->tanggal }}')" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus Pesanan" class="btn btn-sm btn-danger"><i class="fas fa-times"></i></a>
                            @endif
                        @endif
                    </td>
                    <td>
                        {{ $status }}
                        @if (($row->order_status == 'paid') || ($row->order_status == 'pending'))
                        <span style="font-size: 10px">
                            <br>ID Pembayaran: {{ $row->payment_id }}
                            <br>Status Pembayaran: {{ $row->transaction_status }}
                        </span>
                        @elseif ($row->order_status == 'delivery')
                        <span style="font-size: 10px">
                            <br>ID Pembayaran: {{ $row->payment_id }}
                            <br>Status Pembayaran: {{ $row->transaction_status }}
                            <br>No. Resi: <a href="https://idexpress.com/lacak-paket/?actionType=delivery&waybillNo={{ $row->awb }}">{{ $row->awb }}</a>
                        </span>
                        @endif
                        <br>Transaksi : {{ number_format($row->amount) }}
                    </td>
                    <td>{{ $row->qty_total }} item</td>
                    <td>{{ $row->transaction_status }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- {{ $list_order->links('pagination::bootstrap-4') }} --}}
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form action="{{ url('order/save-awb') }}" method="post">
      <div class="modal-header">
        <h5 class="modal-title">Masukkan No. Resi Pengiriman</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
            @csrf
            <input type="text" name="payment_id" readonly="readonly" class="form-control">
            <input type="text" placeholder="No. Resi / AWB" name="awb" class="form-control">
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Simpan">
      </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
    function changeID(new_id) {
        $('input[name="payment_id"]').val(new_id);
    }

    function deleteListOrder(instagram, tanggal) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = '{{ url('selling/delete') }}/'+instagram+'/'+tanggal;
        }
    }

    function deletePaidOrder(instagram, tanggal,pay_id) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = '{{ url('paid/delete') }}/'+instagram+'/'+tanggal+'/'+pay_id;
            //document.write('{{ url('paid/delete') }}/'+instagram+'/'+tanggal+'/'+pay_id);
        }
    }

    function deleteDelOrder(pay_id) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = '{{ url('delivered/delete') }}/'+pay_id;
            //document.write('{{ url('paid/delete') }}/'+instagram+'/'+tanggal+'/'+pay_id);
        }
    }

    

    $(document).ready(function() {
        $('.datatable-list').DataTable( {
            'pageLength': 25,
            "order": [[ 0, "desc" ]]
        } );
    } );
</script> 
@endsection