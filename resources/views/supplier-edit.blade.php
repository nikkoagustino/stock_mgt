@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Add New Supplier</h1>
    </div>
</div>

<form action="{{ url('supplier/edit/'.$supplier_data->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-2">
            Supplier Code
        </div>
        <div class="col-4">
            <input name="supplier_code" type="text" readonly="readonly" class="form-control" value="{{ $supplier_data->supplier_code }}" placeholder="Code"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Supplier Name <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="supplier_name" type="text" required="required" class="form-control" value="{{ $supplier_data->supplier_name}}" placeholder="Name"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Location
        </div>
        <div class="col-4">
            <textarea name="location" class="form-control">{{ $supplier_data->location }}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Email
        </div>
        <div class="col-4">
            <input name="email" type="email" class="form-control" value="{{ $supplier_data->email }}" placeholder="user@domain.com"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Phone <span class="required">*</span>
        </div>
        <div class="col-4">
            <input name="phone" type="text" required="required" class="form-control" value="{{ $supplier_data->phone }}" placeholder="081212341234"></input>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Repeat Order? <span class="required">*</span>
        </div>
        <div class="col-4">
            <select name="repeat_order" class="form-control">
                <option value="1" {{ ($supplier_data->repeat_order == 1) ? 'selected="selected"' : ''; }}>Ya</option>
                <option value="0" {{ ($supplier_data->repeat_order == 0) ? 'selected="selected"' : ''; }}>Tidak</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <input type="submit" value="Save" class="btn btn-success">
        </div>
    </div>
</form>
@endsection