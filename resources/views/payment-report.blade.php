@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Laporan Pembayaran</h1>
    </div>
</div>
<div class="row">
    <div class="col-12 table-scroll-x">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Order No</th>
                    <th>Instagram</th>
                    <th>Tanggal Order</th>
                    <th>Total Bayar</th>
                    <th>Metode Pembayaran</th>
                    <th>Waktu Transaksi</th>
                    <th>Status Pembayaran</th>
                    <th>No. Resi</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($report as $row)
                <tr>
                    <td>{{ $row->id }}</td>
                    <td><a href='https://instagram.com/{{ $row->instagram }}' target="_blank">{{ $row->instagram }}</a></td>
                    <td>{{ date('d/m/Y', strtotime($row->order_date)) }}</td>
                    <td>{{ number_format($row->amount) }}</td>
                    <td>{{ $row->payment_type }}</td>
                    <td>{{ date('d/m/Y H:i', strtotime($row->transaction_time)) }}</td>
                    <td>{{ $row->transaction_status }}</td>
                    <td>{{ $row->awb }}</td>
                    <td>
                        <button class="btn btn-success" onclick="window.open('{{ url('print/shipping/'.$row->id) }}');" data-bs-toggle="tooltip" data-bs-placement="top" title="Print Label Pengiriman"><i class="fas fa-print"></i></button>
                        @if (empty($row->awb))
                        <button class="btn btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Input Resi"><i class="fas fa-shipping-fast"></i></button>
                        <button class="btn btn-danger" data-bs-toggle="tooltip" data-bs-placement="top" title="Refund"><i class="fas fa-hand-holding-usd"></i></button>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection