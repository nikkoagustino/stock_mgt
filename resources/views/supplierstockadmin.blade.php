@extends('template')
@section('content')

<div class="row p-5">
    <div >
        <div  >
            <h3>Supplier Stock Per Hari Ini</h3>
            <?php date_default_timezone_set('Asia/Jakarta');?>
            List Kode : {{ $kode . ' ' . date(now())}}
        </div>
    </div>
    <?php $ttlstok=0;
          $ttlvaluasi=0;?>
        <div class="col-12">
        <table>
            <tr>
                <td>
                    Total Item         :
                    <br>
                    {{-- <input type="text" readonly="readonly" value="{{ $valuasi->total_item_stock }}" class="" name="totitem"> --}}
                    <b class="fs-4">{{ number_format($valuasi->total_item_stock, 0) }}</b>
                    <br>
                    Valuasi Semua Item : 
                    <br>
                    {{-- <input type="text" readonly="readonly" value="{{ $valuasi->total_valuasi_stock }}" class="" name="totval"> --}}
                    <b class="fs-4">{{ number_format($valuasi->total_valuasi_stock, 0) }}</b>
                    <br>
                </td>
                <td width="100">
                    
                </td>
                
                <td><p style="font-size:9px">
                    @foreach ($stokbysupcode as $row)
                        {{ number_format($row->supplier_price) }} x {{ number_format($row->qty_stock) }} = {{ number_format($row->stock_val) }}<br>
                    @endforeach</p>
                </td>
            </tr>
        </table>
            <table id="example" class="table table-striped center table-bordered" style="text-align:center;font-size:11px;font-family:arial" border="1" cellspacing="0" >
                <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Photo</th>
                        
                        <th>Nama Barang</th>
                        <th>Variant</th>
                        <th>Size</th>
                        <th>Stok</th>
                        <th>Sub Total</th>
                        <th>Total Stok</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($supplier as $row)
                    <tr>
                        <td>{{ $row->internal_code }}</td>
                        @if (!empty($row->images))
                            <td><center><a href="{{ url('public').\Storage::url( $row->images) }}" target="_blank"><img style="height: 70px;width:auto !important" src="{{ url('public').\Storage::url( $row->images) }}" class="d-block w-100" alt="..."></a></center></td>
                        @else
                            @if (!empty($row->master_image))
                                <td><center><a href="{{ url('public').\Storage::url( $row->master_image) }}" target="_blank"><img style="height: 70px;width:auto !important" src="{{ url('public').\Storage::url( $row->master_image) }}" class="d-block w-100" alt="..."></a></center></td>
                            @else
                            <td><center><a href="{{ url('public').\Storage::url( 'no_image.png') }}" target="_blank"><img style="height: 70px;width:auto !important" src="{{ url('public').\Storage::url( 'no_image.png') }}" class="d-block w-100" alt="..."></a></center></td>
                            @endif
                        @endif
                        
                        
                        <td>{{ $row->product_name }}</td>
                        <td>{{ $row->variant }}</td>
                        <td>{{ $row->size }}</td>
                        
                        <td>{{ $row->qty }}</td>
                        <td>{{ number_format($row->supplier_price) }}</td>
                        <td>{{ number_format($row->supplier_price * $row->qty) }}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    
</div>       
@endsection
@section('script')
<script>

    $(document).ready(function(){
        $('input[name=totitem]').val($ttlstok);
        $('input[name=totvaluasi]').val($ttlvaluasi);
    });
</script>
@endsection