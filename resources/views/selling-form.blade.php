@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Form Penjualan</h2>
    </div>
</div>
<div class="row">
        @csrf
    <div class="col-12">
        <input type="file" id="fileinput" class="form-control" accept=".txt" onchange="handleFileSelect()">
        <textarea name="" rows="5" id="editor" readonly="readonly" class="form-control"></textarea>
        <input type="hidden" value="{{ $batch_no }}" name="batch_no">
    </div>
    <div class="col-12">
        <table class="table table-striped tableFixHead">
            <thead>
                <tr>
                    <th width="10%">Instagram</th>
                    <th width="5%">Kode Internal</th>
                    <th width="5%">Qty</th>
                    <th width="5%">Size</th>
                    <th width="10%">Variant</th>
                    <th width="1%"></th>
                    <th width="10%">Tanggal</th>
                    <th width="20%">Comment</th>
                    <th width="1%"></th>
                </tr>
            </thead>
            <tbody class="table-fill"></tbody>
        </table>
    </div>
    <div class="col-12 col-md-4">
        <h4>Total Order : <span class="total_order">0</span></h4>
    </div>
    <div class="col-12 col-md-4">
        <h4>Total Qty : <span class="total_qty">0</span></h4>
    </div>
    <div class="col-12 col-md-4">
        <button class="btn btn-primary form-control" onclick="saveAllSelling()"><i class="fas fa-save"></i> &nbsp; Simpan Semua Order</button>
        <a class="btn btn-success form-control" href="{{ url('stock-check/minus?trigger=print') }}" target="_blank"><i class="fas fa-search"></i> &nbsp; Cek Stok</a>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    var fr = new FileReader();
    var total_order = 0;
    var total_qty = 0;
    function handleFileSelect()
      {               
        if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
          alert('The File APIs are not fully supported in this browser.');
          return;
        }   
      
        var input = document.getElementById('fileinput');
        if (!input) {
          alert("Um, couldn't find the fileinput element.");
        }
        else if (!input.files) {
          alert("This browser doesn't seem to support the `files` property of file inputs.");
        }
        else if (!input.files[0]) {
          alert("Please select a file before clicking 'Load'");               
        }
        else {
          var file = input.files[0];
          fr.onload = receivedText;
          fr.readAsText(file);
          // fr.readAsBinaryString(file); //as bit work with base64 for example upload to server
          // fr.readAsDataURL(file);
        }
      }
      
      function receivedText() {
        document.getElementById('editor').appendChild(document.createTextNode(fr.result));
        toTable();
      }  

      function toTable() {
        var line_array = fr.result.split("[^]");
        var sequence = -1;
        $.each(line_array, function(index, value){
          sequence++;
            if (index == 0) {
                // do nothing
            } else {
                var data_line = value.split("{}");
                var tr = '<tr data-seq='+sequence+'>'+
                        '<td><input type="text" name="instagram" readonly="readonly" value="'+data_line[0]+'" class="form-control"></td>'+
                        '<td><input type="text" name="internal_code" value="'+data_line[1]+'" class="form-control uppercase"></td>'+
                        '<td><input type="number" name="qty" value="'+data_line[2]+'" class="form-control"></td>'+
                        '<td><input type="text" name="size" value="'+data_line[3]+'" class="form-control uppercase"></td>'+
                        '<td><input type="text" name="variant" value="'+data_line[4]+'" class="form-control uppercase"></td>'+
                        '<td><button class="btn btn-danger btn-sm" onclick="deleteRow(this);"><i class="fas fa-trash"></button></td></td>'+
                        '<td><input type="datetime_local" name="tanggal" value="'+data_line[5]+'" class="form-control"></td>'+
                        '<td><input type="text" name="comment" value="'+data_line[6]+'" class="form-control"></td>'+
                        '<td><button class="form-control btn btn-sm btn-primary btn-save" data-seq='+sequence+' onclick="saveSelling(this)"><i class="fas fa-save"></i></button></td>'+
                    '</tr>';
                $('.table-fill').append(tr);
                total_order++;
                total_qty = total_qty + parseInt(data_line[2]);
            }
        });
        $('.total_order').html(total_order);
        $('.total_qty').html(total_qty);
      }

      function deleteRow(thisid) {
        event.preventDefault();
        $(thisid).closest('tr').remove();
      }

      function saveSelling(thisid) {
        event.preventDefault();
        var sequence = $(thisid).attr('data-seq');
        var instagram = $('tr[data-seq='+sequence+'] input[name=instagram]').val();
        var internal_code = $('tr[data-seq='+sequence+'] input[name=internal_code]').val();
        var tanggal = $('tr[data-seq='+sequence+'] input[name=tanggal]').val();
        var qty = $('tr[data-seq='+sequence+'] input[name=qty]').val();
        var size = $('tr[data-seq='+sequence+'] input[name=size]').val();
        var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val();
        var comment = $('tr[data-seq='+sequence+'] input[name=comment]').val();
        var batch_no = $('input[name=batch_no]').val();

        if (sizeValidation(size) == false) {
           $('tr[data-seq='+sequence+'] input[name=size]').css('border', '2px solid red');
           return;
        }
      
        var fd = new FormData();
        fd.append('instagram', instagram);
        fd.append('internal_code', internal_code);
        fd.append('tanggal', tanggal);
        fd.append('qty', qty);
        fd.append('variant', variant);
        fd.append('size', size);
        fd.append('comment', comment);
        fd.append('batch_no', batch_no);
        
        var ajax_url = "{{ url('api/save_selling') }}";

        $.ajax({
            method: 'post',
            url: ajax_url,
            processData: false,
            contentType: false,
            cache: false,
            enctype: 'multipart/form-data',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            data: fd,
            success: function(result) {
              if (result == "ok") {
                $('tr[data-seq='+sequence+'] input').attr('disabled', 'disabled');
                $('tr[data-seq='+sequence+'] select').attr('disabled', 'disabled');
                $('tr[data-seq='+sequence+'] button').attr('disabled', 'disabled');
              }
            }
        });
      }

      function saveAllSelling() {
        event.preventDefault();
        $('.btn-save:enabled').trigger('click');
      }

</script>
@endsection