<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>MomAvel.id - Belanja Fashion Wanita Harga Terjangkau</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <link href="{{ url('public') }}/assetsarsha/img/fashion-logo.png" rel="icon">
  <link href="{{ url('public') }}/assetsarsha/img/fashion-logo.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ url('public') }}/assetsarsha/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{ url('public') }}/assetsarsha/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ url('public') }}/assetsarsha/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{ url('public') }}/assetsarsha/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{ url('public') }}/assetsarsha/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{ url('public') }}/assetsarsha/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{ url('public') }}/assetsarsha/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ url('public') }}/assetsarsha/css/style2.css" rel="stylesheet">
<style>
  
.cta-btn {
  font-family: "Jost", sans-serif;
  font-weight: 500;
  font-size: 16px;
  letter-spacing: 1px;
  display: inline-block;
  padding: 12px 40px;
  border-radius: 50px;
  transition: 0.5s;
  margin: 10px;
  border: 2px solid #fff;
  color: #fff;
  background: #47b2e4;
}

.cta-btn:hover {
  background: #47b2e4;
  border: 2px solid #47b2e4;
}
  #hero {
    height:auto;
  }
  </style>

  <!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '850553866052582');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=850553866052582&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->
  

  <!-- =======================================================
  * Template Name: Arsha - v4.10.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>




<body>

<?php
$stock_list = [];
foreach ($stock as $row) {
    $stock_list[$row->internal_code][$row->variant][$row->size]['qty'] = $row->qty;
    $stock_list[$row->internal_code][$row->variant][$row->size]['keterangan'] = $row->keterangan;
}
 ?>
 

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.html">MomAvel.id</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="{{ url('public') }}/assetsarsha/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#Mengenai">Mengenai</a></li>
          <li><a class="nav-link scrollto" href="#Pelayanan">Pelayanan</a></li>
          <li><a class="nav-link scrollto" href="#Testimoni">Testimoni</a></li>
          <li><a class="nav-link scrollto" href="#faq">FAQ</a></li>
          
          
          <li><a class="getstarted scrollto" href="/ld/beli/{{ $row->internal_code }}"><img style="width: 30px; margin-top: 5px; " src="{{ url('public').\Storage::url($row->master_image) }}" onclick="changeSrc(this)" class="img-fluid animated" alt="" data-bs-toggle="modal" data-bs-target="#imageModal"> &nbsp; &nbsp;  Beli Sekarang Rp. {{ number_format($row->selling_price) }},-</a></li>
          
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div>
      <center><h7>Produk Fashion Paling Up to Date dengan Kualitas Ekspor</h7></center>

      </div>



      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>{{ strtoupper($row->product_name) }}</h1>
          <h2>{{ $row->internal_code }} - {{ $row->variant }} - {{ $row->size }}<br>{!! nl2br(ucwords($row->deskripsi)) !!}</h2>
          <div class="d-flex justify-content-center justify-content-lg-start">
            <a href="/ld/beli/{{ $row->internal_code }}" class="btn-get-started scrollto">Beli Sekarang Rp. {{ number_format($row->selling_price) }},-</a>
            
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">

          <div class="cont">
                            
                    @if($row->hargaasli != null && $row->hargaasli > $row->selling_price && !empty($row->master_image))
          					<div class="ribbon ribbon-top-left">
                              <span>SALE!!!</span>
          					</div>
                    @endif
                            
                    <div class="product-img-wrapper my-5">
                        @if (!empty($row->master_image))
                        <img src="{{ url('public').\Storage::url($row->master_image) }}" onclick="changeSrc(this)" class="img-fluid animated" alt="" data-bs-toggle="modal" data-bs-target="#imageModal">
                        @else
                          
                          <img src="{{ url('public').\Storage::url('no_image.png') }}" onclick="changeSrc(this)" class="img-fluid animated" alt="" data-bs-toggle="modal" data-bs-target="#imageModal">
                        @endif
                              
                        @if($row->hargaasli != null && $row->hargaasli > $row->selling_price && !empty($row->master_image))
                            <div class="sale-price">
                                <s>{{ number_format($row->hargaasli) }}</s>
                                <b>{{ number_format($row->selling_price) }}</b>
                            </div>
                        @endif
                    </div>                      
                         
          </div>

          
          
        </div>
      </div>

      <div>
        <br>
        <center><h1>+1.000 Pcs Have Been Sold</h1></center>
        <center><a class="cta-btn align-middle" href="/ld/beli/{{ $row->internal_code }}">Beli Sekarang Rp. {{ number_format($row->selling_price) }},-</a></center>
            <br>
      </div>
      
      
            
          
    </div>
    
      

  </section><!-- End Hero -->




  

  <main id="main">

   
    <!-- ======= Mengenai Kami Section ======= -->
    <section id="Mengenai" class="Mengenai">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Mengenai Kami</h2>
        </div>

        <div class="row content">
          <div class="col-lg-6">
          <img src="{{ url('public').\Storage::url('0A1.jpg') }}" onclick="changeSrc(this)" class="img-fluid animated" alt="" data-bs-toggle="modal" data-bs-target="#imageModal">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            
            <h1>
              Momavel adalah distributor produk fashion yang telah menjual 1.000+ pcs produk fashion. 
            </h1>
            <br>
            <br>
            <h4>
            <ul>
              ✅ Kami memilih produk yang kami tampilkan harus sesuai dengan standard display kami.</li>
              <br><br>✅ Kami melakukan Quality Control setiap produk sebelum di kirim kan kepada client.</li>
              <br><br>✅ Kami menerima retur apabila ada kerusakan / reject disertai dengan video unboxing.</li>
            </ul>
            </h4>
          </div>
        </div>

      </div>
    </section><!-- End Mengenai Kami Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      
      <div class="container" data-aos="zoom-in">
      
        <div class="row">
        	<div class="col-lg-3">
                  <div class="cont">
                            
                            @if($row->hargaasli != null && $row->hargaasli > $row->selling_price && !empty($row->master_image))
                            <div class="ribbon ribbon-top-left">
                                      <span>SALE!!!</span>
                            </div>
                            @endif
                                    
                            <div class="product-img-wrapper">
                                @if (!empty($row->master_image))
                                <img style="width: 300px; margin-top: 5px; " src="{{ url('public').\Storage::url($row->master_image) }}" onclick="changeSrc(this)" class="img-fluid animated" alt="" data-bs-toggle="modal" data-bs-target="#imageModal">
                                @else
                                  
                                  <img style="width: 300px; margin-top: 5px; " src="{{ url('public').\Storage::url('no_image.png') }}" onclick="changeSrc(this)" class="img-fluid animated" alt="" data-bs-toggle="modal" data-bs-target="#imageModal">
                                @endif
                                      
                                @if($row->hargaasli != null && $row->hargaasli > $row->selling_price && !empty($row->master_image))
                                    <div class="sale-price">
                                        <s>{{ number_format($row->hargaasli) }}</s>
                                        <b>{{ number_format($row->selling_price) }}</b>
                                    </div>
                                @endif
                            </div>                      
                                 
                  </div>
          </div>
          <div class="col-lg-6 cta-btn-container">
          <div>
            <h3>Beli Sekarang</h3>
            
            <p> Coba produk kami sekarang, jangan lewatkan Gratis Ongkir Jadebotabek Unk Pembelian 5pcs / Subsidi ongkir yang kami berikan bagi perbelanjaan di luar Jadebotabek.
              </p>
            </div>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            
          
            <a class="cta-btn align-middle" href="/ld/beli/{{ $row->internal_code }}">Beli Sekarang Rp. {{ number_format($row->selling_price) }},-</a>
            
          </div>
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3>Mengapa Kami? <br><strong>Produk Kami Sangat Bervariasi</strong></h3>
              <p>
                Produk fashion adalah produk yang terus berganti mengikuti trend pasar. Di perlukan terus menerus pengembangan produk untuk supply kebutuhan fashion.
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>01</span> Produk kami sangat banyak jenis dan modelnya <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                    <p>
                      Kami bekerja sama dengan banyak produsen pakaian mulai yang memiliki produk hingga puluhan ribu jenis dan model, membuat kami memiliki opsi yang sangat banyak untuk kebutuhan fashion anda.
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>02</span> Produk kami terus berganti model <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Kebanyakan penjual hanya fokus pada 1 model saja tapi dengan kuantiti banyak, perbedaan kami memiliki banyak sekali produk baru setiap minggunya.
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span> Bisa di beli per 1 item saja <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Pada umumnya penjual tidak bisa menjual per item karena sistem penjualan fashion adalah per seri. Namun kami memberikan anda pilihan untuk dapat berbelanja berapapun item yang anda inginkan dengan harga yang mirip dengan harga yang terjangkau.
                    </p>
                  </div>
                </li>

              </ul>
            </div>

          </div>

          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("{{ url('public').\Storage::url('0A2.png') }}' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

   
    
<!-- ======= Skills Section ======= -->
<section id="skills" class="skills">
      <div class="container" data-aos="fade-up">

        <div class="row">
          </br>
</br>
</br>
</br>

          <center><h1>Mengapa Produk Momavel Sangat Bervariasi?</h1></center>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
          <center><h2>Karena produk fashion terus up to date dan mengikuti tren pasar. 
          <br>
          <br>Dengan 1000+ motif yang terus menyesuaikan tren,<br> Momavel berkomitmen untuk memberikan produk yang terus up to date.
</h2></center>

        </div>
        </br>
</br>

      </div>
    </section><!-- End Skills Section -->


    <!-- ======= Pelayanan Section ======= -->
    <section id="Pelayanan" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Pelayanan</h2>
          <p>Kami melayani beberapa cara pemesanan / checkout sesuai dengan yang pelanggan inginkan. Karena bagi kami setiap pelanggan itu memiliki keinginan yang berbeda. Sehingga kami menyediakan beberapa pilihan yang terbaik.</p>
        </div>

        <div class="row">
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4><a href="../allproducts" target="_blank">Checkout Mandiri Via WEB MomAvel.id</a></h4>
              <p>Website kami sudah bekerja sama dengan payment gateway yang memungkinkan anda untuk melakukan belanja dan checkout secara mandiri.</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="https://wa.me/6288905235047" target="_blank">Checkout Mandiri menggunakan Whatsapp Otomatis</a></h4>
              <p>Anda cukup ketik -Daftar (Menggunakan strip sebelum daftar) untuk melakukan pemesanan dan checkout mandiri menggunakan sistem kami. Bisa Klik link ini.</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4>Belanja dari Landing Page</h4>
              <p>Berbelanja langsung melalui landing page seperti layar ini, dapat di lakukan walaupun 1 pembelian saja.</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href="https://wa.me/6281283443452" target="_blank">Checkout dengan Manual Whatsapp</a></h4>
              <p>Jika ingin di melakukan pemesanan secara manual juga bisa di lakukan dengan menyebutkan kode pakaian yang ingin anda beli dan melakukan checkout secara manual.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Pelayanan Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      
      <div class="container" data-aos="zoom-in">
      
        
                  <div class="row">
        	<div class="col-lg-3">
          <div class="cont">
                            
                            @if($row->hargaasli != null && $row->hargaasli > $row->selling_price && !empty($row->master_image))
                            <div class="ribbon ribbon-top-left">
                                      <span>SALE!!!</span>
                            </div>
                            @endif
                                    
                            <div class="product-img-wrapper">
                                @if (!empty($row->master_image))
                                <img style="width: 300px; margin-top: 5px; " src="{{ url('public').\Storage::url($row->master_image) }}" onclick="changeSrc(this)" class="img-fluid animated" alt="" data-bs-toggle="modal" data-bs-target="#imageModal">
                                @else
                                  
                                  <img style="width: 300px; margin-top: 5px; " src="{{ url('public').\Storage::url('no_image.png') }}" onclick="changeSrc(this)" class="img-fluid animated" alt="" data-bs-toggle="modal" data-bs-target="#imageModal">
                                @endif
                                      
                                @if($row->hargaasli != null && $row->hargaasli > $row->selling_price && !empty($row->master_image))
                                    <div class="sale-price">
                                        <s>{{ number_format($row->hargaasli) }}</s>
                                        <b>{{ number_format($row->selling_price) }}</b>
                                    </div>
                                @endif
                            </div>                      
                                 
                  </div>
          </div>
          <div class="col-lg-6 cta-btn-container">
          <div>
            <h3>Beli Sekarang</h3>
            
            <p> Coba produk kami sekarang, jangan lewatkan Gratis Ongkir Jadebotabek Unk Pembelian 5pcs / Subsidi ongkir yang kami berikan bagi perbelanjaan di luar Jadebotabek.
              </p>
            </div>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            
          
            <a class="cta-btn align-middle" href="/ld/beli/{{ $row->internal_code }}">Beli Sekarang Rp. {{ number_format($row->selling_price) }},-</a>
            
          </div>
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="Testimoni" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Testimoni</h2>
          
          <p>Silakan lihat review beberapa customer yang sudah belanja di Momavel:</p>
        </div>

        @php
            $x = 0;
            $slide_count = count($slides);
            @endphp
            @if ($slide_count == 1)
                  <img src="{{ url('public').\Storage::url($slides[0]->image_url) }}" class="d-block w-100" alt="...">
            @else

            <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
              <div class="carousel-indicators">
                @for ($i=0; $i < $slide_count; $i++)
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{ $i }}" {{ (($i == 0) ? 'class=active aria-current=true' : '') }} aria-label="Slide {{ $i }}"></button>
                @endfor
              </div>
              <div class="carousel-inner">
                @foreach ($slides as $row)
                <div class="carousel-item{{ (($x == 0) ? ' active' : '') }}" data-bs-interval="3000">
                  <img src="{{ url('public').\Storage::url($row->image_url) }}" class="d-block w-100" alt="...">
                </div>
                @php $x++; @endphp
                @endforeach
              </div>
              <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
            @endif

      </div>
    </section><!-- End Portfolio Section -->

    

   

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
          <p>Pertanyaan yang sering kali di tanyakan.</p>
        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" class="collapse" data-bs-target="#faq-list-1">Bagaimana cara dapatkan gratis ongkir? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-bs-parent=".faq-list">
                <p>
                Gratis ongkir bagi yang tinggal di JADEBOTABEK & berlaku apabila membeli 5 pcs dan kelipatan.
                <br>
<br>Jika Anda di luar Jakarta, Anda bisa mendapatkan subsidi sebesar Rp10.000 per kelipatan 5 pcs. 
<br>
<br>Jika Anda tinggal di bandung dengan ongkir Rp10.500, Anda mendapatkan subsidi biaya kirim sebesar Rp10.000, sehingga ongkir yang dibayarkan hanya selisihnya yaitu 500 rupiah saja.
<br>
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-2" class="collapsed">Apakah kita bisa mendapatkan gratis ongkir / subsidi ongkir apabila checkout WA Manual? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-bs-parent=".faq-list">
                <p>
                Saat ini Momavel belum ada promo untuk pembelian secara Whatsapp manual. Untuk mendapatkan keuntungan Gratis Ongkir / Subsidi Ongkir diwajibkan untuk checkout mandiri melalui WEB ataupun WA Otomatis.

                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-3" class="collapsed">Bagaimana cara order melalui WA Otomatis? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-bs-parent=".faq-list">
                
                <br>               Silakan WA Kami ke nomor <a href ="https://web.whatsapp.com/send?phone=6288905235047" target="_blank">088905235047</a> dengan mengirimkan kata kunci seperti di bawah:
                <br>
                <br>-CARA : Untuk melihat list perintah
<br>-DAFTAR : Sebelum dapat menggunakan perintah order, customer harus sudah terdaftar di sistem kami dengan cara ketik -ORDER.
<br>-DAFTARULANG : Apabila anda melakukan kesalahan dalam pendaftaran, silahkan lakukan daftar ulang untuk mengisi kembali semua informasi.
<br>-ORDER (spasi) Kode : Untuk memasukan produk ke dalam keranjang. Contoh: -ORDER D102
<br>-HAPUS (spasi) Kode : Untuk membatalkan produk dari keranjang. Contoh: -HAPUS D102
<br>-LIST : Untuk melihat apa saja yang ada di keranjang anda.
<br>-BAYAR : Untuk melakukan pembayaran secara mandiri.
<br>-BAYARULANG : Proses pembayaran sering kali gagal karena user terlambat mencatat Virtual ACC / alasan lainnya, bisa di lakukan Bayar Ulang.
<br>-MASUKWEB : Untuk mendapatkan link otomatis untuk masuk web setelah teregistrasi di whatsapp otomatis.
<br>
<br>Pastikan Anda menambahkan tanda “-” 

                  
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-4" class="collapsed">Mengapa Tidak Ada Bank BCA untuk transfer bank? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-bs-parent=".faq-list">
                <p>
                Untuk sementara, kami sedang mengajukan proses pembuatan pembayaran mandiri menggunakan BCA Virtual Account.
<br>
<br>Maka dari itu, opsi ini belum tersedia. Akan tetapi, pembayaran melalui M-Banking BCA tetap bisa dilakukan dengan memilih opsi Transfer Bank Lainnya / Other.
<br>
<br> Anda dapat melakukan pembayaran melalui Bank BNI melalui Virtual Account Midtrans-Nama Anda, sehingga checkout pun tetap dilakukan secara real time.
<br>
                </p>
              </div>
            </li>

            

          </ul>
        </div>
        <center><a class="cta-btn align-middle" href="/ld/">GANTI KE PRODUK LAIN</a></center>
      </div>
      <br>
      
    </section><!-- End Frequently Asked Questions Section -->

   
    
  </main><!-- End #main -->

  
  
  <!-- ======= Footer ======= -->
  <footer id="footer">

    

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>MomAvel</h3>
            <p>
              Quality Fashion <br>
              Affordable Price<br>
              <br>
              <a href="https://wa.me/6281283443452" target="_blank"><strong>WA Manual / CS :</strong> +62812 8344 3452<br></a>
              <a href="https://wa.me/6288905235047" target="_blank"><strong>WA Otomatis :</strong> +62889 0523 5047<br></a>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Website</h4>
            <li><i class="bx bx-chevron-right"></i> <a href="https://momavel.id/allproducts" target="_blank">MomAvel.id</a></li>
            
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Social Networks</h4>
            <p>Follow Social Media Kami</p>
            <div class="social-links mt-3">
              <a href="https://instagram.com/vincentraditya" target ="_blank" class="instagram"><i class="bx bxl-instagram"></i></a>
             
              <a href="https://instagram.com/fannymargarett" target ="_blank" class="instagram"><i class="bx bxl-instagram"></i></a>

            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>MomAvel.id</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
        
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-body">
                      <img class="image_modal" width="100%" src="" />
                      <br>
                      <br>
                      <button type="button" class="btn btn-danger float-end" data-bs-dismiss="modal" aria-label="Close">Close</button>
                  </div>
              </div>
          </div>
      </div>


  <!-- Vendor JS Files -->
  <script src="{{ url('public') }}/assetsarsha/vendor/aos/aos.js"></script>
  <script src="{{ url('public') }}/assetsarsha/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ url('public') }}/assetsarsha/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{ url('public') }}/assetsarsha/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{ url('public') }}/assetsarsha/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{ url('public') }}/assetsarsha/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="{{ url('public') }}/assetsarsha/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ url('public') }}/assetsarsha/js/main.js"></script>
  <script>
    function changeSrc(thisid) {
                  var src_new = $(thisid).attr('src');
                  console.log(src_new);
                  $('.image_modal').attr("src", src_new);
              }

  </script>
</body>

</html>

