@extends('template')
@section('content')
<div class="row">
    <div class="col-5">
        <h1>Discount</h1>
        <form action="{{ url('discount') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-6">
                    Diskon Web:
                    <input type="number" name="percent" class="form-control form-control-lg mt-2" value="{{ $discount->percent }}">
                    </br>
                    Diskon WA:
                    <input type="number" name="percentwa" class="form-control form-control-lg mt-2" value="{{ $discount->percentwa }}">
                    </br>
                    WA1:
                    <input type="number" name="wa1" class="form-control form-control-lg mt-2" value="{{ $discount->wa1 }}">
                    WA2:
                    <input type="number" name="wa2" class="form-control form-control-lg mt-2" value="{{ $discount->wa2 }}">
                    WA3:
                    <input type="number" name="wa3" class="form-control form-control-lg mt-2" value="{{ $discount->wa3 }}">
                    WA4:
                    <input type="number" name="wa4" class="form-control form-control-lg mt-2" value="{{ $discount->wa4 }}">
                    <input type="submit" class="btn btn-primary" value="Save Discount">
                    </br></br>
                    <a href="javascript:void(0)" class="form-control btn btn-success" onclick="TestKirim()"><i class="fas fa-save"></i>TEST KIRIM WA</a>        
                    
                </div>
                <div class="col-6">
                </br><span class="fs-4 mt-2">%</span> &nbsp;</br></br></br></br><span class="fs-4 mt-2">%</span>
                    
                    
                    
                </div>
                
                    
                
            </div>
        </form>
    </div>
   
 <div class="col-7">
        <h1>Banner Promosi (1500px width)</h1>
        <form action="{{ url('slide/add') }}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="row">
                <div class="col-6">
                    <input type="file" name="slide_image" class="form-control form-control-lg">
                </div>
                <div class="col-6">
                    <button class="btn btn-success" type="submit"><i class="fas fa-plus"></i> Add Slide</button>
                </div>
            </div>
        </form>

        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($slides as $row)
                    <tr>
                        <td>
                            <img style="height: 100px" src="{{ url('public').\Storage::url($row->image_url) }}" alt="">
                        </td>
                        <td>
                            <button onclick="deleteSlide({{ $row->id }})" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection

@section('script')
<script>
    function deleteSlide(slide_id) {
        if (confirm('Yakin Hapus Data?')) {
            window.location.href='{{ url('slide/delete') }}/'+slide_id;
        }
    }

    function TestKirim() {
        var number = "08119994888";
        var message = "atalavista";
        var fd = new FormData();
        fd.append('number', number);
        fd.append('message', message);

        $.ajax({
            url: 'https://momavel.id/api/whatsapp/send-message',
            method: 'post',
            processData: false,
            contentType: false,
            cache: false,
            data: fd,
            headers: {
            },
            success: function(result) {
                console.log(result);
                
                // if (result == 'ok') {

                //     location.reload();
                // }
            },
          	error: function(r) {
                console.log("error=",r);
            }
        });
    }
</script>
@endsection