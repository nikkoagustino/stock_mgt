@extends('template')
@section('content')
<div class="row">
    <div class="col-11">
        <h1>Supplier Payment</h1>
        <a href="{{ url('supplierPayment/add') }}" class="btn btn-primary">+ Add New Supplier Payment</a>
    </div>
</div>
<div class="row">
    <div class="col-11">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Supplier</th>
                    <th>Nominal</th>
                    <th>Total Pcs</th>
                    <th>Detail</th>
                    <th>Option</th>
                </tr>
            </thead>
            <tbody>
                @foreach($supplier as $row)
                <tr>
                    <td>{{ $row->tanggal }}</td>
                    <td>{{ $row->supplier_code }}</td>
                    <td>{{ number_format($row->total) }}</td>
                    <td>{{ number_format($row->pcs) }}</td>
                    <td>{!! nl2br($row->detail) !!}</td>

                    <td>
                        <a href="{{ url('supplierPayment/edit/'.$row->id) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0)" onclick="deleteOrder({{$row->id}})" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
<script>

function deleteOrder(id) {
    if (confirm("Yakin Hapus Data?")) {
        window.location.href = "{{ url('supplierPayment/delete/') }}/"+id;
    }
}

</script>
@endsection