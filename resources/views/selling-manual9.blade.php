@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Form Penjualan Manual</h2>
    </div>
</div>
<div class="row">
    @csrf
    <div class="col-12">
        <input type="hidden" value="{{ $batch_no }}" name="batch_no">
        <table class="table table-striped tableFixHead">
            <thead>
                <tr>
                    <th width="10%">Tanggal Order</th>
                    <th width="10%">Instagram</th>
                    <th width="10%">Kode Internal</th>
                    <th width="10%">Variant</th>
                    <th width="5%">Size</th>
                    <th width="3%">Stok</th>
                    <th width="5%">Qty</th>
                    <th width="3%" style="white-space: nowrap;"></th>
                </tr>
            </thead>
            <tbody class="table-fill">
                <tr data-seq="1">
                    <td>
                        <input type="datetime-local" value="{{date('Y-m-d H:i:s')}}" name="tanggal" class="form-control">
                    </td>
                    <td>
                        <input type="text" name="instagram" required="required" class="form-control">
                        <input type="hidden" value="Order Manual by {{ \Session::get('fullname') }}" name="comment" class="form-control">
                    </td>
                    <td>
                        <select name="internal_code" onchange="updateVariant(this)" class="form-control" required="required">
                            <option value="" disabled="disabled" selected="selected"></option>
                            @foreach($product_main as $row)
                            <option value="{{$row->internal_code}}">{{ $row->internal_code }} - {{ $row->product_name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                    <select name="variant" onchange="updateSize(this)" class="uppercase form-control" required="required"></select>
                </td>
                <td>
                    <select name="size" onchange="getStock(this)" class="uppercase form-control" required="required"></input>
                    </td>
                    <td><span class="max-qty">0</span></td>
                    <td>
                        <input type="number" class="form-control" min="0" name="qty" required="required">
                    </td>
                    <td><button class="btn btn-danger btn-sm" onclick="deleteRow(this);"><i class="fas fa-trash"></i></button>
                    <button class="btn btn-sm btn-primary btn-save" data-seq='0' onclick="saveSelling(this)"><i class="fas fa-save"></i></button></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-4">
        <button class="btn btn-primary form-control" onclick="addTableRow()"><i class="fas fa-plus"></i> &nbsp; Tambah Baris</button>
    </div>
    <div class="col-4"></div>
    <div class="col-4">
        <button class="btn btn-primary form-control" onclick="saveAllSelling()"><i class="fas fa-save"></i> &nbsp; Simpan Semua Order</button>
        <a class="btn btn-success form-control" href="{{ url('selling/compare-stock/'.$batch_no) }}"><i class="fas fa-search"></i> &nbsp; Cek Stok</a>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    var tablerow = '';
    $(document).ready(function() {
        console.log('document is ready');
        tablerow = $('.table-fill').html();
    });
    function addTableRow(){
        event.preventDefault();
        var last_seq = $('.table-fill tr').last().attr('data-seq');
        // add new row
        $('.table-fill').append(tablerow);
        $('.table-fill tr').last().removeData('seq').attr('data-seq', parseInt(last_seq) + 1);
        $('.btn-save').last().removeData('seq').attr('data-seq', parseInt(last_seq) + 1);
    }
    function deleteRow(thisid) {
        event.preventDefault();
        $(thisid).closest('tr').remove();
    }
    function saveSelling(thisid) {
        event.preventDefault();
        var sequence = $(thisid).closest('tr').attr('data-seq');
        var instagram = $('tr[data-seq='+sequence+'] input[name=instagram]').val();
        var internal_code = $('tr[data-seq='+sequence+'] select[name=internal_code]').val();
        var tanggal = $('tr[data-seq='+sequence+'] input[name=tanggal]').val();
        var qty = $('tr[data-seq='+sequence+'] input[name=qty]').val();
        var size = $('tr[data-seq='+sequence+'] select[name=size]').val();
        var variant = $('tr[data-seq='+sequence+'] select[name=variant]').val();
        var comment = $('tr[data-seq='+sequence+'] input[name=comment]').val();
        var batch_no = $('input[name=batch_no]').val();
        console.log(internal_code);
        if (instagram == '') {
            alert('Kolom instagram tidak boleh kosong');
            return;
        }
        if (internal_code == null) {
            alert('Kolom kode internal tidak boleh kosong');
            return;
        }
        if (variant == null) {
            alert('Kolom variant tidak boleh kosong');
            return;
        }
        if (size == null) {
            alert('Kolom size tidak boleh kosong');
            return;
        }
        if (qty == '') {
            alert('Kolom qty tidak boleh kosong');
            return;
        }

        var fd = new FormData();
        fd.append('instagram', instagram);
        fd.append('internal_code', internal_code);
        fd.append('tanggal', tanggal);
        fd.append('qty', qty);
        fd.append('variant', variant);
        fd.append('size', size);
        fd.append('comment', comment);
        fd.append('batch_no', batch_no);

        var ajax_url = "{{ url('api/save_selling') }}";
        $.ajax({
            method: 'post',
            url: ajax_url,
            processData: false,
            contentType: false,
            cache: false,
            enctype: 'multipart/form-data',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            success: function(result) {
                if (result == "ok") {
                    $('tr[data-seq='+sequence+'] input').attr('disabled', 'disabled');
                    $('tr[data-seq='+sequence+'] select').attr('disabled', 'disabled');
                    $('tr[data-seq='+sequence+'] button').attr('disabled', 'disabled');
                }
            }
        });
    }
    function saveAllSelling() {
        event.preventDefault();
        $('.btn-save:enabled').trigger('click');
    }
    function updateVariant(thisid) {
        event.preventDefault();
        var sequence = $(thisid).closest('tr').attr('data-seq');
        var internal_code = $('tr[data-seq='+sequence+'] select[name=internal_code] option:selected').val();
        var ajax_url = "{{ url('api/get-variant') }}/"+internal_code;
        console.log(ajax_url);
        $.ajax({
            url: ajax_url,
            method: "get",
            datatype: "json",
            success: function(result) {
            result = JSON.parse(result);
                console.log(result);
                var option = "<option value='' disabled='disabled' selected='selected'>-- Pilih Variant</option>";
                $.each(result, function(index, value){
                    option += "<option value='"+value.variant+"'>"+value.variant+"</option>";
                });
                $('tr[data-seq='+sequence+'] select[name=variant]').html(option);
            }
        });
    }
    function updateSize(thisid) {
        event.preventDefault();
        var sequence = $(thisid).closest('tr').attr('data-seq');
        var internal_code = $('tr[data-seq='+sequence+'] select[name=internal_code] option:selected').val();
        var variant = $('tr[data-seq='+sequence+'] select[name=variant] option:selected').val();
        var ajax_url = "{{ url('api/get-size') }}/"+internal_code+"/"+variant;
        console.log(ajax_url);
        $.ajax({
            url: ajax_url,
            method: "get",
            datatype: "json",
            success: function(result) {
                result = JSON.parse(result);
                console.log(result);
                var option = "<option value='' disabled='disabled' selected='selected'>-- Pilih Variant</option>";
                $.each(result, function(index, value){
                    option += "<option value='"+value.size+"'>"+value.size+"</option>";
                });
                $('tr[data-seq='+sequence+'] select[name=size]').html(option);
            }
        });
    }

    function getStock(thisid) {
        event.preventDefault();
        var sequence = $(thisid).closest('tr').attr('data-seq');
        var internal_code = $('tr[data-seq='+sequence+'] select[name=internal_code] option:selected').val();
        var variant = $('tr[data-seq='+sequence+'] select[name=variant] option:selected').val();
        var size = $('tr[data-seq='+sequence+'] select[name=size] option:selected').val();
        var ajax_url = "{{ url('api/product') }}/"+internal_code+"/"+variant+"/"+size;
        console.log(ajax_url);
        $.ajax({
            url: ajax_url,
            method: "get",
            datatype: "json",
            success: function(result) {
            result = JSON.parse(result);
            console.log(result);
                $('tr[data-seq='+sequence+'] input[name=qty]').attr('max', result[0].qty);
                $('tr[data-seq='+sequence+'] .max-qty').text(result[0].qty);
            }
        });
    }
</script>
@endsection