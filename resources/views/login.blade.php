<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
        <meta name="author" content="AdminKit">
        <meta name="keywords" content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">
        <meta name="csrf-token" content="{{ Session::token() }}"> 
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="shortcut icon" href="{{ url('public/assets/img/icons/icon-48x48.png') }}" />
        <title>Manmelive Admin</title>
        <link href="{{ url('public/assets/css/app.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/b71ce7388c.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
        <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
  <style type="text/css">
    select.form-control {
        color: #585858;
    }
    .table-icon {
      font-size: 20px;
      padding: 2px 3px;
      border-radius: 3px;
    }
    .login-panel {
      width: 100%;
    }
    .navbar-brand img {
      height: auto !important;
    }
    .form-control {
      margin-bottom: 10px;
    }
  </style>
</head>
<body>
  <div class="row">
    <div class="col"></div>
    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
      <div style="display: block; height: 20vh"></div>
     <form action="login" method="post">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <div class="card">
        <div class="card-body">
          <h4>Login Admin</h4>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" required="required">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" required="required">
          </div>
          <button type="submit" class="form-control btn btn-success">Submit</button>
        </div>
      </div>
     </form>
    </div>
    <div class="col"></div>
  </div>
</body>

</html>
