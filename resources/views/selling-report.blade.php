@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Selling Order</h2>
    </div>
    <div class="col-12 table-scroll-x">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Order Batch</th>
                    <th>Tanggal</th>
                    <th>Total Order</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order as $row)
                <tr>
                    <td>{{ $row->batch_no }}</td>
                    <td>{{ $row->tanggal }}</td>
                    <td>{{ $row->total_order }}</td>
                    <td>
                        {{-- <a href="{{ url('selling/show').'/'.$row->batch_no }}" class="btn btn-sm btn-primary"><i class="far fa-eye"></i> &nbsp; Lihat Order</a> --}}
                        <a class="btn btn-sm btn-success" href="{{ url('selling/compare-stock/'.$row->batch_no) }}"><i class="fas fa-search"></i> &nbsp; Cek Stok</a>
                        <a class="btn btn-sm btn-danger" onclick="deleteOrder({{ $row->batch_no }})" href="javascript:void(0)"><i class="fas fa-trash"></i> &nbsp; Hapus</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script>
    function deleteOrder(id) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = "{{ url('selling/delete') }}/"+id;
        }
    }
</script>
@endsection