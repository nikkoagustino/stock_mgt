@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Edit Berdasarkan Kode</h2>
    </div>
</div>
<div class="row">
    @csrf
    <div class="col-12">
        
        <table class="table table-striped tableFixHead">
            <thead>
                <tr>

                    <th width="10%">Kode Internal</th>
                    
                    <th width="3%" style="white-space: nowrap;"></th>
                </tr>
            </thead>
            <tbody class="table-fill">
                <tr data-seq="1">
                    
                    <td>
                        <input type="hidden" name="internal_code">
                        <input list="internal_code_list" name="internal_code_list"  class="form-control">
                        
                        <datalist id="internal_code_list" name="internal_code_list">
                            <option value="" disabled="disabled" selected="selected"></option>
                            @foreach($product_main as $row)
                            <option value="{{ $row->internal_code }} - {{ $row->product_name }}"></option>
                            @endforeach
                        </datalist>
                    </td>
                    
                    <td>
                    <button class="btn btn-sm btn-primary btn-save" data-seq='0' onclick="Buka()"><i class="fas fa-save">Buka</i></button></td>
                </tr>
            </tbody>
        </table>
    </div>
    
</div>



@endsection

@section('script')
<script>
   function Buka(){
        var internal_code = $('input[name=internal_code_list]').val().split(' - ')[0];
        if (internal_code=="" || internal_code==null){
        } else {
            window.location.href='{{ url('stock/editkode').'/' }}' + internal_code;
        }
        //if ($('select[name=internal_code]').val()=="" || $('select[name=internal_code]').val()==null){
        //} else {
        //    window.location.href='{{ url('stock/editkode').'/' }}' + $('select[name=internal_code]').val();
        //}
    }
</script>
@endsection