@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>List Masalah / Refund</h2>
    </div>
    <div class="col-12 table-scroll-x">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Tanggal Update</th>
                    <th>Nomor Laporan</th>
                    <th>Instagram</th>
                    <th>Nomor Order</th>
                    <th>Alasan</th>
                    <th>Status Masalah</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($refund_list as $row)
                <tr>
                    <td>{{ $row->tanggal_update }}</td>
                    <td>{{ $row->refund_master_id }}</td>
                    <td>{{ $row->instagram }}</td>
                    <td>{{ $row->payment_id }}</td>
                    <td>{{ $row->alasan }}</td>
                    <td>{{ $row->last_step_detail }}</td>
                    <td>
                        @if ($row->last_step_no == 3)
                        <a href="{{ url('refund/manage').'/'.$row->refund_master_id }}" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> &nbsp; Lihat Laporan Refund</a>
                        @else
                        <a href="{{ url('refund/manage').'/'.$row->refund_master_id }}" class="btn btn-sm btn-danger"><i class="fas fa-edit"></i> &nbsp; Lanjutkan Proses Refund</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script>
    function deleteOrder(id) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = "{{ url('selling/delete') }}/"+id;
        }
    }
</script>
@endsection