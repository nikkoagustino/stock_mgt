@extends('template')
@section('content')

<div class="row p-5">
    <div >
        <div  >
            <h3>Rekap Stok & Penjualan Keseluruhan</h3>
            <?php date_default_timezone_set('Asia/Jakarta');?>
            List Kode : {{ $kode . ' - ' . date(now())}}
        </div>
    </div>
    <?php $ttlstok=0;
          $ttlvaluasi=0;?>
        <div class="col-12">
        <br>
        <table>
            <tr>
                <td>
                            Total Item Sudah Terjual       :
                    <br>
                    {{-- <input type="text" readonly="readonly" value="{{ $valuasi->total_item_stock }}" class="" name="totitem"> --}}
                    <b class="fs-4"><a href="../suppliersales/{{ $kode }}">{{ number_format($penjualan->itemsales, 0) }}</a></b>
                    <br>
                    Penjualan Semua Item : 
                    <br>
                    {{-- <input type="text" readonly="readonly" value="{{ $valuasi->total_valuasi_stock }}" class="" name="totval"> --}}
                    <b class="fs-4">{{ number_format($penjualan->totalsales, 0) }}</b>
                    <br>
                </td>

                <td width="100">
                    
                    </td>

                <td>
                            Total Item Di Stok         :
                    <br>
                    <b class="fs-4"><a href="../supplierstok/{{ $kode }}">{{ number_format($valuasi->total_item_stock, 0) }}</a></b>
                    <br>
                    Valuasi Semua Stok : 
                    <br>
                    <b class="fs-4">{{ number_format($valuasi->total_valuasi_stock, 0) }}</b>
                    <br>

                </td>

                <td width="100">
                    
                </td>
           
                <td>
                            Total Item Di Titipkan       :
                    <br>
                    {{-- <input type="text" readonly="readonly" value="{{ $valuasi->total_item_stock }}" class="" name="totitem"> --}}
                    <b class="fs-4">{{ number_format($valuasi->total_item_stock + $penjualan->itemsales, 0) }}</b>
                    <br>
                    Valuasi Semua Item : 
                    <br>
                    {{-- <input type="text" readonly="readonly" value="{{ $valuasi->total_valuasi_stock }}" class="" name="totval"> --}}
                    <b class="fs-4">{{ number_format($valuasi->total_valuasi_stock+ $penjualan->totalsales, 0) }}</b>
                    <br>

                </td>
            </tr>
            <tr height="20">
            </tr>
            <tr>
                <td>
                            Jumlah Item Yang Sudah Di Bayar       :
                    <br>
                    <b class="fs-4">{{ number_format(($pembayaran->pcs ?? 0), 0) }}</b>
                    <br>
                    Penjualan Semua Item : 
                    <br>
                    <b class="fs-4">{{ number_format(($pembayaran->total ?? 0), 0) }}</b>
                    <br>
                </td>
            </tr>
            <tr height="20">
            </tr>
            <tr>
                <td>
                            Sisa Laku Yang Belum Di Bayar       :
                    <br>
                    <b class="fs-4">{{ number_format($penjualan->itemsales-($pembayaran->pcs ?? 0), 0) }}</b>
                    <br>
                    Estimasi Kekurangan(Estimasi Saja) : 
                    <br>
                    <b class="fs-4">{{ number_format($penjualan->totalsales-($pembayaran->total ?? 0), 0) }}</b>
                    <br>
                </td>
            </tr>
        </table>
        
            
            
        </div>
    
</div>       
@endsection
@section('script')
<script>

    $(document).ready(function(){
        $('input[name=totitem]').val($ttlstok);
        $('input[name=totvaluasi]').val($ttlvaluasi);
    });
</script>
@endsection