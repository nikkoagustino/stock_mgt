@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Laporan Penjualan {{ $header }}</h1>
        <button class="btn btn-success noprint" onclick="window.print()"><i class="fas fa-print"></i> &nbsp; Print</button>
    </div>
</div>
<div class="row">
    <div class="col-12 table-scroll-x">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Waktu Transaksi</th>
                    <th>Order No</th>
                    <th>Instagram</th>
                    <th>Total Bayar</th>
                    <th>Payment Status</th>
                    <th>Order Status</th>
                    <th>Jumlah Item</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sales_data as $row)
                <tr>
                    <td>{{ date('d/m/Y H:i', strtotime($row->transaction_time)) }}</td>
                    <td><a href='https://momavel.id/selling/show-paid/{{ $row->payment_id }}'>{{ $row->payment_id }}</a></td>
                    <td><a href='https://instagram.com/{{ $row->instagram }}' target="_blank">{{ $row->instagram }}</a></td>
                    <td style="text-align: right">{{ number_format($row->amount) }}</td>
                    <td>{{ $row->transaction_status }}</td>
                    <td>{{ $row->order_status }}</td>
                    <td style="text-align: right">{{ $row->qty }} pcs</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection