@extends('template')
@section('content')
<?php foreach ($stock as $row) {
    $product_name[$row->internal_code] = $row->product_name;    
} 
$totalord=0;
?>
<div class="row">
    <div class="col-13">
        <h2>List Order</h2>
    </div>
</div>
<div class="row">
    <div class="col-13 col-sm-8">
        <div class="table-scroll-x">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Kode Internal</th>
                    <th>Nama Produk</th>
                    <th style="width: 150px">Variant</th>
                    <th style="width: 100px">Size</th>
                    <th style="width:60px">Qty</th>
                    <th style="width:60px">Price</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($item_order as $row)
                <tr data-seq='{{$row->id}}'>
                    <td>{{ $row->internal_code }}</td>
                    <td>{{ (isset($product_name[$row->internal_code])) ? $product_name[$row->internal_code] : '' }}</td>
                    <td>{{ $row->variant }}</td>
                    <td>{{ $row->size }}</td>
                    <td>{{ $row->qty }}</td>
                    <td>{{ number_format($row->selling_price) }}</td>
                    <?php $totalord = $totalord + ($row->qty * $row->selling_price); ?>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
    <div class="col-12 col-sm-4">
        <h4>Data Pembayaran</h4>
        <div class="row">
            <div class="col-4">
                Tanggal Order
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ $payment->order_date }}">
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Total Order
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="<?php echo number_format($totalord); ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Biaya Kirim /  Lain
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="<?php echo number_format($payment->amount-$totalord); ?>">
            </div>
        </div>
        
        <div class="row">
            <div class="col-4">
                Jumlah Bayar
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ number_format($payment->amount) }}">
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Metode Pembayaran
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ $payment->payment_type }}">
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Status Pembayaran
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ $payment->transaction_status }}">
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                Alamat Pengiriman
            </div>
            <div class="col-8">
                {!! nl2br($payment->shipping_address) !!}    <br>&nbsp;            
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                No. Resi
            </div>
            <div class="col-8">
                <input type="text" class="form-control" disabled="disabled" value="{{ $payment->awb }}">
            </div>
        </div>
    </div>
</div>
@endsection