@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Stock In Form</h1>
    </div>
</div>
    <div class="row">
        <div class="col-7 form">
            <div class="row">
                <div class="col-4">
                    Supplier
                </div>
                <div class="col-8">
                    <select name="supplier_code" class="form-control fill_supplier">
                        <option value="" disabled="disabled" selected="selected"></option>
                        @foreach ($supplier as $row)
                        <option value="{{ $row->supplier_code }}">[{{ $row->supplier_code }}] {{ $row->supplier_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Kode External <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input name="external_code" required="required" type="text" class="form-control fill_external_code" placeholder="Code"></input>
                    <i class="msg_external_green" style="color:green; display: none;">External Code Sudah Terdaftar</i>
                    <i class="msg_external_red" style="color:red; display: none;">External Code Belum Terdaftar</i>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Kode Internal <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input name="internal_code" type="text" required="required" readonly="readonly" class="form-control" placeholder="Code"></input>
                    <i class="msg_internal_green" style="color:green; display: none;">Internal Code Sudah Terdaftar</i>
                    <i class="msg_internal_red" style="color:red; display: none;">Internal Code Belum Terdaftar</i>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Nama Produk <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input name="product_name" type="text" required="required" class="form-control fill_product_name" placeholder="Product Name"></input>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Kategori <span class="required">*</span>
                </div>
                <div class="col-8">
                    <select name="category" required="required" class="form-select form-control">
                        @foreach ($category as $row)
                        <option value="{{ $row->category_name }}">{{ $row->category_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Harga Supplier <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input name="supplier_price" type="number"  min="1000" step="100" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required="required" class="form-control fill_supplier_price"></input>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Harga Jual <span class="required">*</span>
                </div>
                <div class="col-8">
                    <input name="selling_price" type="number"  min="1000" step="100" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required="required" class="form-control fill_selling_price"></input>
                </div>
            </div>
            
            <div class="row">
                <div class="col-4">
                    Harga Asli (Jgn di isi kl ga Promo) 
                </div>
                <div class="col-8">
                    <input name="hargaasli" type="number"  min="1000" step="100" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  class="form-control"></input>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Deskripsi
                </div>
                <div class="col-8">
                <textarea name="deskripsi" placeholder="Deskripsi Produk Harap Di Isi Detil"  class="form-control"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Branded(Ga masuk tiktok) :
                </div>
                <div class="col-8">
                    <select name="branded" class="form-control">
                    <option value="Tdk">Tdk</option>
                        <option value="Ya">Ya</option>
                        
                        
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Masuk Landing Page :
                </div>
                <div class="col-8">
                    <select name="diskon" class="form-control">
                        <option value="Tidak">Tidak</option>
                        <option value="Ya">Ya</option>
                        
                    </select>
                </div>
            </div>
            
            <div class="row">
                <div class="col-4">
                    Tampilkan di Store? <span class="required">*</span>
                </div>
                <div class="col-8">
                    <select name="visibility" required="required" class="form-control">
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Foto Master
                </div>
                <div class="col-8">
                <input type="file" accept="image/*;capture=camera" onchange="uploadMasterImage()" name="master_image" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-1"></div>
        <div class="col-4">
            <img style="max-height: 250px" src="{{ url('public/storage/no_image.png') }}" class="master_image_frame" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="10%">Variant</th>
                        <th width="10%">Size</th>
                        <th width="10%">Keterangan</th>
                        <th width="7%">Qty Saat Ini</th>
                        <th width="7%">Qty Masuk</th>
                        <th colspan="2" width="10%">Gambar</th>
                        <th width="2%">Print Sticker</th>
                        <th width="5%"></th>
                    </tr>
                </thead>
                <tbody class="variant_table">
                    <tr data-seq='1'>
                        <td>
                            <select name="old_variant" class="form-control cbo_variant" onchange="selectVariant(this)">
                                <option value="" disabled="disabled" selected="selected"></option>
                                <option value="add_new">Add New Variant...</option>
                            </select>
                            <input class="form-control uppercase" name="variant" required="required" placeholder="Color? Model?" onchange="textVariant(this)" hidden="hidden"></input>
                        </td>
                        <td>
                            <select name="size" class="form-control cbo_size" onchange="selectSize(this)">
                                <option value="NOSIZE">NOSIZE</option>
                                <option value="ALLSIZE">ALLSIZE</option>
                                <option disabled="disabled">-------------</option>
                                <option value="3XS">3XS</option>
                                <option value="2XS">2XS</option>
                                <option value="XS">XS</option>
                                <option value="S">S</option>
                                <option value="M">M</option>
                                <option value="L">L</option>
                                <option value="XL">XL</option>
                                <option value="2XL">2XL</option>
                                <option value="3XL">3XL</option>
                                <option value="4XL">4XL</option>
                                <option value="5XL">5XL</option>
                                <option value="6XL">6XL</option>
                                <option disabled="disabled">-------------</option>
                                @for ($i = 25; $i < 45; $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </td>
                        <td><textarea name="keterangan" rows="3" class="form-control"></textarea></td>
                        <td>
                            <input class="form-control fill_qty" type="number" step="1" name="current_qty" value="0" readonly="readonly">
                        </td>
                        <td>
                            <input class="form-control" type="number" step="1" name="qty_in" min="0" value="0" required="required">
                        </td>
                        <td width="5%">
                            <img src="{{ url('public/storage/no_image.png') }}" class="fill_img" alt="">
                        </td>
                        <td width="5%">
                            <input class="form-control" type="file" accept="image/*;capture=camera" name="product_photo">
                        </td>
                        <td style="text-align: center">
                            <input type="checkbox" name="print_sticker" value="Y">
                        </td>
                        <td>
                            <button class="btn btn-primary btn-save" onclick="saveVariant(this)" data-seq='1'>Simpan</button>
                        </td>
                    </tr>
                </tbody>
                <tr>
                    <td>
                        <button class="form-control btn btn-primary btn-add-row"><i class="fas fa-plus"></i> Tambah Data</button>
                    </td>
                    <td colspan="4">
                    </td>
                    <td colspan="2">
                        <button class="form-control btn btn-primary" onclick="saveAll()"><i class="fas fa-save"></i> Simpan Semua</button>
                    </td>
                    <td colspan="2">
                        <button class="form-control btn btn-warning" onclick="printSticker()"><i class="fas fa-print"></i> Print Sticker</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
@section('script')
<style>
    input[type=checkbox] {
        width: 20px;
        height: 20px;
    }
    .form i {
    margin-top: -40px;
    float: right;
    font-size: 12px;
    margin-right: 10px;
    }
</style>
<script type="text/javascript">
    var tablerow = '';
$(document).ready(function() {
    console.log('document is ready');
    tablerow = $('.variant_table').html();

    $('input[name=internal_code]').on('change paste keyup', function() {
        checkData();
    });
    $('input[name=external_code]').on('change paste keyup', function() {
        checkExternalCode();
        generateInternalCode();
    });
    $('select[name=supplier_code]').on('change', function() {
        $('input[name=internal_code]').val($('select[name=supplier_code]').val());
        $('input').val('').removeAttr('readonly');
        $('input[type=number]').val(0);
        $('.fill_qty').val(0).attr('readonly', 'readonly');
    });

    $('.btn-add-row').on('click', function(){
        event.preventDefault();
        var last_seq = $('.variant_table tr').last().attr('data-seq');
        // add new row
        $('.variant_table').append(tablerow);
        $('.variant_table tr').last().removeData('seq').attr('data-seq', parseInt(last_seq) + 1);
        $('.btn-save').last().removeData('seq').attr('data-seq', parseInt(last_seq) + 1);
    });
});

function uploadMasterImage() {
    event.preventDefault();
    var external_code = $('input[name=external_code]').val();

    var files = $('input[name=master_image]').prop('files')[0]; 
    console.log(files);
    var ajax_url = "{{ url('api/save_master_image') }}";

    var fd = new FormData();
    fd.append('external_code', external_code);
    fd.append('master_image', files);


    $.ajax({
        method: 'post',
        url: ajax_url,
        processData: false,
        contentType: false,
        cache: false,
        enctype: 'multipart/form-data',
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        data: fd,
        success: function(result) {
            console.log(result);
            var new_src = "{{ url('/') }}/" + result.replace("public", "public/storage") + "";
            $('.master_image_frame').attr('src', new_src);
        }
    })
}

function saveAll() {
    $('.btn-save:enabled').trigger('click');
}

function checkExternalCode() {
    var external_code = $('input[name=external_code]').val();
    var ajax_url = "{{ url('api/check-external') }}/" + external_code;
    if (external_code != '') {
        $('input[name=master_image]').removeAttr('disabled');
    } else {
        $('input[name=master_image]').attr('disabled', 'disabled');
    }
    console.log(ajax_url);
    $.ajax({
        method: 'get',
        url: ajax_url,
        success: function(result) {
            result = JSON.parse(result);
            if (result == null) {
                $('input[name=internal_code]').val('');
                $('input[name=product_name]').val('').removeAttr('readonly');
                $('select[name=category]').val('');
                $('.msg_external_red').show();
                $('.msg_external_green').hide();
                $('input[name=supplier_price]').val('0');
                $('input[name=selling_price]').val('0');
                $('input[name=hargaasli]').val('0');
                $('textarea[name=deskripsi]').val('');
                $('input[name=diskon]').val('Tidak');


            var cbo_variant_option = '<option value="" disabled="disabled" selected="selected"></option>';
            
            cbo_variant_option += "<option value='add_new'>Add New Variant...</option>"
            $('.cbo_variant').html(cbo_variant_option);


                generateInternalCode();
            } else {
                $('.msg_external_red').hide();
                $('.msg_external_green').show();
                $('input[name=internal_code]').val(result.internal_code).attr('readonly', 'readonly').trigger('change');
                //$('select[name=supplier_code]').val(result.supplier_code).attr('readonly', 'readonly');
                $('select[name=category]').val(result.category);
                $('input[name=supplier_price]').val(result.supplier_price);
                $('input[name=hargaasli]').val(result.hargaasli);
                
                $('textarea[name=deskripsi]').val(result.deskripsi);
                $('input[name=diskon]').val(result.diskon);
            }
        }
    });
}

function generateInternalCode() {
    event.preventDefault();

    var product_code = $('select[name=supplier_code]').val();
    var ajax_url = "{{ url('api/generate') }}/" + product_code;
    console.log(ajax_url);
    $.ajax({
        method: 'get',
        url: ajax_url,
        success: function(result) {
            $('input[name=internal_code]').val(result).attr('readonly', 'readonly');
            $('input[name=product_name]').val('').removeAttr('readonly');
                $('.msg_internal_green').hide();
                $('.msg_internal_red').show();
        }
    });
}

function checkData() {
    event.preventDefault();
    var product_code = $('input[name=internal_code]').val();
    var ajax_url = "{{ url('api/master-product') }}/" + product_code;
    console.log(ajax_url);
    $.ajax({
        method: 'get',
        url: ajax_url,
        dataType: 'json',
        success: function(result) {
            if (result) {
                $('.fill_product_name').val(result.product_name);
                $('.fill_external_code').val(result.external_code);
                //$('.fill_supplier').val(result.supplier_code);
                $('.fill_supplier_price').val(result.supplier_price);
                $('.fill_selling_price').val(result.selling_price);
                $('select[name=category]').val(result.category);
                $('.msg_internal_green').show();
                $('.msg_internal_red').hide();
                $('.msg_external_red').hide();
                $('.msg_external_green').show();
                tablerow = $('.variant_table').html();
                getVariant(product_code);
                getMasterImage();
            } else {
                $('.msg_internal_green').hide();
                $('.msg_internal_red').show();
                $('.fill_supplier_price').val('0');
                $('.fill_selling_price').val('0');
                $('.fill_qty').val('0');
                $('.fill_img').attr('src', '{{ url('public/storage/no_image.png') }}');
                $('.master_image_frame').attr('src', '{{ url('public/storage/no_image.png') }}');
            }
        }
    });
}

function getVariant(internal_code) {

    var product_code = $('input[name=internal_code]').val();
    var ajax_url = "{{ url('api/get-variant') }}/" + internal_code;
    console.log(ajax_url);
    $.ajax({
        method: 'get',
        url: ajax_url,
        dataType: 'json',
        success: function(result) {
            var cbo_variant_option = '<option value="" disabled="disabled" selected="selected"></option>';
            if (result) {
                $.each(result, function(key, value) {
                    cbo_variant_option += "<option value='" + value.variant + "'>" + value.variant + "</option>";
                });
            }

            cbo_variant_option += "<option value='add_new'>Add New Variant...</option>"
            $('.cbo_variant').html(cbo_variant_option);
        }
    });
}

function getMasterImage() {
    var external_code = $(".fill_external_code").val();
    var ajax_url = "{{ url('api/get-master-image') }}/" + external_code;
    console.log(ajax_url);
    $.ajax({
        method: 'get',
        url: ajax_url,
            success: function(result) {
            console.log(result);
            $('.master_image_frame').attr('src', '{{ url('public/storage') }}' + result.replace('public/', '/'));
        }
    });
}


function saveVariant(thisid) {
    event.preventDefault();
    var sequence = $(thisid).attr('data-seq');
    var supplier_code = $('select[name=supplier_code]').val();
    var category = $('select[name=category]').val();
    var visibility = $('select[name=visibility]').val();
    var internal_code = $('input[name=internal_code]').val();
    var external_code = $('input[name=external_code]').val();
    var product_name = $('input[name=product_name]').val();
    var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val();
    var size = $('tr[data-seq='+sequence+'] select[name=size]').val();
    var keterangan = $('tr[data-seq='+sequence+'] textarea[name=keterangan]').val();
    var qty_in = $('tr[data-seq='+sequence+'] input[name=qty_in]').val();
    
    var supplier_price = $('input[name=supplier_price]').val();
    var branded = $('select[name=branded]').val();
    var selling_price = $('input[name=selling_price]').val();
    var diskon = $('select[name=diskon]').val();
    var hargaasli = $('input[name=hargaasli]').val();
    var deskripsi = $('textarea[name=deskripsi]').val();
    

    var files = $('tr[data-seq='+sequence+'] input[name=product_photo]').prop('files')[0]; 
    console.log(branded);
    var ajax_url = "{{ url('api/save_product') }}";

    var fd = new FormData();
    fd.append('internal_code', internal_code);
    fd.append('external_code', external_code);
    fd.append('product_name', product_name);
    fd.append('supplier_code', supplier_code);
    fd.append('category', category);
    fd.append('visibility', visibility);
    fd.append('variant', variant);
    fd.append('size', size);
    fd.append('keterangan', keterangan);
    fd.append('supplier_price', supplier_price);
    fd.append('selling_price', selling_price);
    fd.append('branded', branded);
    fd.append('diskon', diskon);
    fd.append('hargaasli', hargaasli);
    fd.append('deskripsi', deskripsi);
    fd.append('qty_in', qty_in);
    fd.append('product_photo', files);

    $.ajax({
        method: 'post',
        url: ajax_url,
        processData: false,
        contentType: false,
        cache: false,
        enctype: 'multipart/form-data',
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        data: fd,
        success: function(result) {
            console.log(result);
            $('tr[data-seq='+sequence+'] input').attr('disabled', 'disabled');
            $('tr[data-seq='+sequence+'] select').attr('disabled', 'disabled');
            $('tr[data-seq='+sequence+'] .btn-save').attr('disabled', 'disabled');
            $('tr[data-seq='+sequence+'] input[name=print_sticker]').removeAttr('disabled');
        }
    })
    
}

function selectVariant(thisid) {
    event.preventDefault();
    var sequence = $(thisid).closest('tr').attr('data-seq');
    var cbo_value = $(thisid).val();
    if (cbo_value == 'add_new') {
        $('tr[data-seq='+sequence+'] input[name=variant]').removeAttr('hidden');
    } else {
        $('tr[data-seq='+sequence+'] input[name=variant]').val(cbo_value).change();
    }
}

function textVariant(thisid) {
    var sequence = $(thisid).closest('tr').attr('data-seq');
    var internal_code = $('input[name=internal_code]').val();
    var variant = $(thisid).val();
    var size = $('tr[data-seq='+sequence+'] select[name=size]').val();
    var ajax_url = "{{ url('api/product') }}/" + internal_code + '/' + variant + '/' + size;
    updateProductData(ajax_url, sequence);
}

function selectSize(thisid) {
    var sequence = $(thisid).closest('tr').attr('data-seq');
    var internal_code = $('input[name=internal_code]').val();
    var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val();
    var size = $(thisid).val();
    var ajax_url = "{{ url('api/product') }}/" + internal_code + '/' + variant + '/' + size;
    updateProductData(ajax_url, sequence);
}

function updateProductData(ajax_url, sequence) {
    console.log(ajax_url);
    $.ajax({
        method: 'get',
        url: ajax_url,
        dataType: 'json',
        success: function(result) {
            if (result.length > 0) {
                $('tr[data-seq='+sequence+'] input[name=supplier_price]').val(result[0].supplier_price);
                //$('tr[data-seq='+sequence+'] input[name=selling_price]').val(result[0].selling_price);
                //$('tr[data-seq='+sequence+'] input[name=diskon]').val(result[0].diskon);
                //$('tr[data-seq='+sequence+'] input[name=hargaasli]').val(result[0].hargaasli);
                $('tr[data-seq='+sequence+'] input[name=current_qty]').val(result[0].qty);
                $('tr[data-seq='+sequence+'] textarea[name=keterangan]').val(result[0].keterangan);
                if (result[0].images != null) {
                    $('tr[data-seq='+sequence+'] img').attr('src', '{{ url('public/storage') }}' + result[0].images.replace('public/', '/'));
                } else {
                    $('tr[data-seq='+sequence+'] img').attr('src', '{{ url('public/storage/no_image.png') }}');
                }
            } else {
                $('tr[data-seq='+sequence+'] input[name=supplier_price]').val('0');
                //$('tr[data-seq='+sequence+'] input[name=selling_price]').val('0');
                //$('tr[data-seq='+sequence+'] input[name=diskon]').val('0');
                //$('tr[data-seq='+sequence+'] input[name=hargaasli]').val('0');
                
                $('tr[data-seq='+sequence+'] input[name=current_qty]').val('0');
                $('tr[data-seq='+sequence+'] textarea[name=keterangan]').val('');
                $('tr[data-seq='+sequence+'] img').attr('src', '{{ url('public/storage/no_image.png') }}');
            }
        }
    });
}

function printSticker() {
    event.preventDefault();
    var print_parameter = '';
    console.log('print triggered');
    $.each($('input[name=print_sticker]'), function(){
        var val = (this.checked ? "1" : "0");
        if (val == 1) {
            var sequence = $(this).closest('tr').attr('data-seq');
            var internal_code = $('input[name=internal_code]').val().toUpperCase();
            var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val().toUpperCase();
            var size = $('tr[data-seq='+sequence+'] select[name=size]').val();
            var qty_in = parseInt($('tr[data-seq='+sequence+'] input[name=qty_in]').val());
            var selling_price = parseInt($('tr[data-seq='+sequence+'] input[name=selling_price]').val());
            var external_code =($('input[name=external_code]').val()).toUpperCase();
            
            print_parameter += internal_code + '_' + variant + '_' + size + '_' + qty_in + '_' + selling_price + '_' + external_code + '|';
        }
    });
    window.open('{{ url('print/sticker') }}/'+print_parameter);
}

</script>
@endsection