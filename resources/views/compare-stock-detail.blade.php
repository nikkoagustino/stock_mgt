@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Detail Order</h2>
    </div>
    <div class="col-6">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Kode Internal</th>
                    <th>Supplier</th>
                    <th>Kode Eksternal</th>
                    <th>Variant</th>
                    <th>Size</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $query['internal_code'] }}</td>
                    <td>{{ $master->supplier_code }}</td>
                    <td>{{ $master->external_code }}</td>
                    <td>{{ $query['variant'] }}</td>
                    <td>{{ $query['size'] }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-6">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Instagram</th>
                    <th>Tanggal Order</th>
                    <th>Qty Order</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order as $row)
                <tr>
                    <td>{{ $row->instagram }}</td>
                    <td>{{ $row->tanggal }}</td>
                    <td>{{ $row->qty }}</td>
                    <td><button class="btn btn-sm btn-danger" onclick="deleteOrder('{{ $row->id }}')"><i class="fas fa-times"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section ('script')
<script type="text/javascript">
function deleteOrder(rowid) {
    event.preventDefault();
    if (confirm('Yakin hapus order ini?')) {
        window.location.href = '{{ url("delete-order") }}/'+rowid;
    }
}
</script>
@endsection