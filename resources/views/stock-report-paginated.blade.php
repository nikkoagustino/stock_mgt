@extends('template')
@section('content')
<?php 
$variant_array = [];
foreach ($stock as $row):
    if (isset($variant_array[$row->internal_code])) {
        array_push($variant_array[$row->internal_code], (array) $row);
    } else {
        $variant_array[$row->internal_code] = [(array) $row];
    }
endforeach;
?>
<div class="row">
    <div class="col-12 col-md-9">
        <h1>Stock Report</h1>
        <button class="btn btn-primary btn-reprint noprint" onclick="reprintSticker()"><i class="fas fa-tags"></i> Re-Print Stiker</button>
        <button class="btn btn-success noprint" onclick="window.print()"><i class="fas fa-print"></i> Print Laporan</button>
    </div>
    <div class="col-12 col-md-3 pt-5">
        <form action="{{ url('stock-report/search') }}" method="GET">
            @csrf
            <div class="row">
                <div class="col-8">
                    <input class="form-control" value="{{ $_GET['search_query'] ?? '' }}" type="text" name="search_query">
                </div>
                <div class="col-4">
                    <button class="btn form-control btn-warning"><i class="fas fa-search"></i> Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-12 table-scroll-x">
        <table class="table table-striped display">
            <thead>
                <tr>
                    <th width="5%">Kode Internal</th>
                    <th width="10%">Kode External</th>
                    <th width="20%">Nama Produk</th>
                    <th width="15%">Harga Supplier</th>
                    <th width="15%">Harga Jual</th>
                    <th width="15%">Harga Asli</th>
                    <th width="10%">Variant / Size</th>
                    <th width="10%">Keterangan</th>
                    <th width="5%">Qty</th>
                    <th width="5%">LDP</th>
                    <th width="5%">BRD</th>
                    <th width="5%" class="no-print"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($master as $row)
                @if (isset($row->internal_code) && ($row->internal_code != ""))
                <tr>
                    <td>
                        @if ($variant_array[$row->internal_code][0]['pin_to_top'] > 0)
                        <a style="color: orange;" href="{{ url('stock-report/pin') }}/{{ $row->internal_code }}/0">
                            <i style="color: orange" class="fas fa-thumbtack" data-bs-toggle="tooltip" data-bs-placement="top" title="Lepaskan Sematan"></i>
                        </a>
                        @else
                        <a style="color: inherit;" href="{{ url('stock-report/pin') }}/{{ $row->internal_code }}/1">
                            <i class="fas fa-thumbtack" data-bs-toggle="tooltip" data-bs-placement="top" title="Sematkan Produk Ini Di Atas"></i>
                        </a>
                        @endif
                        &nbsp;
                        {{ $row->internal_code }}
                    </td>
                    <td>{{ $row->external_code }}<br>Kategori: {{ $row->category }}</td>
                    <td>
                        <h5>{{ $row->product_name }}</h5>
                        <img style="cursor: pointer;" src="{{ (isset($row->master_image)) ? url('public').Storage::url($row->master_image) : url('public').Storage::url('public/no_image.png') }}"  onclick="changeSrc(this)" data-bs-toggle="modal" data-bs-target="#imageModal" width="100px">
                    </td>
                    <td>{{ $row->supplier_price }}</td>
                    <td>{{ $row->selling_price }}</td>
                    <td>{{ $row->hargaasli ?? 0 }}</td>
                    <td class="nowrap">
                    @foreach ($variant_array[$row->internal_code] as $child)
                    <div class="thumbnail">
                        <img style="cursor: pointer;" src="{{ (isset($child['images'])) ? url('public').Storage::url($child['images']) : url('public').Storage::url('public/no_image.png') }}"  onclick="changeSrc(this)" data-bs-toggle="modal" data-bs-target="#imageModal">
                    </div>
                    {{ $child['variant'] }} / {{ $child['size'] }}<br>
                    @endforeach
                    </td>
                    <td class="nowrap">
                    @foreach ($variant_array[$row->internal_code] as $child)
                    {{ $child['keterangan'] }}<br>
                    @endforeach
                    </td>
                    <td class="nowrap">
                    @foreach ($variant_array[$row->internal_code] as $child)
                    {{ $child['qty'] }}<br>
                    @endforeach
                    </td>
                    <td>{{ $row->diskon }}</td>
                    <td>{{ $row->branded }}</td>
                    <td class="nowrap noprint">
                    @foreach ($variant_array[$row->internal_code] as $child)
                        <input type="checkbox" name="reprint_sticker" class="noprint form-check-input" value="{{ $child['internal_code'] }}_{{ $child['variant'] }}_{{ $child['size'] }}_{{ $child['qty'] }}_{{ $child['selling_price'] }}_{{ $child['external_code'] }}">
                        <button class="btn btn-sm btn-warning" data-bs-toggle="tooltip" data-bs-placement="right" title="Edit" onclick="editProduct({{ $child['id'] }})"><i class="fas fa-edit"></i></button>
                        <button class="btn btn-sm btn-danger" data-bs-toggle="tooltip" data-bs-placement="right" title="Delete" onclick="deleteProduct({{ $child['id'] }})"><i class="fas fa-times-circle"></i></button> <br>
                        @endforeach
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
        {{ $master->appends($_GET)->links() }}
    </div>
</div>

<div class="modal" id="modalSticker" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reprint Sticker</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body table-scroll-x">
        <table class="table table-striped" style="width: 100%">
            <thead>
                <tr>
                    <th>Kode Internal</th>
                    <th>Kode Eksternal</th>
                    <th>Variant</th>
                    <th>Size</th>
                    <th>Qty Stock</th>
                    <th>Banyak Print</th>
                </tr>
            </thead>
            <tbody id="reprintTable"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="printSticker()">Print</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('script')
<script>
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
});

function editProduct(id) {
    window.location.href = '{{ url('stock/edit') }}/'+id;
}

function deleteProduct(id) {
    if (confirm('Yakin Hapus Data?')) {
        window.location.href = '{{ url('stock/delete') }}/'+id;
    }
}

function changeSrc(thisid) {
    var src_new = $(thisid).attr('src');
    $('.image_modal').attr("src", src_new);
}

function reprintSticker() {
    $('#reprintTable').empty();
    var tr = ''; var seq = 1;
    $('input[name=reprint_sticker]:checked').each(function(){
        var cbo_value = $(this).val();
        cbo_value = cbo_value.split('_');
        tr += '<tr data-seq="'+seq+'">' + 
                '<td><input type="text" style="width: 150px" readonly="readonly" name="internal_code" value="'+cbo_value[0]+'">' + 
                '<input type="hidden" name="selling_price" value="'+cbo_value[4]+'"></td>' + 
                '<td><input type="text" style="width: 200px" readonly="readonly" name="external_code" value="'+cbo_value[5]+'"></td>' + 
                '<td><input type="text" style="width: 150px" readonly="readonly" name="variant" value="'+cbo_value[1]+'"></td>' + 
                '<td><input type="text" style="width: 60px" readonly="readonly" name="size" value="'+cbo_value[2]+'"></td>' + 
                '<td><input type="number" readonly="readonly" style="width: 60px" name="qty" value="'+cbo_value[3]+'"></td>' + 
                '<td><input type="number" style="width: 60px" step="1" min="1" name="qty_print" value="'+cbo_value[3]+'"></td>' + 
                '</tr>';
        seq++;
        $('#reprintTable').html(tr);
    });
    $('#modalSticker').modal("show");
}

function printSticker() {
    event.preventDefault();
    var print_parameter = '';
    console.log('print triggered');
    $.each($('input[name=internal_code]'), function(){
            var sequence = $(this).closest('tr').attr('data-seq');
            var internal_code = $('tr[data-seq='+sequence+'] input[name=internal_code]').val().toUpperCase();
            var variant = $('tr[data-seq='+sequence+'] input[name=variant]').val().toUpperCase();
            var size = $('tr[data-seq='+sequence+'] input[name=size]').val();
            var qty_in = parseInt($('tr[data-seq='+sequence+'] input[name=qty_print]').val());
            var selling_price = parseInt($('tr[data-seq='+sequence+'] input[name=selling_price]').val());
            var external_code =($('tr[data-seq='+sequence+'] input[name=external_code]').val()).toUpperCase();
            
            print_parameter += internal_code + '_' + variant + '_' + size + '_' + qty_in + '_' + selling_price + '_' + external_code + '|';
    });
    window.open('{{ url('print/sticker') }}/'+print_parameter);
}
</script>

<style type="text/css">
    table {
        line-height: 30px;
    }
</style>
@endsection