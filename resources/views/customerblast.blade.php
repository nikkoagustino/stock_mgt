@extends('template')
@section('content')
<form action="{{ url('customerblastsend') }}" id="blastEmailForm" method="post">
    @csrf
<div class="row">
    <div class="col-13">
        <h2>List Customer Blast</h2>
        <br>
        Total: {{ number_format(count($list_order)) }}
        <br>
        <br>
        <table>
            <tr>
                
                <td>
                <input type="text" class="form-control" name = "pilihan" value ="{{ $pilihan }}" readonly="readonly">
                <a href="{{ url('customerselect') }}"class="form-control btn btn-success"><i class="fas fa-map-marker"></i> &nbsp; Pilih Seleksi Ulang</a>
                <a href="javascript:void(0)" onclick="SendWAMark()" class="form-control btn btn-success"><i class="fas fa-map-marker"></i> &nbsp; Kirim WA Marketing</a>
                <a href="javascript:void(0)" onclick="SendEmailMark()" class="form-control btn btn-success"><i class="fas fa-map-marker"></i> &nbsp; Kirim Email Marketing</a>
                <br>
                
                </td>
                <td width="100">

                </td>
                <td width="300">
                
                    <textarea  name="pesanwa" placeholder="Pesan WA" rows="5" class="form-control"></textarea>
                    
                <br>
                    
                </td>
                <td width="100">

                </td>
                <td width="300">
                    Subject: <input type="text" name="subjectemail" class="form-control" placeholder="Subject">
                    
                    <textarea  name="pesanemail"  placeholder="Pesan Email" rows="5" class="form-control"></textarea>
                    
                
                    
                </td>

                <td width="50">

                </td>
                <td>
                    Kirim : 
                    <select name="command" required="required" class="form-control">
                        
                        <option value="Kirim WA">Kirim WA</option>
                        <option value="Kirim Email">Kirim Email</option>
                    </select>
                    <button type="submit" id="submitButton" class="form-control btn btn-success">
                        <i class="fas fa-cash-register"></i> &nbsp; Kirim
                    </button>
                </td>
            </tr>
        </table>


    </div>
    <div class="col-13 table-scroll-x">
        <table class="table table-striped display nowrap datatable-list">
            <thead>
                <tr>
                    <th>Instagram</th>
                    <th>Telpon</th>
                    <th>Nama Depan</th>
                    <th>Nama Belakang</th>
                    <th>Email</th>
                    <th>Pembelian</th>
                    <th>Reffer</th>
                    <th>Unsub</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($list_order as $row)
                <tr>
                    <td><a href="{{ url('customer-edit').'/' .$row->username}}">{{ strtolower($row->username) }}</a></td>
                    <td>
                        <a href="http://wa.me/{{ $row->phone }}" target="_blank">{{ $row->phone }}</a>
                    </td>
                    <td>{{ $row->firstname }}</td>
                    <td>{{ $row->lastname }}</td>
                    <td>{{ $row->email }}</td>
                    <td>
                    {{ number_format($row->paidamount ??0) }}
                    </td>
                    <th>{{ $row->reffer }}</th>
                    <th>{{ $row->unsub }}</th>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- {{ $list_order->links('pagination::bootstrap-4') }} --}}
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form action="{{ url('order/save-awb') }}" method="post">
      <div class="modal-header">
        <h5 class="modal-title">Masukkan No. Resi Pengiriman</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
            @csrf
            <input type="text" name="payment_id" readonly="readonly" class="form-control">
            <input type="text" placeholder="No. Resi / AWB" name="awb" class="form-control">
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Simpan">
      </div>
        </form>
    </div>
  </div>
</div>
</form>
@endsection

@section('script')
<script>
    function changeID(new_id) {
        $('input[name="payment_id"]').val(new_id);
    }

    function deleteListOrder(instagram, tanggal) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = '{{ url('selling/delete') }}/'+instagram+'/'+tanggal;
        }
    }

    $(document).ready(function() {
        $('.datatable-list').DataTable( {
            'pageLength': 25,
            "order": [[ 0, "desc" ]]
        } );
    } );

    function SendWAMark() {
    
    
        window.location.href = "{{ url('whatsappsendmedia') }}/"+$('input[name=pilihan]').val();
}

function SendEmailMark() {
    
    
    window.location.href = "{{ url('emailsendmedia') }}/"+$('input[name=pilihan]').val();
}

    $('#submitButton').on('click', function(e){
        e.preventDefault();
        var cmd = $('select[name=command]').val();
        if (confirm('Yakin '+cmd+'?')) {
            // Validate Input
            if (cmd == 'Kirim WA') {
                if ($('textarea[name=pesanwa]') == '') {
                    console.log('empty pesanwa');
                    console.log($('textarea[name=pesanwa]').html());
                    alert('Pesan WA masih kosong!!!');
                    return(false);
                }
            } else if (cmd == 'Kirim Email') {

                if ($('input[name=subjectemail]').val() == '') {
                    console.log('empty subjectemail');
                    alert('Subject Email masih kosong!!!');
                    return(false);
                }
                if ($('textarea[name=pesanemail]') == '') {
                    console.log('empty pesanemail');
                    alert('Pesan Email masih kosong!!!');
                    return(false);
                }
            }
            console.log('submitting');
            $('#blastEmailForm').submit();
        }
    });
</script> 
@endsection