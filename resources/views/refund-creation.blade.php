@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col"></div>
            <div class="col-12 col-md-7">
                <h3>Form Pengembalian Barang</h3>
                <form action="{{ url('refund/update') }}" method="post">
                    @csrf
                     <input type="hidden" name="step_no" value="0">
                     <input type="hidden" name="step_detail" value="Laporan Refund Dibuat">
                     <input type="hidden" name="refund_master_id" value="RF-{{ time() }}">
                    Nomor Order
                    <input type="text" readonly="readonly" name="payment_id" class="form-control" value="{{ $payment->id }}">
                    Instagram
                    <input type="text" readonly="readonly" name="instagram" class="form-control" value="{{ $payment->instagram}}">
                    Alasan Pengembalian
                    <textarea name="alasan" required="required" rows="3" class="form-control"></textarea>
                    Pilih Barang Yang Bermasalah
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Kode Internal</th>
                                <th>Nama Produk</th>
                                <th>Variant</th>
                                <th>Size</th>
                                <th>Harga Jual</th>
                                <th>Qty Order</th>
                                <th>Qty Bermasalah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($item as $row)
                            <tr data-seq='{{ $row->id }}'>
                                <td>
                                    <input type="checkbox" class="checkbox_masalah" name="item_masalah[]" value="{{ $row->id }}">
                                </td>
                                <td>{{ $row->internal_code }}</td>
                                <td>{{ $master[$row->internal_code]['product_name'] }}</td>
                                <td>{{ $row->variant }}</td>
                                <td>{{ $row->size }}</td>
                                <td><span class="price">{{ number_format($master[$row->internal_code]['selling_price']) }}</span>
                                </td>
                                <td>{{ $row->qty }}</td>
                                <td><input type="number" min="1" max="{{ $row->qty }}" class="form-control qty_masalah" name="qty_masalah[{{ $row->id }}]" style="width: 50px" step="1" value="{{ $row->qty }}"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    Nominal Produk Yang Bermasalah
                    <input type="number" step="1" name="nominal" class="form-control" value="0">
                    <div class="row">
                        <div class="col-6"></div>
                        <div class="col-6">
                            <button class="btn btn-danger form-control"><i class="fas fa-save"></i> &nbsp; Buat Laporan</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col"></div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('.checkbox_masalah').on('click', function(){
            populateProblem();
        });
        $('.qty_masalah').on('change paste keyup', function() {
            populateProblem();
        });
    });

    function populateProblem() {
        var nominal = 0;
        $.each($('.checkbox_masalah'), function(){
            var val = (this.checked ? "1" : "0");
            if (val == 1) {
                var sequence = $(this).closest('tr').attr('data-seq');
                var qty_masalah = $('tr[data-seq='+sequence+'] input.qty_masalah').val();
                var selling_price = $('tr[data-seq='+sequence+'] span.price').text().replace(',', '').replace('.', '');
                nominal = nominal + (parseInt(qty_masalah) * parseInt(selling_price));
            }
        });
        $('input[name=nominal]').val(nominal);        
    }
</script>
@endsection