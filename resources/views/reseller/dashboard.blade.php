@extends('reseller/template')
@section('content')

                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                        
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-area me-1"></i>
                                        Grafik Penjualan
                                    </div>
                                    <div class="card-body">
                                    <canvas id="sellingChart" width="100%" height="40"></canvas>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar me-1"></i>
                                        Penjualan Hari Ini
                                    </div>
                                    <div class="card-body">
                                        Total Transaksi : 
                                        <br>
                                        <b class="fs-4">{{ $daily_tx }}</b>
                                        <br>
                                        Total Item         :
                                        <br>
                                        <b class="fs-4">{{ $daily_qty }}</b>
                                        <br>
                                        Valuasi Semua Item : 
                                        <br>
                                        <b class="fs-4">{{ number_format($daily_amount, 0) }}</b>
                                        <br>
                                    </div>
                                </div>
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar me-1"></i>
                                        Penjualan 30 hari terakhir
                                    </div>
                                    <div class="card-body">
                                        Total Transaksi : 
                                        <br>
                                        <b class="fs-4">{{ $monthly_tx }}</b>
                                        <br>
                                        Total Item         : 
                                        <br>
                                        <b class="fs-4">{{ $monthly_qty }}</b>
                                        <br>
                                        Valuasi Semua Item : 
                                        <br>
                                        <b class="fs-4">{{ number_format($monthly_amount, 0) }}</b>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
@endsection

@section('script')
<script>
var ctx = document.getElementById("sellingChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: {!! $chart_tgl !!},
    datasets: [{
      label: "Qty Sold",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0.2)",
      borderColor: "rgba(2,117,216,1)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(2,117,216,1)",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(2,117,216,1)",
      pointHitRadius: 50000,
      pointBorderWidth: 2,
      data: {{ $chart_qty }},
    },{
      label: "Total Sold",
      lineTension: 0.3,
      backgroundColor: "rgba(200,200,0,0.2)",
      borderColor: "rgba(200,200,0,1)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(200,200,0,1)",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(200,200,0,1)",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: {{ $chart_amount }},
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 30
        }
      }],
      yAxes: [{
        ticks: {
            min: 0,
          maxTicksLimit: 10
        },
        gridLines: {
          color: "rgba(0, 0, 0, .125)",
        }
      }],
    },
    legend: {
      display: false
    }
  }
});

</script>
@endsection