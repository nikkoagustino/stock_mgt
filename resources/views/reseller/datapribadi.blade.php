@extends('reseller/template')
@section('content')
<div class="col-12 col-md-8 p-4 border bg-light mx-auto">
   <center><h4>Perubahan Data Dropshipper / Reseller</h4></center>
   <hr>

    <div class="row">
		
        <div class="col-6">
		<form action="{{ url('reseller/editdata') }}" method="post">
    @csrf
		<h4>Detail Pengguna</h4>
			<input type="hidden" name="reffer" value="{{ \Session::get('reffer') }}">
			
			Login Name (Tanpa Spasi)
            <input type="text" value="{{ $datauser->username }}" class="form-control mb-2" readonly="readonly" required="required" name="instagram">

			
			
			

            
            <input type="hidden" name="user_status" value="register">
			
			<br>
            Nama Toko
            <input type="text" value="{{ $datauser->nama_toko }}"class="form-control mb-2" required="required" name="nama_toko">
            Nama Pemilik
            <input type="text" value="{{ $datauser->firstname }}" class="form-control mb-2" required="required" name="nama_depan">
            
            
            
			

            
            Email
            <input type="email" value="{{ $datauser->email }}" class="form-control mb-2" name="email">
            <br>
			<br>
            Telepon
            
            <input type="telepon" value="{{ $datauser->phone }}" readonly="readonly"  class="form-control mb-2" required="required" name="telepon">
			<div class="row">
                <div class="col-12">
                    <button type="submit" id="save button"  class="btn form-control mb-2 btn-warning"><i class="fas fa-user-plus"></i> Simpan Data Pribadi</button>
                </div>
                
            </div>
		</form>	
		<form action="{{ url('reseller/editpassword') }}" method="post">
			@csrf
		Password
			<input type="hidden" name="ig">
            <input type="password"  name="password" required="required" class="form-control mb-2">
			<div class="row">
                <div class="col-12">
                    <button type="submit" id="save button"  class="btn form-control mb-2 btn-warning"><i class="fas fa-user-plus"></i> Ganti Password</button>
                </div>
                
            </div>
		</form>	
        <form action="{{ url('reseller/editphoto') }}" enctype="multipart/form-data" method="post">
			@csrf
            <input type="hidden" name="igs">
            <a href="{{ url('public').\Storage::url($datauser->logo_toko) }}" target="_blank"><img style="height: 100px" src="{{ url('public').\Storage::url($datauser->logo_toko) }}" alt=""></a>
            <br>
            Logo Toko (Square 1:1 Max 1MB)
			<input type="file" name="slide_image" class="form-control mb-2" accept="image/png, image/gif, image/jpeg">
            <button type="submit" id="save button"  class="btn form-control mb-2 btn-warning"><i class="fas fa-user-plus"></i> Upload Logo Baru</button>
		</form>	
        </div>

        


        <div class="col-6">
		<form action="{{ url('reseller/editaddress') }}" method="post">
		@csrf
			<input type="hidden" name="nama_depan1">
			<input type="hidden" name="telepon1">
			<input type="hidden" name="email1">
			<input type="hidden" name="instagram1">
            <h4>Alamat Pengiriman</h4>
			{!! nl2br($datauser->shipping_address) !!}
            Provinsi
            <select name="province_id" class="form-control mb-2" required="required" onchange="loadCity()"></select>
            <input type="hidden" name="province">
            Kota / Kabupaten
            <select name="city_id" class="form-control mb-2" required="required" onchange="loadSubdistrict()"></select>
            <input type="hidden" name="city">
            Kecamatan
            <select name="subdistrict_id" class="form-control mb-2" required="required" onchange="loadOngkir()"></select>
            <input type="hidden" name="subdistrict">
            Alamat
            <textarea name="address" required="required" placeholder="Alamat Lengkap" rows="5" class="form-control mb-2"></textarea>
            <h4>Tarif Pengiriman ID Express</h4>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Jasa Pengiriman</th>
                        <th>Ongkir / kg</th>
                    </tr>
                </thead>
                <tbody class="choice_ongkir">
                </tbody>
            </table>
            <input type="hidden" name="shipping">
            <input type="hidden" name="shipping_cost">
            <div class="row">
                <div class="col-6">
                    <button type="submit" id="checkoutButton" disabled="disabled" class="btn form-control mb-2 btn-warning"><i class="fas fa-user-plus"></i> Ubah Alamat</button>
                </div>
                
            </div>
		</form>
        </div>
    </div>


</div>
@endsection
@section('script')
<script>

    $(document).ready(function(){
        loadProvince();
        
        $('input[name=igs]').val($('input[name=instagram]').val());
		$('input[name=password]').on("change keyup input",function(e) { 
            
               
                $('input[name=ig]').val($('input[name=instagram]').val());
                
            
            
        });




        $('input[name=instagram]').on("change keyup input",function(e) { 
            if (e.keyCode == 64 || e.keyCode == 32 ) {  
            return false;  
            }  else {
                var newValue = $('input[name=instagram]').val().replace('@','');
                $('input[name=instagram]').val(newValue);
                newValue = $('input[name=instagram]').val().replace(' ','');
                $('input[name=instagram]').val(newValue);
                
            }
            
        });


        $('input[name=telepon2]').bind('keypress', function (e) {  
            if (e.keyCode == 43 || e.keyCode == 32 || e.keyCode == 45|| e.keyCode == 46 || e.keyCode == 40 || e.keyCode == 41) {  
            return false;  
            } else {
                var newValu1 = $('input[name=telepon2]').val().replace(' ','');
                $('input[name=telepon2]').val(newValue1);
                newValue1 = $('input[name=telepon2]').val().replace('0','');
                $('input[name=telepon2]').val(newValue1);
            } 
            

        }); 



    });
    function loadProvince() {
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/province',
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var province = '<option disabled="disabled" selected="selected">-- Pilih Provinsi</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    province += '<option value="'+item.province_id+'">'+item.province+'</option>';
                });
                $('select[name=province_id]').html(province);
            }
        });
    }

    function loadCity() {
        var province_id = $('select[name=province_id]').find(':selected').val();
        console.log(province_id);
        var province_name = $('select[name=province_id]').find(':selected').text();
        $('input[name=province]').val(province_name);
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/city?province='+province_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var city = '<option disabled="disabled" selected="selected">-- Pilih Kota</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    city += '<option value="'+item.city_id+'">'+item.type+' '+item.city_name+'</option>';
                });
                $('select[name=city_id]').html(city);
            }
        });
    }

    function loadSubdistrict() {
        var city_id = $('select[name=city_id]').find(':selected').val();
        var city_name = $('select[name=city_id]').find(':selected').text();
        $('input[name=city]').val(city_name);
        $.ajax({
            method: 'POST',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/subdistrict?city='+city_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var subdistrict = '<option disabled="disabled" selected="selected">-- Pilih Kecamatan</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    subdistrict += '<option value="'+item.subdistrict_id+'">'+item.subdistrict_name+'</option>';
                });
                $('select[name=subdistrict_id]').html(subdistrict);
            }
        });
    }

    function loadOngkir() {

        var subdistrict_id = $('select[name=subdistrict_id]').find(':selected').val();
        var subdistrict_name = $('select[name=subdistrict_id]').find(':selected').text();
        $('input[name=subdistrict]').val(subdistrict_name);
        var origin_city = 155; // jakarta utara
        var postdata = 'origin='+origin_city+'&originType=city&destination='+subdistrict_id+'&destinationType=subdistrict&weight=1000&courier=ide';
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/cost',
                'method': 'post',
                'postdata' : postdata,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var tr = '';
                var response = JSON.parse(response);
                var shipping_cost = response.rajaongkir.results[0].costs[0].cost[0].value;
                var shipping = response.rajaongkir.results[0].costs[0].service+' '+response.rajaongkir.results[0].costs[0].description;
                 console.log(response);
                 // $.each(response.rajaongkir.results[0].costs, function(index, item){
                    tr += '<tr>'+
                    '<td>'+shipping+'</td>'+
                    '<td>'+shipping_cost+' /kg</td>'+
                    '</tr>'; 
                 // });
				 $('input[name=nama_depan1]').val($('input[name=nama_depan]').val());
				 $('input[name=telepon1]').val($('input[name=telepon]').val());
				 $('input[name=email1]').val($('input[name=email1]').val());
				 $('input[name=instagram1]').val($('input[name=instagram]').val());
                 $('.choice_ongkir').html(tr);
                 $('input[name=shipping]').val(shipping);
                 $('input[name=shipping_cost]').val(shipping_cost);
                 $('#checkoutButton').removeAttr('disabled');
                 
            }
        });
    }

    
</script>
@endsection