<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MomAvel Reseller Login</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="{{ url('public/assetsbs/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('public/assetsbs/font-awesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ url('public/assetsbs/css/form-elements.css') }}">
        <link rel="stylesheet" href="{{ url('public/assetsbs/css/style.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="{{ url('public/assetsarsha/img/fashion-logo.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ url('public/assetsarsha/img/fashion-logo.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ url('public/assetsarsha/img/fashion-logo.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ url('public/assetsarsha/img/fashion-logo.png') }}">
        <link rel="apple-touch-icon-precomposed" href="{{ url('public/assetsarsha/img/fashion-logo.png') }}">
		<script>$.backstretch("{{ url('public/assetsbs/img/backgrounds/1.jpg') }}");</script>
    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                	
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>MomAvel.id</strong> Dropship <br>&amp; Reseller Panel</h1>
                            <div class="description">
                            	<p>
	                            	Jadi bagian menjadi mitra <strong>MomAvel.id.</strong> Jual produk kami kemanapun. <br>Kami membuatnya jadi lebih mudah
	                            	menjadi dropshipper/reseller kami. <br><strong>Gabung Sekarang!!</strong> 
                            	</p>
                            </div>
                        </div>
                    </div>

					<div class="social-login">
	                        	
		                        	<a class="btn btn-link-1 btn-link-1-google-plus" href="reseller/register">
		                        		Daftar Disini Jadi Dropshipper / Reseller
		                        	</a>
	                        	</div>
	                        </div>


        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
          <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
          </symbol>
          <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
          </symbol>
          <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
          </symbol>
        </svg>

                    <form action="{{ url('reseller/login') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 text">
                        	
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible show" role="alert">
                                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                                  <strong>{{ $message }}</strong>
                                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                              </div>
                            @endif

                            @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-dismissible show" role="alert">
                                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                                  <strong>{{ $message }}</strong>
                                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                              </div>
                            @endif
                        	<div class="form-box">
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Login ke panel reseller</h3>
	                            		<p>Masukan informasi login:</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-key"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form role="form" action="" method="post" class="login-form">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-username">Username</label>
				                        	<input type="text" name="instagram" placeholder="Username..." class="form-username form-control" id="form-username">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-password">Password</label>
				                        	<input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
				                        </div>
				                        <button type="submit" class="btn">Masuk!</button>
				                    </form>
			                    </div>
		                    </div>
		                
		                	
	                        
                        </div>
                        
                        
                    </div>
                    </form>
                </div>
            </div>
            
        </div>

        <!-- Footer -->
        <footer>
        	<div class="container">
        		<div class="row">
        			
        			<div class="col-sm-8 col-sm-offset-2">
        				<div class="footer-border"></div>
        				<p>MomAvel Dropship / Reseller</p>
        			</div>
        			
        		</div>
        	</div>
        </footer>

        <!-- Javascript -->
		
        <script src="{{ url('public/assetsbs/js/jquery-1.11.1.min.js') }}"></script>
		
        <script src="{{ url('public/assetsbs/bootstrap/js/bootstrap.min.js') }}"></script>
		
        <script src="{{ url('public/assetsbs/js/jquery.backstretch.min.js') }}"></script>
		<script>$.backstretch("{{ url('public/assetsbs/img/backgrounds/1.jpg') }}");</script>
        <script src="{{ url('public/assetsbs/js/scripts.js') }}"></script>
		
		
        <!--[if lt IE 10]>
            <script src="{{ url('public/assetsbs/js/placeholder.js') }}"></script>
        <![endif]-->

    </body>

</html>