@extends('reseller/template')
@section('content')
<div class="row mt-3 p-5">
    <h5>Hi, {{ '@'.\Session::get('reseller') }}</h5>
    <div class="tableFixHead">

    <div class="col-12 p-4 border bg-light mx-auto">
    
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="list-order" role="tabpanel" aria-labelledby="list-order-tab">
                <h4>List Order</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nomor Order</th>
                            <th>Tanggal</th>
                            <th>Jumlah Pembayaran</th>
                            <th>Status Order</th>
                            <th>No. Resi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($paid_order as $row)
                        <tr>
                            <td><a href="selling/user-paid/{{ $row->id }}" class="border border-warning">{{ $row->id }}</a></td>
                            <td><a href="selling/user-paid/{{ $row->id }}" class="border border-warning">{{ $row->order_date }}</a></td>
                            <td><a href="selling/user-paid/{{ $row->id }}" class="border border-warning">{{ number_format($row->amount) }}</a></td>
                            @if ($row->order_status == 'pending')
                            <td>
                                Pembayaran Pending<br>
                                @if ($row->payment_type == 'Midtrans')
                                <button class="btn btn-warning" onclick="window.location.href='{{ url('pay-now').'/'.$row->id }}'">Bayar Sekarang</button>
                                @endif
                                <button onclick="window.location.href='{{ url('reseller-cancel-payment').'/'.$row->id }}';this.disabled=true;this.form.submit()" class="btn btn-sm btn-danger" data-bs-toggle="tooltip" data-bs-placement="top" title="Pembayaran Sering Kali Gagal karena tidak ada bank yg di pilih, Bisa di ulang kembali. Kembali ke cek pesanan untuk lakukan pembayaran ulang.">Ulangi Pembayaran (Jika Gagal)<i class="fas fa-times"></i></button>
                            </td>
                            @elseif ($row->order_status == 'paid')
                            <td>Standby Pengiriman</td>
                            @elseif ($row->order_status == 'delivery')
                            <td>Dikirim</td>
                            @endif
                            <td>
                                @if (isset($row->awb))
                                <a href="https://idexpress.com/lacak-paket/?actionType=delivery&waybillNo={{ $row->awb }}" target="_blank" class="border border-warning">{{ $row->awb }}</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            
        </div>
    </div>
    </div>
</div>
@include('userpage/add-address-modal')
@endsection
@section('script')
<style>
.tableFixHead          { overflow: auto; height: 80vh; }
@media only screen and (max-width: 768px) {
.tableFixHead th, .tableFixHead td {
font-size: 0.49rem;
}
}
.tableFixHead thead th { position: sticky; top: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.tableFixHead tfoot, .tableFixHead tfoot td, .tableFixHead tfoot th { position: -webkit-sticky; position: sticky; bottom: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.btn-success:disabled {
    background-color: #aaa;
    border: black;
}

</style>
<script>

    $(document).ready(function(){
        loadProvince();
        
		$('input[name=phone]').bind('keypress', function (e) {  
            if (e.keyCode == 43 || e.keyCode == 32 || e.keyCode == 45|| e.keyCode == 46 || e.keyCode == 40 || e.keyCode == 41) {  
            return false;  
            } else {
                var newValu1 = $('input[name=phone]').val().replace(' ','');
                $('input[name=phone]').val(newValue1);
                newValue1 = $('input[name=phone]').val().replace('0','');
                $('input[name=phone]').val(newValue1);
            } 
            

        }); 
    });



    function matchingPassword() {
        var pass1 = $('input[name=password]').val();
        var pass2 = $('input[name=confirm_password]').val();
        if (pass1 == pass2) {
            $('input[name=passw]').removeAttr('disabled');
            $('.passnomatch').hide();
        } else {
            $('input[type=passw]').attr('disabled', 'disabled');
            $('.passnomatch').show();
        }
    }
    function deleteAddress(address_id) {
        if (confirm('Yakin hapus alamat ini?')) {
            window.location.href="{{ url('user/delete-address') }}/"+address_id;
        }
    }
    function saveAdd(rowid) {
        
    var instagram1 = $('input[name=instagram]').val();
    var phone = $('input[name=phone]').val();
    var firstname = $('input[name=firstname]').val();
    var lastname = $('input[name=lastname]').val();
    var email = $('input[name=email]').val();
    
    var fd = new FormData();
    fd.append('instagram', instagram1.toLowerCase());
    fd.append('phone', phone);
    fd.append('firstname', firstname);
    fd.append('lastname', lastname);
    fd.append('email', email);

    

    $.ajax({
        url: '{{url("my-account/savephone")}}',
        method: 'post',
        processData: false,
        contentType: false,
        cache: false,
        data: fd,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
            
            if (result == 'ok') {

                location.reload();
            }
        }
    });

}
</script>
@endsection