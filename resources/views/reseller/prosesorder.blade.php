@extends('reseller/template')
@section('content')
<?php
$stock_array = [];
$repeat = [];
foreach ($stock as $row) {
    $stock_array[$row->internal_code][$row->variant][$row->size]['qty'] = $row->qty;
    $stock_array[$row->internal_code][$row->variant][$row->size]['selling_price'] = $row->selling_price;
    
      $product_name[$row->internal_code] = $row->product_name;    
}
foreach ($supplier as $row) {
    $repeat[$row->supplier_code] = $row->repeat_order;
}
?>
<form action="{{ url('resellerpayment') }}" method="post">
<div class="row">
    <div class="col-3 p-4 border bg-light">
            @csrf

            <input type="hidden" value="{{ $batch_no }}" name="batch_no">
            <?php date_default_timezone_set("Asia/Jakarta"); ?>
            
            <input type="hidden" value="Order Reseller By {{ \Session::get('reseller') }}" name="comments" class="form-control">
            <input type="hidden" value="{{date('Y-m-d H:i:s')}}" name="tglskrg">
            <input type="hidden" value="{{\Session::get('reffer')}}" name="reffer">
            <input type="hidden" name="order_id" value="{{ strtotime(now()) }}">
            <h6>Masukkan Username Instagram</h6>
            <input type="text" class="form-control" value="{{ $cart[0]->instagram }}" readonly="readonly" name="instagram">
            <h6>Masukkan Tanggal Pembelian</h6>
            <input type="text" class="form-control" value="{{ $cart[0]->tanggal }}" readonly="readonly" name="tanggal">


                <br>
                <div>
                    <h6><input type="radio" id="myadd" value ="myadd" name ="addtype" >Alamat Saya</h6>
                    <input type="hidden" value="{!! nl2br($custdata->shipping_address) !!}" name="alamatsaya">
                    <input type="hidden" value="{{ $custdata->email }}" name="emailsaya">
                    <input type="hidden" value="{{ $custdata->phone }}" name="teleponsaya">
                    <input type="hidden" value="{{ $custdata->firstname }}" name="namasaya">
                    <input type="hidden" value="{{ $custdata->subdistrict_id }}" name="subdistrict_idsaya">
                    
                    {!! nl2br($custdata->shipping_address) !!}
                </div>

                <br>
                <div>
                    
                    <h6><input type="radio" id="otheradd" value ="otheradd" name ="addtype" selected="true" checked>Alamat Penerima Lain</h6>
                    Nama 
                    <input type="text" required="required" value="" class="form-control" name="nama_depand">

                    Telepon
                    <select name="kodearea" class="form-control mb-2" required="required" onchange="">
                    
                    <option data-countryCode="ID" value="62">Indonesia (+62)</option>
                
                    <optgroup label="Other countries">
                        <option data-countryCode="DZ" value="213">Algeria (+213)</option>
                        <option data-countryCode="AD" value="376">Andorra (+376)</option>
                        <option data-countryCode="AO" value="244">Angola (+244)</option>
                        <option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
                        <option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
                        <option data-countryCode="AR" value="54">Argentina (+54)</option>
                        <option data-countryCode="AM" value="374">Armenia (+374)</option>
                        <option data-countryCode="AW" value="297">Aruba (+297)</option>
                        <option data-countryCode="AU" value="61">Australia (+61)</option>
                        <option data-countryCode="AT" value="43">Austria (+43)</option>
                        <option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
                        <option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
                        <option data-countryCode="BH" value="973">Bahrain (+973)</option>
                        <option data-countryCode="BD" value="880">Bangladesh (+880)</option>
                        <option data-countryCode="BB" value="1246">Barbados (+1246)</option>
                        <option data-countryCode="BY" value="375">Belarus (+375)</option>
                        <option data-countryCode="BE" value="32">Belgium (+32)</option>
                        <option data-countryCode="BZ" value="501">Belize (+501)</option>
                        <option data-countryCode="BJ" value="229">Benin (+229)</option>
                        <option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
                        <option data-countryCode="BT" value="975">Bhutan (+975)</option>
                        <option data-countryCode="BO" value="591">Bolivia (+591)</option>
                        <option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
                        <option data-countryCode="BW" value="267">Botswana (+267)</option>
                        <option data-countryCode="BR" value="55">Brazil (+55)</option>
                        <option data-countryCode="BN" value="673">Brunei (+673)</option>
                        <option data-countryCode="BG" value="359">Bulgaria (+359)</option>
                        <option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
                        <option data-countryCode="BI" value="257">Burundi (+257)</option>
                        <option data-countryCode="KH" value="855">Cambodia (+855)</option>
                        <option data-countryCode="CM" value="237">Cameroon (+237)</option>
                        <option data-countryCode="CA" value="1">Canada (+1)</option>
                        <option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
                        <option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
                        <option data-countryCode="CF" value="236">Central African Republic (+236)</option>
                        <option data-countryCode="CL" value="56">Chile (+56)</option>
                        <option data-countryCode="CN" value="86">China (+86)</option>
                        <option data-countryCode="CO" value="57">Colombia (+57)</option>
                        <option data-countryCode="KM" value="269">Comoros (+269)</option>
                        <option data-countryCode="CG" value="242">Congo (+242)</option>
                        <option data-countryCode="CK" value="682">Cook Islands (+682)</option>
                        <option data-countryCode="CR" value="506">Costa Rica (+506)</option>
                        <option data-countryCode="HR" value="385">Croatia (+385)</option>
                        <option data-countryCode="CU" value="53">Cuba (+53)</option>
                        <option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
                        <option data-countryCode="CY" value="357">Cyprus South (+357)</option>
                        <option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
                        <option data-countryCode="DK" value="45">Denmark (+45)</option>
                        <option data-countryCode="DJ" value="253">Djibouti (+253)</option>
                        <option data-countryCode="DM" value="1809">Dominica (+1809)</option>
                        <option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
                        <option data-countryCode="EC" value="593">Ecuador (+593)</option>
                        <option data-countryCode="EG" value="20">Egypt (+20)</option>
                        <option data-countryCode="SV" value="503">El Salvador (+503)</option>
                        <option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
                        <option data-countryCode="ER" value="291">Eritrea (+291)</option>
                        <option data-countryCode="EE" value="372">Estonia (+372)</option>
                        <option data-countryCode="ET" value="251">Ethiopia (+251)</option>
                        <option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
                        <option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
                        <option data-countryCode="FJ" value="679">Fiji (+679)</option>
                        <option data-countryCode="FI" value="358">Finland (+358)</option>
                        <option data-countryCode="FR" value="33">France (+33)</option>
                        <option data-countryCode="GF" value="594">French Guiana (+594)</option>
                        <option data-countryCode="PF" value="689">French Polynesia (+689)</option>
                        <option data-countryCode="GA" value="241">Gabon (+241)</option>
                        <option data-countryCode="GM" value="220">Gambia (+220)</option>
                        <option data-countryCode="GE" value="7880">Georgia (+7880)</option>
                        <option data-countryCode="DE" value="49">Germany (+49)</option>
                        <option data-countryCode="GH" value="233">Ghana (+233)</option>
                        <option data-countryCode="GI" value="350">Gibraltar (+350)</option>
                        <option data-countryCode="GR" value="30">Greece (+30)</option>
                        <option data-countryCode="GL" value="299">Greenland (+299)</option>
                        <option data-countryCode="GD" value="1473">Grenada (+1473)</option>
                        <option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
                        <option data-countryCode="GU" value="671">Guam (+671)</option>
                        <option data-countryCode="GT" value="502">Guatemala (+502)</option>
                        <option data-countryCode="GN" value="224">Guinea (+224)</option>
                        <option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
                        <option data-countryCode="GY" value="592">Guyana (+592)</option>
                        <option data-countryCode="HT" value="509">Haiti (+509)</option>
                        <option data-countryCode="HN" value="504">Honduras (+504)</option>
                        <option data-countryCode="HK" value="852">Hong Kong (+852)</option>
                        <option data-countryCode="HU" value="36">Hungary (+36)</option>
                        <option data-countryCode="IS" value="354">Iceland (+354)</option>
                        <option data-countryCode="IN" value="91">India (+91)</option>
                        
                        <option data-countryCode="IR" value="98">Iran (+98)</option>
                        <option data-countryCode="IQ" value="964">Iraq (+964)</option>
                        <option data-countryCode="IE" value="353">Ireland (+353)</option>
                        <option data-countryCode="IL" value="972">Israel (+972)</option>
                        <option data-countryCode="IT" value="39">Italy (+39)</option>
                        <option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
                        <option data-countryCode="JP" value="81">Japan (+81)</option>
                        <option data-countryCode="JO" value="962">Jordan (+962)</option>
                        <option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
                        <option data-countryCode="KE" value="254">Kenya (+254)</option>
                        <option data-countryCode="KI" value="686">Kiribati (+686)</option>
                        <option data-countryCode="KP" value="850">Korea North (+850)</option>
                        <option data-countryCode="KR" value="82">Korea South (+82)</option>
                        <option data-countryCode="KW" value="965">Kuwait (+965)</option>
                        <option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
                        <option data-countryCode="LA" value="856">Laos (+856)</option>
                        <option data-countryCode="LV" value="371">Latvia (+371)</option>
                        <option data-countryCode="LB" value="961">Lebanon (+961)</option>
                        <option data-countryCode="LS" value="266">Lesotho (+266)</option>
                        <option data-countryCode="LR" value="231">Liberia (+231)</option>
                        <option data-countryCode="LY" value="218">Libya (+218)</option>
                        <option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
                        <option data-countryCode="LT" value="370">Lithuania (+370)</option>
                        <option data-countryCode="LU" value="352">Luxembourg (+352)</option>
                        <option data-countryCode="MO" value="853">Macao (+853)</option>
                        <option data-countryCode="MK" value="389">Macedonia (+389)</option>
                        <option data-countryCode="MG" value="261">Madagascar (+261)</option>
                        <option data-countryCode="MW" value="265">Malawi (+265)</option>
                        <option data-countryCode="MY" value="60">Malaysia (+60)</option>
                        <option data-countryCode="MV" value="960">Maldives (+960)</option>
                        <option data-countryCode="ML" value="223">Mali (+223)</option>
                        <option data-countryCode="MT" value="356">Malta (+356)</option>
                        <option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
                        <option data-countryCode="MQ" value="596">Martinique (+596)</option>
                        <option data-countryCode="MR" value="222">Mauritania (+222)</option>
                        <option data-countryCode="YT" value="269">Mayotte (+269)</option>
                        <option data-countryCode="MX" value="52">Mexico (+52)</option>
                        <option data-countryCode="FM" value="691">Micronesia (+691)</option>
                        <option data-countryCode="MD" value="373">Moldova (+373)</option>
                        <option data-countryCode="MC" value="377">Monaco (+377)</option>
                        <option data-countryCode="MN" value="976">Mongolia (+976)</option>
                        <option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
                        <option data-countryCode="MA" value="212">Morocco (+212)</option>
                        <option data-countryCode="MZ" value="258">Mozambique (+258)</option>
                        <option data-countryCode="MN" value="95">Myanmar (+95)</option>
                        <option data-countryCode="NA" value="264">Namibia (+264)</option>
                        <option data-countryCode="NR" value="674">Nauru (+674)</option>
                        <option data-countryCode="NP" value="977">Nepal (+977)</option>
                        <option data-countryCode="NL" value="31">Netherlands (+31)</option>
                        <option data-countryCode="NC" value="687">New Caledonia (+687)</option>
                        <option data-countryCode="NZ" value="64">New Zealand (+64)</option>
                        <option data-countryCode="NI" value="505">Nicaragua (+505)</option>
                        <option data-countryCode="NE" value="227">Niger (+227)</option>
                        <option data-countryCode="NG" value="234">Nigeria (+234)</option>
                        <option data-countryCode="NU" value="683">Niue (+683)</option>
                        <option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
                        <option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
                        <option data-countryCode="NO" value="47">Norway (+47)</option>
                        <option data-countryCode="OM" value="968">Oman (+968)</option>
                        <option data-countryCode="PW" value="680">Palau (+680)</option>
                        <option data-countryCode="PA" value="507">Panama (+507)</option>
                        <option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
                        <option data-countryCode="PY" value="595">Paraguay (+595)</option>
                        <option data-countryCode="PE" value="51">Peru (+51)</option>
                        <option data-countryCode="PH" value="63">Philippines (+63)</option>
                        <option data-countryCode="PL" value="48">Poland (+48)</option>
                        <option data-countryCode="PT" value="351">Portugal (+351)</option>
                        <option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
                        <option data-countryCode="QA" value="974">Qatar (+974)</option>
                        <option data-countryCode="RE" value="262">Reunion (+262)</option>
                        <option data-countryCode="RO" value="40">Romania (+40)</option>
                        <option data-countryCode="RU" value="7">Russia (+7)</option>
                        <option data-countryCode="RW" value="250">Rwanda (+250)</option>
                        <option data-countryCode="SM" value="378">San Marino (+378)</option>
                        <option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
                        <option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
                        <option data-countryCode="SN" value="221">Senegal (+221)</option>
                        <option data-countryCode="CS" value="381">Serbia (+381)</option>
                        <option data-countryCode="SC" value="248">Seychelles (+248)</option>
                        <option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
                        <option data-countryCode="SG" value="65">Singapore (+65)</option>
                        <option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
                        <option data-countryCode="SI" value="386">Slovenia (+386)</option>
                        <option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
                        <option data-countryCode="SO" value="252">Somalia (+252)</option>
                        <option data-countryCode="ZA" value="27">South Africa (+27)</option>
                        <option data-countryCode="ES" value="34">Spain (+34)</option>
                        <option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
                        <option data-countryCode="SH" value="290">St. Helena (+290)</option>
                        <option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
                        <option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
                        <option data-countryCode="SD" value="249">Sudan (+249)</option>
                        <option data-countryCode="SR" value="597">Suriname (+597)</option>
                        <option data-countryCode="SZ" value="268">Swaziland (+268)</option>
                        <option data-countryCode="SE" value="46">Sweden (+46)</option>
                        <option data-countryCode="CH" value="41">Switzerland (+41)</option>
                        <option data-countryCode="SI" value="963">Syria (+963)</option>
                        <option data-countryCode="TW" value="886">Taiwan (+886)</option>
                        <option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
                        <option data-countryCode="TH" value="66">Thailand (+66)</option>
                        <option data-countryCode="TG" value="228">Togo (+228)</option>
                        <option data-countryCode="TO" value="676">Tonga (+676)</option>
                        <option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
                        <option data-countryCode="TN" value="216">Tunisia (+216)</option>
                        <option data-countryCode="TR" value="90">Turkey (+90)</option>
                        <option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
                        <option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
                        <option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
                        <option data-countryCode="TV" value="688">Tuvalu (+688)</option>
                        <option data-countryCode="UG" value="256">Uganda (+256)</option>
                        <option data-countryCode="GB" value="44">UK (+44)</option> -->
                        <option data-countryCode="UA" value="380">Ukraine (+380)</option>
                        <option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
                        <option data-countryCode="UY" value="598">Uruguay (+598)</option>
                        <option data-countryCode="US" value="1">USA (+1)</option> -->
                        <option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
                        <option data-countryCode="VU" value="678">Vanuatu (+678)</option>
                        <option data-countryCode="VA" value="379">Vatican City (+379)</option>
                        <option data-countryCode="VE" value="58">Venezuela (+58)</option>
                        <option data-countryCode="VN" value="84">Vietnam (+84)</option>
                        <option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
                        <option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
                        <option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
                        <option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
                        <option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
                        <option data-countryCode="ZM" value="260">Zambia (+260)</option>
                        <option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
                    </optgroup>
                        
                        
                        
                        
                            </select>
                            
                
                
                            <input type="text" class="form-control mb-2" name="telepon2">
                            <input type="hidden" value=""  class="form-control mb-2" name="telepon">


                    Email
                    <input type="email" class="form-control" value=""  name="emaild">
                    <input type="hidden" class="form-control" value=""  name="email">
                    
                    <h6>Masukkan Alamat Pengiriman</h6>
                    <select name="province_id" class="form-control" onchange="loadCity()"></select>
                    <input type="hidden" name="province">
                    <input type="hidden" name="nama_depan">
                    <input type="hidden" name="nama_belakang">
                    <input type="hidden" name="shipping_address">
                    <input type="hidden" name="shipto">
                    <input type="hidden" name="transaction_status" value ="Midtrans - Pending">
                    <select name="city_id" class="form-control" onchange="loadSubdistrict()"></select>
                    <input type="hidden" name="city">
                    <select name="subdistrict_id" class="form-control" onchange="loadOngkir()"></select>
                    <input type="hidden" name="subdistrict">
                    <textarea name="address" placeholder="Alamat Lengkap" rows="5" class="form-control"></textarea>
                    
                    
                </div>       
            
            
            
    </div>
    <div class="col-7 p-4 border bg-light">
        <h6>Item Pesanan</h6>
        <div class="tableFixHead">
            <table class="table table-striped">
                <thead>
                    <tr>
                        
                        <th>Kode</th>
                        <th>Nama Barang</th>
                        <th>Variant</th>
                        <th>Size</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Subtotal</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="table-fill">
                    
                    <?php $total_price = 0; $total_qty = 0; $qty_ready = 0;
                    ?>
                    @foreach ($cart as $row)
                    <?php 
                    $total_qty += $row->qty;
                    
                    if (isset($master[$row->internal_code]['supplier_code'])) {
                        // jika supplier terdaftar, cek apakah supplier restock?
                        if ($repeat[$master[$row->internal_code]['supplier_code']] == 1) {
                            // jika supplier repeat order, cek apakah qty terdaftar untuk variant dan size tsb
                            if (isset($stock_array[$row->internal_code][$row->variant][$row->size]['qty'])) {
                                if ($row->qty <= $stock_array[$row->internal_code][$row->variant][$row->size]['qty']) {
                                    $item_stock = "Ready";
                                    $selling_price = $stock_array[$row->internal_code][$row->variant][$row->size]['selling_price'];
                                } else {
                                    $item_stock = "Waiting";
                                    $selling_price = 0;
                                }
                            } else {
                                $item_stock = "Waiting";
                                $selling_price = 0;
                            }
                        } else {
                            // jika supplier tidak repeat order
                            if (isset($stock_array[$row->internal_code][$row->variant][$row->size]['qty'])) {
                                if ($row->qty <= $stock_array[$row->internal_code][$row->variant][$row->size]['qty']) {
                                    $item_stock = "Ready";
                                    $selling_price = $stock_array[$row->internal_code][$row->variant][$row->size]['selling_price'];
                                } else {
                                    $item_stock = "Not Available";
                                    $selling_price = 0;
                                }
                            } else {
                                $item_stock = "Not Available";
                                $selling_price = 0;
                            }
                        }
                    } else {
                        // jika supplier tidak terdaftar
                        $item_stock = "Not Available";
                        $selling_price = 0;
                    }

                    $subtotal = (int) $selling_price * (int) $row->qty;
                    $total_price = (int) $total_price + (int) $subtotal;
                        if ($item_stock == "Ready") {
                            $qty_ready += $row->qty;
                            echo "<tr style='color:green'>";
                        } else if ($item_stock == "Not Available") {
                            echo "<tr style='color:red'>";
                        } else if ($item_stock == "Waiting") {
                            echo "<tr style='color:orange'>";
                        }

                        ?>
                        <td>
                            <a href="{{ url('product').'/'.$row->internal_code }}" target="_blank">{{ $row->internal_code }}</a>
                            @if ($item_stock == 'Ready')
                            <input type="hidden" name="internal_code[]" value="{{ $row->internal_code }}">
                            <input type="hidden" name="variant[]" value="{{ $row->variant }}">
                            <input type="hidden" name="size[]" value="{{ $row->size }}">
                            <input type="hidden" name="qty[]" value="{{ $row->qty }}">
                            <input type="hidden" name="selling_price[]" value="{{ $selling_price }}">
                            <input type="hidden" name="comment[]" value="Order Reseller By {{ \Session::get('reseller') .' reffer:' . \Session::get('reffer') }}">
                            @endif
                        </td>
                        <td >{{ (isset($master[$row->internal_code]['product_name'])) ? $master[$row->internal_code]['product_name'] : 'N/A' }}</td>
                        
                        <td>{{ $row->variant }}</td>
                        <td>{{ $row->size }}</td>
                        <td>{{ number_format($selling_price) }}</td>
                        <td>{{ $row->qty }} ({{ $item_stock }})</td>
                        <td>{{ number_format($subtotal) }}</td>
                        <td><a href="javascript:void(0)" onclick="deleteOrdermanual({{ $row->id }})" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a></td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            












            <div class="row">
    
    <div class="col-12 table-scroll-x">
        
        <table class="table">
            <form action="{{ url('selling/add') }}" method="post">
                @csrf
                
                <input type="hidden" name="return_url" value="{{ url()->current() }}">
                <input type="hidden" name="comments" value="Order Reseller By {{ \Session::get('reseller') }}">
                <tr>
                    <td colspan="5"><b>Tambah Order</b> <i>Hanya berlaku untuk barang ready stock</i></td>
                </tr>
                <tr>
                    
                    <td width="20%">
                        Pilih Produk
                        {{-- <select name="internal_code" onchange="updateVariant()" class="form-control" > --}}
                        <input type="hidden" name="internal_codes">
                        <input list="internal_code_list" name="internal_code_list" onchange="updateVariant(this)" class="form-control">
                        <datalist id="internal_code_list" name="internal_code_list">
                            <option value="" disabled="disabled" selected="selected"></option>
                            @foreach($product_mainSimple as $row)
                            <option value="{{ $row->internal_code }} - {{ $row->product_name }}"></option>
                            @endforeach
                        </datalist>
                    </td>
                    {{-- <td width="20%">
                        Pilih Variant
                        <select name="variants" onchange="updateSize()" class="uppercase form-control" ></select>
                    </td>
                    <td width="10%">
                        Pilih Size
                        <select name="sizess" onchange="getStock()" class="uppercase form-control" ></input>
                    </td> --}}
                    <td width="20%">
                        Pilih Variant
                        <select name="variants" onchange="updateSize()" class="uppercase form-control" ></select>
                    </td>
                    <td width="10%">
                        Size
                        <select name="sizess" onchange="getStock()" class="form-control" >
                            <option value="NOSIZE">NOSIZE</option>
                            <option value="ALLSIZE">ALLSIZE</option>
                            <option disabled="disabled">-------------</option>
                            <option value="3XS">3XS</option>
                            <option value="2XS">2XS</option>
                            <option value="XS">XS</option>
                            <option value="S">S</option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                            <option value="XL">XL</option>
                            <option value="2XL">2XL</option>
                            <option value="3XL">3XL</option>
                            <option value="4XL">4XL</option>
                            <option value="5XL">5XL</option>
                            <option value="6XL">6XL</option>
                            <option disabled="disabled">-------------</option>
                            @for ($i = 25; $i < 45; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </td>
                    <td>
                        Stok    
                        <span class="max-qtys">0</span>
                    </td>
                    <td width="20%">
                        Masukkan Qty {{-- (Max: <span class="max-qtys">0</span>) --}}
                        <input type="number" class="form-control" name="qtys" >
                    </td>
                    <td width="10%">
                        
                        <a href="javascript:void(0)" class="form-control btn btn-success" onclick="Tambah()"><i class="fas fa-save"></i></a>
                    </td>
                </tr>
            </form>
        </table>
        
    </div>
</div>













            
    
   

























            
        </div>
        
    </div>
    <div class="col-2 p-4 border bg-light">
        <input type="hidden" name="shipping">
        
        <input type="hidden" name="id" class="form-control" value="{{ strtotime(now()) }}">
        <input type="hidden" name="order_id" class="form-control" value="{{ strtotime(now()) }}">

        <h6>Subtotal</h6>
        <input type="number" readonly="readonly" value="{{ $total_price }}" class="form-control" name="subtotal">
        <input type="number" value="0" class="form-control" readonly="readonly" name="discount_value">
        <h6>Ongkos Kirim /kg</h6>
        <input type="number" readonly="readonly" value="" class="form-control" name="shipping_cost">
        
        <h6>Berat Kirim (kg)</h6>
        <input type="number" readonly="readonly" min="1" max="50" value="{{ ceil($qty_ready / 3) }}" class="form-control" onchange="calculateTotal()" name="shipping_weight">
        <h6>Total Pembayaran</h6>
        <input type="number" readonly="readonly" min="1000" step="100" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="{{ $total_price }}" class="form-control" name="total_payment">
        <br>
        <input type="hidden" name="amount">
        <input type="hidden" name="gurah">
        <button type="submit" id="submitButtons" onclick="ValEntries();this.form.submit();" class="form-control btn btn-success" disabled="disbaled">
            <i class="fas fa-cash-register"></i> &nbsp; Bayar Sekarang
        </button>
                    
    </div>
</div>
</form>
@endsection

@section('script')

<style>
.tableFixHead          { overflow: auto; height: 80vh; }
.tableFixHead thead th { position: sticky; top: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.tableFixHead tfoot, .tableFixHead tfoot td, .tableFixHead tfoot th { position: -webkit-sticky; position: sticky; bottom: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.btn-success:disabled {
    background-color: #aaa;
    border: black;
}
</style>
<script>
    $('input[name=amount]').on('change paste keyup', function(){
        var amount_to_pay = parseInt($('input[name=total_payment]').val());
        var amount_will_pay = parseInt($('input[name=amount]').val());
        if ((amount_will_pay >= amount_to_pay) && (amount_will_pay <= (amount_to_pay * 2))) {
            $('#submitButton').removeClass('disabled');
        } else {
            $('#submitButton').addClass('disabled');
        }
    });
    $('input[type="radio"]').on('click change', function(e) {
        if(this.value=='myadd'){
            $('input[name=telepon2]').attr('disabled', 'disabled');
            $('input[name=telepon2]').removeAttr('required');
            
            $('input[name=telepon]').removeAttr('required');
            $('select[name=kodearea]').attr('disabled', 'disabled');
            $('select[name=kodearea]').removeAttr('required');
            $('input[name=nama_depand]').attr('disabled', 'disabled');
            $('input[name=nama_depand]').removeAttr('required');
            $('input[name=emaild]').attr('disabled', 'disabled');
            $('input[name=emaild]').removeAttr('required');
            $('select[name=province_id]').attr('disabled', 'disabled');
            $('select[name=province_id]').removeAttr('required');
            $('select[name=city_id]').attr('disabled', 'disabled');
            $('select[name=city_id]').removeAttr('required');
            $('select[name=subdistrict_id]').attr('disabled', 'disabled');
            $('select[name=subdistrict_id]').removeAttr('required');
            $('textarea[name=address]').attr('readonly', 'readonly');
            $('textarea[name=address]').removeAttr('required');
            $('input[name=shipping_cost]').val({{$custdata->shipping_cost}});
                 calculateTotal();
                 checkRadio();
        } else {
            $('#submitButtons').attr('disabled','disabled');
            $('input[name=shipping_cost]').val(null);
            $('input[name=telepon2]').val('');
            $('input[name=telepon2]').removeAttr('disabled');
            $('input[name=telepon2]').attr('required', 'required');
            $('input[name=telepon]').val('');
            
            
            $('select[name=kodearea]').val('62');
            $('select[name=kodearea]').removeAttr('disabled');
            $('select[name=kodearea]').attr('required', 'required');
            $('input[name=nama_depand]').val('');
            $('input[name=nama_depand]').removeAttr('disabled');
            $('input[name=nama_depand]').attr('required', 'required');
            $('input[name=emaild]').val('');
            $('input[name=emaild]').removeAttr('disabled');
            $('input[name=emaild]').attr('required', 'required');
            $('select[name=province_id]').val('');
            $('select[name=province_id]').removeAttr('disabled');
            $('select[name=province_id]').attr('required', 'required');
            $('select[name=city_id]').val('');
            $('select[name=city_id]').removeAttr('disabled');
            $('select[name=city_id]').attr('required', 'required');
            $('select[name=subdistrict_id]').val('');
            $('select[name=subdistrict_id]').removeAttr('disabled');
            $('select[name=subdistrict_id]').attr('required', 'required');
            $('textarea[name=address]').val('');
            $('textarea[name=address]').removeAttr('readonly');
            $('textarea[name=address]').attr('required', 'required');
            checkRadio();
        }
    //console.log();
    });

    $(document).ready(function(){
        loadProvince();


        $('input[name=telepon2]').bind('keypress', function (e) {  
            if (e.keyCode == 43 || e.keyCode == 32 || e.keyCode == 45|| e.keyCode == 46 || e.keyCode == 40 || e.keyCode == 41) {  
            return false;  
            } else {
                //$ab=$('input[name=telepon2]').val();

                //$newValue1 = ltrim(strval($aa),'0');
                //$('input[name=telepon2]').val(newValue1);

                 
                
                
            } 
            

        }); 
    });
    function ValEntries() {
        
            if($('input[name=addtype]:checked').val()=='myadd'){
                //$('input[name=gurah]').val($('input[name=addtype]').val());
                $('input[name=tanggal]').val($('input[name=tglskrg]').val());
                $('input[name=nama_depan]').val($('input[name=namasaya]').val());
                $('input[name=shipping_address]').val($('input[name=alamatsaya]').val());
                $('input[name=telepon]').val($('input[name=teleponsaya]').val());
                $('input[name=email]').val($('input[name=emailsaya]').val());
                $('input[name=amount]').val($('input[name=total_payment]').val());
                
            } else {
                //$('input[name=gurah]').val($('input[name=addtype]').val());
                $('input[name=tanggal]').val($('input[name=tglskrg]').val());
                $('input[name=shipto]').val('Customer');
                var s = $('input[name=telepon2]').val();
                while(s.charAt(0)=== '0'){
                    s=s.substring(1);
                }
                

                $('input[name=telepon]').val($('select[name=kodearea]').val() + s);
                //$address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
                $('input[name=amount]').val($('input[name=total_payment]').val());
                $('input[name=nama_depan]').val($('input[name=nama_depand]').val());
                $('input[name=nama_belakang]').val('');
                $('input[name=email]').val($('input[name=emaild]').val());
                var address_gen = $('input[name=nama_depan]').val() + '\n' + $('input[name=telepon]').val() + ' / ' + $('input[name=email]').val() + '\n' + $('textarea[name=address]').val() + '\n' + $('input[name=subdistrict]').val()+ ', ' + $('input[name=city]').val()+ ', ' + $('input[name=province]').val();
                $('input[name=shipping_address]').val(address_gen);
                
                
            }
    }
    function loadProvince() {
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/province',
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var province = '<option disabled="disabled" selected="selected">-- Pilih Provinsi</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    province += '<option value="'+item.province_id+'">'+item.province+'</option>';
                });
                $('select[name=province_id]').html(province);
            }
        });
    }

    function loadCity() {
        var province_id = $('select[name=province_id]').find(':selected').val();
        console.log(province_id);
        var province_name = $('select[name=province_id]').find(':selected').text();
        $('input[name=province]').val(province_name);
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/city?province='+province_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var city = '<option disabled="disabled" selected="selected">-- Pilih Kota</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    city += '<option value="'+item.city_id+'">'+item.type+' '+item.city_name+'</option>';
                });
                $('select[name=city_id]').html(city);
            }
        });
    }

    function loadSubdistrict() {
        var city_id = $('select[name=city_id]').find(':selected').val();
        var city_name = $('select[name=city_id]').find(':selected').text();
        $('input[name=city]').val(city_name);
        $.ajax({
            method: 'POST',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/subdistrict?city='+city_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var subdistrict = '<option disabled="disabled" selected="selected">-- Pilih Kecamatan</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    subdistrict += '<option value="'+item.subdistrict_id+'">'+item.subdistrict_name+'</option>';
                });
                $('select[name=subdistrict_id]').html(subdistrict);
            }
        });
    }

    function loadOngkir() {

        var subdistrict_id = $('select[name=subdistrict_id]').find(':selected').val();
        var subdistrict_name = $('select[name=subdistrict_id]').find(':selected').text();
        $('input[name=subdistrict]').val(subdistrict_name);
        var origin_city = 155; // jakarta utara
        var postdata = 'origin='+origin_city+'&originType=city&destination='+subdistrict_id+'&destinationType=subdistrict&weight=1000&courier=ide';
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/cost',
                'method': 'post',
                'postdata' : postdata,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                 var tr = '';
                var response = JSON.parse(response);
                var shipping_cost = response.rajaongkir.results[0].costs[0].cost[0].value;
                var shipping = response.rajaongkir.results[0].costs[0].service+' '+response.rajaongkir.results[0].costs[0].description;
                 console.log(response);
                 // $.each(response.rajaongkir.results[0].costs, function(index, item){
                    tr += '<tr>'+
                    '<td>'+shipping+'</td>'+
                    '<td>'+shipping_cost+' /kg</td>'+
                    '</tr>'; 
                 // });
                 $('.choice_ongkir').html(tr);
                 $('input[name=shipping]').val(shipping);
                 $('input[name=shipping_cost]').val(shipping_cost);
                 calculateTotal();
                 $('#submitButtons').removeAttr('disabled');
            }
        });
    }

    function calculateTotal() {
         var total_amount = parseInt($('input[name=subtotal]').val())  + (parseInt($('input[name=shipping_weight]').val()) * parseInt($('input[name=shipping_cost]').val()));
         $('input[name=total_payment]').val(total_amount);
    }

    function checkRadio() {
         if($('input[name=shipping_cost]').val()==null ||$('input[name=shipping_cost]').val()=='' ){
            $('#submitButtons').attr('disabled','disabled');
         } else {
            $('#submitButtons').removeAttr('disabled');
         }
    }

    function calculateCost() {
        var shipping_cost = 0;
        if ($('input[name=free_shipping]').is(':checked')) {
            shipping_cost = 0;
        } else {
            shipping_cost = $('input[name=shipping_address]:checked').data("ongkir");
        }
        $('input[name=shipping_cost]').val(shipping_cost);
        calculateTotal();
    }

    function EditOrd() {
        
    
        window.location.href = "{{ url('v2/manual-saveadd') }}/";
    

}

function Tambah() {
    
    if ($('input[name=internal_codes]').val() !== null && $('select[name=variants]').val() !== null && $('select[name=size]').val()!== null && $('input[name=qtys]').val()!== null ) {
        
        var batch_no = $('input[name=batch_no]').val();
    var instagram = $('input[name=instagram]').val();
    var tanggal = $('input[name=tanggal]').val();
    var internal_code = $('input[name=internal_codes]').val();
    var variant = $('select[name=variants]').val();
    var sizess = $('select[name=sizess]').val();
    var comment = $('input[name=comment]').val();
    var maxqty=$('.max-qtys').text();
    var qtys = $('input[name=qtys]').val();
   
        if (qtys == '') {
            alert('Kolom qty tidak boleh kosong');
            return;
        }
        if (maxqty<qtys) {
            alert('Stok tidak tersedia, silahakn cek produk lainnya');
            return;
        }

    
    var fd = new FormData();
    fd.append('batch_no', batch_no);
    fd.append('instagram', instagram);
    fd.append('tanggal', tanggal);
    fd.append('internal_code', internal_code);
    fd.append('variant', variant);
    fd.append('size', sizess);
    fd.append('comment',comment);
    fd.append('qty', qtys);


    $.ajax({
        url: '{{url("reseller/selling/add")}}',
        method: 'post',
        processData: false,
        contentType: false,
        cache: false,
        data: fd,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
            
            if (result == 'ok') {

                location.reload();
            }
        }
    });

    location.reload();
    setTimeout(location.reload.bind(location), 500);  



        } 



    
    

}

function saveAdd(rowid) {
    
    var instagram = $('input[name=instagram]').val();
    var nama_depan = $('input[name=nama_depan]').val();
    var nama_belakang = $('input[name=nama_belakang]').val();
    var telepon = $('input[name=telepon]').val();
    var email = $('input[name=email]').val();
    var address = $('textarea[name=address]').val();
    var subdistrict = $('input[name=subdistrict]').val();
    var city = $('input[name=city]').val();
    var province = $('input[name=province]').val();
    var subdistrict_id = $('select[name=subdistrict_id]').val();
    var shipping_cost = $('input[name=shipping_cost]').val();

    var fd = new FormData();
    fd.append('instagram', instagram.toLowerCase());
    fd.append('nama_depan', nama_depan);
    fd.append('nama_belakang', nama_belakang);
    fd.append('telepon', telepon);
    fd.append('email', email);
    fd.append('address', address);
    fd.append('subdistrict', subdistrict);
    fd.append('city', city);
    fd.append('province', province);
    fd.append('subdistrict_id', subdistrict_id);
    fd.append('shipping_cost', shipping_cost);

    $.ajax({
        url: '{{url("v2/manual-saveadd")}}',
        method: 'post',
        processData: false,
        contentType: false,
        cache: false,
        data: fd,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
            
            if (result == 'ok') {
                $('input[name=instagram]').attr('disabled', 'disabled');
                $('input[name=nama_depan]').attr('disabled', 'disabled');
                $('input[name=nama_belakang]').attr('disabled', 'disabled');
                $('input[name=telepon]').attr('disabled', 'disabled');
                $('input[name=email]').attr('disabled', 'disabled');
                location.reload();
            }
        }
    });
    location.reload();
    setTimeout(location.reload.bind(location), 1000);
}

function deleteAdd() {
    
    
    var instagram = $('input[name=instagram]').val();
    
    var fd = new FormData();
    fd.append('instagram', instagram);
    fd.append('username', instagram);
    
    $.ajax({
        url: '{{url("v2/manual-deleteadd")}}',
        method: 'post',
        processData: false,
        contentType: false,
        cache: false,
        data: fd,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(result) {
            console.log(result);
            
            if (result == 'ok') {
                
            }
        }
    });
    
    setTimeout(location.reload.bind(location), 1000);
}



    $(document).ready(function(){
        //$('input[name=shipping_address]:first').attr('checked', 'checked').trigger('click');
    });

    $('input[name=free_shipping]').on('click', function(){
        calculateCost();
    });

    $('input[name=discount_percentage]').on('change paste keyup', function(){
        calculateDiscount();
    })













    function deleteOrdermanual(id) {
        if (confirm("Yakin Hapus Data?")) {
        window.location.href = "{{ url('selling/order/deletereseller') }}/"+id;
    }
}




    function updateVariant(thisid) {
        var internal_code = $(thisid).val().split(' - ')[0];
        $('input[name=internal_codes]').val(internal_code);
    //var internal_code =  $('input[name=internal_codes]').val();
    var ajax_url = "{{ url('api/get-variant') }}/"+internal_code;
    console.log(ajax_url);
    $.ajax({
        url: ajax_url,
        method: "get",
        datatype: "json",
        success: function(result) {
            result = JSON.parse(result);
            console.log(result);
            var option = "<option value='' disabled='disabled' selected='selected'>-- Pilih Variant</option>";
            $.each(result, function(index, value){
                option += "<option value='"+value.variant+"'>"+value.variant+"</option>";
            });
            $('select[name=variants]').html(option);
        }
    });
}

function updateSize() {
    var internal_code =  $('input[name=internal_codes]').val();
    var variant = $('select[name=variants] option:selected').val();
    var ajax_url = "{{ url('api/get-size') }}/"+internal_code+"/"+variant;
    console.log(ajax_url);
    $.ajax({
        url: ajax_url,
        method: "get",
        datatype: "json",
        success: function(result) {
            result = JSON.parse(result);
            console.log(result);
            var option = "<option value='' disabled='disabled' selected='selected'>-- Pilih Variant</option>";
            $.each(result, function(index, value){
                option += "<option value='"+value.size+"'>"+value.size+"</option>";
            });
            $('select[name=sizess]').html(option);
        }
    });
}

function getStock() {
    var internal_code =  $('input[name=internal_codes]').val();
    var variant = $('select[name=variants] option:selected').val();
    var size = $('select[name=sizess] option:selected').val();
    var ajax_url = "{{ url('api/product') }}/"+internal_code+"/"+variant+"/"+size;
    console.log(ajax_url);
    $.ajax({
        url: ajax_url,
        method: "get",
        datatype: "json",
        success: function(result) {
            result = JSON.parse(result);
            console.log(result);
            // $('input[name=qty]').attr('max', result[0].qty);
            $('.max-qtys').text(result[0].qty);
        }
    });

}
















    
</script>
@endsection

