@extends('template')
@section('content')
<?php
$line = [];
foreach ($stock as $row) {
    $line[$row->internal_code][$row->variant][$row->size]['qty'] = $row->qty;
    $line[$row->internal_code][$row->variant][$row->size]['product_name'] = $row->product_name;
    $line[$row->internal_code][$row->variant][$row->size]['selling_price'] = $row->selling_price;
}
?>
<form action="{{ url('manual-payment') }}" method="post">
<div class="row">
    <div class="col-3 p-4 border bg-light">
            @csrf
            <h6>Masukkan Username Instagram</h6>
            <input type="text" class="form-control" value="{{ $cart[0]->instagram }}" readonly="readonly" name="instagram">
            <h6>Masukkan Tanggal Pembelian</h6>
            <input type="text" class="form-control" value="{{ $cart[0]->tanggal }}" readonly="readonly" name="tanggal">
            <h6>Masukkan Detail Penerima</h6>
            Nama Depan
            <input type="text" required="required" class="form-control" name="nama_depan">
            Nama Belakang
            <input type="text" class="form-control" name="nama_belakang">
            Telepon
            <input type="text" required="required" class="form-control" name="telepon">
            Email
            <input type="email" class="form-control" name="email">
            <h6>Masukkan Alamat Pengiriman</h6>
            <select name="province_id" required="required" class="form-control" onchange="loadCity()"></select>
            <input type="hidden" name="province">
            <select name="city_id" required="required" class="form-control" onchange="loadSubdistrict()"></select>
            <input type="hidden" name="city">
            <select name="subdistrict_id" required="required" class="form-control" onchange="loadOngkir()"></select>
            <input type="hidden" name="subdistrict">
            <textarea name="address" required="required" placeholder="Alamat Lengkap" rows="5" class="form-control"></textarea>
    </div>
    <div class="col-6 p-4 border bg-light">
        <h6>Item Pesanan</h6>
        <div class="tableFixHead">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Variant</th>
                        <th>Size</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody class="table-fill">
                    <?php $total_price = 0; $total_qty = 0;
                    $status_cart = true;
                    ?>
                    @foreach ($cart as $row)
                    <?php 
                    $total_qty += $row->qty;
                    $item_stock = "Ready";
                    if (!empty($line[$row->internal_code][$row->variant][$row->size]['qty'])) {
                        $selling_price = $line[$row->internal_code][$row->variant][$row->size]['selling_price'];
                        $product_name = $line[$row->internal_code][$row->variant][$row->size]['product_name'];

                        if ($row->qty > $line[$row->internal_code][$row->variant][$row->size]['qty']) {
                            $item_stock = "Waiting";
                            $selling_price = 0;
                            $status_cart = false;
                        }
                    } else {
                        $product_name = "N/A";
                        $item_stock = "Waiting";
                        $selling_price = 0;
                        $status_cart = false;
                    }
                    $subtotal = (int) $selling_price * (int) $row->qty;
                    $total_price = (int) $total_price + (int) $subtotal; ?>
                    <tr>
                        <td>{{ $row->internal_code }}</td>
                        <td>{{ $row->variant }}</td>
                        <td>{{ $row->size }}</td>
                            <td>{{ number_format($selling_price) }}</td>
                            <td>{{ $row->qty }} ({{ $item_stock }})</td>
                            <td>{{ number_format($subtotal) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-3 p-4 border bg-light">
        <input type="hidden" name="shipping">
        <input type="hidden" name="transaction_status" value="Confirm Manual">
        <input type="hidden" name="id" class="form-control" value="{{ strtotime(now()) }}">
        <h6>Subtotal</h6>
        <input type="number" readonly="readonly" value="{{ $total_price }}" class="form-control" name="subtotal">
        <h6>Ongkos Kirim /kg</h6>
        <input type="number" readonly="readonly" value="" class="form-control" name="shipping_cost">
        <h6>Berat Kirim (kg)</h6>
        <input type="number" min="1" max="50" value="{{ ceil($total_qty / 6) }}" class="form-control" onchange="calculateTotal()" name="shipping_weight">
        <h6>Total Pembayaran</h6>
        <input type="number" readonly="readonly" value="{{ $total_price }}" class="form-control" name="total_payment">
        <h6>Nominal Yang Dibayarkan</h6>
        <input type="number" name="amount" required="required" class="form-control">
        <h6>Metode Pembayaran</h6>
        <select name="payment_method" required="required" class="form-control">
            <option value="" disabled="disabled" selected="selected"></option>
            <option value="Manual - Transfer BCA">Transfer BCA</option>
            <option value="Manual - Transfer Mandiri">Transfer Mandiri</option>
            <option value="QRIS">QRIS / e-Wallet</option>
            <option value="e-Commerce - Tokopedia">Tokopedia</option>
            <option value="e-Commerce - Shopee">Shopee</option>
        </select>
        @if ($status_cart == true)
        <button type="submit" class="form-control btn btn-success">
            <i class="fas fa-cash-register"></i> &nbsp; Simpan Pembayaran
        </button>
        @else
        <button type="submit" class="form-control btn btn-secondary" disabled="disabled">
            <i class="fas fa-cash-register"></i> &nbsp; Simpan Pembayaran
        </button>
        @endif
                    <br>{{ ($status_cart) ? '' : 'Pembayaran hanya dapat dilakukan jika seluruh barang ready' }}
    </div>
</div>
</form>
@endsection

@section('script')

<style>
.tableFixHead          { overflow: auto; height: 80vh; }
.tableFixHead thead th { position: sticky; top: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.tableFixHead tfoot, .tableFixHead tfoot td, .tableFixHead tfoot th { position: -webkit-sticky; position: sticky; bottom: 0; z-index: 1; background: #19b5ff !important; color: white; box-shadow: 1px black;}
.btn-success:disabled {
    background-color: #aaa;
    border: black;
}
</style>
<script>
    $(document).ready(function(){
        loadProvince();
    });

    function loadProvince() {
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/province',
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var province = '<option disabled="disabled" selected="selected">-- Pilih Provinsi</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    province += '<option value="'+item.province_id+'">'+item.province+'</option>';
                });
                $('select[name=province_id]').html(province);
            }
        });
    }

    function loadCity() {
        var province_id = $('select[name=province_id]').find(':selected').val();
        console.log(province_id);
        var province_name = $('select[name=province_id]').find(':selected').text();
        $('input[name=province]').val(province_name);
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/city?province='+province_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var city = '<option disabled="disabled" selected="selected">-- Pilih Kota</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    city += '<option value="'+item.city_id+'">'+item.type+' '+item.city_name+'</option>';
                });
                $('select[name=city_id]').html(city);
            }
        });
    }

    function loadSubdistrict() {
        var city_id = $('select[name=city_id]').find(':selected').val();
        var city_name = $('select[name=city_id]').find(':selected').text();
        $('input[name=city]').val(city_name);
        $.ajax({
            method: 'POST',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/subdistrict?city='+city_id,
                'method': 'GET',
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                var response = JSON.parse(response);
                var subdistrict = '<option disabled="disabled" selected="selected">-- Pilih Kecamatan</option>';
                $.each(response.rajaongkir.results, function(index, item){
                    subdistrict += '<option value="'+item.subdistrict_id+'">'+item.subdistrict_name+'</option>';
                });
                $('select[name=subdistrict_id]').html(subdistrict);
            }
        });
    }

    function loadOngkir() {

        var subdistrict_id = $('select[name=subdistrict_id]').find(':selected').val();
        var subdistrict_name = $('select[name=subdistrict_id]').find(':selected').text();
        $('input[name=subdistrict]').val(subdistrict_name);
        var origin_city = 155; // jakarta utara
        var postdata = 'origin='+origin_city+'&originType=city&destination='+subdistrict_id+'&destinationType=subdistrict&weight=1000&courier=ide';
        $.ajax({
            method: 'post',
            url: '{{ url('api-gateway') }}',
            data: {
                'url': 'https://pro.rajaongkir.com/api/cost',
                'method': 'post',
                'postdata' : postdata,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            datatype: 'json',
            success: function(response) {
                 var tr = '';
                var response = JSON.parse(response);
                var shipping_cost = response.rajaongkir.results[0].costs[0].cost[0].value;
                var shipping = response.rajaongkir.results[0].costs[0].service+' '+response.rajaongkir.results[0].costs[0].description;
                 console.log(response);
                 // $.each(response.rajaongkir.results[0].costs, function(index, item){
                    tr += '<tr>'+
                    '<td>'+shipping+'</td>'+
                    '<td>'+shipping_cost+' /kg</td>'+
                    '</tr>'; 
                 // });
                 $('.choice_ongkir').html(tr);
                 $('input[name=shipping]').val(shipping);
                 $('input[name=shipping_cost]').val(shipping_cost);
                 calculateTotal();
            }
        });
    }

    function calculateTotal() {
         var total_amount = parseInt($('input[name=subtotal]').val()) + (parseInt($('input[name=shipping_weight]').val()) * parseInt($('input[name=shipping_cost]').val()));
         $('input[name=total_payment]').val(total_amount);
    }
</script>
@endsection