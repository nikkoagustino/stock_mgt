@extends('template')
@section('content')
<?php foreach ($stock as $row) {
    $product_name[$row->internal_code] = $row->product_name;    
} ?>
<div class="row">
    <div class="col-12">
        <h2>Data Order</h2>
    </div>
    <div class="col-12 table-scroll-x">
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Instagram</th>
                    <th>Kode Internal</th>
                    <th>Nama Produk</th>
                    <th>Variant</th>
                    <th>Size</th>
                    <th>Qty</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ($order as $row)
                <?php switch ($row->order_status) {
                    case 'new':
                        $status = 'Baru';
                        break;
                    case 'paid':
                        $status = 'Sudah Bayar';
                        break;
                    case 'delivered':
                        $status = 'Sudah Dikirim';
                        break;
                    
                    default:
                        # code...
                        break;
                } ?>
                <tr>
                    <td>{{ date('Y-m-d H:i:s', strtotime($row->tanggal)) }}</td>
                    <td>{{ $row->instagram }}</td>
                    <td>{{ $row->internal_code }}</td>
                    <td>{{ (isset($product_name[$row->internal_code])) ? $product_name[$row->internal_code] : '' }}</td>
                    <td>{{ $row->variant }}</td>
                    <td>{{ $row->size }}</td>
                    <td>{{ $row->qty }}</td>
                    <td>{{ $status }}</td>
                    @if ($status == 'Baru')
                    <td>
                        <a href="javascript:void(0)" onclick="deleteOrder({{ $row->id }})" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a></td>
                    @else
                    <td></td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script>
    function deleteOrder(id) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = "{{ url('selling/order/delete') }}/"+id;
        }
    }
</script>
@endsection