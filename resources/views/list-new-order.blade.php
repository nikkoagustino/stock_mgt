@extends('template')
@section('content')
<div class="row">
    <div class="col-13">
        <h2>List Order</h2>
        <button onclick="deleteListOrder1days()" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus Pesanan" class="btn btn-sm btn-danger"><i class="fas fa-times"></i> Hapus Terdaftar Lebih Dari 1 Hari</button>   <button onclick="deleteListOrder3days()" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus Pesanan" class="btn btn-sm btn-danger"><i class="fas fa-times"></i> Hapus Terdaftar Lebih Dari 3 Hari</button>
    </div>
    <div class="col-14 table-scroll-x">
        <table class="table table-striped display nowrap datatable-list">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Instagram</th>
                    <th>Telpon</th>
                    <th>Nama</th>
                    <th>Action</th>
                    <th>Total Order</th>
                    <th>Comment</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($list_order as $row)
                <tr>
                    <td>{{ $row->tanggal }}</td>
                    
                    <td>
                    <a href="{{ url('customer-edit').'/'.$row->instagram }}">{{ strtolower($row->instagram) }} </a>
                        {{ $row->flag }}
                        @if ((isset($row->flag)) && ($row->flag == 1))
                        <span class="hit-and-run"><i class="fas fa-flag"></i> Hit and Run &nbsp; <a style="color: inherit" href="{{ url('hit-and-run').'/'.$row->instagram }}/0" data-bs-toggle="tooltip" data-bs-placement="top" title="Batalkan Hit And Run"><i class="fas fa-times"></i></a></span>
                        @endif
                    </td>
                    <td>
                        <a href="http://wa.me/{{ $row->phone }}" target="_blank">{{ $row->phone }}</a>
                    </td>
                    <td>{{ (isset($row->firstname)) ? $row->firstname : ''; }} {{ (isset($row->lastname)) ? $row->lastname : ''; }}</td>
                    <td>
                            <button onclick="window.location.href='{{ url('v2/manual-payment').'/'.$row->instagram.'/'.$row->tanggal }}'" class="btn btn-sm btn-success" data-bs-toggle="tooltip" data-bs-placement="top" title="Pembayaran Manual"><i class="fas fa-money-bill-wave"></i></button>
                            <button onclick="window.location.href='{{ url('selling/show').'/'.$row->instagram.'/'.$row->tanggal }}'" class="btn btn-sm btn-warning" data-bs-toggle="tooltip" data-bs-placement="top" title="Ubah Pesanan"><i class="fas fa-edit"></i></button>
                            
                            <button onclick="deleteListOrder('{{ $row->instagram }}', '{{ $row->tanggal }}')" data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus Pesanan" class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
                            
                            @if ($page_status == 'new')
                            @if (\Session::get('access_level') != 'staff')
                            <a href="{{ url('hit-and-run').'/'.$row->instagram }}/1" data-bs-toggle="tooltip" data-bs-placement="top" title="Tandai Hit And Run" class="btn btn-sm btn-purple"><i class="fas fa-flag"></i></a>
                            @endif
                            @endif
                    </td>
                    <td>{{ $row->qty_total }} item</td>
                    <td>{{ $row->comment }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- {{ $list_order->links('pagination::bootstrap-4') }} --}}
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form action="{{ url('order/save-awb') }}" method="post">
      <div class="modal-header">
        <h5 class="modal-title">Masukkan No. Resi Pengiriman</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
            @csrf
            <input type="text" name="payment_id" readonly="readonly" class="form-control">
            <input type="text" placeholder="No. Resi / AWB" name="awb" class="form-control">
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Simpan">
      </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
    function changeID(new_id) {
        $('input[name="payment_id"]').val(new_id);
    }

    function deleteListOrder(instagram, tanggal) {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = '{{ url('selling/delete') }}/'+instagram+'/'+tanggal;
        }
    }
    function deleteListOrder1days() {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = '{{ url('selling/delete1days') }}';
        }
    }
    function deleteListOrder3days() {
        if (confirm("Yakin Hapus Data?")) {
            window.location.href = '{{ url('selling/delete3days') }}';
        }
    }
    

    $(document).ready(function() {
        $('.datatable-list').DataTable( {
            'pageLength': 25,
            "order": [[ 0, "desc" ]]
        } );
    } );
</script> 
@endsection