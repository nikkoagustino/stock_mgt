@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3>Detail Masalah</h3>
                <form action="{{ url('refund/update') }}" method="post">
                    @csrf
                     <input type="hidden" name="step_no" value="0">
                     <input type="hidden" name="step_detail" value="Laporan Refund Dibuat">
                     Nomor Laporan
                     <input type="text" name="refund_master_id" readonly="readonly" class="form-control" value="{{ $refund->refund_master_id }}">
                    Nomor Order
                    <input type="text" readonly="readonly" name="payment_id" class="form-control" value="{{ $refund->payment_id }}">
                    Instagram
                    <input type="text" readonly="readonly" name="instagram" class="form-control" value="{{ $refund->instagram}}">
                    Alasan Pengembalian
                    <textarea name="alasan" readonly="readonly" rows="3" class="form-control">{{ $refund->alasan }}</textarea>
                    Pilih Barang Yang Bermasalah
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Kode Internal</th>
                                <th>Nama Produk</th>
                                <th>Variant</th>
                                <th>Size</th>
                                <th>Harga Jual</th>
                                <th>Qty Bermasalah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($refund_item as $row)
                            <tr data-seq='{{ $row->id }}'>
                                <td>{{ $row->internal_code }}</td>
                                <td>{{ $master[$row->internal_code]['product_name'] }}</td>
                                <td>{{ $row->variant }}</td>
                                <td>{{ $row->size }}</td>
                                <td><span class="price">{{ number_format($master[$row->internal_code]['selling_price']) }}</span>
                                </td>
                                <td>{{ $row->qty_masalah }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    Nominal Produk Yang Bermasalah
                    <input type="number" step="1" name="nominal" readonly="readonly" class="form-control" value="{{ $refund->nominal }}">
                </form>
            </div>
            <div class="col-12 col-md-6">
                <h4>Langkah Penyelesaian Masalah</h4>
                <div class="card">
                    <div class="card-body">
                        <form action="{{ url('refund/update') }}" method='post'>
                            @csrf
                            <input type="hidden" name="step_no" value="1">
                            <input type="hidden" name="step_detail" value="Nomor Resi Barang Retur Sudah Dimasukkan">
                            <input type="hidden" name="refund_master_id" value="{{ $refund->refund_master_id }}">
                            <b>Step 1 : Input Nomor Resi Pengembalian dari Customer</b>
                            <hr>
                            @if ($refund->last_step_no < 1)
                            <div class="row">
                                <div class="col-4">Kurir</div>
                                <div class="col-8"><input type="text" placeholder="JNE / JNT / ID EXPRESS" name="courier_retur" class="form-control uppercase"></div>
                            </div>
                            <div class="row">
                                <div class="col-4">Nomor Resi</div>
                                <div class="col-8"><input type="text" placeholder="ID123456789" name="awb_retur" class="form-control"></div>
                            </div>
                            <div class="row">
                                <div class="col"></div>
                                <div class="col-8">
                                    <button class="btn form-control btn-primary"><i class="fas fa-save"></i> &nbsp; Simpan</button>
                                </div>
                            </div>
                            @else
                            <div class="row">
                                <div class="col-4">Kurir</div>
                                <div class="col-8"><input type="text" value="{{ $refund_detail_steps[1]->courier_retur }}" readonly="readonly" name="courier_retur" class="form-control uppercase"></div>
                            </div>
                            <div class="row">
                                <div class="col-4">Nomor Resi</div>
                                <div class="col-8"><input type="text" value="{{ $refund_detail_steps[1]->awb_retur }}" readonly="readonly" name="awb_retur" class="form-control"></div>
                            </div>
                            @endif
                        </form>
                    </div>
                </div>
                @if ($refund->last_step_no >= 1)
                <div class="card">
                    <div class="card-body">
                        <form action="{{ url('refund/update') }}" enctype="multipart/form-data" method='post'>
                            @csrf
                            <input type="hidden" name="step_no" value="2">
                            <input type="hidden" name="step_detail" value="Barang Retur Sudah Diterima Penjual, Menunggu Opsi Penyelesaian">
                            <input type="hidden" name="refund_master_id" value="{{ $refund->refund_master_id }}">
                            <b>Step 2 : Jika Barang Sudah Diterima, Upload Foto Barang</b>
                            <hr>
                            @if ($refund->last_step_no < 2)
                            <div class="row">
                                <div class="col-8">
                                    <input type="file" class="form-control" accept="image/*" name="received_product_photo">
                                </div>
                                <div class="col-4">
                                    <button class="btn form-control btn-primary"><i class="fas fa-save"></i> &nbsp; Simpan</button>
                                </div>
                            </div>
                            @else
                            <img src="{{ url('public').'/'.Storage::url($refund_detail_steps[2]->received_product_photo) }}" alt="">
                            @endif
                        </form>
                    </div>
                </div>
                @endif
                @if ($refund->last_step_no >= 2)
                <div class="card">
                    <div class="card-body">
                        <form action="{{ url('refund/update') }}" enctype="multipart/form-data" method='post'>
                            @csrf
                            <input type="hidden" name="step_no" value="3">
                            <input type="hidden" name="step_detail" value="Masalah Selesai">
                            <input type="hidden" name="refund_master_id" value="{{ $refund->refund_master_id }}">
                            <b>Step 3 : Pilih Opsi Penyelesaian Masalah dan Lengkapi Data Yang Diminta</b>
                            <hr>
                            @if ($refund->last_step_no < 3)
                            <div class="row">
                                <div class="col-12">
                                    <select name="resolution_option" class="form-control" required="required">
                                        <option value="" disabled="disabled" selected="selected">---- PILIH OPSI PENYELESAIAN MASALAH</option>
                                        <option value="refund">Refund - Kembalikan Uang Customer</option>
                                        <option value="replace">Replace - Kirim Produk Pengganti</option>
                                    </select>
                                    <div class="show-refund" style="display: none;">
                                        <div class="row">
                                            <div class="col-4">Nama Bank</div>
                                            <div class="col-8"><input placeholder="BCA / BNI / MANDIRI" type="text" name="bank_name" class="form-control uppercase"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">Nomor Rekening</div>
                                            <div class="col-8"><input type="text" placeholder="123456789" name="account_no" class="form-control"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">Nama Pada Rekening</div>
                                            <div class="col-8"><input type="text" placeholder="John Doe" name="account_name" class="form-control"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">Bukti Transfer</div>
                                            <div class="col-8"><input type="file" name="transfer_proof" accept="image/*" class="form-control"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col"></div>
                                            <div class="col-8"><button class="btn form-control btn-primary"><i class="fas fa-save"></i> &nbsp; Simpan</button></div>
                                        </div>
                                    </div>
                                    <div class="show-replace" style="display: none;">
                                        <div class="row">
                                            <div class="col-4">Nomor Resi Barang Pengganti</div>
                                            <div class="col-8"><input placeholder="ID123456789" type="text" name="awb_replacement" class="form-control uppercase"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col"></div>
                                            <div class="col-8"><button class="btn form-control btn-primary"><i class="fas fa-save"></i> &nbsp; Simpan</button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="row">
                                <div class="col-12">
                                    @if ($refund_detail_steps[3]->resolution_option == 'refund')
                                    <h5>Refund - Kembalikan Uang Customer</h5>
                                        <div class="row">
                                            <div class="col-4">Nama Bank</div>
                                            <div class="col-8"><input value="{{ $refund_detail_steps[3]->bank_name }}" readonly="readonly" type="text" name="bank_name" class="form-control uppercase"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">Nomor Rekening</div>
                                            <div class="col-8"><input type="text" value="{{ $refund_detail_steps[3]->account_no }}" readonly="readonly" name="account_no" class="form-control"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">Nama Pada Rekening</div>
                                            <div class="col-8"><input type="text" value="{{ $refund_detail_steps[3]->account_name }}" readonly="readonly" name="account_name" class="form-control"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">Bukti Transfer</div>
                                            <div class="col-8">
                                                <img src="{{ url('public').'/'.\Storage::url($refund_detail_steps[3]->transfer_proof) }}" alt="">
                                            </div>
                                        </div>
                                    @else
                                    <h5>Replace - Kirim Produk Pengganti</h5>
                                        <div class="row">
                                            <div class="col-4">Nomor Resi Barang Pengganti</div>
                                            <div class="col-8"><input value="{{ $refund_detail_steps[3]->awb_replacement }}" readonly="readonly" type="text" name="awb_replacement" class="form-control uppercase"></div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            @endif
                        </form>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('.checkbox_masalah').on('click', function(){
            populateProblem();
        });
        $('.qty_masalah').on('change paste keyup', function() {
            populateProblem();
        });
        $('select[name=resolution_option]').on('change paste keyup', function(){
            resolutionChange();
        });
    });

    function resolutionChange() {
        if ($('select[name=resolution_option] option:selected').val() == 'refund') {
            $('.show-refund').show();
            $('.show-replace').hide();
        } else {
            $('.show-replace').show();
            $('.show-refund').hide();
        }

    }
    function populateProblem() {
        var nominal = 0;
        $.each($('.checkbox_masalah'), function(){
            var val = (this.checked ? "1" : "0");
            if (val == 1) {
                var sequence = $(this).closest('tr').attr('data-seq');
                var qty_masalah = $('tr[data-seq='+sequence+'] input.qty_masalah').val();
                var selling_price = $('tr[data-seq='+sequence+'] span.price').text().replace(',', '').replace('.', '');
                nominal = nominal + (parseInt(qty_masalah) * parseInt(selling_price));
            }
        });
        $('input[name=nominal]').val(nominal);        
    }
</script>
@endsection