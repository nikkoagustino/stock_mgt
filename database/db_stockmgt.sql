-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2021 at 06:14 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_stockmgt`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id` int(11) NOT NULL,
  `batch_no` int(11) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `internal_code` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `size` varchar(20) NOT NULL,
  `variant` varchar(50) NOT NULL,
  `tanggal` varchar(30) NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `order_status` enum('new','paid','delivered') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id`, `batch_no`, `instagram`, `internal_code`, `qty`, `size`, `variant`, `tanggal`, `comment`, `order_status`) VALUES
(2, 0, 'afiraindonesia', 'CT25', 3, '3XL', 'BIRU', '2021-07-09', NULL, 'new'),
(3, 0, 'manmeliveadm', 'KN1', 2, 'XL', 'HITAM', '2021-07-09', NULL, 'new'),
(4, 0, 'doragonindonesia', 'K4', 1, 'XL', 'MERAH', '2021-07-09', NULL, 'new'),
(5, 0, 'afiraindonesia', 'X1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.x1.2.xl.merah', 'new'),
(6, 0, 'afiraindonesia', 'A1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.a1.2.xl.merah', 'new'),
(7, 0, 'afiraindonesia', 'A1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.a1.2.xl.merah', 'new'),
(8, 0, 'afiraindonesia', 'B1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.b1.2.xl.merah', 'new'),
(9, 0, 'afiraindonesia', 'C1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c1.1.xl.hitam', 'new'),
(10, 0, 'afiraindonesia', 'D1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.d1.1.xl.hitam', 'new'),
(11, 0, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.l1.1.xl.hitam', 'new'),
(12, 0, 'afiraindonesia', 'C1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c1.1.xl.hitam', 'new'),
(13, 0, 'afiraindonesia', 'M1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.m1.1.xl.hitam', 'new'),
(14, 0, 'afiraindonesia', 'K1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.k1.1.xl.hitam', 'new'),
(15, 0, 'afiraindonesia', 'Z1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.Z1.1.xl.hitam', 'new'),
(16, 0, 'afiraindonesia', 'D1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.D1.1.xl.hitam', 'new'),
(17, 0, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.l1.1.xl.hitam', 'new'),
(18, 0, 'afiraindonesia', 'K1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.k1.1.xl.hitam', 'new'),
(19, 0, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.L1.1.xl.hitam', 'new'),
(20, 0, 'afiraindonesia', 'MK1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.mk1.2.xl.merah', 'new'),
(21, 0, 'afiraindonesia', 'MK1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.mk1.2.xl.merah', 'new'),
(22, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(23, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(24, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(25, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(26, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(27, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(28, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(29, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(30, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(31, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(32, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(33, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(34, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(35, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(36, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(37, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(38, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(39, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(40, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(41, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(42, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(43, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(44, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(45, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(46, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(47, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(48, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(49, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(50, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(51, 0, 'afiraindonesia', 'KM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.km1.2.xl.hitam', 'new'),
(52, 0, 'afiraindonesia', 'KM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.km1.2.xl.hitam', 'new'),
(53, 0, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(54, 0, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(55, 0, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(56, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(57, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(58, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(59, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(60, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(61, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(62, 0, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(63, 0, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(64, 0, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(65, 0, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(66, 0, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(67, 0, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(68, 0, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(69, 0, 'afiraindonesia', 'A3', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.a3.1.xl.hitam', 'new'),
(70, 0, 'afiraindonesia', 'A5', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.a5.2.xl.hitam', 'new'),
(71, 1, 'afiraindonesia', 'K2', 2, 'XL', 'HITAM', '2021-07-09', NULL, 'new'),
(72, 1, 'afiraindonesia', 'CT25', 3, '3XL', 'BIRU', '2021-07-09', NULL, 'new'),
(73, 1, 'manmeliveadm', 'KN1', 2, 'XL', 'HITAM', '2021-07-09', NULL, 'new'),
(74, 1, 'doragonindonesia', 'K4', 1, 'XL', 'MERAH', '2021-07-09', NULL, 'new'),
(75, 1, 'afiraindonesia', 'X1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.x1.2.xl.merah', 'new'),
(76, 1, 'afiraindonesia', 'A1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.a1.2.xl.merah', 'new'),
(77, 1, 'afiraindonesia', 'A1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.a1.2.xl.merah', 'new'),
(78, 1, 'afiraindonesia', 'B1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.b1.2.xl.merah', 'new'),
(79, 1, 'afiraindonesia', 'C1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c1.1.xl.hitam', 'new'),
(80, 1, 'afiraindonesia', 'D1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.d1.1.xl.hitam', 'new'),
(81, 1, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.l1.1.xl.hitam', 'new'),
(82, 1, 'afiraindonesia', 'C1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c1.1.xl.hitam', 'new'),
(83, 1, 'afiraindonesia', 'M1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.m1.1.xl.hitam', 'new'),
(84, 1, 'afiraindonesia', 'K1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.k1.1.xl.hitam', 'new'),
(85, 1, 'afiraindonesia', 'Z1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.Z1.1.xl.hitam', 'new'),
(86, 1, 'afiraindonesia', 'D1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.D1.1.xl.hitam', 'new'),
(87, 1, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.l1.1.xl.hitam', 'new'),
(88, 1, 'afiraindonesia', 'K1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.k1.1.xl.hitam', 'new'),
(89, 1, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.L1.1.xl.hitam', 'new'),
(90, 1, 'afiraindonesia', 'MK1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.mk1.2.xl.merah', 'new'),
(91, 1, 'afiraindonesia', 'MK1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.mk1.2.xl.merah', 'new'),
(92, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(93, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(94, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(95, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(96, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(97, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(98, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(99, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(100, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(101, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(102, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(103, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(104, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(105, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(106, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(107, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(108, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(109, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(110, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(111, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(112, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(113, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(114, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(115, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(116, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(117, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(118, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(119, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(120, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(121, 1, 'afiraindonesia', 'KM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.km1.2.xl.hitam', 'new'),
(122, 1, 'afiraindonesia', 'KM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.km1.2.xl.hitam', 'new'),
(123, 1, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(124, 1, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(125, 1, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(126, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(127, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(128, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(129, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(130, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(131, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(132, 1, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(133, 1, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(134, 1, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(135, 1, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(136, 1, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(137, 1, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(138, 1, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(139, 1, 'afiraindonesia', 'A3', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.a3.1.xl.hitam', 'new'),
(140, 1, 'afiraindonesia', 'A5', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.a5.2.xl.hitam', 'new'),
(141, 2, 'vitazhangs', 'B087', 1, 'M', 'HITAM', '2021-07-14', 'vitazhangs[#]Order.B087.1.M.black', 'new'),
(142, 2, 'leenarooz', 'KE08', 1, 'M', 'BIRUTUA', '2021-07-14', 'leenarooz[#]Order KE08 birutua M', 'new'),
(143, 2, 'ingewibisono', 'KE08', 1, 'M', 'BIRUMUDA', '2021-07-14', 'ingewibisono[#]Order KE08 birumuda M', 'new'),
(144, 2, 'meilina_ng', 'KE08', 1, 'TUA', 'BIRU', '2021-07-14', 'meilina_ng[#]Order KE08 biru tua M, tq', 'new'),
(145, 2, 'yuki_birdnest', 'KE08', 1, 'TUA', 'BIRU', '2021-07-14', 'yuki_birdnest[#]Order KE08 Biru Tua M ulang', 'new'),
(146, 2, 'saintclairen', 'B120', 1, 'M', 'BIRU', '2021-07-14', 'saintclairen[#]Order b120 biru M', 'new'),
(147, 2, 'sellyfrancescarifin', 'B120', 1, 'ALLSIZE', 'BIRU', '2021-07-14', 'sellyfrancescarifin[#]Order B120 biru allsize', 'new'),
(148, 2, 'novalin_mussy', 'B120', 1, 'M', 'PINK', '2021-07-14', 'novalin_mussy[#]Order B120 pink m', 'new'),
(149, 2, 'bunganyaabung', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'bunganyaabung[#]Order b120 biru L', 'new'),
(150, 2, 'dewileeee', 'B120', 1, 'L', 'PINK', '2021-07-14', 'dewileeee[#]order b120 pink L', 'new'),
(151, 2, 'yenni_djong', 'B120', 1, 'L', 'PINK', '2021-07-14', 'yenni_djong[#]Order b120 pink L', 'new'),
(152, 2, 'osiana_yes', 'B120', 1, 'L', 'PINK', '2021-07-14', 'osiana_yes[#]Order B120 PINK L', 'new'),
(153, 2, 'meicymarleni', 'B120', 1, 'L', 'PINK', '2021-07-14', 'meicymarleni[#]ORDER B120 PINK L', 'new'),
(154, 2, 'yap_yuna', 'B12P', 1, 'XL', 'PINK', '2021-07-14', 'yap_yuna[#]Order b12p pink xl', 'new'),
(155, 2, 'meyliana.sari', 'B120', 1, 'L', 'PINK', '2021-07-14', 'meyliana.sari[#]Order b120 pink L', 'new'),
(156, 2, 'sellyfrancescarifin', 'B120', 1, 'S', 'BIRU', '2021-07-14', 'sellyfrancescarifin[#]Order B120 biru S', 'new'),
(157, 2, 'lievalentina792019', 'B120', 1, 'L', 'PINK', '2021-07-14', 'lievalentina792019[#]Order B120 pink L', 'new'),
(158, 2, 'yuki_birdnest', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'yuki_birdnest[#]Order B120 Biru L', 'new'),
(159, 2, 'wm_abigail02', 'KE07', 1, 'L', 'PINK', '2021-07-14', 'wm_abigail02[#]order KE07 PINK L', 'new'),
(160, 2, 'michelle_graciars', 'B120', 1, 'M', 'PINK', '2021-07-14', 'michelle_graciars[#]Order B120 PINK M', 'new'),
(161, 2, 'yenni_djong', 'B120', 1, 'L', 'PINK', '2021-07-14', 'yenni_djong[#]Order B120 PINK L', 'new'),
(162, 2, 'jessicariched', 'B120', 1, 'M', 'PINK', '2021-07-14', 'jessicariched[#]Order b120 pink M', 'new'),
(163, 2, 'sellyfrancescarifin', 'B120', 1, 'M', 'BIRU', '2021-07-14', 'sellyfrancescarifin[#]Order B120 BIRU M', 'new'),
(164, 2, 'mq_immaculata', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'mq_immaculata[#]Order B120 biru L', 'new'),
(165, 2, 'wie_ep', 'B120', 1, 'L', 'PINK', '2021-07-14', 'wie_ep[#]Order B120 pink L', 'new'),
(166, 2, 'lovenzel19', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'lovenzel19[#]Order B120 BIRU L', 'new'),
(167, 2, 'lindathe.kingsexpress', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'lindathe.kingsexpress[#]Order B120 Biru L', 'new'),
(168, 2, 'linkjoannejio', 'B120', 1, 'L', 'PINK', '2021-07-14', 'linkjoannejio[#]Order B120 Pink L', 'new'),
(169, 2, 'lisahuan6', 'B120', 1, 'L', 'PINK', '2021-07-14', 'lisahuan6[#]Order B120 pink L', 'new'),
(170, 2, 'sayu1303_', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'sayu1303_[#]Order B120 Biru L', 'new'),
(171, 2, 'f3nwu', 'B120', 1, 'M', 'PINK', '2021-07-14', 'f3nwu[#]Order B120 PINK M', 'new'),
(172, 2, 'lovenzel19', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'lovenzel19[#]Order B120 Biru L', 'new'),
(173, 2, 'beldandy99', 'B120', 1, 'XL', 'PINK', '2021-07-14', 'beldandy99[#]Order B120 PINk XL', 'new'),
(174, 2, 'lindathe.kingsexpress', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'lindathe.kingsexpress[#]ORDER B120 BIRU L', 'new'),
(175, 2, 'hesiizhy', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'hesiizhy[#]Order B120 BIRU L', 'new'),
(176, 2, 'jessicariched', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'jessicariched[#]Order B120 Biru L', 'new'),
(177, 2, 'iin_nugroho90', 'B120', 1, 'L', 'PINK', '2021-07-14', 'iin_nugroho90[#]ORDER B120 PINK L', 'new'),
(178, 2, 'dvy_16', 'B120', 1, 'L', 'PINK', '2021-07-14', 'dvy_16[#]Order b120 pink L', 'new'),
(179, 2, 'lidya_wangg', 'B120', 1, 'BIRU', 'L', '2021-07-14', 'lidya_wangg[#]Order B120 L biru', 'new'),
(180, 2, 'fanni_natalia', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'fanni_natalia[#]Order B120 Biru L', 'new'),
(181, 2, 'h3nny_h3nny', 'B120', 1, 'S', 'BIRU', '2021-07-14', 'h3nny_h3nny[#]ORDER B120 BIRU S', 'new'),
(182, 2, 'ling_ling87', 'B120', 1, 'L', 'PINK', '2021-07-14', 'ling_ling87[#]Order B120 Pink L', 'new'),
(183, 2, 'kikijoe703', '1X', 1, 'BLUE', 'B120', '2021-07-14', 'kikijoe703[#]Order 1x B120 blue', 'new'),
(184, 2, 'christhinatendean', 'KE07', 1, 'M', 'BIRUTUA', '2021-07-14', 'christhinatendean[#]Order KE07 birutua M', 'new'),
(185, 2, 'linkjoannejio', 'B120', 1, 'L', 'PINK', '2021-07-14', 'linkjoannejio[#]ORDER B120 PINK L', 'new'),
(186, 2, 'thalia_maximilian', 'B120', 1, 'S', 'PINK', '2021-07-14', 'thalia_maximilian[#]ORDER B120 PINK S', 'new'),
(187, 2, 'ori3n_yang', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'ori3n_yang[#]ORDER B120 BIRU L', 'new'),
(188, 2, 'fanni_natalia', 'B120', 1, 'L', 'PINK', '2021-07-14', 'fanni_natalia[#]ORDER B120 PINK L', 'new'),
(189, 2, 'susanna_kathryn', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'susanna_kathryn[#]Order B120 Biru L', 'new'),
(190, 2, 'winniep4ng_86', 'B120', 1, 'S', 'BIRU', '2021-07-14', 'winniep4ng_86[#]Order B120 Biru S', 'new'),
(191, 2, 'siaumie_fang', 'B120', 1, 'BIRU', 'M', '2021-07-14', 'siaumie_fang[#]ORDER B120 M BIRU', 'new'),
(192, 2, 'cindynatasya51', 'MASKER', 1, 'PINK', 'WARNA', '2021-07-14', 'cindynatasya51[#]Order masker warna pink 1', 'new'),
(193, 2, 'lay7ulia', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'lay7ulia[#]ORDER B120 BIRU L', 'new'),
(194, 2, 'kikijoe703', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'kikijoe703[#]ORDER B120 BLUE L', 'new'),
(195, 2, 'vialiviali080', 'B120', 1, 'S', 'BIRU', '2021-07-14', 'vialiviali080[#]Order B120 Biru S', 'new'),
(196, 2, 'sovi_tze', 'B120', 1, 'S', 'BIRU', '2021-07-14', 'sovi_tze[#]Order B120 biru S', 'new'),
(197, 2, 'lhlink', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'lhlink[#]Order b120 biru L', 'new'),
(198, 2, 'lisahuan6', 'B120', 1, 'L', 'PINK', '2021-07-14', 'lisahuan6[#]ORDER B120 PINK L', 'new'),
(199, 2, 'jessyln_sutopo', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'jessyln_sutopo[#]ORDER B120 biru L', 'new'),
(200, 2, 'dvy_16', 'B120', 1, 'L', 'PINK', '2021-07-14', 'dvy_16[#]Order B120 pink L', 'new'),
(201, 2, 'natasya_deselly', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'natasya_deselly[#]ORDER B120 BIRU L', 'new'),
(202, 2, 'huhu.hehe2020', 'B120', 1, 'S', 'BIRU', '2021-07-14', 'huhu.hehe2020[#]ORDER B120 biru S', 'new'),
(203, 2, 'cindynjt', 'B120', 1, 'M', 'BIRU', '2021-07-14', 'cindynjt[#]Order B120 BIRU M', 'new'),
(204, 2, 'mini.mercy', 'B120', 1, 'XL', 'BIRU', '2021-07-14', 'mini.mercy[#]Order B120 BIRU XL', 'new'),
(205, 2, 'huhu.hehe2020', 'B120', 1, 'M', 'BIRU', '2021-07-14', 'huhu.hehe2020[#]Order B120 BIRU M', 'new'),
(206, 2, 'yanih705', 'J16', 1, 'ALLSIZE', 'HITAM', '2021-07-14', 'yanih705[#]Order J16 HITAM ALLSIZE', 'new'),
(207, 2, 'yuyun_makeuppalembang', 'J16', 1, 'XL', 'HITAM', '2021-07-14', 'yuyun_makeuppalembang[#]Order J16 Hitam XL', 'new'),
(208, 2, 'prettycoolshop0', 'B120', 1, 'S', 'PINK', '2021-07-14', 'prettycoolshop0[#]ORDER B120 PINK S', 'new'),
(209, 2, 'huhu.hehe2020', 'B120', 1, 'SMALL', 'BIRU', '2021-07-14', 'huhu.hehe2020[#]Order B120 Biru small', 'new'),
(210, 2, 'yanih705', 'J16', 1, 'XL', 'HITAM', '2021-07-14', 'yanih705[#]Order J16 HITAM XL', 'new'),
(211, 2, 'meicymarleni', 'J16', 1, 'XL', 'PUTIH', '2021-07-14', 'meicymarleni[#]Order J16 putih XL', 'new'),
(212, 2, 'cahyono1630', 'J16', 1, 'M', 'HITAM', '2021-07-14', 'cahyono1630[#]Order J16 hitam M', 'new'),
(213, 2, 'huhu.hehe2020', 'B120', 1, 'MEDIUM', 'BIRU', '2021-07-14', 'huhu.hehe2020[#]Order B120 biru medium', 'new'),
(214, 2, 'mini.mercy', 'B120', 1, 'XL', 'BIRU', '2021-07-14', 'mini.mercy[#]Order B120 Biru XL', 'new'),
(215, 2, 'acunlies', ',', 0, ',', 'J16', '2021-07-14', 'acunlies[#]Cancel , J16 , All', 'new'),
(216, 2, 'kikijoe703', 'B120', 1, 'L', 'PINK', '2021-07-14', 'kikijoe703[#]Order B120 pink L', 'new'),
(217, 2, 'rachmanyani_anindya', 'J16', 1, 'FITXL', 'PUTIH', '2021-07-14', 'rachmanyani_anindya[#]Order J16 Putih fitXL', 'new'),
(218, 2, 'lhlink', 'B120', 1, 'L', 'PINK', '2021-07-14', 'lhlink[#]Order b120 pink L', 'new'),
(219, 2, 'huhu.hehe2020', 'B120', 1, 'SMALL', 'BIRU', '2021-07-14', 'huhu.hehe2020[#]Order b120 biru small', 'new'),
(220, 2, 'risti_anggreini', 'B120', 1, 'A(', 'BIRU', '2021-07-14', 'risti_anggreini[#]Order b120 biru a(', 'new'),
(221, 2, 'sellyfrancescarifin', 'B120', 0, 'L', 'BIRU', '2021-07-14', 'sellyfrancescarifin[#]Cancel B120 biru L', 'new'),
(222, 2, 'kui_ing8', 'B116', 1, 'M', 'HIJAU', '2021-07-14', 'kui_ing8[#]Order B116 Hijau M', 'new'),
(223, 2, 'dvy_16', 'B120', 1, 'L', 'BIRU', '2021-07-14', 'dvy_16[#]Order B120 biru L', 'new'),
(224, 2, 'yuritapuji', 'J16', 1, 'M', 'PUTIH', '2021-07-14', 'yuritapuji[#]Order j16 putih m', 'new'),
(225, 2, 'prettycoolshop0', 'B120', 1, 'S', 'PINK', '2021-07-14', 'prettycoolshop0[#]ORDER B120 PINK S', 'new'),
(226, 2, 'otumanalu', 'J16', 1, 'XL', 'PUTIH', '2021-07-14', 'otumanalu[#]Order J16 Putih XL', 'new'),
(227, 2, 'mini.mercy', 'B120', 1, 'XL', 'BIRU', '2021-07-14', 'mini.mercy[#]Order B120 biru XL', 'new'),
(228, 2, 'huhu.hehe2020', 'B120', 1, 'M', 'BIRU', '2021-07-14', 'huhu.hehe2020[#]Order B120 biru m', 'new'),
(229, 2, 'indahpermatalimijaya', 'MY3034', 1, 'M', 'PUTIH', '2021-07-14', 'indahpermatalimijaya[#]Order my3034 putih M', 'new'),
(230, 2, 'cenny79', 'MY0304', 1, 'M', 'PUTIH', '2021-07-14', 'cenny79[#]Order MY0304 putih M', 'new'),
(231, 2, 'ribinajeny', 'MY0304', 1, 'S', 'PUTIH', '2021-07-14', 'ribinajeny[#]ORDER MY0304 PUTIH S', 'new'),
(232, 2, 'veniyuanita84', 'FY304', 1, 'PUTIH', 'WARNA', '2021-07-14', 'veniyuanita84[#]ORDER FY304 WARNA PUTIH SIZE L', 'new'),
(233, 2, 'saintclairen', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'saintclairen[#]Order my0304 hitam M', 'new'),
(234, 2, 'cindy_gladies', 'MY0304', 1, 'M', 'PUTIH', '2021-07-14', 'cindy_gladies[#]ORDER MY0304 PUTIH M', 'new'),
(235, 2, 'lhlink', 'MY3034', 1, 'L', 'HITAM', '2021-07-14', 'lhlink[#]Order my3034 hitam L', 'new'),
(236, 2, 'yusi_alice', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'yusi_alice[#]Order my0304 hitam m', 'new'),
(237, 2, 'kikijoe703', 'B120', 1, 'L', 'PINK', '2021-07-14', 'kikijoe703[#]Order B120 pink L', 'new'),
(238, 2, 'yhn.octa', 'MY0304', 1, 'M', 'PUTIH', '2021-07-14', 'yhn.octa[#]ORDER MY0304 PUTIH M', 'new'),
(239, 2, 'yap_yuna', 'MY0304', 1, 'XL', 'HITAM', '2021-07-14', 'yap_yuna[#]Order My0304 hitam XL', 'new'),
(240, 2, 'lunxie212', 'MY0304', 1, 'L', 'HITAM', '2021-07-14', 'lunxie212[#]ORDER MY0304 BLACK L', 'new'),
(241, 2, 'liex.ye', 'MY304', 1, 'M', 'PUTIH', '2021-07-14', 'liex.ye[#]ORDER MY304 putih M', 'new'),
(242, 2, 'scindz', 'B120', 1, 'L', 'PINK', '2021-07-14', 'scindz[#]Order B120 PINK L', 'new'),
(243, 2, 'jessicariched', 'MY304', 1, 'L', 'PUTIH', '2021-07-14', 'jessicariched[#]Order MY304 putih L', 'new'),
(244, 2, 'khe_iambarsari', 'MY304', 1, 'S', 'PUTIH', '2021-07-14', 'khe_iambarsari[#]Order MY304 putih S', 'new'),
(245, 2, 'gestya__', 'MY304', 1, 'M', 'PUTIH', '2021-07-14', 'gestya__[#]Order my304 putih m', 'new'),
(246, 2, 'nanazz83', 'MY3034', 1, 'L', 'PUTIH', '2021-07-14', 'nanazz83[#]Order MY3034 putih L', 'new'),
(247, 2, 'toromo_mossrose', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'toromo_mossrose[#]Order MY0304 Hitam M', 'new'),
(248, 2, 'maxpress_manyartirtoyoso', 'MY0304', 1, 'M', 'PUTIH', '2021-07-14', 'maxpress_manyartirtoyoso[#]ORDER MY0304 PUTIH M', 'new'),
(249, 2, 'beyza_kf', 'MY0304', 1, 'M', 'PUTIH', '2021-07-14', 'beyza_kf[#]Order MY0304 putih M', 'new'),
(250, 2, 'lhlink', 'MY3034', 1, 'L', 'HITAM', '2021-07-14', 'lhlink[#]Order my3034 black L', 'new'),
(251, 2, 'dessy_jayantie', 'MY0304', 1, 'L', 'HITAM', '2021-07-14', 'dessy_jayantie[#]ORDER MY0304 Hitam L', 'new'),
(252, 2, 'veniyuanita84', 'MY0304', 1, 'L', 'PUTIH', '2021-07-14', 'veniyuanita84[#]ORDER MY0304 PUTIH L', 'new'),
(253, 2, 'lhlink', 'MY3024', 1, 'L', 'PUTIH', '2021-07-14', 'lhlink[#]Order my3024 putih L', 'new'),
(254, 2, 'litansinaga', 'MY0304', 1, 'L', 'HITAM', '2021-07-14', 'litansinaga[#]Order MY0304 Hitam L', 'new'),
(255, 2, 'p_lie0607', 'MY0304', 1, 'L', 'PUTIH', '2021-07-14', 'p_lie0607[#]Order My0304 putih L', 'new'),
(256, 2, 'jessicariched', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'jessicariched[#]Order MY0304 hitam M', 'new'),
(257, 2, 'mini.mercy', 'MY0304', 1, 'XL', 'HITAM', '2021-07-14', 'mini.mercy[#]Order my0304 hitam XL', 'new'),
(258, 2, 'nanazz83', 'MY0304', 1, 'L', 'PUTIH', '2021-07-14', 'nanazz83[#]Order my0304 putih L', 'new'),
(259, 2, 'meidyanakho88', 'MY0304', 1, 'L', 'HITAM', '2021-07-14', 'meidyanakho88[#]Order my0304 hitam L', 'new'),
(260, 2, 'yap_yuna', 'MY', 0, 'XL', '0304', '2021-07-14', 'yap_yuna[#]Cancel my 0304 Xl tadi ci', 'new'),
(261, 2, 'yenni_djong', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'yenni_djong[#]Order MY0304 HITAM M', 'new'),
(262, 2, 'liex.ye', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'liex.ye[#]Order MY0304 hitam M', 'new'),
(263, 2, 'mery_ch3n', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'mery_ch3n[#]Order my0304 hitam m', 'new'),
(264, 2, 'diana_jesslyn', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'diana_jesslyn[#]Order MY0304 Hitam M', 'new'),
(265, 2, 'yennyceni', 'J21', 1, 'ALL', 'CHECK', '2021-07-14', 'yennyceni[#]Order j21 check all', 'new'),
(266, 2, 'diana_jesslyn', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'diana_jesslyn[#]Order MY0304 Hitam M ulang dpt ga ci erna? Dress tadi my0304 kan ya', 'new'),
(267, 2, 'yenni_djong', 'SM8', 1, 'L', 'BIRU', '2021-07-14', 'yenni_djong[#]Order SM8 BIRU L', 'new'),
(268, 2, 'yenni_djong', 'SM8', 1, 'L', 'BIRU', '2021-07-14', 'yenni_djong[#]Order sm8 biru L', 'new'),
(269, 2, 'sherly_tjahjadi', 'SM8', 1, 'L', 'BIRU', '2021-07-14', 'sherly_tjahjadi[#]ORDER SM8 BIRU L', 'new'),
(270, 2, 'liex.ye', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'liex.ye[#]Order my0304 hitam M', 'new'),
(271, 2, '889tya', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', '889tya[#]Order my0304 hitam M', 'new'),
(272, 2, 'yenni_djong', 'MY0304', 1, 'M', 'HITAM', '2021-07-14', 'yenni_djong[#]order My0304 hitam m', 'new'),
(273, 2, 'diana_jesslyn', 'SM8', 1, 'WARNA', 'ADA', '2021-07-14', 'diana_jesslyn[#]Order SM8 ada warna apa aja em', 'new'),
(274, 2, 'yenni_djong', 'SM8', 1, 'L', 'BIRU', '2021-07-14', 'yenni_djong[#]Order SM8 BIRU L', 'new'),
(275, 2, 'sherly_tjahjadi', 'SM8', 1, 'L', 'BIRU', '2021-07-14', 'sherly_tjahjadi[#]ORDER SM8 BIRU L', 'new'),
(276, 2, '889tya', 'MY0304', 1, 'L', 'PUTIH', '2021-07-14', '889tya[#]Order my0304 putih L', 'new'),
(277, 2, 'sherly_tjahjadi', 'SM8', 1, 'L', 'BIRU', '2021-07-14', 'sherly_tjahjadi[#]ORDER SM8 BIRU L', 'new'),
(278, 2, 'macharlotee', 'SM8', 1, 'M', 'BIRU', '2021-07-14', 'macharlotee[#]Order SM8 BIRU M', 'new'),
(279, 2, 'sherly_tjahjadi', 'SM8', 1, 'L', 'BIRU', '2021-07-14', 'sherly_tjahjadi[#]ORDER SM8 BIRU L', 'new'),
(280, 2, 'yenni_djong', 'SM8', 1, 'L', 'BIRU', '2021-07-14', 'yenni_djong[#]Order sm8 biru l', 'new'),
(281, 3, 'nikko_agustino', 'ABJI2', 2, 'S', 'ORANGE', '2021-07-15', 'nikkoagustino[#]order.abji2.2.s.orange', 'paid'),
(282, 3, 'nikko_agustino', 'ABCD1', 2, 'S', 'BLACK', '2021-07-15', 'nikkoagustino[#]order.abcd1.2.m.black', 'paid');

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment`
--

CREATE TABLE `tb_payment` (
  `payment_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `order_date` varchar(20) NOT NULL,
  `shipping_address` text NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `transaction_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `transaction_status` varchar(50) NOT NULL,
  `awb` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_payment`
--

INSERT INTO `tb_payment` (`payment_id`, `id`, `instagram`, `order_date`, `shipping_address`, `amount`, `payment_type`, `transaction_time`, `transaction_status`, `awb`) VALUES
(11, 1626336455, 'nikko_agustino', '2021-07-15', 'Nikko Agustino\\n\r\n                        +6281280016060 / nikkoagustino@gmail.com\\n\r\n                        Ruko Elang Laut Boulevard Blok B No 5\r\nPantai Indah Kapuk\\n\r\n                        Penjaringan, Kota Jakarta Utara, DKI Jakarta', 1321000, 'bca_klikpay', '2021-07-16 12:05:07', 'settlement', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_product_unused`
--

CREATE TABLE `tb_product_unused` (
  `id` int(5) NOT NULL,
  `internal_code` varchar(10) NOT NULL,
  `external_code` varchar(20) DEFAULT NULL,
  `supplier_code` varchar(5) DEFAULT NULL,
  `product_name` varchar(50) NOT NULL,
  `variant` varchar(20) DEFAULT NULL,
  `images` varchar(200) DEFAULT NULL,
  `supplier_price` int(10) DEFAULT NULL,
  `selling_price` int(10) DEFAULT NULL,
  `size_xs` int(3) DEFAULT NULL,
  `size_s` int(3) DEFAULT NULL,
  `size_m` int(3) DEFAULT NULL,
  `size_l` int(3) DEFAULT NULL,
  `size_xl` int(3) DEFAULT NULL,
  `size_2xl` int(3) DEFAULT NULL,
  `size_3xl` int(3) DEFAULT NULL,
  `size_4xl` int(3) DEFAULT NULL,
  `size_5xl` int(3) DEFAULT NULL,
  `size_6xl` int(3) DEFAULT NULL,
  `size_25` int(3) DEFAULT NULL,
  `size_26` int(3) DEFAULT NULL,
  `size_27` int(3) DEFAULT NULL,
  `size_28` int(3) DEFAULT NULL,
  `size_29` int(3) DEFAULT NULL,
  `size_30` int(3) DEFAULT NULL,
  `size_31` int(3) DEFAULT NULL,
  `size_32` int(3) DEFAULT NULL,
  `size_33` int(3) DEFAULT NULL,
  `size_34` int(3) DEFAULT NULL,
  `size_35` int(3) DEFAULT NULL,
  `size_36` int(3) DEFAULT NULL,
  `size_37` int(3) DEFAULT NULL,
  `size_38` int(3) DEFAULT NULL,
  `size_39` int(3) DEFAULT NULL,
  `size_40` int(3) DEFAULT NULL,
  `size_41` int(3) DEFAULT NULL,
  `size_42` int(3) DEFAULT NULL,
  `size_43` int(3) DEFAULT NULL,
  `size_44` int(3) DEFAULT NULL,
  `size_45` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_product_unused`
--

INSERT INTO `tb_product_unused` (`id`, `internal_code`, `external_code`, `supplier_code`, `product_name`, `variant`, `images`, `supplier_price`, `selling_price`, `size_xs`, `size_s`, `size_m`, `size_l`, `size_xl`, `size_2xl`, `size_3xl`, `size_4xl`, `size_5xl`, `size_6xl`, `size_25`, `size_26`, `size_27`, `size_28`, `size_29`, `size_30`, `size_31`, `size_32`, `size_33`, `size_34`, `size_35`, `size_36`, `size_37`, `size_38`, `size_39`, `size_40`, `size_41`, `size_42`, `size_43`, `size_44`, `size_45`) VALUES
(1, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Putih', NULL, 17000, 25000, NULL, 2, 6, 4, NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Pink', NULL, 17000, 25000, 4, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Biru', NULL, 17000, 25000, NULL, 3, 5, NULL, 6, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'ABJI2', '34281937', 'ABJI', 'Sweater Hoodie', 'Biru', NULL, 32000, 50000, NULL, 6, 5, 9, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'ABJI2', '34281937', 'ABJI', 'Sweater Hoodie', 'Hitam', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'ABJI3', '34281937', 'ABJI', 'Piyama', 'Hitam', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'ABJI3', '34281937', 'ABJI', 'Piyama', 'Putih', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'ABJI4', '4672168', 'ABJI', 'Long Dress', 'Pink', NULL, 17000, 25000, NULL, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'ABJI4', '4672168', 'ABJI', 'Long Dress', 'Putih', NULL, 17000, 25000, NULL, 8, 2, NULL, 3, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'ABJI5', '35325', 'ABJI', 'Celana Formal', 'Hitam', NULL, 45000, 70000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 12, 17, 8, 4, 8, 12, 11, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'ABJI5', '35325', 'ABJI', 'Celana Formal', 'Khaki', NULL, 45000, 70000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, 7, 4, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'ABJI6', '642532', 'ABJI', 'Celana Pendek', 'Putih', NULL, 14000, 30000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 2, 5, 5, 7, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'ABJI6', '642532', 'ABJI', 'Celana Pendek', 'Biru', NULL, 14000, 30000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 5, 6, NULL, 2, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_stock`
--

CREATE TABLE `tb_stock` (
  `id` int(11) NOT NULL,
  `internal_code` varchar(10) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `images` text DEFAULT NULL,
  `variant` varchar(20) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `supplier_code` varchar(5) DEFAULT NULL,
  `external_code` varchar(20) DEFAULT NULL,
  `supplier_price` int(11) DEFAULT NULL,
  `selling_price` int(11) DEFAULT NULL,
  `pin_to_top` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_stock`
--

INSERT INTO `tb_stock` (`id`, `internal_code`, `product_name`, `images`, `variant`, `size`, `qty`, `supplier_code`, `external_code`, `supplier_price`, `selling_price`, `pin_to_top`) VALUES
(3, 'ABCD1', 'Sweater Hoodie', 'public/products/NgT5gWmSKsAgCuiupk22j3FdJ2OIXdhQs7tdTiWF.jpg', 'BLACK', 'M', 20, 'ABCD', '73719629614', 500000, 700000, 0),
(4, 'ABCD1', 'Sweater Hoodie', 'public/products/EmLsPvCqtJFp8QxxlvxGGsk2g9I1oOKCm8TL0Yv9.jpg', 'BLACK', 'S', -5, 'ABCD', '73719629614', 500000, 600000, 0),
(5, 'ABCD1', 'Sweater Hoodie', NULL, 'BLACK', '2XL', 3, 'ABCD', '73719629614', 500000, 720000, 0),
(6, 'ABCD1', 'Sweater hoodie', 'public/products/IgRWX6Lo5qHUJh1NYAMmQzbyYFsglA5M5XWVzFjR.jpg', 'Blue', 'L', 10, 'ABCD', '73719629614', 500000, 700000, 0),
(7, 'CB1', 'Plain T-shirt', 'public/products/tCfhjyF6famDnoWZZ84oy52l2TSjG2hWcHtTSB5p.jpg', 'White', 'M', 35, 'ABJI', '5181145', 12000, 25000, 0),
(8, 'CB1', 'Plain T-shirt', 'public/products/dTOdnpf7nITLnTh8U4qk2xqeqlkbqU9Nq9XtZj2y.jpg', 'White', 'XL', 41, 'ABJI', '5181145', 12000, 27000, 0),
(9, 'CB1', 'Plain T-shirt', 'public/products/gDbNNV4vTUYIBUEDulpawcsCXiOkkYqFpZzYQABh.jpg', 'White', 'S', 52, 'ABJI', '5181145', 11000, 20000, 0),
(10, 'CB1', 'Plain T-shirt', 'public/products/Se1nXnmq4PPXCvt60rG7FPerqxIBL2jceCS9Nhuv.jpg', 'Pink', 'S', 34, 'ABJI', '5181145', 21000, 52000, 0),
(12, 'CB1', 'Plain T-shirt', NULL, 'MERAH', 'XL', 5, 'ABJI', '5181145', 22000, 54000, 0),
(13, 'ABJI2', 'Polo Shirt', 'public/products/nzDXc3bu2IJo6NbL2ZLlW7oi48xrQmvxabL6VGYI.jpg', 'ORANGE', 'S', -4, 'ABJI', '742753853', 32000, 56000, 0),
(14, 'ABJI2', 'Polo Shirt', NULL, 'White', 'S', 4, 'ABJI', '742753853', 32000, 56000, 0),
(15, 'CB1', 'Plain T-shirt', NULL, 'Orange', 'M', 10, 'ABJI', '5181145', 21000, 50000, 0),
(16, 'CB1', 'Plain T-shirt', NULL, 'Khaki', 'S', 5, 'ABJI', '5181145', 21000, 51000, 0),
(17, 'ABCD1', 'Sweater Hoodie', 'public/products/9Wlw44mhiyzp4NsfOrMaZP4RjYgZfv0AIc7yAKM4.jpg', 'Orange', 'M', 2, 'ABCD', '73719629614', 40000, 70000, 0),
(18, 'ABCD1', 'Sweater Hoodie', 'public/products/tCKVlUhqX5HVd99OsgQyfQNbESbMbvSAxJJ3urNE.jpg', 'Blue', 'S', 6, 'ABCD', '73719629614', 40000, 70000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

CREATE TABLE `tb_supplier` (
  `id` int(11) NOT NULL,
  `supplier_code` varchar(5) NOT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `location` text DEFAULT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_supplier`
--

INSERT INTO `tb_supplier` (`id`, `supplier_code`, `supplier_name`, `location`, `phone`, `email`) VALUES
(1, 'TEST', 'PT Test Supplier 1', 'Jln. Coba Coba\r\nKel. Penjaringan', '081231213214', 'email@email.com'),
(5, 'ABCD', 'PT Antar Bangsa Citra Dharmaindo', 'Jln. Pluit Raya No 132W\r\nKel. Penjaringan', '+6221-668 2060 / +6221-668 2215 / +6221-668 2216 / +6221-668 2220', 'ptabcdjkt_office@yahoo.com'),
(6, 'ABJI', 'PT Antar Bangsa Jaya Indo', NULL, '021 66674784', NULL),
(7, 'CB', 'CV Coba', 'ITC Mangga Dua', '7278913792', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_system_log`
--

CREATE TABLE `tb_system_log` (
  `id` int(11) NOT NULL,
  `action` varchar(200) NOT NULL,
  `time_log` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_system_log`
--

INSERT INTO `tb_system_log` (`id`, `action`, `time_log`) VALUES
(1, '[TBA] Add new supplier: ABJI - PT Antar Bangsa Jaya Indo', '2021-07-03 15:54:36'),
(2, '[TBA] Add new product abcd1 / Blue / L', '2021-07-04 05:13:46'),
(3, '[TBA] Add new product ABJI1 / white / M', '2021-07-05 06:03:37'),
(4, '[TBA] Add new product ABJI1 / white / XL', '2021-07-05 06:24:46'),
(5, '[TBA] Add new product ABJI1 / white / S', '2021-07-05 06:33:08'),
(6, '[TBA] Update stock product ABJI1 / White / M', '2021-07-05 06:38:17'),
(7, '[TBA] Update stock product ABJI1 / White / XL', '2021-07-05 06:44:35'),
(8, '[TBA] Update stock product ABJI1 / White / XL', '2021-07-05 06:44:56'),
(9, '[TBA] Add new product ABJI1 / pink / S', '2021-07-05 07:21:17'),
(10, '[TBA] Add new product ABJI1 /  / S', '2021-07-05 07:52:24'),
(11, '[TBA] Update stock product ABJI1 / White / S', '2021-07-05 07:56:14'),
(12, '[TBA] Update stock product ABCD1 / Black / S', '2021-07-05 08:06:05'),
(13, '[TBA] Update stock product ABCD1 / Black / M', '2021-07-05 08:07:40'),
(14, '[TBA] Update stock product ABCD1 / Blue / L', '2021-07-05 08:08:33'),
(15, '[TBA] Update stock product ABJI1 / White / M', '2021-07-05 09:33:19'),
(16, '[TBA] Update stock product  /  / ', '2021-07-10 06:17:02'),
(17, '[TBA] Update stock product  /  / ', '2021-07-10 06:17:54'),
(18, '[TBA] Add new product  /  / ', '2021-07-10 06:20:40'),
(19, '[TBA] Update stock product  /  / ', '2021-07-10 06:27:04'),
(20, '[TBA] Update stock product  /  / ', '2021-07-10 06:32:14'),
(21, '[TBA] Add new product  /  / ', '2021-07-10 06:38:27'),
(22, '[TBA] Add new product  /  / ', '2021-07-10 06:38:48'),
(23, '[TBA] Update stock product  /  / ', '2021-07-10 07:57:53'),
(24, '[TBA] Add new product  /  / ', '2021-07-10 09:03:20'),
(25, '[TBA] Add new product  /  / ', '2021-07-10 09:03:23'),
(26, '[TBA] Add new product abcd1 / Orange / M', '2021-07-10 09:44:40'),
(27, '[TBA] Add new product abcd1 / Blue / S', '2021-07-10 09:45:25'),
(28, '[TBA] Update stock product ABCD1 / Blue / S', '2021-07-10 09:46:54'),
(29, '[TBA] Update stock product ABCD1 / Blue / S', '2021-07-10 09:47:31'),
(30, '[TBA] Update stock product ABJI2 / Orange / S', '2021-07-10 11:09:13'),
(31, '[TBA] Add new supplier: CB - CV Coba', '2021-07-13 03:23:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `username` varchar(20) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `access_level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`username`, `fullname`, `password`, `access_level`) VALUES
('admin', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', 'superuser');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_payment`
--
ALTER TABLE `tb_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tb_product_unused`
--
ALTER TABLE `tb_product_unused`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_stock`
--
ALTER TABLE `tb_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_system_log`
--
ALTER TABLE `tb_system_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;

--
-- AUTO_INCREMENT for table `tb_payment`
--
ALTER TABLE `tb_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_product_unused`
--
ALTER TABLE `tb_product_unused`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tb_stock`
--
ALTER TABLE `tb_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_system_log`
--
ALTER TABLE `tb_system_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
