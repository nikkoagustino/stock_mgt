CREATE TABLE tb_slideshow (
    id int(11) auto_increment NOT NULL,
    image_url varchar(200) NOT NULL,
    created_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT tb_slideshow_pk PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;