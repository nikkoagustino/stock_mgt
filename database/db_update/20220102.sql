CREATE TABLE tb_category (
    category_name varchar(100) NOT NULL,
    CONSTRAINT tb_category_pk PRIMARY KEY (category_name)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

INSERT INTO tb_category (category_name) VALUES
     ('Baju Pria'),
     ('Baju Wanita');

ALTER TABLE tb_paid_order MODIFY COLUMN order_date datetime NOT NULL;

ALTER TABLE tb_order MODIFY COLUMN tanggal DATETIME NOT NULL;
