CREATE TABLE `tb_payment` (
  `id` int(11) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `order_date` varchar(20) NOT NULL,
  `shipping_address` text NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `transaction_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `transaction_status` varchar(50) NOT NULL,
  `awb` varchar(20) DEFAULT NULL,
  `order_status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `tb_payment`
  ADD PRIMARY KEY (`id`);
COMMIT;

CREATE TABLE `tb_customer` (
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`username`);
COMMIT;

CREATE TABLE `tb_customer_address` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `shipping_address` text NOT NULL,
  `shipping_cost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `tb_customer_address`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tb_customer_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;


CREATE TABLE `tb_paid_order` (
  `id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `internal_code` varchar(50) NOT NULL,
  `variant` varchar(50) NOT NULL,
  `size` varchar(20) NOT NULL,
  `qty` int(5) NOT NULL,
  `selling_price` int(11) NOT NULL,
  `order_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `tb_paid_order`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tb_paid_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;