-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2021 at 10:58 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_stockmgt`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_refund_detail`
--

CREATE TABLE `tb_refund_detail` (
  `id` int(11) NOT NULL,
  `refund_master_id` varchar(20) NOT NULL,
  `step_no` int(11) NOT NULL,
  `step_detail` varchar(100) NOT NULL,
  `courier_retur` varchar(50) DEFAULT NULL,
  `awb_retur` varchar(50) DEFAULT NULL,
  `received_product_photo` varchar(200) DEFAULT NULL,
  `resolution_option` varchar(20) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `account_no` varchar(50) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `transfer_proof` varchar(200) DEFAULT NULL,
  `awb_replacement` varchar(50) DEFAULT NULL,
  `log_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_refund_item`
--

CREATE TABLE `tb_refund_item` (
  `id` int(11) NOT NULL,
  `refund_master_id` varchar(20) NOT NULL,
  `paid_order_item_id` int(11) NOT NULL,
  `internal_code` varchar(50) NOT NULL,
  `variant` varchar(50) NOT NULL,
  `size` varchar(50) NOT NULL,
  `qty_masalah` int(11) NOT NULL,
  `selling_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_refund_master`
--

CREATE TABLE `tb_refund_master` (
  `refund_master_id` varchar(20) NOT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `instagram` varchar(50) NOT NULL,
  `alasan` text DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `last_step_no` int(2) DEFAULT NULL,
  `last_step_detail` varchar(200) DEFAULT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_refund_detail`
--
ALTER TABLE `tb_refund_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_refund_item`
--
ALTER TABLE `tb_refund_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_refund_master`
--
ALTER TABLE `tb_refund_master`
  ADD PRIMARY KEY (`refund_master_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_refund_detail`
--
ALTER TABLE `tb_refund_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_refund_item`
--
ALTER TABLE `tb_refund_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE `tb_customer` CHANGE `password` `password` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL;

ALTER TABLE `tb_customer_address` ADD `subdistrict_id` VARCHAR(8) NULL AFTER `shipping_address`;