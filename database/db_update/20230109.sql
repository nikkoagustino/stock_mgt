CREATE TABLE tb_whatsapp (
    id bigint(20) auto_increment NOT NULL,
    sender varchar(100) NULL,
    message text NULL,
    created_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT tb_whatsapp_pk PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;
