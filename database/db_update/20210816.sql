ALTER TABLE `tb_order` CHANGE `order_status` `order_status` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'new';

CREATE TABLE `tb_hit_and_run` (
  `instagram` varchar(50) NOT NULL,
  `flag` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `tb_hit_and_run`
  ADD PRIMARY KEY (`instagram`);
COMMIT;

ALTER TABLE `tb_stock_master` ADD `category` VARCHAR(50) NOT NULL DEFAULT 'Uncategorized' AFTER `selling_price`;