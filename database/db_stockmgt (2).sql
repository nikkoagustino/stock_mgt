-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2021 at 08:10 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_stockmgt`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id` int(11) NOT NULL,
  `batch_no` int(11) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `internal_code` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `size` varchar(20) NOT NULL,
  `variant` varchar(50) NOT NULL,
  `tanggal` varchar(30) NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `order_status` enum('new','paid','delivered') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id`, `batch_no`, `instagram`, `internal_code`, `qty`, `size`, `variant`, `tanggal`, `comment`, `order_status`) VALUES
(1, 0, 'afiraindonesia', 'K2', 2, 'XL', 'HITAM', '2021-07-09', NULL, 'new'),
(2, 0, 'afiraindonesia', 'CT25', 3, '3XL', 'BIRU', '2021-07-09', NULL, 'new'),
(3, 0, 'manmeliveadm', 'KN1', 2, 'XL', 'HITAM', '2021-07-09', NULL, 'new'),
(4, 0, 'doragonindonesia', 'K4', 1, 'XL', 'MERAH', '2021-07-09', NULL, 'new'),
(5, 0, 'afiraindonesia', 'X1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.x1.2.xl.merah', 'new'),
(6, 0, 'afiraindonesia', 'A1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.a1.2.xl.merah', 'new'),
(7, 0, 'afiraindonesia', 'A1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.a1.2.xl.merah', 'new'),
(8, 0, 'afiraindonesia', 'B1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.b1.2.xl.merah', 'new'),
(9, 0, 'afiraindonesia', 'C1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c1.1.xl.hitam', 'new'),
(10, 0, 'afiraindonesia', 'D1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.d1.1.xl.hitam', 'new'),
(11, 0, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.l1.1.xl.hitam', 'new'),
(12, 0, 'afiraindonesia', 'C1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c1.1.xl.hitam', 'new'),
(13, 0, 'afiraindonesia', 'M1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.m1.1.xl.hitam', 'new'),
(14, 0, 'afiraindonesia', 'K1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.k1.1.xl.hitam', 'new'),
(15, 0, 'afiraindonesia', 'Z1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.Z1.1.xl.hitam', 'new'),
(16, 0, 'afiraindonesia', 'D1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.D1.1.xl.hitam', 'new'),
(17, 0, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.l1.1.xl.hitam', 'new'),
(18, 0, 'afiraindonesia', 'K1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.k1.1.xl.hitam', 'new'),
(19, 0, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.L1.1.xl.hitam', 'new'),
(20, 0, 'afiraindonesia', 'MK1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.mk1.2.xl.merah', 'new'),
(21, 0, 'afiraindonesia', 'MK1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.mk1.2.xl.merah', 'new'),
(22, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(23, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(24, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(25, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(26, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(27, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(28, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(29, 0, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(30, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(31, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(32, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(33, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(34, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(35, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(36, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(37, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(38, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(39, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(40, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(41, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(42, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(43, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(44, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(45, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(46, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(47, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(48, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(49, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(50, 0, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(51, 0, 'afiraindonesia', 'KM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.km1.2.xl.hitam', 'new'),
(52, 0, 'afiraindonesia', 'KM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.km1.2.xl.hitam', 'new'),
(53, 0, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(54, 0, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(55, 0, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(56, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(57, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(58, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(59, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(60, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(61, 0, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(62, 0, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(63, 0, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(64, 0, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(65, 0, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(66, 0, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(67, 0, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(68, 0, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(69, 0, 'afiraindonesia', 'A3', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.a3.1.xl.hitam', 'new'),
(70, 0, 'afiraindonesia', 'A5', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.a5.2.xl.hitam', 'new'),
(71, 1, 'afiraindonesia', 'K2', 2, 'XL', 'HITAM', '2021-07-09', NULL, 'new'),
(72, 1, 'afiraindonesia', 'CT25', 3, '3XL', 'BIRU', '2021-07-09', NULL, 'new'),
(73, 1, 'manmeliveadm', 'KN1', 2, 'XL', 'HITAM', '2021-07-09', NULL, 'new'),
(74, 1, 'doragonindonesia', 'K4', 1, 'XL', 'MERAH', '2021-07-09', NULL, 'new'),
(75, 1, 'afiraindonesia', 'X1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.x1.2.xl.merah', 'new'),
(76, 1, 'afiraindonesia', 'A1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.a1.2.xl.merah', 'new'),
(77, 1, 'afiraindonesia', 'A1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.a1.2.xl.merah', 'new'),
(78, 1, 'afiraindonesia', 'B1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.b1.2.xl.merah', 'new'),
(79, 1, 'afiraindonesia', 'C1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c1.1.xl.hitam', 'new'),
(80, 1, 'afiraindonesia', 'D1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.d1.1.xl.hitam', 'new'),
(81, 1, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.l1.1.xl.hitam', 'new'),
(82, 1, 'afiraindonesia', 'C1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c1.1.xl.hitam', 'new'),
(83, 1, 'afiraindonesia', 'M1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.m1.1.xl.hitam', 'new'),
(84, 1, 'afiraindonesia', 'K1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.k1.1.xl.hitam', 'new'),
(85, 1, 'afiraindonesia', 'Z1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.Z1.1.xl.hitam', 'new'),
(86, 1, 'afiraindonesia', 'D1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.D1.1.xl.hitam', 'new'),
(87, 1, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.l1.1.xl.hitam', 'new'),
(88, 1, 'afiraindonesia', 'K1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.k1.1.xl.hitam', 'new'),
(89, 1, 'afiraindonesia', 'L1', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.L1.1.xl.hitam', 'new'),
(90, 1, 'afiraindonesia', 'MK1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.mk1.2.xl.merah', 'new'),
(91, 1, 'afiraindonesia', 'MK1', 2, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.mk1.2.xl.merah', 'new'),
(92, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(93, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(94, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(95, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(96, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(97, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(98, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(99, 1, 'afiraindonesia', 'CB2', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb2.1.xl.merah', 'new'),
(100, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(101, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(102, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(103, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(104, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(105, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(106, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(107, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(108, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(109, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(110, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(111, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(112, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(113, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(114, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(115, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(116, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(117, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(118, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(119, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(120, 1, 'afiraindonesia', 'CB1', 1, 'XL', 'MERAH', '2021-07-09', 'afiraindonesia[#]order.cb1.1.xl.merah', 'new'),
(121, 1, 'afiraindonesia', 'KM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.km1.2.xl.hitam', 'new'),
(122, 1, 'afiraindonesia', 'KM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.km1.2.xl.hitam', 'new'),
(123, 1, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(124, 1, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(125, 1, 'afiraindonesia', 'C51', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.c51.1.xl.hitam', 'new'),
(126, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(127, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(128, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(129, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(130, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(131, 1, 'afiraindonesia', 'XM1', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.xm1.2.xl.hitam', 'new'),
(132, 1, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(133, 1, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(134, 1, 'afiraindonesia', 'B2', 2, 'XL', 'KUNING', '2021-07-09', 'afiraindonesia[#]order.b2.2.xl.kuning', 'new'),
(135, 1, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(136, 1, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(137, 1, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(138, 1, 'afiraindonesia', 'A2', 2, 'XL', 'HIJAU', '2021-07-09', 'afiraindonesia[#]order.a2.2.xl.hijau', 'new'),
(139, 1, 'afiraindonesia', 'A3', 1, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.a3.1.xl.hitam', 'new'),
(140, 1, 'afiraindonesia', 'A5', 2, 'XL', 'HITAM', '2021-07-09', 'afiraindonesia[#]order.a5.2.xl.hitam', 'new');

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment`
--

CREATE TABLE `tb_payment` (
  `id` int(11) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `order_date` varchar(20) NOT NULL,
  `shipping_address` text NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `transaction_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `transaction_status` varchar(50) NOT NULL,
  `awb` varchar(20) DEFAULT NULL,
  `order_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_payment`
--

INSERT INTO `tb_payment` (`id`, `instagram`, `order_date`, `shipping_address`, `amount`, `payment_type`, `transaction_time`, `transaction_status`, `awb`, `order_status`) VALUES
(1626336455, 'doragonindonesia', '2021-07-09', 'Nikko Agustino\\n\r\n                        +6281280016060 / nikkoagustino@gmail.com\\n\r\n                        Ruko Elang Laut Boulevard Blok B No 5\r\nPantai Indah Kapuk\\n\r\n                        Penjaringan, Kota Jakarta Utara, DKI Jakarta', 1321000, 'bca_klikpay', '2021-07-23 16:58:58', 'settlement', 'ID894027101', 'delivery');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product_unused`
--

CREATE TABLE `tb_product_unused` (
  `id` int(5) NOT NULL,
  `internal_code` varchar(10) NOT NULL,
  `external_code` varchar(20) DEFAULT NULL,
  `supplier_code` varchar(5) DEFAULT NULL,
  `product_name` varchar(50) NOT NULL,
  `variant` varchar(20) DEFAULT NULL,
  `images` varchar(200) DEFAULT NULL,
  `supplier_price` int(10) DEFAULT NULL,
  `selling_price` int(10) DEFAULT NULL,
  `size_xs` int(3) DEFAULT NULL,
  `size_s` int(3) DEFAULT NULL,
  `size_m` int(3) DEFAULT NULL,
  `size_l` int(3) DEFAULT NULL,
  `size_xl` int(3) DEFAULT NULL,
  `size_2xl` int(3) DEFAULT NULL,
  `size_3xl` int(3) DEFAULT NULL,
  `size_4xl` int(3) DEFAULT NULL,
  `size_5xl` int(3) DEFAULT NULL,
  `size_6xl` int(3) DEFAULT NULL,
  `size_25` int(3) DEFAULT NULL,
  `size_26` int(3) DEFAULT NULL,
  `size_27` int(3) DEFAULT NULL,
  `size_28` int(3) DEFAULT NULL,
  `size_29` int(3) DEFAULT NULL,
  `size_30` int(3) DEFAULT NULL,
  `size_31` int(3) DEFAULT NULL,
  `size_32` int(3) DEFAULT NULL,
  `size_33` int(3) DEFAULT NULL,
  `size_34` int(3) DEFAULT NULL,
  `size_35` int(3) DEFAULT NULL,
  `size_36` int(3) DEFAULT NULL,
  `size_37` int(3) DEFAULT NULL,
  `size_38` int(3) DEFAULT NULL,
  `size_39` int(3) DEFAULT NULL,
  `size_40` int(3) DEFAULT NULL,
  `size_41` int(3) DEFAULT NULL,
  `size_42` int(3) DEFAULT NULL,
  `size_43` int(3) DEFAULT NULL,
  `size_44` int(3) DEFAULT NULL,
  `size_45` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_product_unused`
--

INSERT INTO `tb_product_unused` (`id`, `internal_code`, `external_code`, `supplier_code`, `product_name`, `variant`, `images`, `supplier_price`, `selling_price`, `size_xs`, `size_s`, `size_m`, `size_l`, `size_xl`, `size_2xl`, `size_3xl`, `size_4xl`, `size_5xl`, `size_6xl`, `size_25`, `size_26`, `size_27`, `size_28`, `size_29`, `size_30`, `size_31`, `size_32`, `size_33`, `size_34`, `size_35`, `size_36`, `size_37`, `size_38`, `size_39`, `size_40`, `size_41`, `size_42`, `size_43`, `size_44`, `size_45`) VALUES
(1, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Putih', NULL, 17000, 25000, NULL, 2, 6, 4, NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Pink', NULL, 17000, 25000, 4, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Biru', NULL, 17000, 25000, NULL, 3, 5, NULL, 6, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'ABJI2', '34281937', 'ABJI', 'Sweater Hoodie', 'Biru', NULL, 32000, 50000, NULL, 6, 5, 9, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'ABJI2', '34281937', 'ABJI', 'Sweater Hoodie', 'Hitam', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'ABJI3', '34281937', 'ABJI', 'Piyama', 'Hitam', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'ABJI3', '34281937', 'ABJI', 'Piyama', 'Putih', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'ABJI4', '4672168', 'ABJI', 'Long Dress', 'Pink', NULL, 17000, 25000, NULL, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'ABJI4', '4672168', 'ABJI', 'Long Dress', 'Putih', NULL, 17000, 25000, NULL, 8, 2, NULL, 3, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'ABJI5', '35325', 'ABJI', 'Celana Formal', 'Hitam', NULL, 45000, 70000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 12, 17, 8, 4, 8, 12, 11, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'ABJI5', '35325', 'ABJI', 'Celana Formal', 'Khaki', NULL, 45000, 70000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, 7, 4, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'ABJI6', '642532', 'ABJI', 'Celana Pendek', 'Putih', NULL, 14000, 30000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 2, 5, 5, 7, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'ABJI6', '642532', 'ABJI', 'Celana Pendek', 'Biru', NULL, 14000, 30000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 5, 6, NULL, 2, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_stock`
--

CREATE TABLE `tb_stock` (
  `id` int(11) NOT NULL,
  `internal_code` varchar(10) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `images` text DEFAULT NULL,
  `variant` varchar(20) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `supplier_code` varchar(5) DEFAULT NULL,
  `external_code` varchar(20) DEFAULT NULL,
  `supplier_price` int(11) DEFAULT NULL,
  `selling_price` int(11) DEFAULT NULL,
  `pin_to_top` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_stock`
--

INSERT INTO `tb_stock` (`id`, `internal_code`, `product_name`, `images`, `variant`, `size`, `qty`, `supplier_code`, `external_code`, `supplier_price`, `selling_price`, `pin_to_top`) VALUES
(3, 'ABCD1', 'Sweater Hoodie', 'public/products/NgT5gWmSKsAgCuiupk22j3FdJ2OIXdhQs7tdTiWF.jpg', 'Black', 'M', 20, 'ABCD', '73719629614', 500000, 700000, 0),
(4, 'ABCD1', 'Sweater Hoodie', 'public/products/EmLsPvCqtJFp8QxxlvxGGsk2g9I1oOKCm8TL0Yv9.jpg', 'Black', 'S', 15, 'ABCD', '73719629614', 500000, 600000, 0),
(6, 'ABCD1', 'Sweater hoodie', 'public/products/IgRWX6Lo5qHUJh1NYAMmQzbyYFsglA5M5XWVzFjR.jpg', 'Blue', 'L', 10, 'ABCD', '73719629614', 500000, 700000, 0),
(7, 'ABJI1', 'Plain T-shirt', 'public/products/tCfhjyF6famDnoWZZ84oy52l2TSjG2hWcHtTSB5p.jpg', 'White', 'M', 35, 'ABJI', '5181145', 12000, 25000, 0),
(8, 'ABJI1', 'Plain T-shirt', 'public/products/dTOdnpf7nITLnTh8U4qk2xqeqlkbqU9Nq9XtZj2y.jpg', 'White', 'XL', 41, 'ABJI', '5181145', 12000, 27000, 0),
(9, 'ABJI1', 'Plain T-shirt', 'public/products/gDbNNV4vTUYIBUEDulpawcsCXiOkkYqFpZzYQABh.jpg', 'White', 'S', 52, 'ABJI', '5181145', 11000, 20000, 0),
(10, 'ABJI1', 'Plain T-shirt', 'public/products/Se1nXnmq4PPXCvt60rG7FPerqxIBL2jceCS9Nhuv.jpg', 'Pink', 'S', 34, 'ABJI', '5181145', 21000, 52000, 0),
(12, 'ABJI1', 'Plain T-shirt', NULL, 'Pink', 'M', 5, 'ABJI', '5181145', 22000, 54000, 0),
(13, 'ABJI2', 'Polo Shirt', 'public/products/nzDXc3bu2IJo6NbL2ZLlW7oi48xrQmvxabL6VGYI.jpg', 'Orange', 'S', 6, 'ABJI', '742753853', 32000, 56000, 0),
(15, 'ABJI1', 'Plain T-shirt', NULL, 'Orange', 'M', 10, 'ABJI', '5181145', 21000, 50000, 0),
(16, 'ABJI1', 'Plain T-shirt', NULL, 'Khaki', 'S', 5, 'ABJI', '5181145', 21000, 51000, 0),
(17, 'ABCD1', 'Sweater Hoodie', 'public/products/9Wlw44mhiyzp4NsfOrMaZP4RjYgZfv0AIc7yAKM4.jpg', 'Orange', 'M', 14, 'ABCD', '73719629614', 40000, 70000, 0),
(18, 'ABCD1', 'Sweater Hoodie', 'public/products/tCKVlUhqX5HVd99OsgQyfQNbESbMbvSAxJJ3urNE.jpg', 'Blue', 'S', 11, 'ABCD', '73719629614', 40000, 70000, 0),
(19, 'ABJI1', 'Plain T-shirt', NULL, 'Pink', 'L', 4, 'ABJI', '5181145', 20000, 50000, 0),
(20, 'ABCD1', 'Sweater Hoodie', 'public/products/vOqlPDeRO3LBM1OCjpFVU0AQMjGYcfUO0e31Rb4O.jpg', 'TORQUISE', 'S', 14, 'ABCD', '73719629614', 40000, 72000, 0),
(21, 'ABCD1', 'Sweater Hoodie', NULL, 'ORANGE', 'XL', 10, 'ABCD', '73719629614', 50000, 78000, 0),
(22, 'ABCD1', 'Sweater Hoodie', NULL, 'BLACK', 'XL', 5, 'ABCD', '73719629614', 50000, 78000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_stock_master`
--

CREATE TABLE `tb_stock_master` (
  `external_code` varchar(200) NOT NULL,
  `master_image` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_stock_master`
--

INSERT INTO `tb_stock_master` (`external_code`, `master_image`) VALUES
('5181145', 'public/products/vDgQOFrxZxu7amXLxfAkNff5zAoZrlG3wIKoKjfo.jpg'),
('73719629614', 'public/products/x8zEidBrL62ljAf4VZBlJoSnL3R8wz15FqNZDXaJ.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

CREATE TABLE `tb_supplier` (
  `id` int(11) NOT NULL,
  `supplier_code` varchar(5) NOT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `location` text DEFAULT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_supplier`
--

INSERT INTO `tb_supplier` (`id`, `supplier_code`, `supplier_name`, `location`, `phone`, `email`) VALUES
(1, 'TEST', 'PT Test Supplier 1', 'Jln. Coba Coba\r\nKel. Penjaringan', '081231213214', 'email@email.com'),
(5, 'ABCD', 'PT Antar Bangsa Citra Dharmaindo', 'Jln. Pluit Raya No 132W\r\nKel. Penjaringan', '+6221-668 2060 / +6221-668 2215 / +6221-668 2216 / +6221-668 2220', 'ptabcdjkt_office@yahoo.com'),
(6, 'ABJI', 'PT Antar Bangsa Jaya Indo', NULL, '021 66674784', NULL),
(7, 'CB', 'CV Coba', 'ITC Mangga Dua', '7278913792', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_system_log`
--

CREATE TABLE `tb_system_log` (
  `id` int(11) NOT NULL,
  `action` varchar(200) NOT NULL,
  `time_log` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_system_log`
--

INSERT INTO `tb_system_log` (`id`, `action`, `time_log`) VALUES
(1, '[TBA] Add new supplier: ABJI - PT Antar Bangsa Jaya Indo', '2021-07-03 15:54:36'),
(2, '[TBA] Add new product abcd1 / Blue / L', '2021-07-04 05:13:46'),
(3, '[TBA] Add new product ABJI1 / white / M', '2021-07-05 06:03:37'),
(4, '[TBA] Add new product ABJI1 / white / XL', '2021-07-05 06:24:46'),
(5, '[TBA] Add new product ABJI1 / white / S', '2021-07-05 06:33:08'),
(6, '[TBA] Update stock product ABJI1 / White / M', '2021-07-05 06:38:17'),
(7, '[TBA] Update stock product ABJI1 / White / XL', '2021-07-05 06:44:35'),
(8, '[TBA] Update stock product ABJI1 / White / XL', '2021-07-05 06:44:56'),
(9, '[TBA] Add new product ABJI1 / pink / S', '2021-07-05 07:21:17'),
(10, '[TBA] Add new product ABJI1 /  / S', '2021-07-05 07:52:24'),
(11, '[TBA] Update stock product ABJI1 / White / S', '2021-07-05 07:56:14'),
(12, '[TBA] Update stock product ABCD1 / Black / S', '2021-07-05 08:06:05'),
(13, '[TBA] Update stock product ABCD1 / Black / M', '2021-07-05 08:07:40'),
(14, '[TBA] Update stock product ABCD1 / Blue / L', '2021-07-05 08:08:33'),
(15, '[TBA] Update stock product ABJI1 / White / M', '2021-07-05 09:33:19'),
(16, '[TBA] Update stock product  /  / ', '2021-07-10 06:17:02'),
(17, '[TBA] Update stock product  /  / ', '2021-07-10 06:17:54'),
(18, '[TBA] Add new product  /  / ', '2021-07-10 06:20:40'),
(19, '[TBA] Update stock product  /  / ', '2021-07-10 06:27:04'),
(20, '[TBA] Update stock product  /  / ', '2021-07-10 06:32:14'),
(21, '[TBA] Add new product  /  / ', '2021-07-10 06:38:27'),
(22, '[TBA] Add new product  /  / ', '2021-07-10 06:38:48'),
(23, '[TBA] Update stock product  /  / ', '2021-07-10 07:57:53'),
(24, '[TBA] Add new product  /  / ', '2021-07-10 09:03:20'),
(25, '[TBA] Add new product  /  / ', '2021-07-10 09:03:23'),
(26, '[TBA] Add new product abcd1 / Orange / M', '2021-07-10 09:44:40'),
(27, '[TBA] Add new product abcd1 / Blue / S', '2021-07-10 09:45:25'),
(28, '[TBA] Update stock product ABCD1 / Blue / S', '2021-07-10 09:46:54'),
(29, '[TBA] Update stock product ABCD1 / Blue / S', '2021-07-10 09:47:31'),
(30, '[TBA] Update stock product ABJI2 / Orange / S', '2021-07-10 11:09:13'),
(31, '[TBA] Add new supplier: CB - CV Coba', '2021-07-13 03:23:03'),
(32, '[] Add new product ABJI1 / Pink / L', '2021-07-17 11:14:16'),
(33, '[Administrator] Update stock product ABCD1 / Blue / S', '2021-07-21 04:27:04'),
(34, '[Administrator] Update stock product ABCD1 / Orange / M', '2021-07-21 04:27:05'),
(35, '[Administrator] Add new product ABCD1 / torquise / S', '2021-07-21 04:27:06'),
(36, '[Administrator] Update stock product ABCD1 / Torquise / S', '2021-07-21 04:30:00'),
(37, '[Administrator] Update stock product ABCD1 / Orange / M', '2021-07-21 04:30:01'),
(38, '[Administrator] Update stock product ABCD1 / Torquise / S', '2021-07-21 04:37:26'),
(39, '[Administrator] Update stock product ABCD1 / Black / S', '2021-07-21 04:37:26'),
(40, '[Administrator] Update stock product ABCD1 / Orange / M', '2021-07-21 04:37:26'),
(41, '[Administrator] Update stock product ABCD1 / Torquise / S', '2021-07-21 04:38:59'),
(42, '[Administrator] Add new product ABCD1 / Orange / XL', '2021-07-21 04:38:59'),
(43, '[Administrator] Update stock product ABCD1 / Black / S', '2021-07-21 04:38:59'),
(44, '[Administrator] Add new product ABCD1 / Black / XL', '2021-07-21 04:38:59'),
(45, '[Administrator] Update stock product ABCD1 / Orange / M', '2021-07-21 04:38:59'),
(46, '[Administrator] Update stock product ABCD1 / Blue / S', '2021-07-21 04:42:12'),
(47, '[Administrator] Update stock product ABCD1 / Orange / M', '2021-07-21 04:43:48'),
(48, '[Administrator] Update stock product ABCD1 / Blue / S', '2021-07-21 04:43:59'),
(49, '[Administrator] Update stock product ABCD1 / Black / S', '2021-07-21 04:43:59'),
(50, '[Administrator] Delete stock ABCD1 / Black / 2XL', '2021-07-21 05:15:32'),
(51, '[Administrator] Delete stock ABJI2 / White / S', '2021-07-21 05:16:32'),
(52, '[Administrator] Update stock product ABCD1 / Orange / XL', '2021-07-23 07:16:56'),
(53, '[Administrator] Update stock product ABCD1 / Orange / XL', '2021-07-23 07:18:34'),
(54, '[Administrator] Update foto master 73719629614', '2021-07-23 08:02:11'),
(55, '[Administrator] Update foto master 73719629614', '2021-07-23 08:04:44'),
(56, '[Administrator] Update foto master 73719629614', '2021-07-23 08:09:51'),
(57, '[Administrator] Update foto master 73719629614', '2021-07-23 08:11:18'),
(58, '[Administrator] Update foto master 73719629614', '2021-07-23 08:11:45'),
(59, '[Administrator] Update foto master 73719629614', '2021-07-23 08:14:50'),
(60, '[Administrator] Update foto master 73719629614', '2021-07-23 08:15:55'),
(61, '[Administrator] Update foto master 5181145', '2021-07-23 08:19:28'),
(62, '[Administrator] Update foto master 5181145', '2021-07-23 08:19:58'),
(63, '[Administrator] Update foto master 5181145', '2021-07-23 08:27:57'),
(64, '[Administrator] Save AWB for 1626336455 : ID89402710', '2021-07-23 16:57:03'),
(65, '[Administrator] Save AWB for 1626336455 : ID894027101', '2021-07-23 16:58:58');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `username` varchar(20) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `access_level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`username`, `fullname`, `password`, `access_level`) VALUES
('admin', 'Administrator', '0f359740bd1cda994f8b55330c86d845', 'superuser');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_payment`
--
ALTER TABLE `tb_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_product_unused`
--
ALTER TABLE `tb_product_unused`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_stock`
--
ALTER TABLE `tb_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_stock_master`
--
ALTER TABLE `tb_stock_master`
  ADD PRIMARY KEY (`external_code`);

--
-- Indexes for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_system_log`
--
ALTER TABLE `tb_system_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `tb_product_unused`
--
ALTER TABLE `tb_product_unused`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tb_stock`
--
ALTER TABLE `tb_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_system_log`
--
ALTER TABLE `tb_system_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
