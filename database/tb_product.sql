-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2021 at 08:34 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_stockmgt`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `id` int(5) NOT NULL,
  `internal_code` varchar(10) NOT NULL,
  `external_code` varchar(20) DEFAULT NULL,
  `supplier_code` varchar(5) DEFAULT NULL,
  `product_name` varchar(50) NOT NULL,
  `variant` varchar(20) DEFAULT NULL,
  `images` varchar(200) DEFAULT NULL,
  `supplier_price` int(10) DEFAULT NULL,
  `selling_price` int(10) DEFAULT NULL,
  `size_xs` int(3) DEFAULT NULL,
  `size_s` int(3) DEFAULT NULL,
  `size_m` int(3) DEFAULT NULL,
  `size_l` int(3) DEFAULT NULL,
  `size_xl` int(3) DEFAULT NULL,
  `size_2xl` int(3) DEFAULT NULL,
  `size_3xl` int(3) DEFAULT NULL,
  `size_4xl` int(3) DEFAULT NULL,
  `size_5xl` int(3) DEFAULT NULL,
  `size_6xl` int(3) DEFAULT NULL,
  `size_25` int(3) DEFAULT NULL,
  `size_26` int(3) DEFAULT NULL,
  `size_27` int(3) DEFAULT NULL,
  `size_28` int(3) DEFAULT NULL,
  `size_29` int(3) DEFAULT NULL,
  `size_30` int(3) DEFAULT NULL,
  `size_31` int(3) DEFAULT NULL,
  `size_32` int(3) DEFAULT NULL,
  `size_33` int(3) DEFAULT NULL,
  `size_34` int(3) DEFAULT NULL,
  `size_35` int(3) DEFAULT NULL,
  `size_36` int(3) DEFAULT NULL,
  `size_37` int(3) DEFAULT NULL,
  `size_38` int(3) DEFAULT NULL,
  `size_39` int(3) DEFAULT NULL,
  `size_40` int(3) DEFAULT NULL,
  `size_41` int(3) DEFAULT NULL,
  `size_42` int(3) DEFAULT NULL,
  `size_43` int(3) DEFAULT NULL,
  `size_44` int(3) DEFAULT NULL,
  `size_45` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`id`, `internal_code`, `external_code`, `supplier_code`, `product_name`, `variant`, `images`, `supplier_price`, `selling_price`, `size_xs`, `size_s`, `size_m`, `size_l`, `size_xl`, `size_2xl`, `size_3xl`, `size_4xl`, `size_5xl`, `size_6xl`, `size_25`, `size_26`, `size_27`, `size_28`, `size_29`, `size_30`, `size_31`, `size_32`, `size_33`, `size_34`, `size_35`, `size_36`, `size_37`, `size_38`, `size_39`, `size_40`, `size_41`, `size_42`, `size_43`, `size_44`, `size_45`) VALUES
(1, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Putih', NULL, 17000, 25000, NULL, 2, 6, 4, NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Pink', NULL, 17000, 25000, 4, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'ABJI1', '327189472189', 'ABJI', 'T-Shirt Polos', 'Biru', NULL, 17000, 25000, NULL, 3, 5, NULL, 6, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'ABJI2', '34281937', 'ABJI', 'Sweater Hoodie', 'Biru', NULL, 32000, 50000, NULL, 6, 5, 9, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'ABJI2', '34281937', 'ABJI', 'Sweater Hoodie', 'Hitam', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'ABJI3', '34281937', 'ABJI', 'Piyama', 'Hitam', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'ABJI3', '34281937', 'ABJI', 'Piyama', 'Putih', NULL, 32000, 50000, 5, 2, 7, 2, 4, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'ABJI4', '4672168', 'ABJI', 'Long Dress', 'Pink', NULL, 17000, 25000, 4, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
