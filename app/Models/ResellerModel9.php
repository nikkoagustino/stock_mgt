<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ResellerModel extends Model
{
    use HasFactory;
    static function getCustomerDataByInstagram($instagram) {
        $result = DB::table('tb_reseller')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }
    static function getCustomerDataByInstagram2($instagram) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function registerUser($req) {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $insert = DB::table('tb_reseller')
                    ->insert([
                        'username' => $req->instagram,
                        'firstname' => $req->nama_depan,
                        'password' => md5($req->password),
                        'nama_toko' => $req->nama_toko,
                        'phone' => $req->telepon,
                        'email' => $req->email,
                        'logo_toko' => $req->sd,
                        'reffer' => $req->reffer,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $insert;
    }

    static function editUser($req) {
        
        $update = DB::table('tb_reseller')
                    ->where('username', '=', $req->instagram)
                    ->update([
                        
                        'firstname' => $req->nama_depan,
                        'nama_toko' => $req->nama_toko,
                        
                        'email' => $req->email,
                        
                    ]);
        return $update;


    }

    static function editPassword($req) {
        
        $update = DB::table('tb_reseller')
                    ->where('username', '=', $req->ig)
                    ->update([
                        
                        'password' => md5($req->password),
                        
                        
                    ]);
        return $update;


    }

    static function EditPhotos($ig,$path) {
        
        $update = DB::table('tb_reseller')
                    ->where('username', '=', $ig)
                    ->update([
                        
                        'logo_toko' => $path,
                        
                        
                    ]);
        return $update;


    }

    static function editAddress($req) {
        
        $address_gen = "$req->nama_depan1 \n $req->telepon1 / $req->email1 \n $req->address \n $req->subdistrict, $req->city, $req->province";
        //dd($address_gen);
        $update = DB::table('tb_reseller')
                    ->where('username', '=', $req->instagram1)
                    ->update([
                        
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $update;


    }

    

    static function login($req) {
        $result = DB::table('tb_reseller')
                    ->where('username', '=', $req->instagram)
                    ->where('password', '=', md5($req->password))
                    ->first();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    static function getOrderByParameterV2($instagram, $date) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    ->where('tanggal', '<=', $date)
                    ->groupBy('internal_code')
                    ->groupBy('variant')
                    ->groupBy('size')
                    ->select('id','internal_code', 'variant', 'size', DB::raw('SUM(qty) AS qty, MAX(tanggal) AS tanggal, MAX(instagram) AS instagram'))
                    ->get();
        return $result;
    }

    static function getAllStock() {
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    ->orderBy('pin_to_top', 'desc')
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc')
                    ->get();
        return $result;
    }

    static function getSupplierList() {
    	$result = DB::table('tb_supplier')
    				->get();
    	return $result;
    }

    static function getMasterData() {
        $result = DB::table('tb_stock_master')
                    ->get();
        return $result;
    }

    
    static function getCustomer($instagram) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function getCustomerAddress($instagram) {
        $result = DB::table('tb_customer_address')
                    ->where('username', '=', $instagram)
                    ->get();
        return $result;
    }

    static function getProductMainData() {
        $result = DB::table('tb_stock')
                    ->groupBy('internal_code')
                    ->select('internal_code', DB::raw('MAX(product_name) AS product_name'))
                    ->get();
        return $result;
    }

    static function getProductMainDataSimple() {
        $result = DB::table('tb_stock')
                    ->where('qty', '>', 0)
                    ->groupBy('internal_code')
                    ->select('internal_code', DB::raw('MAX(product_name) AS product_name'))
                    ->get();
        return $result;
    }

    static function getOrderByParameter($instagram, $date) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    ->where('tanggal', '=', $date)
                    ->orderBy('id', 'asc')
                    ->get();
        return $result;
    }

} 
