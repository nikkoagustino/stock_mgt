<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class WhatsappModel extends Model
{
    use HasFactory;

    static function insert($req) {
        $insert = DB::table('tb_whatsapp')
                    ->insert($req);
        return $insert;
    }
  
  
    static function insertOrder($instagram,$internal_code,$qty,$size,$variant,$tanggal) {
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => '-3',
                        'instagram' => $instagram,
                        'internal_code' => $internal_code,
                        'qty' => $qty,
                      	'size' => $size,
                        'variant' => $variant,
                        'tanggal' => $tanggal,
                        'comment' => 'Order dari whatsapp',
                      	'order_status' => 'new',
                    ]);
        return $insert;
    }
        

    static function readAll() {
        $result = DB::table('tb_whatsapp')
                    ->get();
        return $result;
    }
  

    static function getOrderByPhone($instagram) {
        $result = DB::table('tb_order')
          			->leftJoin('tb_customer', 'tb_order.instagram', '=', 'tb_customer.username')
                    ->where('username', '=', $instagram)
                    ->groupBy('internal_code')
                    ->groupBy('variant')
                    ->groupBy('size')
                    ->select('internal_code', 'variant', 'size', DB::raw('SUM(qty) AS qty, MAX(tanggal) AS tanggal, MAX(instagram) AS instagram'))
                    ->get();
        return $result;
    }
  
  static function getPaymentIDList($instagram) {
        $result = DB::table('tb_payment')
          			
                    ->where('instagram', '=', $instagram)
          			->where('transaction_status', '=', 'Midtrans - pending')

                    
                    ->get();
        return $result;
    }
  

      static function getPriceByParameter($internal_code, $variant,$size) {
        $result = DB::table('tb_stock')
                    ->where('internal_code','=',$internal_code)
                    ->where('variant','=',$variant)
          			->where('size', '=', $size)
                    ->select('selling_price','qty')
                    ->distinct()
                    ->first();
        return $result;
    }
  
        static function getDiscountWA() {
        $result = DB::table('tb_discount')
                    ->where('id','=','1')
                    ->select('percentwa')
                    ->distinct()
                    ->first();
        return $result;
    }
  
    static function getShippingCost($username) {
        $result = DB::table('tb_customer_address')
                    ->where('username','=',$username)
                    ->select('shipping_address','shipping_cost')
                    ->distinct()
                    ->first();
        return $result;
    }
  
        static function getVariantbyInternalCode($internal_code) {
        $result = DB::table('tb_stock')
          		    ->where('qty', '>', 0)
                    ->where('internal_code','=',$internal_code)
                    ->select('variant')
                    ->distinct()
                    ->get();
        return $result;
    }
  
          static function getSize($internal_code,$variant) {
        $result = DB::table('tb_stock')
          			->where('qty', '>', 0)
                    ->where('internal_code','=',$internal_code)
          			->where('variant','=',$variant)
                    ->select('size','qty')
                    ->distinct()
                    ->get();
        return $result;
    }
  
  
       static function checkPhoneAdded($phone) {
        $result = DB::table('tb_whatsappbot')
                    ->where('phone','=',$phone)
                    ->distinct()
                    ->get();
        return $result;
    }
  
  static function AddPhoneNumber($phone) {
        $insert = DB::table('tb_whatsappbot')
                    ->insert([
                        'phone' => $phone,
                    ]);
                    
        return $insert;
    }
  
     static function checkPhoneRegistered($phone) {
        $result = DB::table('tb_customer')
                    ->where('username','=',$phone)
                    ->distinct()
                    ->get();
        return $result;
    }
  static function CheckWhatsappBotStatus($phone) {
        $result = DB::table('tb_whatsappbot')
                    ->where('phone','=',$phone)
                    ->distinct()
                    ->first();
        return $result;
    }
    static function CheckLastOrder($phone) {
        $result = DB::table('tb_order')
                    ->where('instagram','=',$phone)
                    ->distinct()
                    ->get();
        return $result;
    }
  
    static function updateWhatsappBotStatus($phone,$status) {
        $update = DB::table('tb_whatsappbot')
                    ->where('phone', '=', $phone)
                    ->update([
                        'status' => $status,
                    ]);
        return $update;
    }
  
     static function updateWhatsappBotVariant($phone,$internal_code,$Variant_list) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'SELECTVARIANT',
                      	'internal_code' => $internal_code,
                        'variant_list' => $Variant_list,
                        'variant_select' => '',
                      	'size_list' => '',
                        'size_select' => '',
                        'qty' => 0,
                        'qtyav' => 0,
                    ]);
        return $update;
    }
  
       static function clearWhatsappBot($phone) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => '',
                      	'internal_code' => '',
                        'variant_list' => '',
                        'variant_select' => '',
                      	'size_list' => '',
                        'size_select' => '',
                        'qty' => 0,
                        'qtyav' => 0,
                    ]);
        return $update;
    }
  
  
  static function updateWhatsappBotSize($phone,$size_list,$variant_select) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERSIZE',
                      	'size_list' => $size_list,
                        'variant_select' => $variant_select,
                    ]);
        return $update;
    }
        static function updateWhatsappBotSizeSelect($phone,$size_select,$qty) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERQUANTITY',
                      	'size_select' => $size_select,
                        'qtyav' => $qty,
                    ]);
        return $update;
    }
  
          static function updateWhatsappBotQty($phone,$qty) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERORDCONF',
                      	'qty' => $qty,
                    ]);
        return $update;
    }
  
    static function updateWhatsappTotalBayar($phone,$TotalBayar) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'READYPAYMENT',
                      	'totalbayar' => $TotalBayar,
                    ]);
        return $update;
    }
      static function updateWhatsappBotFirstName($phone,$FirstName) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERLASTNAME',
                      	'firstname' => $FirstName,
                    ]);
        return $update;
    }
  
  
        static function updateWhatsappBotLastName($phone,$LastName) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTEREMAIL',
                      	'lastname' => $LastName,
                    ]);
        return $update;
    }
  
     static function updateWhatsappBotEmail($phone,$Email) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERPROVINCE',
                      	'email' => $Email,
                    ]);
        return $update;
    }
  
  
       static function updateWhatsappBotProvinsi($phone,$Province,$ProvinceID) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERCITY',
                        'provincelist'=> $ProvinceID,
                      	'provinceselect' => $Province,
                    ]);
        return $update;
    }
  
  static function updateWhatsappBotCity($phone,$City,$CityID) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERSUBDIST',
                        'citylist'=> $CityID,
                      	'cityselect' => $City,
                    ]);
        return $update;
    }
  
  
    static function updateWhatsappBotSubDist($phone,$SubDist,$SubDistID,$shipping_cost) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERADDRESS',
                        'subdislist'=> $SubDistID,
                        'subdislist'=> $SubDistID,
                      	'subdisselect' => $SubDist,
                        'shipping_cost' => $shipping_cost,
                    ]);
        return $update;
    }
   static function updateWhatsappBotAddress($phone,$Address) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERPASSWORD',
                      	'address' => $Address,
                    ]);
        return $update;
    }
     static function updateWhatsappBotPassword($phone,$Password,$shipadd) {
        $update = DB::table('tb_whatsappbot')
          			->where('phone', '=', $phone)
                    ->update([
                        'status' => 'ENTERKONFIRMASI',
                      	'password' => $Password,
                        'shipping_address' => $shipadd,
                    ]);
        return $update;
    }
  
   static function RegisterID($username,$password,$firstname,$lastname,$phone,$email) {
    
        $insert = DB::table('tb_customer')
                    ->insert([
                        'username' => $username,
                      	'password' => md5($password),
                        'firstname' => $firstname,
                        'lastname' => $lastname,
                        'phone' => $phone,
                        'email' => $email,
                        'reffer' => 'WA OTO',
                    ]);
                    
        return $insert;
    }
  
     static function UpdatePassword($username,$password) {
    

       $update = DB::table('tb_customer')
                    ->where('username', '=', $username)
                    ->update([
                        'password' => md5($password),
                    ]);
        $update2 = DB::table('tb_whatsappbot')
                    ->where('phone', '=', $username)
                    ->update([
                        'password' => $password,
                    ]);
        return $update;
    }
  
  
    static function RegisterAddress($username,$address,$subdistrict_id,$shipping_cost) {
    
        $insert = DB::table('tb_customer_address')
                    ->insert([
                        'username' => $username,
                      	'shipping_address' => $address,
                        'subdistrict_id' => $subdistrict_id,
                        'shipping_cost' => $shipping_cost,
                    ]);
                    
        return $insert;
    }
    
        
                    

     static function deleteOrder($username,$internal_code) {
        $delete = DB::table('tb_order')
                    ->where('instagram', '=', $username)
          			->where('internal_code', '=', $internal_code)
                    ->delete();      
        return $delete;
    }
  
       static function deletePhoneRegistered($phone) {
        $delete = DB::table('tb_customer')
                    ->where('username', '=', $phone)
                    ->delete();      
        $delete2 = DB::table('tb_customer_address')
                    ->where('username', '=', $phone)
                    ->delete();  
        return $delete;
    }
  

  
}
