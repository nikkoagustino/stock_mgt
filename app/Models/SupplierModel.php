<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class SupplierModel extends Model
{
    use HasFactory;

    static function loginAttempt($req) {
        
        $result = DB::table('tb_supplierlogin')
                    ->where('username', strtoupper($req->username))
                    ->where('password', $req->password)
                    ->first();
                    
        return $result;
    }

    static function getAllStock() {
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    ->orderBy('pin_to_top', 'desc')
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc')
                    ->get();
        return $result;
    }

    static function getStockHariIni($req) {
        //dd($req);
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where('tb_stock.supplier_code', '=', $req->supplier1)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier2)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier3)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier4)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier5)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier6)
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc')
                    ->get();
        return $result;
    }
  
  	static function getRekapValuasi($req) {
  
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where('tb_stock.supplier_code', '=', $req->supplier1)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier2)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier3)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier4)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier5)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier6)
          			->select(DB::raw('SUM(tb_stock.qty) AS total_item_stock, SUM(tb_stock.qty * tb_stock_master.supplier_price) AS total_valuasi_stock'))
                    ->first();
        return $result;

    }

    

    static function getSales($req) {
        //dd($req);
        $result = DB::table('tb_paid_order')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    //->leftJoin('tb_stock', 'tb_paid_order.internal_code', '=', 'tb_stock.internal_code')
                    ->leftJoin('tb_stock_master', 'tb_paid_order.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where(function($result) use ($req){
                        $result->where('tb_stock_master.supplier_code', '=', $req->supplier1)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier2)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier3)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier4)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier5)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier6);
                    })
                    ->where(function($result) use ($req){
                        $result->where('tb_paid_order.order_date', '>=', $req->tanggaldr .' 00:00:00')
                            ->where('tb_paid_order.order_date', '<=', $req->tanggalsmp.' 23:59:59');
                        })
                    ->orderBy('tb_paid_order.internal_code', 'asc')
                    ->orderBy('tb_paid_order.variant', 'asc')
                    ->get();
        return $result;
    }





    static function getDailySales($req) {
        $result = DB::table('tb_paid_order')
                    ->leftJoin('tb_stock_master', 'tb_paid_order.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where(function($result) use ($req){
                        $result->where('tb_stock_master.supplier_code', '=', $req->supplier1)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier2)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier3)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier4)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier5)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier6);
                    })
                    ->where(function($result) use ($req){
                        $result->where('tb_paid_order.order_date', '>=', date('Y-m-d H:i:s', strtotime('last month')))
                            ->where('tb_paid_order.order_date', '<=', date('Y-m-d H:i:s', strtotime('today')));
                        })
                    ->groupBy(DB::raw('DATE(tb_paid_order.order_date)'))
                    ->orderBy(DB::raw('DATE(tb_paid_order.order_date)'), 'asc')
                    ->select(DB::raw('SUM(tb_paid_order.qty) AS qty_sold, DATE_FORMAT(tb_paid_order.order_date, "%d/%m") AS tanggal'))
                    ->get();
        return $result;

    }



    static function getRekapSales($req) {
        //dd($req);
        $result = DB::table('tb_paid_order')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    //->leftJoin('tb_stock', 'tb_paid_order.internal_code', '=', 'tb_stock.internal_code')
                    ->leftJoin('tb_stock_master', 'tb_paid_order.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where(function($result) use ($req){
                        $result->where('tb_stock_master.supplier_code', '=', $req->supplier1)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier2)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier3)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier4)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier5)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier6);
                    })
                    ->where(function($result) use ($req){
                        $result->where('tb_paid_order.order_date', '>=', $req->tanggaldr .' 00:00:00')
                            ->where('tb_paid_order.order_date', '<=', $req->tanggalsmp.' 23:59:59');
                        })
                    ->select(DB::raw('SUM(tb_paid_order.qty) AS total_item_sold, SUM(tb_paid_order.qty * tb_stock_master.supplier_price) AS total_valuasi_sold'))
                    ->first();
        return $result;
    }
  

    
    static function getRekapSalesALL($req) {
        //dd($req);
        $result = DB::table('tb_paid_order')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    //->leftJoin('tb_stock', 'tb_paid_order.internal_code', '=', 'tb_stock.internal_code')
                    ->leftJoin('tb_stock_master', 'tb_paid_order.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where(function($result) use ($req){
                        $result->where('tb_stock_master.supplier_code', '=', $req->supplier1)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier2)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier3)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier4)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier5)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier6);
                    })

                    ->select(DB::raw('SUM(tb_paid_order.qty) AS total_item_sold, SUM(tb_paid_order.qty * tb_stock_master.supplier_price) AS total_valuasi_sold'))
                    ->first();
        return $result;
    }


    static function getRekapSalesALLO($req) {
        //dd($req);
        $result = DB::table('tb_paid_order')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    //->leftJoin('tb_stock', 'tb_paid_order.internal_code', '=', 'tb_stock.internal_code')
                    ->leftJoin('tb_stock_master', 'tb_paid_order.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where(function($result) use ($req){
                        $result->where('tb_stock_master.supplier_code', '=', $req->supplier1)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier2)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier3)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier4)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier5)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier6);
                    })

                    ->select(DB::raw('SUM(tb_paid_order.qty) AS itemsales, SUM(tb_paid_order.qty * tb_stock_master.supplier_price) AS totalsales'))
                    ->first();
        return $result;
    }
  

    static function getRekapPayment($req) {
        //dd($req);
        $result = DB::table('tb_supplierpayment')
                    ->groupBy('tb_supplierpayment.supplier_code')
                    ->where(function($result) use ($req){
                        $result->where('tb_supplierpayment.supplier_code', '=', $req->supplier1)
                            ->orwhere('tb_supplierpayment.supplier_code', '=', $req->supplier2)
                            ->orwhere('tb_supplierpayment.supplier_code', '=', $req->supplier3)
                            ->orwhere('tb_supplierpayment.supplier_code', '=', $req->supplier4)
                            ->orwhere('tb_supplierpayment.supplier_code', '=', $req->supplier5)
                            ->orwhere('tb_supplierpayment.supplier_code', '=', $req->supplier6);
                    })

                    ->select(DB::raw('SUM(tb_supplierpayment.pcs) AS pcs, SUM(tb_supplierpayment.total ) AS total, MAX(tb_supplierpayment.tanggal ) AS tanggalakhir'))
                    ->first();
        return $result;
    }

    static function getRekapBySupPrice($req) {
        //dd($req);
        $result = DB::table('tb_paid_order')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    //->leftJoin('tb_stock', 'tb_paid_order.internal_code', '=', 'tb_stock.internal_code')
                    ->leftJoin('tb_stock_master', 'tb_paid_order.internal_code', '=', 'tb_stock_master.internal_code')
                    ->groupBy('tb_stock_master.supplier_price')
                    ->where(function($result) use ($req){
                        $result->where('tb_stock_master.supplier_code', '=', $req->supplier1)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier2)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier3)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier4)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier5)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier6);
                    })
                    ->where(function($result) use ($req){
                        $result->where('tb_paid_order.order_date', '>=', $req->tanggaldr .' 00:00:00')
                            ->where('tb_paid_order.order_date', '<=', $req->tanggalsmp.' 23:59:59');
                        })
                    ->select(DB::raw('tb_stock_master.supplier_price AS supplier_price, SUM(tb_paid_order.qty) AS qty_sold, SUM(tb_paid_order.qty)*tb_stock_master.supplier_price AS total_sold'))
                    ->distinct()
                    ->get();
        return $result;
    }

    static function getRekapValuasiBySupCode($req) {
  
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->groupBy('tb_stock_master.supplier_price')
                    ->where('tb_stock.supplier_code', '=', $req->supplier1)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier2)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier3)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier4)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier5)
                    ->orwhere('tb_stock.supplier_code', '=', $req->supplier6)
          			->select(DB::raw('tb_stock_master.supplier_price AS supplier_price, SUM(tb_stock.qty) AS qty_stock, SUM(tb_stock.qty)*tb_stock_master.supplier_price AS stock_val'))
                    ->get();
        return $result;

    }


    static function getMasterPhotoByInternalCode(Request $req) {
        dd($req);
        $result = DB::table('tb_stock_master')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    ->where('tb_stock_master.internal_code', '=', $req)
                    
                    ->first();
        return $result;
    }

    static function getSupplierPaymentList() {
    	$result = DB::table('tb_supplierpayment')
    				->get();
    	return $result;
    }

} 
