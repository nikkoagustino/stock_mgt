<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class SlideshowModel extends Model
{
    use HasFactory;

    static function addSlide($path) {
        $insert = DB::table('tb_slideshow')
                    ->insert([
                        'image_url' => $path,
                    ]);
        return $insert;
    }

    static function getSlides() {
        $result = DB::table('tb_slideshow')
                    ->orderBy('id', 'asc')
                    ->get();
        return $result;
    }

    static function getSlidesAds() {
        $result = DB::table('tb_slideshow')
                    ->where('id', '!=', 1)
                    ->where('id', '!=', 2)
                    ->where('id', '!=', 3)
                    ->orderBy('id', 'asc')
                    ->get();
        return $result;
    }

    static function deleteSlide($id) {
        $delete = DB::table('tb_slideshow')
                    ->where('id', '=', $id)
                    ->delete();
        return $delete;
    }
}
