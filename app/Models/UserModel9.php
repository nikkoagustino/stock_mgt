<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserModel extends Model
{
    use HasFactory;

    static function getOrder($req) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $req->instagram)
                    ->where('tanggal', '<=', date('Y-m-d H:i:s', strtotime($req->tanggal)))
                    ->get();
        return $result;
    }

    static function getMasterData() {
        $result = DB::table('tb_stock_master')
                    ->get();
        return $result;
    }

    static function checkWebVisitAv() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_webvisitor')
                    ->where('tanggal','=',date('Y-m-d'))
                    ->first();
        return $result;
    }
    static function insertWebVisitAv() {
        date_default_timezone_set('Asia/Jakarta');
        $insert = DB::table('tb_webvisitor')
                    ->insert([
                        'tanggal' => date('Y-m-d')
                        
                    ]);
                    
        return $insert;
    }

    static function updateWebVisiNormal() {
        date_default_timezone_set('Asia/Jakarta');
        $update = DB::table('tb_webvisitor')
                    ->where('tanggal','=',date('Y-m-d'))
                    ->increment('webvisitor', 1);
        return $update;
    }

    static function updateWebVisiAds() {
        date_default_timezone_set('Asia/Jakarta');
        $update = DB::table('tb_webvisitor')
                    ->where('tanggal','=',date('Y-m-d'))
                    ->increment('adsvisitor', 1);
        return $update;
    }

    static function getMasterDataPaginate($sortBy = null) {
        DB::statement("SET SQL_MODE=''");
        $query = DB::table('tb_stock_master')
                    ->leftJoin('tb_stock', 'tb_stock_master.internal_code', '=', 'tb_stock.internal_code')
                    ->havingRaw('SUM(tb_stock.qty) > ?', [0])
                    ->groupBy('tb_stock.internal_code')
                    ->where('tb_stock_master.visibility', '=', 1)
                    ->where('tb_stock_master.selling_price', '>', 0);
        switch ($sortBy) {
            case 'code_ASC':
                $query->orderBy('tb_stock_master.internal_code', 'asc');
                break;

            case 'code_DESC':
                $query->orderBy('tb_stock_master.internal_code', 'desc');
                break;

            case 'price_ASC':
                $query->orderBy('tb_stock_master.selling_price', 'asc');
                break;

            case 'price_DESC':
                $query->orderBy('tb_stock_master.selling_price', 'desc');
                break;

            case 'name_ASC':
                $query->orderBy('tb_stock_master.product_name', 'asc');
                break;

            case 'name_DESC':
                $query->orderBy('tb_stock_master.product_name', 'desc');
                break;
            
            
            case 'newest':
            default:
                $query->orderBy('tb_stock_master.register_time', 'desc');
                break;
        }
        $result = $query->select('tb_stock_master.*')
                        ->paginate(28);
        return $result;
    }

    static function getMasterDataByCategory($category, $sortBy = null) {
        DB::statement("SET SQL_MODE=''");
        $query = DB::table('tb_stock_master')
                    ->leftJoin('tb_stock', 'tb_stock_master.internal_code', '=', 'tb_stock.internal_code')
                    ->havingRaw('SUM(tb_stock.qty) > ?', [0])
                    ->groupBy('tb_stock.internal_code')
                    ->where('tb_stock_master.visibility', '=', 1)
                    ->where('tb_stock_master.selling_price', '>', 0)
                    ->where('tb_stock_master.category', '=', $category);
        switch ($sortBy) {
            case 'code_ASC':
                $query->orderBy('tb_stock_master.internal_code', 'asc');
                break;

            case 'code_DESC':
                $query->orderBy('tb_stock_master.internal_code', 'desc');
                break;

            case 'price_ASC':
                $query->orderBy('tb_stock_master.selling_price', 'asc');
                break;

            case 'price_DESC':
                $query->orderBy('tb_stock_master.selling_price', 'desc');
                break;

            case 'name_ASC':
                $query->orderBy('tb_stock_master.product_name', 'asc');
                break;

            case 'name_DESC':
                $query->orderBy('tb_stock_master.product_name', 'desc');
                break;
            
            
            case 'newest':
            default:
                $query->orderBy('tb_stock_master.register_time', 'desc');
                break;
        }
        $result = $query->select('tb_stock_master.*')
                        ->paginate(28);
        return $result;
    }



    static function getMasterDataByPromo($sortBy = null) {
        DB::statement("SET SQL_MODE=''");
        $query = DB::table('tb_stock_master')
                    ->leftJoin('tb_stock', 'tb_stock_master.internal_code', '=', 'tb_stock.internal_code')
                    ->havingRaw('SUM(tb_stock.qty) > ?', [0])
                    ->groupBy('tb_stock.internal_code')
                    ->where('tb_stock_master.visibility', '=', 1)
                    ->where('tb_stock_master.selling_price', '>', 0)
                    ->where('tb_stock_master.hargaasli', '>', 0);
                    
        switch ($sortBy) {
            case 'code_ASC':
                $query->orderBy('tb_stock_master.internal_code', 'asc');
                break;

            case 'code_DESC':
                $query->orderBy('tb_stock_master.internal_code', 'desc');
                break;

            case 'price_ASC':
                $query->orderBy('tb_stock_master.selling_price', 'asc');
                break;

            case 'price_DESC':
                $query->orderBy('tb_stock_master.selling_price', 'desc');
                break;

            case 'name_ASC':
                $query->orderBy('tb_stock_master.product_name', 'asc');
                break;

            case 'name_DESC':
                $query->orderBy('tb_stock_master.product_name', 'desc');
                break;
            
            
            case 'newest':
            default:
                $query->orderBy('tb_stock_master.register_time', 'desc');
                break;
        }
        $result = $query->select('tb_stock_master.*')
                        ->paginate(28);
        return $result;
    }





    static function getMasterDataSearch($search_query, $sortBy = null) {
        DB::statement("SET SQL_MODE=''");
        $query = DB::table('tb_stock_master')
                    ->leftJoin('tb_stock', 'tb_stock_master.internal_code', '=', 'tb_stock.internal_code')
                    ->havingRaw('SUM(tb_stock.qty) > ?', [0])
                    ->groupBy('tb_stock.internal_code')
                    ->where('tb_stock_master.visibility', '=', 1)
                    ->where('tb_stock_master.selling_price', '>', 0)
                    ->where(function($query) use ($search_query){
                        $query->where('tb_stock_master.internal_code', 'like', '%'.$search_query.'%')
                                ->orWhere('tb_stock_master.product_name', 'like', '%'.$search_query.'%');
                    });
        switch ($sortBy) {
            case 'code_ASC':
                $query->orderBy('tb_stock_master.internal_code', 'asc');
                break;

            case 'code_DESC':
                $query->orderBy('tb_stock_master.internal_code', 'desc');
                break;

            case 'price_ASC':
                $query->orderBy('tb_stock_master.selling_price', 'asc');
                break;

            case 'price_DESC':
                $query->orderBy('tb_stock_master.selling_price', 'desc');
                break;

            case 'name_ASC':
                $query->orderBy('tb_stock_master.product_name', 'asc');
                break;

            case 'name_DESC':
                $query->orderBy('tb_stock_master.product_name', 'desc');
                break;
            
            case 'newest':
            default:
                $query->orderBy('tb_stock_master.register_time', 'desc');
                break;
        }
        $result = $query->select('tb_stock_master.*')
                        ->paginate(28);
        return $result;
    }

    static function getSupplier() {
        $result = DB::table('tb_supplier')
                    ->get();
        return $result;
    }

    static function getStock() {
        $result = DB::table('tb_stock')
                    ->get();
        return $result;
    }

    static function getAllStockAvailable() {
        $result = DB::table('tb_stock')
                    ->where('qty', '>', 0)
                    ->orderBy('internal_code', 'asc')
                    ->get();
        return $result;
    }
    


    static function getSliderData() {
        $result = DB::table('tb_stock')->inRandomOrder()
                    ->where('qty', '>', 0)
                    ->where('images', '<>', '')
                    ->get();
        return $result;
    }

    static function getLDData() {
        $result = DB::table('tb_stock')->inRandomOrder()
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where('tb_stock.qty', '>', 0)
                    ->where('tb_stock_master.master_image', '<>', null)
                    ->where('tb_stock_master.diskon', '=', 'Ya')
                    ->where('tb_stock_master.visibility', '=', '1')
                    ->get();
        return $result;
    }
       

    static function updateMidtransPayment($status) {
        if (($status->transaction_status == 'settlement') || ($status->transaction_status == 'capture')) {
            $order_status = 'paid';
        } else {
            $order_status = 'pending';
        }
        $update = DB::table('tb_payment')
                    ->where('id', '=', $status->order_id)
                    ->update([
                        'transaction_status' => 'Midtrans - '.$status->transaction_status,
                        'order_status' => $order_status,
                        'amount' => (int) $status->gross_amount,
                        'payment_type' => $status->payment_type,
                    ]);
        return $update;
    }

    static function getPhoneNumberByOrderID($orderId) {
        $result = DB::table('tb_payment')
                    ->leftJoin('tb_customer', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('id', '=', $orderId)
                    ->select('phone')
                    ->distinct()
                    ->first();
        return $result;
    }

    // static function insertPayment($req) {
    //     $insert = DB::table('tb_payment')
    //                 ->insert([
    //                     'id' => $req->order_id,
    //                     'instagram' => $req->custom_field1,
    //                     'order_date' => $req->custom_field2,
    //                     'shipping_address' => $req->custom_field3,
    //                     'amount' => (int) $req->gross_amount,
    //                     'payment_type' => $req->payment_type,
    //                     'transaction_time' => $req->transaction_time,
    //                     'transaction_status' => 'Midtrans - '.$req->transaction_status,
    //                     'order_status' => 'paid',
    //                 ]);
    //     $update = DB::table('tb_order')
    //                 ->where('instagram', '=', $req->custom_field1)
    //                 ->where('tanggal', '=', $req->custom_field2)
    //                 ->update([
    //                     'order_status' => 'paid'
    //                 ]);
                    
    //     if ($insert) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // static function updateStock($req) {
    //     $read = DB::table('tb_order')
    //                 ->where('instagram', '=', $req->custom_field1)
    //                 ->where('tanggal', '=', $req->custom_field2)
    //                 ->get();

    //     foreach ($read as $row) {
    //         $update = DB::table('tb_stock')
    //                     ->where('internal_code', '=', $row->internal_code)
    //                     ->where('variant', '=', $row->variant)
    //                     ->where('size', '=', $row->size)
    //                     ->decrement('qty', $row->qty);
    //     }
    //     return true;
    // }

    static function getOrderByParameterV2($instagram, $date) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    ->groupBy('internal_code')
                    ->groupBy('variant')
                    ->groupBy('size')
                    ->select('internal_code', 'variant', 'size', DB::raw('SUM(qty) AS qty, MAX(tanggal) AS tanggal, MAX(instagram) AS instagram, comment'))
                    ->get();
        return $result;
    }

    static function insertPaymentV2($req) {
        $insert = DB::table('tb_payment')
                    ->insert([
                        'id' => $req->order_id,
                        'instagram' => $req->instagram,
                        'order_date' => $req->tanggal,
                        'shipping_address' => $req->shipping_address,
                        'amount' => (int) $req->amount,
                        'payment_type' => 'Midtrans',
                        'transaction_time' => date('Y-m-d H:i:s'),
                        'transaction_status' => 'Midtrans - pending',
                        'order_status' => 'pending',
                    ]);
                    
        return $insert;
    }

    static function deletePendingOrder($data) {
        $delete = DB::table('tb_order')
                    ->where('instagram', '=', $data['instagram'])
                    ->where('tanggal', '<=', $data['order_date'])
                    ->where('internal_code', '=', $data['internal_code'])
                    ->where('variant', '=', $data['variant'])
                    ->where('size', '=', $data['size'])
                    ->delete();
        return $delete;
    }

    static function insertPaidOrder($data) {
        $insert = DB::table('tb_paid_order')
                    ->insert([
                        'payment_id' => $data['payment_id'],
                        'internal_code' => $data['internal_code'],
                        'variant' => $data['variant'],
                        'size' => $data['size'],
                        'qty' => $data['qty'],
                        'selling_price' => $data['selling_price'],
                        'order_date' => $data['order_date'],
                        'comment' => $data['comment'],
                    ]);
        return $insert;
    }

    static function reduceStock($data) {
        $update = DB::table('tb_stock')
                    ->where('internal_code', '=', $data['internal_code'])
                    ->where('variant', '=', $data['variant'])
                    ->where('size', '=', $data['size'])
                    ->decrement('qty', $data['qty']);
        return $update;
    }

    static function getCustomerData($req) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $req->instagram)
                    ->first();
        return $result;
    }

    static function getCustomerDataByInstagram($instagram) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function registerUser($req) {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $insert = DB::table('tb_customer')
                    ->insert([
                        'username' => $req->instagram,
                        'firstname' => $req->nama_depan,
                        'lastname' => $req->nama_belakang,
                        'password' => md5($req->password),
                        'phone' => $req->telepon,
                        'email' => $req->email,
                        'reffer' => $req->reffer,
                    ]);

        $insert2 = DB::table('tb_customer_address')
                    ->insert([
                        'username' => $req->instagram,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $insert;
    }

    static function registerUserLD($req) {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $insert = DB::table('tb_customer')
                    ->insert([
                        'username' => $req->telepon,
                        'firstname' => $req->nama_depan,
                        'password' => md5($req->password),
                        'phone' => $req->telepon,
                        'email' => $req->email,
                        'reffer' => $req->reffer,
                    ]);

        $insert2 = DB::table('tb_customer_address')
                    ->insert([
                        'username' => $req->telepon,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $insert;
    }

    static function getShippingAddress($instagram) {
        $result = DB::table('tb_customer_address')
                    ->where('username', '=', $instagram)
                    ->get();
        return $result;
    }

    static function login($req) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $req->instagram)
                    ->where('password', '=', md5($req->password))
                    ->first();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    static function getPendingPayment() {
        $result = DB::table('tb_payment')
                    ->where('order_status', 'pending')
                    ->get();
        return $result;
    }

    static function getAllStock() {
        $result = DB::table('tb_stock')
                    ->get();
        return $result;
    }

    
    static function getProductMainData() {
        $result = DB::table('tb_stock')
                    ->groupBy('internal_code')
                    ->select('internal_code', DB::raw('MAX(product_name) AS product_name'))
                    ->get();
        return $result;
    }
    


    static function getPaidOrderByInstagram($instagram) {
        $result = DB::table('tb_payment')
                    ->where('instagram', '=', $instagram)
                    ->get();
        return $result;
    }

    static function getUser($instagram) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function getPaymentDataByID($payment_id) {
        $result = DB::table('tb_payment')
                    ->where('id', '=', $payment_id)
                    ->first();
        return $result;
    }

    static function addToCart($req) {
        date_default_timezone_set('Asia/Jakarta');
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => '-2',
                        'instagram' => $req->instagram,
                        'internal_code' => $req->internal_code,
                        'variant' => $req->variant,
                        'size' => $req->size,
                        'qty' => $req->qty,
                        'tanggal' => date('Y-m-d H:i:s'),
                        'comment' => 'Order dari Website ' ,
                        'order_status' => 'new',
                    ]); 
        return $insert;
    }

    static function addToCartV2($req, $instagram) {
        date_default_timezone_set('Asia/Jakarta');
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => '-2',
                        'instagram' => $instagram,
                        'internal_code' => $req['internal_code'],
                        'variant' => $req['variant'],
                        'size' => $req['size'],
                        'qty' => $req['qty'],
                        'tanggal' => date('Y-m-d H:i:s'),
                        'comment' => 'Order dari Website',
                        'order_status' => 'new',
                    ]); 
        return $insert;
    }

    static function CheckLastOrder($phone) {
        $result = DB::table('tb_order')
                    ->where('instagram','=',$phone)
                    ->distinct()
                    ->get();
        return $result;
    }


    static function addToCartV3($req, $instagram, $reffer) {
        date_default_timezone_set('Asia/Jakarta');
        $comment = 'Order dari Website';
        if($reffer=='ADS'){
            $comment='Order ADS';
        }
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => '-2',
                        'instagram' => $instagram,
                        'internal_code' => $req->internal_code,
                        'variant' => $req->variant,
                        'size' => $req->size,
                        'qty' => $req->qty,
                        
                        'tanggal' => $req->tgl,
                        'comment' => $comment,
                        'order_status' => 'new',
                    ]); 
        return $insert;
    }

    


    static function getAllSubdistrictID() {
        $result = DB::table('tb_customer_address')
                    ->where('subdistrict_id', '>', 0)
                    ->select('subdistrict_id')
                    ->distinct()
                    ->get();
        return $result;
    }

    static function updateShippingCost($subdistrict_id, $shipping_cost) {
        $update = DB::table('tb_customer_address')
                    ->where('subdistrict_id', '=', $subdistrict_id)
                    ->update([
                        'shipping_cost' => $shipping_cost,
                    ]);
        return $update;
    }

    static function getCustomerFromInstagram($instagram) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function getCustomerDataFromParam($instagram, $password) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->where('password', '=', $password)
                    ->first();
        return $result;
    }

    static function submitResetPassword($req) {
        $update = DB::table('tb_customer')
                    ->where('username', '=', $req->instagram)
                    ->update([
                        'password' => md5($req->password),
                    ]);
        return $update;
    }

    static function getPaymentById($id) {
        $result = DB::table('tb_payment')
          			->leftJoin('tb_customer', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('id', '=', $id)
                    ->first();
        return $result;
    }
    static function getPaidOrderByPaymentID($payment_id) {
        $result = DB::table('tb_paid_order')
                    ->where('payment_id', '=', $payment_id)
                    ->get();
        return $result;
    }

    static function returnToUnpaidOrder($req, $custdata) {
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => '-1',
                        'instagram' => $custdata->instagram,
                        'internal_code' => strtoupper($req->internal_code),
                        'variant' => strtoupper($req->variant),
                        'size' => $req->size,
                        'qty' => $req->qty,
                        'tanggal' => $custdata->order_date,
                        'comment' => 'Retur dari cancel payment',
                        'order_status' => 'new'
                    ]);
        return $insert;
    }

    static function returnToStock($req) {
        $update = DB::table('tb_stock')
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->where('variant', '=', strtoupper($req->variant))
                    ->where('size', '=', $req->size)
                    ->increment('qty', $req->qty);
        return $update;
    }

    static function deletePaidOrderList($req, $payment_id) {
        $delete = DB::table('tb_paid_order')
                    ->where('payment_id', '=', $payment_id)
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->where('variant', '=', strtoupper($req->variant))
                    ->where('size', '=', $req->size)
                    ->delete();
        return $delete;
    }

    static function deletePayment($payment_id) {
        $delete = DB::table('tb_payment')
                    ->where('id', '=', $payment_id)
                    ->delete();
        return $delete;
    }

    static function addSystemLog($action) {
        $insert = DB::table('tb_system_log')
                    ->insert([
                        'action' => '['.\Session::get('fullname').'] '.$action
                    ]);
        return $insert;
    }

    static function submitPhoneChange($req) {
        $update = DB::table('tb_customer')
        ->where('username', '=', $req->instagram)
                    ->update([
                        'phone' =>$req->phone,
                        'firstname' =>$req->firstname,
                        'lastname' =>$req->lastname,
                        'email' =>$req->email,
                    ]);
        return $update;
    }

    static function insertCustomerAddress($req) {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $insert = DB::table('tb_customer_address')
                    ->insert([
                        'username' => $req->instagram,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $insert;
    }

    static function deleteCustomerAddress($id) {
        $delete = DB::table('tb_customer_address')
                    ->where('username', '=', \Session::get('instagram'))
                    ->where('id', '=', $id)
                    ->delete();
        return $delete;
    }

    // static function getAllCategory() {
    //     $result = DB::table('tb_stock_master')
    //                 ->orderBy('category', 'asc')
    //                 ->select('category')
    //                 ->distinct()
    //                 ->get();
    //     return $result;
    // }
    static function getAllCategory() {
        $result = DB::table('tb_category')
                    ->join('tb_stock_master', 'tb_category.category_name', 'tb_stock_master.category')
                    ->orderBy('category_name', 'asc')
                    ->select(DB::raw('category_name AS category'))
                    ->distinct()
                    ->get();
        return $result;
    }

    static function saveUserPassword($req) {
        $update = DB::table('tb_customer')
                    ->where('username', '=', $req->instagram)
                    ->update([
                        'password' => md5($req->password),
                    ]);
        return $update;
    }

    static function cancelOrderItem($instagram, $internal_code) {
        $delete = DB::table('tb_order')
                    ->where('instagram', $instagram)
                    ->where('internal_code', $internal_code)
                    ->delete();
        return $delete;
    }
    static function getDiscount() {
        $result = DB::table('tb_discount')
                    ->first();
        return $result;
    }
    static function getAdminWa() {
        $result = DB::table('tb_discount')
                    ->first();
        return $result;
    }

   

    static function getProductByInternalCode($internal_code) {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_stock')
                    ->orderBy('variant')
                    ->orderBy('size')
                    ->where('internal_code', '=', $internal_code)
                    ->get();
        return $result;
    }

    static function getProductByInternalCodeHaveStock($internal_code) {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_stock')
                    ->orderBy('variant')
                    ->orderBy('size')
                    ->where('internal_code', '=', $internal_code)
                    ->where('qty', '>', 0)
                    ->get();
        return $result;
    }

    static function getMasterDataByInternalCode($internal_code) {
        $result = DB::table('tb_stock_master')
                    ->where('internal_code', '=', $internal_code)
                    ->first();
        return $result;
    }
}
