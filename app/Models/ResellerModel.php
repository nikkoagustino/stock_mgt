<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class ResellerModel extends Model
{
    use HasFactory;
    static function getCustomerDataByInstagram($instagram) {
        $result = DB::table('tb_reseller')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function returnToUnpaidOrder($req, $custdata) {
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => '-1',
                        'instagram' => $custdata->instagram,
                        'internal_code' => strtoupper($req->internal_code),
                        'variant' => strtoupper($req->variant),
                        'size' => $req->size,
                        'qty' => $req->qty,
                        'tanggal' => $custdata->order_date,
                        'comment' => 'Retur dari cancel payment Reseller',
                        'order_status' => 'new'
                    ]);
        return $insert;
    }

    static function getCustomerDataByInstagram2($instagram) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function saveSingleSelling($req) {
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => $req->batch_no,
                        'instagram' => $req->instagram,
                        'tanggal' => $req->tanggal,
                        'internal_code' => strtoupper($req->internal_code),
                        'variant' => strtoupper($req->variant),
                        'size' => $req->size,
                        'qty' => $req->qty,
                        'comment' => $req->comment,
                    ]);
        return $insert;
    }
    

    static function getOrderByID($instagram) {
        $result = DB::table('tb_order')
          			->leftJoin('tb_customer', 'tb_order.instagram', '=', 'tb_customer.username')
                    ->where('username', '=', $instagram)
                    ->groupBy('internal_code')
                    ->groupBy('variant')
                    ->groupBy('size')
                    ->select('internal_code', 'variant', 'size', DB::raw('SUM(qty) AS qty, MAX(tanggal) AS tanggal, MAX(instagram) AS instagram'))
                    ->get();
        return $result;
        //dd($result);
    }

    static function registerUser($req) {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $insert = DB::table('tb_reseller')
                    ->insert([
                        'username' => $req->instagram,
                        'firstname' => $req->nama_depan,
                        'password' => md5($req->password),
                        'nama_toko' => $req->nama_toko,
                        'phone' => $req->telepon,
                        'email' => $req->email,
                        'logo_toko' => $req->sd,
                        'reffer' => $req->reffer,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $insert;
    }

    static function editUser($req) {
        
        $update = DB::table('tb_reseller')
                    ->where('username', '=', $req->instagram)
                    ->update([
                        
                        'firstname' => $req->nama_depan,
                        'nama_toko' => $req->nama_toko,
                        
                        'email' => $req->email,
                        
                    ]);
        return $update;


    }

    static function editPassword($req) {
        
        $update = DB::table('tb_reseller')
                    ->where('username', '=', $req->ig)
                    ->update([
                        
                        'password' => md5($req->password),
                        
                        
                    ]);
        return $update;


    }

    static function EditPhotos($ig,$path) {
        
        $update = DB::table('tb_reseller')
                    ->where('username', '=', $ig)
                    ->update([
                        
                        'logo_toko' => $path,
                        
                        
                    ]);
        return $update;


    }

    static function editAddress($req) {
        
        $address_gen = "$req->nama_depan1 \n $req->telepon1 / $req->email1 \n $req->address \n $req->subdistrict, $req->city, $req->province";
        //dd($address_gen);
        $update = DB::table('tb_reseller')
                    ->where('username', '=', $req->instagram1)
                    ->update([
                        
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $update;


    }

    

    static function login($req) {
        $result = DB::table('tb_reseller')
                    ->where('username', '=', $req->instagram)
                    ->where('password', '=', md5($req->password))
                    ->first();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    static function getPaidOrderByInstagram($instagram) {
        $result = DB::table('tb_payment')
                    ->where('instagram', '=', $instagram)
                    ->get();
        return $result;
    }
    static function getShippingAddress($instagram) {
        $result = DB::table('tb_customer_address')
                    ->where('username', '=', $instagram)
                    ->get();
        return $result;
    }
    

    static function getOrderByParameterV2($instagram, $date) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    ->where('tanggal', '<=', $date)
                    ->groupBy('internal_code')
                    ->groupBy('variant')
                    ->groupBy('size')
                    ->select('id','internal_code', 'variant', 'size', DB::raw('SUM(qty) AS qty, MAX(tanggal) AS tanggal, MAX(instagram) AS instagram'))
                    ->get();
        return $result;
    }

    static function getAllStock() {
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    ->orderBy('pin_to_top', 'desc')
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc')
                    ->get();
        return $result;
    }

    static function getSupplierList() {
    	$result = DB::table('tb_supplier')
    				->get();
    	return $result;
    }

    static function getMasterData() {
        $result = DB::table('tb_stock_master')
                    ->get();
        return $result;
    }

    static function registerCustomerManually($req) {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $insert = DB::table('tb_customer')
                    ->insert([
                        'username' => $req->instagram,
                        'firstname' => $req->nama_depan,
                        'lastname' => $req->nama_belakang,
                        'password' => null,
                        'phone' => $req->telepon,
                        'email' => $req->email,
                        'reffer' => 'MANUAL',
                    ]);

        $insert2 = DB::table('tb_customer_address')
                    ->insert([
                        'username' => $req->instagram,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $insert;
    }

    static function insertPayment($req) {
        if (isset($req->shipping_address)) {
            $address_gen = $req->shipping_address;
        } else {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        }
        $insert = DB::table('tb_payment')
                    ->insert([
                        'id' => $req->order_id,
                        'instagram' => $req->instagram,
                        'order_date' => $req->tanggal,
                        'shipping_address' => $address_gen,
                        'amount' => (int) $req->amount,
                        'payment_type' => $req->payment_type,
                        'transaction_time' => date('Y-m-d H:i:s'),
                        'transaction_status' => $req->transaction_status,
                        'order_status' => 'paid',
                    ]);
                    
        return $insert;
    }


    static function reduceStock($data) {
        $update = DB::table('tb_stock')
                    ->where('internal_code', '=', $data['internal_code'])
                    ->where('variant', '=', $data['variant'])
                    ->where('size', '=', $data['size'])
                    ->decrement('qty', $data['qty']);
        return $update;
    }
    static function insertPaidOrder($data) {
        $insert = DB::table('tb_paid_order')
                    ->insert([
                        'payment_id' => $data['payment_id'],
                        'internal_code' => strtoupper($data['internal_code']),
                        'variant' => $data['variant'],
                        'size' => $data['size'],
                        'qty' => $data['qty'],
                        'selling_price' => $data['selling_price'],
                        'order_date' => $data['order_date'],
                        'comment' => $data['comment'],
                    ]);
        return $insert;
    }

    static function deletePendingOrder($data) {
        $delete = DB::table('tb_order')
                    ->where('instagram', '=', $data['instagram'])
                    ->where('tanggal', '<=', $data['order_date'])
                    ->where('internal_code', '=', $data['internal_code'])
                    ->where('variant', '=', $data['variant'])
                    ->where('size', '=', $data['size'])
                    ->delete();
        return $delete;
    }

    static function getDailySalesYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()')
                        ->sum(DB::raw('qty * selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()')
                        ->sum(DB::raw('qty * supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->whereRaw('DATE(transaction_time) = CURDATE()')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }
    static function deleteListOrder($list_id) {
        $delete = DB::table('tb_order')
                    ->where('id', '=', $list_id)
                    ->delete();
        return $delete;
    }


    
    static function getCustomer($instagram) {
        $result = DB::table('tb_reseller')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function getCustomerAddress($instagram) {
        $result = DB::table('tb_customer_address')
                    ->where('username', '=', $instagram)
                    ->get();
        return $result;
    }

    static function getProductMainData() {
        $result = DB::table('tb_stock')
                    ->groupBy('internal_code')
                    ->select('internal_code', DB::raw('MAX(product_name) AS product_name'))
                    ->get();
        return $result;
    }

    static function getProductMainDataSimple() {
        $result = DB::table('tb_stock')
                    ->where('qty', '>', 0)
                    ->groupBy('internal_code')
                    ->select('internal_code', DB::raw('MAX(product_name) AS product_name'))
                    ->get();
        return $result;
    }

    static function getOrderByParameter($instagram, $date) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    ->where('tanggal', '=', $date)
                    ->orderBy('id', 'asc')
                    ->get();
        return $result;
    }

    static function getOrderByIG($instagram) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    
                    ->orderBy('id', 'asc')
                    ->get();
        return $result;
    }

    static function getMasterDataFromInternalCode($internal_code) {
        $result = DB::table('tb_stock_master')
                    ->where('internal_code', $internal_code)
                    ->first();
        return $result;
    }

    static function getVariantDataFromInternalCode($req) {
        $result = DB::table('tb_stock')
                    ->where('internal_code', $req->internal_code)
                    ->where('variant', $req->variant)
                    ->first();
        return $result;
    }

    static function getPurchaseDataDaily() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_payment AS py')
                    ->leftJoin('tb_paid_order AS po', 'po.payment_id', '=', 'py.id')
                    ->where('py.instagram', Session::get('reseller'))
                    ->where('py.order_status', '!=', 'pending')
                    ->whereRaw('DATE(py.order_date) = "'.date('Y-m-d').'"')
                    ->select(DB::raw('DATE(py.order_date) AS tanggal, SUM(po.qty) AS total_item_qty, COUNT(py.id) AS tx_count, SUM(py.amount) AS amount_tx'))
                    ->first();
        return $result;
    }

    static function getPurchaseDataMonthly() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_payment AS py')
                    ->leftJoin('tb_paid_order AS po', 'po.payment_id', '=', 'py.id')
                    ->where('py.instagram', Session::get('reseller'))
                    ->where('py.order_date', '>=', date('Y-m-d', strtotime('30 days before now')).' 00:00:00')
                    ->where('py.order_status', '!=', 'pending')
                    ->groupBy(DB::raw('DATE(py.order_date)'))
                    ->select(DB::raw('DATE(py.order_date) AS tanggal, SUM(po.qty) AS total_item_qty, COUNT(py.id) AS tx_count, SUM(py.amount) AS amount_tx'))
                    ->get();
        return $result;
    }

} 
