<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class AdminModel extends Model
{
    use HasFactory;

    static function loginAttempt($req) {
        $result = DB::table('tb_user')
                    ->where('username', $req->username)
                    ->where('password', md5($req->password))
                    ->first();
        return $result;
    }

    static function addSystemLog($action) {
        $insert = DB::table('tb_system_log')
                    ->insert([
                        'action' => '['.\Session::get('fullname').'] '.$action
                    ]);
        return $insert;
    }

    static function getSystemLog() {
        $result = DB::table('tb_system_log')
                    ->orderBy('time_log', 'desc')
                    ->paginate(50);
        return $result;
    }

    static function getSupplierList() {
    	$result = DB::table('tb_supplier')
    				->get();
    	return $result;
    }

    static function submitSupplier($req) {
        $insert = DB::table('tb_supplier')
                    ->insert([
                        'supplier_code' => strtoupper($req->supplier_code),
                        'supplier_name' => $req->supplier_name,
                        'location' => $req->location,
                        'phone' => str_replace(' ','',$req->phone),
                        'email' => $req->email,
                        'repeat_order' => $req->repeat_order,
                    ]);
        return $insert;
    }

    static function getSupplierById($id) {
        $result = DB::table('tb_supplier')
                    ->where('id', '=', $id)
                    ->first();
        return $result;
    }

    static function submitEditSupplier($req) {
        $update = DB::table('tb_supplier')
                    ->where('id', '=', $req->id)
                    ->update([
                        'supplier_code' => strtoupper($req->supplier_code),
                        'supplier_name' => $req->supplier_name,
                        'location' => $req->location,
                        'phone' => str_replace(' ','',$req->phone),
                        'email' => $req->email,
                        'repeat_order' => $req->repeat_order,
                    ]);
        return $update;
    }

    static function deleteSupplier($req) {
        $delete = DB::table('tb_supplier')
                    ->where('id', '=', $req->id)
                    ->delete();
        return $delete;
    }

    static function getAllStock() {
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
                    ->orderBy('pin_to_top', 'desc')
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc')
                    ->get();
        return $result;
    }
    
    static function getAllStockBrandedInStock() {
        $result = DB::table('tb_stock')
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where('tb_stock.qty', '>', 0)
                    
                    ->where('tb_stock_master.branded', '=', 'Ya')
                    ->orderby('tb_stock_master.register_time','desc')
                    ->get();
        return $result;
    }


    static function getAllStockNonBranded() {
        $result = DB::table('tb_stock')
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where('tb_stock.qty', '>', 0)
                    
                    ->where('tb_stock_master.branded', '=', null)
                    ->orderby('tb_stock_master.register_time','desc')
                    ->get();
        return $result;
    }

    static function getAllStockLD() {
        $result = DB::table('tb_stock')->inRandomOrder()
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where('tb_stock.qty', '>', 0)
                    ->where('tb_stock_master.master_image', '<>', null)
                    ->where('tb_stock_master.diskon', '=', 'Ya')
                    ->where('tb_stock_master.visibility', '=', '1')
                    ->get();
        return $result;
    }

    static function getAllStockav() {
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
          			->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->orderBy('pin_to_top', 'desc')
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc')
                    ->where('tb_stock.qty','>',0)
                    ->get();
        return $result;
    }

    static function getAllStockemp() {
        $result = DB::table('tb_stock')
                    // ->leftJoin('tb_supplier', 'tb_stock.supplier_code', '=', 'tb_supplier.supplier_code')
          			->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->orderBy('pin_to_top', 'desc')
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc')
                    ->where('tb_stock.qty','=',0)
                    ->get();
        return $result;
    }


    static function getNoPhotoVariant() {
        $result = DB::table('tb_stock')
                    //->leftJoin('tb_stock_master', 'tb_stock.supplier_code', '=', 'tb_stock_master.supplier_code')
                    ->where('images', '=', "")
                    ->where('qty', '>', "0")
                    //->where('master_image', '=', null)
                    ->orderBy('pin_to_top', 'desc')
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc')
                    ->get();
        return $result;
    }



    static function getProductByInternalCode($internal_code) {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_stock')
                    ->orderBy('variant')
                    ->orderBy('size')
                    ->where('internal_code', '=', $internal_code)
                    ->get();
        return $result;
    }

    static function getProductByInternalCodeDetail($internal_code, $variant, $size) {
        $result = DB::table('tb_stock')
                    ->where('internal_code', '=', $internal_code)
                    ->where('variant', '=', $variant)
                    ->where('size', '=', $size)
                    ->get();
        return $result;
    }

    static function getLastInternalCodeBySupplier($supplier_code) {
        $result = DB::table('tb_stock_master')
                    ->where('supplier_code', '=', $supplier_code)
                    ->orderBy('register_time', 'desc')
                    ->select(DB::Raw('supplier_code,internal_code,count(*) as total'))
                    ->first();
        return $result;
    }

    static function getProductByParameter($internal_code, $variant, $size) {
        $result = DB::table('tb_stock')
                    ->where('internal_code', '=', strtoupper($internal_code))
                    ->where('variant', '=', strtoupper($variant))
                    ->where('size', '=', $size)
                    ->first();
        return $result;
    }

    static function updateStockProduct($req, $path) {
        $update = DB::table('tb_stock')
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->where('variant', '=', strtoupper($req->variant))
                    ->where('size', '=', $req->size)
                    ->update([
                        'keterangan' => $req->keterangan,
                        'supplier_price' => $req->supplier_price,
                        'selling_price' => $req->selling_price,
                        'qty' => DB::raw('qty + '.$req->qty_in),
                    ]);
        $update2 = false;
        if (isset($path)) {
            $update2 = DB::table('tb_stock')
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->where('variant', '=', strtoupper($req->variant))
                    ->where('size', '=', $req->size)
                    ->update([
                        'images' => $path,
                    ]);
        }
        if ($update || $update2) {
            return true;
        } else {
            return false;
        }
    }

    static function newStockProduct($req, $path) {
        $insert = DB::table('tb_stock')
                    ->insert([
                        'internal_code' => strtoupper($req->internal_code),
                        'external_code' => $req->external_code,
                        'product_name' => $req->product_name,
                        'variant' => strtoupper($req->variant),
                        'size' => $req->size,
                        'keterangan' => $req->keterangan,
                        'supplier_code' => strtoupper($req->supplier_code),
                        'supplier_price' => $req->supplier_price,
                        'selling_price' => $req->selling_price,
                        'qty' => (int) $req->qty_in,
                    ]);
        $update2 = false;
        if (isset($path)) {
            $update2 = DB::table('tb_stock')
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->where('variant', '=', strtoupper($req->variant))
                    ->where('size', '=', $req->size)
                    ->update([
                        'images' => $path,
                    ]);
        }
        if ($insert || $update2) {
            return true;
        } else {
            return false;
        }
    }

    // static function getAllVariants() {
    //     $result = DB::table('tb_product')
    //                 ->select('variant')
    //                 ->orderBy('variant', 'asc')
    //                 ->distinct()
    //                 ->get();
    //     return $result;
    // }

    // static function getProducts() {
    //     $result = DB::table('tb_product')
    //                 ->get();
    //     return $result;
    // }

    // static function getAllProductsCode() {
    //     $result = DB::table('tb_product')
    //                 ->select('internal_code', 'product_name')
    //                 ->orderBy('internal_code', 'asc')
    //                 ->distinct()
    //                 ->get();
    //     return $result;
    // }

    static function pinStockReport($req) {
        $update = DB::table('tb_stock')
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->update([
                        'pin_to_top' => $req->status
                    ]);
        return $update;
    }

    static function get_last_batch() {
        $last_batch = DB::table('tb_order')
                        ->orderBy('batch_no', 'desc')
                        ->select('batch_no')
                        ->first();
        if ($last_batch) {
            return $last_batch->batch_no;
        } else {
            return "0";
        }
    }

    static function submitSelling($data) {
        $insert = DB::table('tb_order')
                    ->insert($data);
        if ($insert) {
            return true;
        } else {
            return false;
        }
    }

    static function saveSingleSelling($req) {
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => $req->batch_no,
                        'instagram' => $req->instagram,
                        'tanggal' => $req->tanggal,
                        'internal_code' => strtoupper($req->internal_code),
                        'variant' => strtoupper($req->variant),
                        'size' => $req->size,
                        'qty' => $req->qty,
                        'comment' => $req->comment,
                    ]);
        return $insert;
    }
    

    static function getOrderByBatchNo($batch_no) {
        $result = DB::table('tb_order')
                    ->where('batch_no', '=', $batch_no)
                    ->get();
        return $result;
    }

    static function getSellingReportBatch() {
        $result = DB::table('tb_order')
                    ->select('batch_no', 'tanggal', DB::raw('SUM(qty) as total_order'))
                    ->groupBy('batch_no')
                    ->groupBy('tanggal')
                    ->orderBy('batch_no', 'desc')
                    ->get();
        return $result;
    }

    static function getSellingOrderByBatch($batch_no) {
        $result = DB::table('tb_order')
                    ->where('batch_no', $batch_no)
                    ->get();
        return $result;
    }

    static function deleteListOrder($list_id) {
        $delete = DB::table('tb_order')
                    ->where('id', '=', $list_id)
                    ->delete();
        return $delete;
    }

    static function getAdminWa() {
        $result = DB::table('tb_discount')
                    ->first();
        return $result;
    }


    static function getPayment() {
        $result = DB::table('tb_payment')
                    ->orderBy('id', 'desc')
                    ->get();
        return $result;
    }

    static function getPaymentById($id) {
        $result = DB::table('tb_payment')
                    ->where('id', '=', $id)
                    ->first();
        return $result;
    }

    static function getOrderByPaymentId($pid) {
        $result = DB::table('tb_paid_order')
                    ->where('payment_id', '=', $pid)
                    ->get();
        return $result;
    }

    static function getProductByExternalCode($external_code) {
        $result = DB::table('tb_stock')
                    ->where('external_code', '=', $external_code)
                    ->first();
        return $result;
    }

    static function getMarketingStokData() {
        $result = DB::table('tb_stock')->inRandomOrder()
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where('tb_stock.qty', '>', 0)
                    ->where('tb_stock.images', '<>', '')
                    ->where('tb_stock_master.diskon', '=', 'Ya')
                    ->where('tb_stock_master.visibility', '=', '1')
                    ->first();
        return $result;
    }
    
    static function getIGByPhone($phone) {
        $result = DB::table('tb_customer')
                    ->where('phone', '=', $phone)
                    ->first();
        return $result;
    }

    static function getPhoneByIG($username) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $username)
                    ->first();
        return $result;
    }

    static function getProductById($id) {
        $result = DB::table('tb_stock')
                    ->where('id', '=', $id)
                    ->first();
        return $result;
    }
    static function getProductByIdIntCode($id) {
        $result = DB::table('tb_stock')
                    ->where('internal_code', '=', $id)
                    ->first();
        return $result;
    }

    static function deleteStock($id) {
        $result = DB::table('tb_stock')
                    ->where('id', '=', $id)
                    ->delete();
        return $result;
    }

    static function getMasterData() {
        $result = DB::table('tb_stock_master')
                    ->get();
        return $result;
    }

    static function getAllStockSearch($search_query = null) {
        $query = DB::table('tb_stock')
                    ->orderBy('pin_to_top', 'desc')
                    ->orderBy('tb_stock.internal_code', 'asc')
                    ->orderBy('tb_stock.variant', 'asc');
        if (isset($search_query)) {
            $query->where('internal_code', 'LIKE', '%'.$search_query.'%')
                ->orWhere('external_code', 'LIKE', '%'.$search_query.'%')
                ->orWhere('product_name', 'LIKE', '%'.$search_query.'%')
                ->orWhere('variant', 'LIKE', '%'.$search_query.'%')
                ->orWhere('keterangan', 'LIKE', '%'.$search_query.'%')
                ->orWhere('size', 'LIKE', '%'.$search_query.'%');
        }
        $result = $query->get();
        return $result;
    }

    static function getMasterDataPaginated($search_query = null) {
        $query = DB::table('tb_stock_master')
                    ->leftJoin('tb_stock', 'tb_stock_master.internal_code', '=', 'tb_stock.internal_code')
                    ->groupBy('tb_stock_master.internal_code')
                    ->orderBy('tb_stock.pin_to_top', 'desc')
                    ->orderBy('tb_stock_master.internal_code', 'asc');
        if (isset($search_query)) {
            $query->where('tb_stock_master.internal_code', 'LIKE', '%'.$search_query.'%')
                ->orWhere('tb_stock_master.external_code', 'LIKE', '%'.$search_query.'%')
                ->orWhere('tb_stock_master.product_name', 'LIKE', '%'.$search_query.'%');
        }
        $result = $query->paginate(10);
        return $result;
    }

    static function getMasterDataByExternalCode($external_code) {
        $result = DB::table('tb_stock_master')
                    ->where('external_code', '=', $external_code)
                    ->first();
        return $result;
    }

    static function getMasterDataByInternalCode($internal_code) {
        $result = DB::table('tb_stock_master')
                    ->where('internal_code', '=', $internal_code)
                    ->first();
        return $result;
    }

    static function updateMasterImage($req, $path) {
        $upsert = DB::table('tb_stock_master')
                    ->upsert([
                        [
                            'external_code' => $req->external_code,
                            'master_image' => $path,
                        ]
                    ], ['external_code'], ['master_image']);
        return $upsert;
    }

    static function insertMasterData($req) {
        //dd($req);
        $upsert = DB::table('tb_stock_master')
                    ->upsert([
                        [
                            'external_code' => $req->external_code,
                            'internal_code' => strtoupper($req->internal_code),
                            'supplier_code' => strtoupper($req->supplier_code),
                            'product_name' => $req->product_name,
                            'category' => $req->category,
                            'selling_price' => $req->selling_price,
                            'supplier_price' => $req->supplier_price,                            
                            'diskon' => $req->diskon,
                            'branded' => $req->branded,
                            'hargaasli' => $req->hargaasli,
                            'deskripsi' => $req->deskripsi,
                            'visibility' => $req->visibility,
                        ]
                    ], ['external_code'], ['internal_code', 'supplier_code', 'product_name', 'category', 'supplier_price', 'diskon', 'branded', 'hargaasli', 'deskripsi', 'selling_price', 'visibility']);
        self::updateStockForTransitionToMaster($req);
        return $upsert;
    }

    static function updateStockForTransitionToMaster($req) {
        // fungsi ini untuk update data stock mengikuti data master
        $update = DB::table('tb_stock')
                    ->where('external_code', '=', $req->external_code)
                    ->update([
                        'internal_code' => strtoupper($req->internal_code),
                        'supplier_code' => strtoupper($req->supplier_code),
                        'product_name' => $req->product_name,
                        'selling_price' => $req->selling_price,
                        'supplier_price' => $req->supplier_price,
                    ]);
        return $update;
    }

    static function getListPendingOrder() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_order')
                    ->leftJoin('tb_payment', function($join){
                        $join->on('tb_order.instagram', '=', 'tb_payment.instagram')
                            ->on('tb_order.tanggal', '=', 'tb_payment.order_date');
                    })
                    ->leftJoin('tb_customer', 'tb_order.instagram', '=', 'tb_customer.username')
                    ->leftJoin('tb_reseller', 'tb_payment.instagram', '=', 'tb_reseller.username') //TAMBAHAN
                    ->leftJoin('tb_hit_and_run', 'tb_order.instagram', '=', 'tb_hit_and_run.instagram')
                    ->where('tb_customer.username' , '!=', 'tb_reseller.username') //TAMBAHAN
                    ->where('tb_order.order_status', '=', 'new')
                    ->groupBy('tb_order.instagram')
                    ->groupBy('tb_order.tanggal')
                    ->orderBy('tb_order.tanggal', 'desc')
                    ->orderBy('tb_order.instagram', 'asc')
                    ->select('tb_order.instagram','tb_order.comment', 'tb_customer.firstname', 'tb_customer.lastname', 'tb_customer.phone','tb_order.tanggal', DB::raw('SUM(tb_order.qty) AS qty_total, MAX(tb_payment.id) AS payment_id, MAX(tb_payment.order_status) AS order_status, MAX(tb_payment.transaction_status) AS transaction_status, MAX(tb_payment.awb) AS awb, tb_hit_and_run.flag AS flag'))
                    ->get();
        return $result;
    }

    static function getListPendingOrderReseller() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_order')
                    ->leftJoin('tb_payment', function($join){
                        $join->on('tb_order.instagram', '=', 'tb_payment.instagram')
                            ->on('tb_order.tanggal', '=', 'tb_payment.order_date');
                    })
                    
                    ->leftJoin('tb_reseller', 'tb_order.instagram', '=', 'tb_reseller.username') //TAMBAHAN
                    ->leftJoin('tb_hit_and_run', 'tb_order.instagram', '=', 'tb_hit_and_run.instagram')
                    ->where('tb_reseller.username', '!=', null)
                    ->where('tb_order.order_status', '=', 'new')
                    ->groupBy('tb_order.instagram')
                    ->groupBy('tb_order.tanggal')
                    ->orderBy('tb_order.tanggal', 'desc')
                    ->orderBy('tb_order.instagram', 'asc')
                    ->select('tb_order.instagram','tb_order.comment', 'tb_reseller.firstname', 'tb_reseller.lastname', 'tb_reseller.phone','tb_order.tanggal', DB::raw('SUM(tb_order.qty) AS qty_total, MAX(tb_payment.id) AS payment_id, MAX(tb_payment.order_status) AS order_status, MAX(tb_payment.transaction_status) AS transaction_status, MAX(tb_payment.awb) AS awb, tb_hit_and_run.flag AS flag'))
                    ->get();
        return $result;
    }

    static function getResellerDataByInstagram($instagram) {
        $result = DB::table('tb_reseller')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function getListReseller() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_reseller')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_reseller.username')
                    ->orderBy('username', 'asc')
                    ->groupBy('tb_reseller.username')
                    ->select(DB::raw('tb_reseller.username as username ,tb_reseller.reffer as reffer ,tb_reseller.phone as phone ,tb_reseller.firstname as firstname,tb_reseller.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->get();
        return $result;
    }

    static function getListCustomer() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_customer')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->orderBy('username', 'asc')
                    ->groupBy('tb_customer.username')
                    ->select(DB::raw('tb_customer.username as username ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->get();
        return $result;
    }

    static function getListCustomerVincent() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_customer')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('tb_customer.unsub','=',null)
                    ->where('username','=','628119994888')
                    ->orwhere('username','=','6281280016060')
                    ->orwhere('phone','=','628119994888')
                    ->orwhere('phone','=','6281280016060')
                    ->groupBy('tb_customer.username')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->get();
        return $result;
    }

    static function getListCustomerBlastPaidPE() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_customer')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('tb_customer.unsub','=',null)
                    ->where('tb_customer.phone','!=',null)
                    ->where('tb_customer.email','!=',null)
                    ->where('tb_payment.id','!=',null)
                    ->groupby('tb_payment.instagram')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    
                    ->orderBy('username', 'desc')
                    ->get();
        return $result;
    }

    static function getListCustomerBlastPaidP() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_customer')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('tb_customer.unsub','=',null)
                    ->where('tb_customer.phone','!=',null)
                    ->where('tb_payment.id','!=',null)
                    ->groupby('tb_payment.instagram')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->orderBy('username', 'desc')
                    ->get();
        return $result;
    }

    static function getListCustomerBlastPaidE() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_customer')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('tb_customer.unsub','=',null)
                    ->where('tb_customer.email','!=',null)
                    ->where('tb_payment.id','!=',null)
                    ->groupby('tb_payment.instagram')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->orderBy('username', 'desc')
                    ->get();
        return $result;
    }



    static function getListCustomerBlastNPaidPE() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_customer')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('tb_customer.unsub','=',null)
                    ->where('tb_customer.phone','!=',null)
                    ->where('tb_customer.email','!=',null)
                    ->where('tb_payment.id','=',null)
                    ->groupby('tb_customer.username')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->orderBy('username', 'desc')
                    ->get();
        return $result;
    }

    static function getListCustomerBlastNPaidP() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_customer')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('tb_customer.unsub','=',null)
                    ->where('tb_customer.phone','!=',null)
                    ->where('tb_payment.id','=',null)
                    ->groupby('tb_customer.username')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->orderBy('username', 'desc')
                    ->get();
        return $result;
    }

    static function getListCustomerBlastNPaidE() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_customer')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('tb_customer.unsub','=',null)
                    ->where('tb_customer.email','!=',null)
                    ->where('tb_payment.id','=',null)
                    ->groupby('tb_customer.username')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->orderBy('username', 'desc')
                    ->get();
        return $result;
    }

    
    static function getListCustomerBlastWOAll() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_whatsappbot')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_whatsappbot.phone')
                    ->leftJoin('tb_customer', 'tb_customer.username', '=', 'tb_whatsappbot.phone')
                    ->where('tb_customer.unsub','=',null)
                    ->orderBy('username', 'asc')
                    ->groupBy('tb_customer.username')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->get();
        return $result;
    }

    static function getListCustomerBlastWOPaid() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_whatsappbot')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_whatsappbot.phone')
                    ->leftJoin('tb_customer', 'tb_customer.username', '=', 'tb_whatsappbot.phone')
                    ->where('tb_payment.id','!=',null)
                    ->where('tb_customer.unsub','=',null)
                    ->orderBy('username', 'asc')
                    ->groupBy('tb_customer.username')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->get();
        return $result;
    }

    static function getListCustomerBlastWONPaid() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_whatsappbot')
                    ->leftJoin('tb_payment', 'tb_payment.instagram', '=', 'tb_whatsappbot.phone')
                    ->leftJoin('tb_customer', 'tb_customer.username', '=', 'tb_whatsappbot.phone')
                    ->where('tb_payment.id','=',null)
                    ->where('tb_customer.unsub','=',null)
                    ->orderBy('username', 'asc')
                    ->groupBy('tb_customer.username')
                    ->select(DB::raw('tb_customer.username as username, tb_customer.unsub as unsub  ,tb_customer.reffer as reffer ,tb_customer.phone as phone ,tb_customer.firstname as firstname,tb_customer.lastname as lastname,tb_customer.email as email, SUM(tb_payment.amount) as paidamount'))
                    ->get();
        return $result;
    }

    static function submitResetPassword($req) {
        $update = DB::table('tb_customer')
                    ->where('username', '=', $req->instagram)
                    ->update([
                        'password' => md5($req->password),
                    ]);
        return $update;
    }

    static function getPaidOrderByInstagram($instagram) {
        $result = DB::table('tb_payment')
                    ->where('instagram', '=', $instagram)
                    ->get();
        return $result;
    }
    static function getShippingAddress($instagram) {
        $result = DB::table('tb_customer_address')
                    ->where('username', '=', $instagram)
                    ->get();
        return $result;
    }

    static function getCustomerDataByInstagram($instagram) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function insertCustomerAddress($req) {
    
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $insert = DB::table('tb_customer_address')
                    ->insert([
                        'username' => $req->instagram,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $insert;
    }

    static function deleteCustomerAddress($id) {
        $delete = DB::table('tb_customer_address')
                    
                    ->where('id', '=', $id)
                    ->delete();
        return $delete;
    }


    static function getListPaidOrder() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_payment')
                    ->leftJoin('tb_paid_order', 'tb_payment.id', '=', 'tb_paid_order.payment_id')
                    ->leftJoin('tb_customer', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->leftJoin('tb_customer_address', 'tb_payment.instagram', '=', 'tb_customer_address.username')
                    ->groupBy('tb_paid_order.payment_id')
                    ->orderBy('tb_payment.order_date', 'desc')
                    ->orderBy('tb_payment.instagram', 'asc')
                    ->where('tb_payment.order_status', '=', 'paid')
                    ->select(DB::raw('MAX(tb_paid_order.payment_id) AS payment_id, MAX(tb_payment.order_status) AS order_status, MAX(tb_customer_address.shipping_address) AS shipping_address, MAX(tb_payment.instagram) AS instagram, MAX(tb_payment.transaction_status) AS transaction_status, MAX(tb_payment.order_date) AS tanggal, SUM(tb_paid_order.qty) AS qty_total, MAX(tb_payment.awb) AS awb,MAX(tb_payment.amount) AS amount, tb_customer.firstname, tb_customer.lastname,tb_customer.phone, MAX(tb_payment.packing_status) AS packing_status, MAX(tb_payment.shipping_status) AS shipping_status'))
                    ->get();
        return $result;
    }

    static function getListUnpaidOrder() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_payment')
                    ->leftJoin('tb_paid_order', 'tb_payment.id', '=', 'tb_paid_order.payment_id')
                    ->leftJoin('tb_customer', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->groupBy('tb_paid_order.payment_id')
                    ->orderBy('tb_payment.order_date', 'desc')
                    ->orderBy('tb_payment.instagram', 'asc')
                    ->orWhere('tb_payment.order_status', '=', 'pending')
                    ->select(DB::raw('MAX(tb_paid_order.payment_id) AS payment_id, MAX(tb_payment.order_status) AS order_status, MAX(tb_payment.instagram) AS instagram, MAX(tb_payment.transaction_status) AS transaction_status, MAX(tb_payment.order_date) AS tanggal, SUM(tb_paid_order.qty) AS qty_total, MAX(tb_payment.awb) AS awb, MAX(tb_payment.amount) AS amount, tb_customer.firstname, tb_customer.lastname, tb_customer.phone, MAX(tb_payment.packing_status) AS packing_status'))
                    ->get();
        return $result;
    }


    static function getListDeliveredOrder() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_payment')
                    ->leftJoin('tb_paid_order', 'tb_payment.id', '=', 'tb_paid_order.payment_id')
                    ->leftJoin('tb_customer', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->groupBy('tb_paid_order.payment_id')
                    ->orderBy('tb_payment.order_date', 'desc')
                    ->orderBy('tb_payment.instagram', 'asc')
                    ->where('tb_payment.order_status', '=', 'delivery')
                    ->select(DB::raw('MAX(tb_paid_order.payment_id) AS payment_id, MAX(tb_payment.order_status) AS order_status, MAX(tb_payment.instagram) AS instagram, MAX(tb_payment.transaction_status) AS transaction_status, MAX(tb_payment.order_date) AS tanggal, SUM(tb_paid_order.qty) AS qty_total, MAX(tb_payment.awb) AS awb,MAX(tb_payment.amount) AS amount,  tb_customer.firstname, tb_customer.lastname,tb_customer.phone'))
                    ->get();
        return $result;
    }

    static function getAllPendingOrder() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_order')
                    ->where('order_status', '=', 'new')
                    ->groupBy('internal_code')
                    ->groupBy('variant')
                    ->groupBy('size')
                    ->select('internal_code', 'variant', 'size', DB::raw('SUM(qty) AS qty'))
                    ->get();
        return $result;
    }

    static function getAllPendingOrderByProduct($req) {
        $result = DB::table('tb_order')
                    ->where('internal_code', '=', $req->internal_code)
                    ->where('variant', '=', $req->variant)
                    ->where('size', '=', $req->size)
                    ->get();
        return $result;
    }

    static function getOrderByParameter($instagram, $date) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    ->where('tanggal', '=', $date)
                    ->orderBy('id', 'asc')
                    ->get();
        return $result;
    }

    static function getOrderByParameterV2($instagram, $date) {
        $result = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    ->where('tanggal', '<=', $date)
                    ->groupBy('internal_code')
                    ->groupBy('variant')
                    ->groupBy('size')
                    ->select('id','internal_code', 'variant', 'size', DB::raw('SUM(qty) AS qty, MAX(tanggal) AS tanggal, MAX(instagram) AS instagram, comment'))
                    ->get();
        return $result;
    }


    static function saveAWB($req) {
        $update = DB::table('tb_payment')
                    ->where('id', '=', $req->payment_id)
                    ->update([
                        'awb' => $req->awb,
                        'order_status' => 'delivery'
                    ]);
        return $update;
    }
    static function deleteDelivdOrd($req) {
        $update = DB::table('tb_payment')
                    ->where('id', '=', $req->payment_id)
                    ->update([
                        'awb' => null,
                        'order_status' => 'paid'
                    ]);
        return $update;
    }

    static function registerCustomerManually($req) {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $insert = DB::table('tb_customer')
                    ->insert([
                        'username' => $req->instagram,
                        'firstname' => $req->nama_depan,
                        'lastname' => $req->nama_belakang,
                        'password' => null,
                        'phone' => str_replace(' ','',$req->telepon),
                        'email' => $req->email,
                        'reffer' => 'MANUAL',
                    ]);

        $insert2 = DB::table('tb_customer_address')
                    ->insert([
                        'username' => $req->instagram,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);
        return $insert;
    }

    static function updateCustomerManually($req) {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        $update = DB::table('tb_customer')
                    ->where('username', '=', $req->instagram)
                    ->update([
                        'firstname' => $req->nama_depan,
                        'lastname' => $req->nama_belakang,
                        'phone' => str_replace(' ','',$req->telepon),
                        'email' => $req->email,
                    ]);
        



        $insert2 = DB::table('tb_customer_address')
                    ->insert([
                        'username' => $req->instagram,
                        'shipping_address' => $address_gen,
                        'subdistrict_id' => $req->subdistrict_id,
                        'shipping_cost' => $req->shipping_cost,
                    ]);



        
        

        
        return $insert2;
    }


    static function deleteAddress($req) {
        
        $delete = DB::table('tb_customer_address')
                    ->where('username', '=', $req)
                    ->delete();
                    
        return $delete;



    }

    static function insertPayment($req) {
        if (isset($req->shipping_address)) {
            $address_gen = $req->shipping_address;
        } else {
        $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";
        }
        $insert = DB::table('tb_payment')
                    ->insert([
                        'id' => $req->order_id,
                        'instagram' => $req->instagram,
                        'order_date' => $req->tanggal,
                        'shipping_address' => $address_gen,
                        'amount' => (int) $req->amount,
                        'payment_type' => $req->payment_type,
                        'transaction_time' => date('Y-m-d H:i:s'),
                        'transaction_status' => $req->transaction_status,
                        'order_status' => 'paid',
                    ]);
                    
        return $insert;
    }


    static function getNorekBySupp($req) {
    	$result = DB::table('tb_supplierlogin')
                    ->where('supplier1', '=',$req)
    				->first();
    	return $result;
    }


    static function getSupplierLoginList() {
    	$result = DB::table('tb_supplierlogin')
    				->get();
    	return $result;
    }

    static function getSupplierSales($req) {
    	$result = DB::table('tb_paid_order')
                    ->leftJoin('tb_stock_master', 'tb_paid_order.internal_code', '=', 'tb_stock_master.internal_code')
                    ->where(function($result) use ($req){
                        $result->where('tb_stock_master.supplier_code', '=', $req->supplier1)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier2)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier3)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier4)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier5)
                            ->orwhere('tb_stock_master.supplier_code', '=', $req->supplier6);
                    })
                    ->where('tb_paid_order.qty','>=','1')
                    
    				->get();
    	return $result;
    }

    static function submitSupplierLogin($req) {
        $insert = DB::table('tb_supplierlogin')
                    ->insert([
                        'username' => strtoupper($req->username),
                        'nama' => $req->nama,
                        'password' => $req->password,
                        'supplier1' => $req->supplier_code1,
                        'supplier2' => $req->supplier_code2,
                        'supplier3' => $req->supplier_code3,
                        'supplier4' => $req->supplier_code4,
                        'supplier5' => $req->supplier_code5,
                        'supplier6' => $req->supplier_code6,
                        'demodemode' => $req->demodemode,

                    ]);
        return $insert;
    }

    static function getSupplierLoginById($id) {
        $result = DB::table('tb_supplierlogin')
                    ->where('id', '=', $id)
                    ->first();
        return $result;
    }

    static function getSupplierLoginByUsername($id) {
        $result = DB::table('tb_supplierlogin')
                    ->where('username', '=', $id)
                    ->first();
        return $result;
    }

    static function submitEditSupplierLogin($req) {
        
        $update = DB::table('tb_supplierlogin')
                    ->where('id', '=', $req->id)
                    ->update([
                        'nama' => $req->nama,
                        'password' => $req->password,
                        'supplier1' => $req->supplier_code1,
                        'supplier2' => $req->supplier_code2,
                        'supplier3' => $req->supplier_code3,
                        'supplier4' => $req->supplier_code4,
                        'supplier5' => $req->supplier_code5,
                        'supplier6' => $req->supplier_code6,
                        'demodemode' => $req->demodemode,
                    ]);
        return $update;
    }

    static function deleteSupplierLogin($req) {
        $delete = DB::table('tb_supplierlogin')
                    ->where('id', '=', $req->id)
                    ->delete();
        return $delete;
    }








    static function getSupplierPaymentList() {
    	$result = DB::table('tb_supplierpayment')
    				->get();
    	return $result;
    }

    static function submitSupplierPayment($req) {
        $insert = DB::table('tb_supplierpayment')
                    ->insert([
                        'supplier_code' => strtoupper($req->supplier_code),
                        'tanggal' => $req->tanggal,
                        'total' => $req->total,
                        'pcs' => $req->pcs,
                        'detail' => $req->detail,

                    ]);
        return $insert;
    }

    static function getSupplierPaymentById($id) {
        $result = DB::table('tb_supplierpayment')
                    ->where('id', '=', $id)
                    ->first();
        return $result;
    }

    static function submitEditSupplierPayment($req) {
        
        $update = DB::table('tb_supplierpayment')
                    ->where('id', '=', $req->id)
                    ->update([
                        'tanggal' => $req->tanggal,
                        'total' => $req->total,
                        'pcs' => $req->pcs,
                        'detail' => $req->detail,
                    ]);
        return $update;
    }

    static function deleteSupplierPayment($req) {
        $delete = DB::table('tb_supplierpayment')
                    ->where('id', '=', $req->id)
                    ->delete();
        return $delete;
    }





    static function updateStock($req) {
        $read = DB::table('tb_order')
                    ->where('instagram', '=', $req->instagram)
                    ->where('tanggal', '=', $req->order_date)
                    ->get();

        foreach ($read as $row) {
            $update = DB::table('tb_stock')
                        ->where('internal_code', '=', $row->internal_code)
                        ->where('variant', '=', $row->variant)
                        ->where('size', '=', $row->size)
                        ->decrement('qty', $row->qty);
        }
        return true;
    }

    static function summaryOrder($batch_no) {
        $result = DB::table('tb_order')
                    ->where('batch_no', '=', $batch_no)
                    ->groupBy('internal_code')
                    ->groupBy('variant')
                    ->groupBy('size')
                    ->select(DB::raw('internal_code, variant, size, SUM(qty) as qty'))
                    ->get();
        return $result;
    }

    static function changeOrder($req) {
        $update = DB::table('tb_order')
                    ->where('id', '=', $req->id)
                    ->update([
                        'qty' => $req->qty,
                        'variant' => strtoupper($req->variant),
                        'size' => strtoupper($req->size),
                    ]);
        return $update;
    }

    static function getProductMainData() {
        $result = DB::table('tb_stock')
                    ->groupBy('internal_code')
                    ->select('internal_code', DB::raw('MAX(product_name) AS product_name'))
                    ->get();
        return $result;
    }

    static function getProductMainDataSimple() {
        $result = DB::table('tb_stock')
                    ->where('qty', '>', 0)
                    ->groupBy('internal_code')
                    ->select('internal_code', DB::raw('MAX(product_name) AS product_name'))
                    ->get();
        return $result;
    }

    static function getVariant($internal_code) {
        $result = DB::table('tb_stock')
                    ->where('internal_code','=',$internal_code)
                    ->select('variant')
                    ->distinct()
                    ->get();
        return $result;
    }

    static function getSize($internal_code, $variant) {
        $result = DB::table('tb_stock')
                    ->where('internal_code','=',$internal_code)
                    ->where('variant','=',$variant)
                    ->select('size')
                    ->distinct()
                    ->get();
        return $result;
    }

    static function deleteBatchNo($batch_no) {
        $delete = DB::table('tb_order')
                    ->where('batch_no', '=', $batch_no)
                    ->delete();
        return $delete;
    }

    static function getAllUser() {
        $result = DB::table('tb_user')
                    ->get();
        return $result;
    }

    static function submitUser($req) {
        $insert = DB::table('tb_user')
                    ->insert([
                        'username' => $req->username,
                        'password' => md5($req->password),
                        'fullname' => $req->fullname,
                        'access_level' => $req->access_level,
                    ]);
        return $insert;
    }

    static function deleteUser($username) {
        $delete = DB::table('tb_user')
                    ->where('username', '=', $username)
                    ->delete();
        return $delete;
    }

   
    static function deletePendingOrder($data) {
        $delete = DB::table('tb_order')
                    ->where('instagram', '=', $data['instagram'])
                    ->where('tanggal', '<=', $data['order_date'])
                    ->where('internal_code', '=', $data['internal_code'])
                    ->where('variant', '=', $data['variant'])
                    ->where('size', '=', $data['size'])
                    ->delete();
        return $delete;
    }

    static function insertPaidOrder($data) {
        $insert = DB::table('tb_paid_order')
                    ->insert([
                        'payment_id' => $data['payment_id'],
                        'internal_code' => strtoupper($data['internal_code']),
                        'variant' => $data['variant'],
                        'size' => $data['size'],
                        'qty' => $data['qty'],
                        'selling_price' => $data['selling_price'],
                        'order_date' => $data['order_date'],
                        'comment' => $data['comment'],
                    ]);
        return $insert;
    }

    static function reduceStock($data) {
        $update = DB::table('tb_stock')
                    ->where('internal_code', '=', $data['internal_code'])
                    ->where('variant', '=', $data['variant'])
                    ->where('size', '=', $data['size'])
                    ->decrement('qty', $data['qty']);
        return $update;
    }

    static function getMasterImage($external_code) {
        $result = DB::table('tb_stock_master')
                    ->where('external_code', '=', $external_code)
                    ->first();
        return $result;
    }

    static function deleteListOrderPersonal($instagram, $tanggal) {
        $delete = DB::table('tb_order')
                    ->where('instagram', '=', $instagram)
                    ->where('tanggal', '=', $tanggal)
                    ->delete();
        return $delete;
    }

    static function deleteListOrderPersonal3days($tanggal) {
        $delete = DB::table('tb_order')
                    ->where('tanggal', '<=', $tanggal)
                    ->delete();
        return $delete;
    }

    static function getUniqueStock() {
        $result = DB::table('tb_stock')
                    ->select('internal_code', 'external_code', 'product_name', 'supplier_code', 'selling_price', 'supplier_price')
                    ->distinct()
                    ->get();
        return $result;
    }

    static function getPaidOrderByPaymentID($payment_id) {
        $result = DB::table('tb_paid_order')
                    ->where('payment_id', '=', $payment_id)
                    ->get();
        return $result;
    }

    static function returnToStock($req) {
        $update = DB::table('tb_stock')
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->where('variant', '=', strtoupper($req->variant))
                    ->where('size', '=', $req->size)
                    ->increment('qty', $req->qty);
        return $update;
    }

    static function deletePaidOrderList($req, $payment_id) {
        $delete = DB::table('tb_paid_order')
                    ->where('payment_id', '=', $payment_id)
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->where('variant', '=', strtoupper($req->variant))
                    ->where('size', '=', $req->size)
                    ->delete();
        return $delete;
    }

    static function deletePayment($payment_id) {
        $delete = DB::table('tb_payment')
                    ->where('id', '=', $payment_id)
                    ->delete();
        return $delete;
    }

    static function returnToUnpaidOrder($req, $custdata) {
        $insert = DB::table('tb_order')
                    ->insert([
                        'batch_no' => '-1',
                        'instagram' => $custdata->instagram,
                        'internal_code' => strtoupper($req->internal_code),
                        'variant' => strtoupper($req->variant),
                        'size' => $req->size,
                        'qty' => $req->qty,
                        'tanggal' => $custdata->order_date,
                        'comment' => 'Retur dari cancel payment',
                        'order_status' => 'new'
                    ]);
        return $insert;
    }

    static function getCustomerDataFromPaymentID($payment_id) {
        $result = DB::table('tb_payment')
                    ->leftJoin('tb_customer', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->where('tb_payment.id', '=', $payment_id)
                    ->first();
        return $result;
    }

    static function changePackingStatus($payment_id, $status) {
        $update = DB::table('tb_payment')
                    ->where('id', '=', $payment_id)
                    ->update([
                        'packing_status' => $status
                    ]);
        return $update;
    }

    static function changeShippingStatus($payment_id, $status) {
        $update = DB::table('tb_payment')
                    ->where('id', '=', $payment_id)
                    ->update([
                        'shipping_status' => $status
                    ]);
        return $update;
    }

    static function updateManualConfirmation($req) {
        $update = DB::table('tb_payment')
                    ->where('id', '=', $req->payment_id)
                    ->update([
                        'payment_type' => $req->payment_type,
                        'transaction_status' => 'Confirm Manual - '.$req->payment_type,
                        'order_status' => 'paid',
                    ]);
        return $update;
    }

    static function getRefundList() {
        $result = DB::table('tb_refund_master')
                    ->get();
        return $result;
    }

    static function insertNewRefund($req) {
        $insert1 = DB::table('tb_refund_master')
                    ->insert([
                        'refund_master_id' => $req->refund_master_id,
                        'payment_id' => $req->payment_id,
                        'instagram' => $req->instagram,
                        'alasan' => $req->alasan,
                        'nominal' => $req->nominal,
                        'last_step_no' => $req->step_no,
                        'last_step_detail' => $req->step_detail,
                    ]);
        $insert2 = DB::table('tb_refund_detail')
                    ->insert([
                        'refund_master_id' => $req->refund_master_id,
                        'step_no' => $req->step_no,
                        'step_detail' => $req->step_detail,
                    ]);
        foreach ($req->item_masalah as $order_id) {
            $order_item = DB::table('tb_paid_order')
                            ->where('id', '=', $order_id)
                            ->first();
            $insert3 = DB::table('tb_refund_item')
                        ->insert([
                            'refund_master_id' => $req->refund_master_id,
                            'paid_order_item_id' => $order_id,
                            'internal_code' => strtoupper($order_item->internal_code),
                            'variant' => strtoupper($order_item->variant),
                            'size' => $order_item->size,
                            'qty_masalah' => $req->qty_masalah[$order_id],
                            'selling_price' => $order_item->selling_price,
                        ]);
        }
        if ($insert1 && $insert2 && $insert3) {
            return true;
        } else {
            return false;
        }
    }

    static function updateRefundStep1($req) {
        $update = DB::table('tb_refund_master')
                    ->where('refund_master_id', '=', $req->refund_master_id)
                    ->update([
                        'last_step_no' => $req->step_no,
                        'last_step_detail' => $req->step_detail,
                    ]);
        $update2 = DB::table('tb_refund_detail')
                    ->insert([
                        'refund_master_id' => $req->refund_master_id,
                        'step_no' => $req->step_no,
                        'step_detail' => $req->step_detail,
                        'courier_retur' => $req->courier_retur,
                        'awb_retur' => $req->awb_retur,
                    ]);
        return $update2;
    }

    static function updateRefundStep2($req, $path) {
        $update = DB::table('tb_refund_master')
                    ->where('refund_master_id', '=', $req->refund_master_id)
                    ->update([
                        'last_step_no' => $req->step_no,
                        'last_step_detail' => $req->step_detail,
                    ]);
        $update2 = DB::table('tb_refund_detail')
                    ->insert([
                        'refund_master_id' => $req->refund_master_id,
                        'step_no' => $req->step_no,
                        'step_detail' => $req->step_detail,
                        'received_product_photo' => $path,
                    ]);
        return $update2;
    }

    static function updateRefundStep3($req, $path) {
        $update = DB::table('tb_refund_master')
                    ->where('refund_master_id', '=', $req->refund_master_id)
                    ->update([
                        'last_step_no' => $req->step_no,
                        'last_step_detail' => $req->step_detail,
                    ]);
        if ($req->resolution_option == 'refund') {
            $update2 = DB::table('tb_refund_detail')
                        ->insert([
                            'refund_master_id' => $req->refund_master_id,
                            'step_no' => $req->step_no,
                            'step_detail' => $req->step_detail,
                            'resolution_option' => $req->resolution_option,
                            'bank_name' => $req->bank_name,
                            'account_no' => $req->account_no,
                            'account_name' => $req->account_name,
                            'transfer_proof' => $path
                        ]);
        } else {
            $update2 = DB::table('tb_refund_detail')
                        ->insert([
                            'refund_master_id' => $req->refund_master_id,
                            'step_no' => $req->step_no,
                            'step_detail' => $req->step_detail,
                            'resolution_option' => $req->resolution_option,
                            'awb_replacement' => $req->awb_replacement,
                        ]);
        }
        return $update2;
    }

    static function getRefundMasterByID($refund_master_id) {
        $result = DB::table('tb_refund_master')
                    ->where('refund_master_id', '=', $refund_master_id)
                    ->first();
        return $result;
    }

    static function getRefundItemByMasterID($refund_master_id) {
        $result = DB::table('tb_refund_item')
                    ->where('refund_master_id', '=', $refund_master_id)
                    ->get();
        return $result;
    }

    static function getRefundDetailByMasterID($refund_master_id) {
        $result = DB::table('tb_refund_detail')
                    ->where('refund_master_id', '=', $refund_master_id)
                    ->get();
        return $result;
    }
    static function getRefundDetailByMasterIDAndStep($refund_master_id, $step) {
        $result = DB::table('tb_refund_detail')
                    ->where('refund_master_id', '=', $refund_master_id)
                    ->where('step_no', '=', $step)
                    ->first();
        return $result;
    }

    static function returnRefundedItemToStock($req) {
        $update = DB::table('tb_stock')
                    ->where('internal_code', '=', strtoupper($req->internal_code))
                    ->where('variant', '=', strtoupper($req->variant))
                    ->where('size', '=', $req->size)
                    ->increment('qty', $req->qty_masalah);
        return $update;
    }

    static function getCustomer($instagram) {
        $result = DB::table('tb_customer')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function getCustomerAddress($instagram) {
        $result = DB::table('tb_customer_address')
                    ->where('username', '=', $instagram)
                    ->get();
        return $result;
    }

    static function getAllShippingAddress() {
        $result = DB::table('tb_payment')
                    ->get();
        return $result;
    }

    static function repopulateCustomerData($data) {
        var_dump($data);
        $upsert = DB::table('tb_customer')
                    ->upsert([
                        [
                            'username' => $data['username'],
                            'firstname' => $data['firstname'],
                            'lastname' => $data['lastname'],
                            'phone' => $data['phone'],
                            'email' => $data['email'],
                        ]
                    ], ['username'], ['firstname', 'lastname', 'phone', 'email']);

        $upsert2 = DB::table('tb_customer_address')
                    ->upsert([
                        [
                            'username' => $data['username'],
                            'shipping_address' => $data['shipping_address'],
                            'shipping_cost' => 99999,
                        ]
                    ], ['username'], ['shipping_address']);
        return $upsert;
    }

    static function getTotalSales() {
        $result = DB::table('tb_payment')
                    ->select(DB::raw('SUM(amount) AS amount'))
                    ->first();
        return $result->amount;
    }

    static function getTotalSalesCount() {
        $result = DB::table('tb_payment')
                    ->get();
        return count($result);
    }

    static function getRefundListOnProgress() {
        $result = DB::table('tb_refund_master')
                    ->where('last_step_no', '<', 3)
                    ->get();
        return $result;
    }

    static function getMonthlySales() {
        $result = DB::table('tb_payment')
                    ->whereRaw('transaction_time >= DATE_FORMAT(NOW() ,"%Y-%m-01") - INTERVAL 12 MONTH')
                    ->groupBy(DB::raw('YEAR(transaction_time), MONTH(transaction_time)'))
                    ->select(DB::raw('DATE_FORMAT(transaction_time, "%b %Y") AS order_month, SUM(amount) AS total'))
                    ->get();
        return $result;
    }

    static function getHitAndRunByInstagram($instagram) {
        $result = DB::table('tb_hit_and_run')
                    ->where('instagram', '=', $instagram)
                    ->first();
        return $result;
    }

    static function removeHitAndRun($req) {
        $delete = DB::table('tb_hit_and_run')
                    ->where('instagram', '=', $req->instagram)
                    ->delete();
        return $delete;
    }

    static function insertHitAndRun($req) {
        $insert = DB::table('tb_hit_and_run')
                    ->insert([
                        'instagram' => $req->instagram,
                        'flag' => $req->status,
                    ]);
        return $insert;
    }

    static function moveOrderToHitAndRun($req) {
        $update = DB::table('tb_order')
                    ->where('instagram', '=', $req->instagram)
                    ->update([
                        'order_status' => 'hit and run',
                    ]);
        return $update;
    }

    static function returnHitAndRunToActiveOrder($req) {
        $update = DB::table('tb_order')
                    ->where('instagram', '=', $req->instagram)
                    ->update([
                        'order_status' => 'new',
                    ]);
        return $update;
    }

    static function getListHitAndRunOrder() {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_order')
                    ->leftJoin('tb_payment', function($join){
                        $join->on('tb_order.instagram', '=', 'tb_payment.instagram')
                            ->on('tb_order.tanggal', '=', 'tb_payment.order_date');
                    })
                    ->leftJoin('tb_customer', 'tb_payment.instagram', '=', 'tb_customer.username')
                    ->leftJoin('tb_hit_and_run', 'tb_order.instagram', '=', 'tb_hit_and_run.instagram')
                    ->where('tb_order.order_status', '=', 'hit and run')
                    ->groupBy('tb_order.instagram')
                    ->groupBy('tb_order.tanggal')
                    ->orderBy('tb_order.tanggal', 'desc')
                    ->orderBy('tb_order.instagram', 'asc')
                    ->select('tb_order.instagram', 'tb_customer.firstname', 'tb_customer.lastname','tb_customer.phone', 'tb_order.tanggal', DB::raw('SUM(tb_order.qty) AS qty_total, MAX(tb_payment.id) AS payment_id, MAX(tb_payment.order_status) AS order_status, MAX(tb_payment.transaction_status) AS transaction_status, MAX(tb_payment.awb) AS awb, tb_hit_and_run.flag AS flag'))
                    ->get();
        return $result;
    }

    static function getAllSalesYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->sum(DB::raw('tb_paid_order.qty * tb_paid_order.selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->sum(DB::raw('tb_paid_order.qty * tb_paid_order.supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }

    static function getWebVisitAv() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_webvisitor')
                    ->where('tanggal','=',date('Y-m-d'))
                    ->first();
        return $result;
    }
    static function getWebVisitAvYesterday() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_webvisitor')
                    ->where('tanggal','=',date('Y-m-d',strtotime('-1 days')))
                    ->first();
        return $result;
    }

    static function getRegisteredReffer() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_customer')
                    ->where('tanggaldaftar','>=',date('Y-m-d') . ' 00:00:00')
                    ->where('tanggaldaftar','<=',date('Y-m-d') . ' 23:59:59')
                    ->groupBy('reffer')
                    ->select(DB::raw('tb_customer.reffer as reffer , count(*) as counter'))
                    ->get();
        return $result;
    }
    static function getRegisteredRefferYesterday() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_customer')
                    ->where('tanggaldaftar','>=',date('Y-m-d',strtotime('-1 days')) . ' 00:00:00')
                    ->where('tanggaldaftar','<=',date('Y-m-d',strtotime('-1 days')) . ' 23:59:59')
                    ->groupBy('reffer')
                    ->select(DB::raw('tb_customer.reffer as reffer , count(*) as counter'))
                    ->get();
        return $result;
    }

    static function getReseller($instagram) {
        $result = DB::table('tb_reseller')
                    ->where('username', '=', $instagram)
                    ->first();
        return $result;
    }

    static function getTotalCustomer() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_customer')
                   
                    ->select(DB::raw('count(*) as counter'))
                    ->first();
        return $result;
    }

    static function getPaymentReffer() {
        date_default_timezone_set('Asia/Jakarta');
        $result = DB::table('tb_payment')
                    ->where('order_date','>=',date('Y-m-d') . ' 00:00:00')
                    ->where('order_date','<=',date('Y-m-d') . ' 23:59:59')
                    ->groupBy('transaction_status')
                    ->select(DB::raw('tb_payment.transaction_status as transaction_status , count(*) as counter'))
                    ->get();
        return $result;
    }


    static function getMonthlySalesYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEAR(tb_payment.transaction_time) = YEAR(CURDATE()) AND MONTH(tb_payment.transaction_time) = MONTH(CURDATE())')
                        ->sum(DB::raw('qty * selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEAR(tb_payment.transaction_time) = YEAR(CURDATE()) AND MONTH(tb_payment.transaction_time) = MONTH(CURDATE())')
                        ->sum(DB::raw('qty * supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEAR(tb_payment.transaction_time) = YEAR(CURDATE()) AND MONTH(tb_payment.transaction_time) = MONTH(CURDATE())')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->whereRaw('YEAR(transaction_time) = YEAR(CURDATE()) AND MONTH(transaction_time) = MONTH(CURDATE())')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }

    static function getYearlySalesYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEAR(tb_paid_order.order_date) = YEAR(CURDATE())')
                        ->sum(DB::raw('qty * selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEAR(tb_paid_order.order_date) = YEAR(CURDATE())')
                        ->sum(DB::raw('qty * supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEAR(tb_paid_order.order_date) = YEAR(CURDATE())')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->whereRaw('YEAR(transaction_time) = YEAR(CURDATE())')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }

    static function getWeeklySalesYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEARWEEK(tb_paid_order.order_date, 1) = YEARWEEK(CURDATE(), 1)')
                        ->sum(DB::raw('qty * selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEARWEEK(tb_paid_order.order_date, 1) = YEARWEEK(CURDATE(), 1)')
                        ->sum(DB::raw('qty * supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('YEARWEEK(tb_paid_order.order_date, 1) = YEARWEEK(CURDATE(), 1)')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->whereRaw('YEARWEEK(transaction_time, 1) = YEARWEEK(CURDATE(), 1)')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }
    static function getDailySaless() {
        $result = DB::table('tb_paid_order')
                    //->leftJoin('tb_stock_master', 'tb_paid_order.internal_code', '=', 'tb_stock_master.internal_code')
                    ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                    ->where('tb_payment.order_status', '!=', 'pending')
                    ->groupBy(DB::raw('DATE(tb_paid_order.order_date)'))
                    ->orderBy(DB::raw('DATE(tb_paid_order.order_date)'), 'asc')
                    ->select(DB::raw('SUM(tb_paid_order.qty) AS qty_sold,SUM(tb_paid_order.qty*tb_paid_order.selling_price) AS total_sold, DATE_FORMAT(tb_paid_order.order_date, "%d/%m") AS tanggal'))
                    ->get();
        return $result;

    }

    static function getDailySalesYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()')
                        ->sum(DB::raw('qty * selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()')
                        ->sum(DB::raw('qty * supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->whereRaw('DATE(transaction_time) = CURDATE()')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }

    static function getDayMinusTwoYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 3 DAY')
                        ->sum(DB::raw('qty * selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 3 DAY')
                        ->sum(DB::raw('qty * supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 3 DAY')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->whereRaw('DATE(transaction_time) = CURDATE()- INTERVAL 3 DAY')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }

    static function getDayBeforeSalesYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 2 DAY')
                        ->sum(DB::raw('qty * selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 2 DAY')
                        ->sum(DB::raw('qty * supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 2 DAY')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->whereRaw('DATE(transaction_time) = CURDATE()- INTERVAL 2 DAY')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }

    static function getYesterdaySalesYTD() {
        $total      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 1 DAY')
                        ->sum(DB::raw('qty * selling_price'));
        $modal      = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 1 DAY')
                        ->sum(DB::raw('qty * supplier_price'));
        $pcs        = DB::table('tb_paid_order')
                        ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                        ->where('tb_payment.order_status', '!=', 'pending')
                        ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 1 DAY')
                        ->sum('tb_paid_order.qty');
        $shipment   = DB::table('tb_payment')
                        ->where('order_status', '!=', 'pending')
                        ->whereRaw('DATE(transaction_time) = CURDATE()- INTERVAL 1 DAY')
                        ->count();
        $data = [
            'total' => $total,
            'pcs' => $pcs,
            'shipment' => $shipment,
            'modal' => $modal,
        ];
        return $data;
    }

    
    static function getSalesReportByDateFilter($req) {
        DB::statement("SET SQL_MODE=''");
        $result = DB::table('tb_paid_order')
                    ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                    ->groupBy('tb_payment.id')
                    ->whereRaw('DATE(tb_payment.transaction_time) >= "'.$req->start.'" AND DATE(tb_payment.transaction_time) <= "'.$req->end.'"')
                    ->orderBy('tb_payment.transaction_time')
                    ->select(DB::raw('tb_payment.id AS payment_id, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                    ->get();
        return $result;
    }

    static function getSalesReportByPeriode($periode) {
        DB::statement("SET SQL_MODE=''");
        switch ($periode) {
            case 'yearly':
                $result = DB::table('tb_paid_order')
                            ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                            ->groupBy('tb_payment.id')
                            ->where('tb_payment.order_status', '!=', 'pending')
                            ->whereRaw('YEAR(tb_paid_order.order_date) = YEAR(CURDATE())')
                            ->orderBy('tb_paid_order.order_date')
                            ->select(DB::raw('tb_payment.id AS payment_id,tb_payment.transaction_status AS transaction_status,tb_payment.order_status AS order_status, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                            ->get();
                break;
            case 'monthly':
                $result = DB::table('tb_paid_order')
                            ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                            ->groupBy('tb_payment.id')
                            ->where('tb_payment.order_status', '!=', 'pending')
                            ->whereRaw('YEAR(tb_paid_order.order_date) = YEAR(CURDATE()) AND MONTH(tb_payment.transaction_time) = MONTH(CURDATE())')
                            ->orderBy('tb_paid_order.order_date')
                            ->select(DB::raw('tb_payment.id AS payment_id,tb_payment.transaction_status AS transaction_status,tb_payment.order_status AS order_status, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                            ->get();
                break;
            case 'weekly':
                $result = DB::table('tb_paid_order')
                            ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                            ->groupBy('tb_payment.id')
                            ->where('tb_payment.order_status', '!=', 'pending')
                            ->whereRaw('YEARWEEK(tb_paid_order.order_date, 1) = YEARWEEK(CURDATE(), 1)')
                            ->orderBy('tb_paid_order.order_date')
                            ->select(DB::raw('tb_payment.id AS payment_id,tb_payment.transaction_status AS transaction_status,tb_payment.order_status AS order_status, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                            ->get();
                break;
            case 'dayminustwo':
                    $result = DB::table('tb_paid_order')
                                ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                                ->groupBy('tb_payment.id')
                                ->where('tb_payment.order_status', '!=', 'pending')
                                ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 3 DAY')
                                ->orderBy('tb_paid_order.order_date')
                                ->select(DB::raw('tb_payment.id AS payment_id,tb_payment.transaction_status AS transaction_status,tb_payment.order_status AS order_status, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                                ->get();
                    break;
            case 'daybefore':
                    $result = DB::table('tb_paid_order')
                                ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                                ->groupBy('tb_payment.id')
                                ->where('tb_payment.order_status', '!=', 'pending')
                                ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 2 DAY')
                                ->orderBy('tb_paid_order.order_date')
                                ->select(DB::raw('tb_payment.id AS payment_id,tb_payment.transaction_status AS transaction_status,tb_payment.order_status AS order_status, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                                ->get();
                    break;
            case 'yesterday':
                    $result = DB::table('tb_paid_order')
                                ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                                ->groupBy('tb_payment.id')
                                ->where('tb_payment.order_status', '!=', 'pending')
                                ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()- INTERVAL 1 DAY')
                                ->orderBy('tb_paid_order.order_date')
                                ->select(DB::raw('tb_payment.id AS payment_id,tb_payment.transaction_status AS transaction_status,tb_payment.order_status AS order_status, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                                ->get();
                    break;
            case 'daily':
                $result = DB::table('tb_paid_order')
                            ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                            ->groupBy('tb_payment.id')
                            ->where('tb_payment.order_status', '!=', 'pending')
                            ->whereRaw('DATE(tb_paid_order.order_date) = CURDATE()')
                            ->orderBy('tb_paid_order.order_date')
                            ->select(DB::raw('tb_payment.id AS payment_id,tb_payment.transaction_status AS transaction_status,tb_payment.order_status AS order_status, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                            ->get();
                break;

            case 'all':
            default:
                $result = DB::table('tb_paid_order')
                            ->leftJoin('tb_payment', 'tb_paid_order.payment_id', '=', 'tb_payment.id')
                            ->groupBy('tb_payment.id')
                            ->where('tb_payment.order_status', '!=', 'pending')
                            ->orderBy('tb_paid_order.order_date')
                            ->select(DB::raw('tb_payment.id AS payment_id,tb_payment.transaction_status AS transaction_status,tb_payment.order_status AS order_status, tb_payment.instagram AS instagram, tb_payment.amount AS amount, tb_paid_order.order_date AS transaction_time, SUM(tb_paid_order.qty) AS qty'))
                            ->get();
                break;
        }
        return $result;
    }

    static function getOrderItemByID($id) {
        $result = DB::table('tb_order')
                    ->where('id', '=', $id)
                    ->first();
        return $result;
    }

    static function deleteOrderByID($id) {
        $delete = DB::table('tb_order')
                    ->where('id', '=', $id)
                    ->delete();
        return $delete;
    }

    static function updateSupplierPricePaidOrder($internal_code, $supplier_price) {
        $update = DB::table('tb_paid_order')
                    ->where('internal_code', '=', $internal_code)
                    ->update([
                        'supplier_price' => $supplier_price,
                    ]);
        return $update;
    }

    static function getValuationItems() {
        $result = DB::table('tb_stock')
                    ->leftJoin('tb_stock_master', 'tb_stock.internal_code', '=', 'tb_stock_master.internal_code')
                    ->groupBy('tb_stock.internal_code')
                    ->select(DB::raw('tb_stock.internal_code, SUM(tb_stock.qty) AS qty_stock, tb_stock_master.supplier_price'))
                    ->get();
        return $result;
    }

    static function saveDiscount($req) {
        $update = DB::table('tb_discount')
                    ->where('id', 1)
                    ->update([
                        'percent' => $req->percent,
                        'percentwa' => $req->percentwa,
                        'wa1' => $req->wa1,
                        'wa2' => $req->wa2,
                        'wa3' => $req->wa3,
                        'wa4' => $req->wa4,
                    ]);
        return $update;
    }

    static function getDiscount() {
        $result = DB::table('tb_discount')
                    ->first();
        return $result;
    }

    static function getLaporanPenjualanSupplier($req) {
        $query = DB::table('tb_paid_order AS ord')
                    ->leftJoin('tb_stock_master AS mst', 'ord.internal_code', '=', 'mst.internal_code')
                    ->leftJoin('tb_supplier AS sup', 'mst.supplier_code', '=', 'sup.supplier_code');
        if (isset($req->report_date)) {
            $query->where(DB::raw('DATE(ord.order_date)'), $req->report_date);
        }
        if (isset($req->supplier_code)) {
            $query->where('mst.supplier_code', $req->supplier_code);
        }
        $result = $query->get();
        return $result;
    }

    static function getRekapLaporanPenjualanSupplier($req) {
        $query = DB::table('tb_paid_order AS ord')
                    ->leftJoin('tb_stock_master AS mst', 'ord.internal_code', '=', 'mst.internal_code')
                    ->leftJoin('tb_supplier AS sup', 'mst.supplier_code', '=', 'sup.supplier_code');
        if (isset($req->report_date)) {
            $query->where(DB::raw('DATE(ord.order_date)'), $req->report_date);
        }
        if (isset($req->supplier_code)) {
            $query->where('mst.supplier_code', $req->supplier_code);
        }
        $query->groupBy('mst.supplier_price')
            ->select('mst.supplier_price', DB::raw('SUM(ord.qty) AS qty_total'), 'mst.supplier_code');
        $result = $query->get();
        return $result;
    }

    static function getKategori() {
        $result = DB::table('tb_category')
                    ->orderBy('category_name', 'asc')
                    ->get();
        return $result;
    }

    static function addKategori($req) {
        $insert = DB::table('tb_category')
                    ->insert([
                        'category_name' => $req->category_name,
                    ]);
        return $insert;
    }

    static function deleteKategori($category_name) {
        $delete = DB::table('tb_category')
                    ->where('category_name', $category_name)
                    ->delete();
        return $delete;
    }
} 
