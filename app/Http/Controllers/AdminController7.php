<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminModel;
use App\Models\SupplierModel;
use App\Models\ResellerModel;
use App\Models\SlideshowModel;
use Session;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;


class AdminController extends Controller
{

    function showDashboard() {
        if (empty(Session::get('username'))) { return redirect('login'); }

       


        $daily_sales = AdminModel::getDailySaless();
        //dd($daily_sales);
        $tanggal_string = null;
        $total_jual_string = null;
        $total_sold_string = 0;
        foreach ($daily_sales as $row) {
            if ($tanggal_string == null) {
                $tanggal_string = '"'.$row->tanggal.'"';
            } else {
                $tanggal_string = $tanggal_string.', "'.$row->tanggal.'"';
            }
            if ($total_jual_string == null) {
                $total_jual_string = $row->qty_sold;
            } else {
                $total_jual_string = $total_jual_string.', '.$row->qty_sold;
            }
            if ($total_sold_string == null) {
                $total_sold_string = $row->total_sold/100000;
            } else {
                $total_sold_string = $total_sold_string.', '.$row->total_sold/100000;
            }
        }
        $chart_data = [
            'tanggal' => $tanggal_string,
            'total_jual' => $total_jual_string,
            'total_sold' => $total_sold_string,
        ];
        
        $data = [
            // check AppServiceProvider for shared data
            'webvisitor' => AdminModel::getWebVisitAv(),
            'webvisitoryesterday' => AdminModel::getWebVisitAvYesterday(),
            'reguser' => AdminModel::getRegisteredReffer(),
            'reguseryesterday' => AdminModel::getRegisteredRefferYesterday(),
            'paymentrefer' => AdminModel::getPaymentReffer(),
            'chart_data_sales' => AdminModel::getMonthlySales(),
            'totalregistered' => AdminModel::getTotalCustomer(),
            'sales_all' => AdminModel::getAllSalesYTD(),
            'sales_yearly' => AdminModel::getYearlySalesYTD(),
            'sales_monthly' => AdminModel::getMonthlySalesYTD(),
            'sales_weekly' => AdminModel::getWeeklySalesYTD(),
            'sales_minustwo' => AdminModel::getDayMinusTwoYTD(),
            'sales_daybefore' => AdminModel::getDayBeforeSalesYTD(),
            'sales_yesterday' => AdminModel::getYesterdaySalesYTD(),
            'sales_daily' => AdminModel::getDailySalesYTD(),
            'chart' => (object) $chart_data,
            'valuation' => AdminModel::getValuationItems(),
        ];
        // dd($data);
        return view('dashboard')->with($data);
    }

    function login(Request $req) {
        $userdata = AdminModel::loginAttempt($req);
        if (isset($userdata)) {
            Session::put('username', $userdata->username);
            Session::put('fullname', $userdata->fullname);
            Session::put('access_level', $userdata->access_level);
            return redirect('dashboard');
        } else {
            return redirect('login');
        }
    }

    function logout() {
        Session::flush();
        Session::regenerate();
        return redirect('login');
    }

    function showSystemLog() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'system_log' => AdminModel::getSystemLog(),
        ];
        return view('system-log')->with($data);
    }

    function showSupplier() {
        if (empty(Session::get('username'))) { return redirect('login'); }
    	$data = [
    		'supplier' => AdminModel::getSupplierList(),
    	];
    	return view('supplier')->with($data);
    }

    function showSupplierAddForm() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        return view('supplier-add');
    }

    function submitSupplier(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::submitSupplier($req)) {
            AdminModel::addSystemLog('Add new supplier: '.$req->supplier_code.' - '.$req->supplier_name);
            return redirect('supplier')->with(['success' => 'Successfully Add Supplier']);
        } else {
            return redirect('supplier')->with(['error' => 'Failed To Add Supplier']);
        }
    }

    function showSupplierEditForm(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'supplier_data' => AdminModel::getSupplierById($req->id)
        ];
        return view('supplier-edit')->with($data);
    }

    function submitEditSupplier(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::submitEditSupplier($req)) {
            AdminModel::addSystemLog('Edit supplier: '.$req->supplier_code.' - '.$req->supplier_name);
            return redirect('supplier')->with(['success' => 'Successfully Edit Supplier']);
        } else {
            return redirect('supplier')->with(['error' => 'Failed To Edit Supplier']);
        }
    }

    function deleteSupplier(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if ((Session::get('access_level') != 'superuser') && (Session::get('access_level') != 'supervisor'))  { return abort(403, 'Unauthorized action.'); }
        if (AdminModel::deleteSupplier($req)) {
            $supplier_data = AdminModel::getSupplierById($req->id);
            AdminModel::addSystemLog('Delete supplier: '.$supplier_data->supplier_code.' - '.$supplier_data->supplier_name);
            return redirect('supplier')->with(['success' => 'Successfully Delete Supplier']);
        } else {
            return redirect('supplier')->with(['error' => 'Failed To Delete Supplier']);
        }
    }

    function showStockInForm() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $master_data = AdminModel::getMasterData();
        $master_image = [];
        foreach ($master_data as $row) {
            $master_image[$row->external_code] = $row->master_image;
        }
        $data = [
            'supplier' => AdminModel::getSupplierList(),
            'category' => AdminModel::getKategori(),
            'master_image' => $master_image,
        ];
        return view('stock-in')->with($data);
    }

    function showStockReportPaginated() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStock(),
            'master' => AdminModel::getMasterDataPaginated(),
        ];
        return view('stock-report-paginated')->with($data);
    }
    function showStockReportPaginatedSearch(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStockSearch($req->search_query),
            'master' => AdminModel::getMasterDataPaginated($req->search_query),
        ];
        return view('stock-report-paginated')->with($data);
    }

    function showStockReport() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStock(),
            'master' => $this->getMasterDataArray(),
        ];
        return view('stock-report')->with($data);
    }

    function showStockReportBrandedInStock() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStockBrandedInStock(),
            'master' => $this->getMasterDataArray(),
        ];
        return view('stock-report')->with($data);
    }

    function showStockReportNonBranded() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStockNonBranded(),
            'master' => $this->getMasterDataArray(),
        ];
        return view('stock-report')->with($data);
    }


    function showStockReportld() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStockLD(),
            'master' => $this->getMasterDataArray(),
        ];
        return view('stock-report')->with($data);
    }
    function showStockReportav() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStockav(),
            'master' => $this->getMasterDataArray(),
        ];
        return view('stock-report')->with($data);
    }
    function showStockReportemp() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStockemp(),
            'master' => $this->getMasterDataArray(),
        ];
        return view('stock-report')->with($data);
    }

    function showNoPhotoVariant() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getNoPhotoVariant(),
            'master' => $this->getMasterDataArray(),
        ];
        return view('stock-nophotovariant')->with($data);
    }


    // function submitStockIn(Request $req) {
    //     $stock_query = false;
    //     $path = null;
    //     if ($req->hasFile('product_photo')) {
    //         $path = $req->file('product_photo')->store('public/products');
    //     }

    //     if (count(AdminModel::getProductByParameter($req->internal_code, $req->variant, $req->size)) > 0) {
    //         $stock_query = AdminModel::updateStockProduct($req, $path);
    //         AdminModel::addSystemLog('Update stock product '.strtoupper($req->internal_code).' / '.ucwords(strtolower($req->variant)).' / '.$req->size);
    //     } else {
    //         $stock_query = AdminModel::newStockProduct($req, $path);
    //         AdminModel::addSystemLog('Add new product '.$req->internal_code.' / '.$req->variant.' / '.$req->size);
    //     }

    //     if ($stock_query) {
    //         return redirect('stock-report')->with(['success' => 'Successfully Add Stock']);
    //     } else {
    //         return redirect('stock-report')->with(['error' => 'Failed To Add Stock']);
    //     }
    // }

    function showProducts() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'products' => AdminModel::getProducts(),
            'variants' => AdminModel::getAllVariants(),
            'product_code' => AdminModel::getAllProductsCode(),
        ];
        return view('products-view')->with($data);
    }

    function ajaxSingleProduct(Request $req) {
        $result = AdminModel::getProductByInternalCode($req->internal_code);
        echo json_encode($result);
    }

    function ajaxMasterProduct(Request $req) {
        $result = AdminModel::getMasterDataByInternalCode($req->internal_code);
        echo json_encode($result);
    }

    function ajaxSingleProductDetail(Request $req) {
        $result = AdminModel::getProductByInternalCodeDetail($req->internal_code, $req->variant, $req->size);
        echo json_encode($result);
    }

    function ajaxGenerateCode(Request $req) {
        $lastid = AdminModel::getLastInternalCodeBySupplier($req->supplier_code);
        
        if (!empty($lastid)) {
            $number = (int) str_replace($req->supplier_code, '', $lastid->internal_code);
            if($lastid->total>=$number){
                $number =$lastid->total;
            }
            $number++;
        } else {
            $number = 1;
        }
        $newid = $req->supplier_code.$number;
        echo $newid;
    }

    function showAddProductsForm() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'supplier' => AdminModel::getSupplierList(),
        ];
        return view('products-add')->with($data);
    }

    function pinStockReport(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::pinStockReport($req)) {
            return redirect('stock-report');
        }
    }

    function postAjaxSaveProduct(Request $req) {
        $path = null;
        if ($req->hasFile('product_photo')) {
            $path = $req->file('product_photo')->store('public/products');
        } else {
            $path = '';
        }
        
        if (!empty(AdminModel::getProductByParameter($req->internal_code, $req->variant, $req->size))) {
            $stock_query = AdminModel::updateStockProduct($req, $path);
            AdminModel::insertMasterData($req);
            AdminModel::addSystemLog('Update stock product '.strtoupper($req->internal_code).' / '.ucwords(strtolower($req->variant)).' / '.$req->size);
            
                echo "Stock updated for ".$req->product_name." ".$req->variant." / ".$req->size;
        } else {
            $stock_query = AdminModel::newStockProduct($req, $path);
            AdminModel::insertMasterData($req);
            AdminModel::addSystemLog('Add new product '.$req->internal_code.' / '.$req->variant.' / '.$req->size);
                echo "New Product Saved ".$req->product_name." ".$req->variant." / ".$req->size;
        }
    }

    // function ajaxSaveProduct(Request $req) {
    //     $param = explode('_', $req->param);
    //     $data = (object) [
    //         'internal_code' => strtoupper($param[0]),
    //         'variant' => ucwords(strtolower($param[1])),
    //         'size' => $param[2],
    //         'qty_in' => (int) $param[3],
    //         'supplier_price' => (int) $param[4],
    //         'selling_price' => (int) $param[5],
    //         'supplier_code' => $param[6],
    //         'external_code' => $param[7],
    //         'product_name' => $param[8],
    //     ];
    //     // if product exist
    //     if (count(AdminModel::getProductByParameter($data->internal_code, $data->variant, $data->size)) > 0) {
    //         // update stock
    //         if (AdminModel::updateStockProduct($data, null)) {
    //             AdminModel::addSystemLog('Update stock product '.strtoupper($req->internal_code).' / '.ucwords(strtolower($req->variant)).' / '.$req->size);
    //             echo "Stock updated for ".$data->product_name." ".$data->variant." / ".$data->size;
    //         } else {
    //             echo "Failed to Update Stock";
    //         }
    //     } else {
    //         // add variant
    //         if (AdminModel::newStockProduct($data, null)) {
    //             AdminModel::addSystemLog('Add new product '.$req->internal_code.' / '.$req->variant.' / '.$req->size);
    //             echo "New Product Saved ".$data->product_name." ".$data->variant." / ".$data->size;
    //         } else {
    //             echo "Failed to Add Product";
    //         }
    //     }
    // }

    function printSticker(Request $req) {
        $data = [
            'param' => $req->param,
        ];
        return view('print-sticker')->with($data);
    }

    function showSellingForm() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'batch_no' => (int) AdminModel::get_last_batch() + 1,
        ];
        return view('selling-form')->with($data);
    }

    function processSelling(Request $req) {
        $last_batch = AdminModel::get_last_batch();
        $last_batch++;
        $array_data = array();
        for ($i=0; $i < count($req->instagram); $i++) { 
            $line = [
                    'instagram' => $req->instagram[$i],
                    'internal_code' => $req->internal_code[$i],
                    'qty' => $req->qty[$i],
                    'size' => strtoupper($req->size[$i]),
                    'variant' => strtoupper($req->variant[$i]),
                    'tanggal' => $req->tanggal[$i],
                    'comment' => $req->comment[$i],
                    'batch_no' => $last_batch
                ];
            array_push($array_data, $line);
        }

        if (AdminModel::submitSelling($array_data)) {
            AdminModel::addSystemLog('Submit order batch no '.$last_batch);
            return redirect('selling/order/'.$last_batch);
        } else {
            return redirect('selling')->with(['error' => 'Failed To Save Data']);
        }
    }

    function showCompareStock(Request $req) {
        $data = [
            'order' => AdminModel::summaryOrder($req->batch_no),
            'stock' => AdminModel::getAllStock(),
            'supplier' => AdminModel::getSupplierList(),
            'master' => $this->getMasterDataArray(),
        ];
        return view('compare-stock')->with($data);
    }

    function showSellingReport() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'order' => AdminModel::getSellingReportBatch(),
        ];
        return view('selling-report')->with($data);
    }

    function showSellingOrder(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'stock' => AdminModel::getAllStock(),
            'order' => AdminModel::getSellingOrderByBatch($req->batch_no),
        ];
        return view('selling-order')->with($data);
    }

    function deleteListOrder(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::deleteListOrder($req->list_id)) {
            AdminModel::addSystemLog('Delete list order '.$req->list_id);
            return back();
        }
    }

    function showPaymentReport() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'report' => AdminModel::getPayment(),
        ];
        return view('payment-report')->with($data);
    }

    function printShippingLabel(Request $req) {
        $customer=null;
        if(AdminModel::getPaymentById($req->param)->shipto=='Customer'){
            $customer=AdminModel::getReseller(AdminModel::getPaymentById($req->param)->instagram);
        }
        
        $data = [
            'shipping' => AdminModel::getPaymentById($req->param),
            'item' => AdminModel::getOrderByPaymentId($req->param),
            'customer' => $customer,
        ];
        //dd($data);
        
        //dd($req);
        if(AdminModel::getPaymentById($req->param)->shipping_type==null || AdminModel::getPaymentById($req->param)->shipping_type==''){
            return view('print-shipping')->with($data);
        } else {
            
        }
        
    }

    function ajaxCheckExternal(Request $req) {
        $data = AdminModel::getProductByExternalCode($req->external_code);
        echo json_encode($data);
    }

    function ajaxCheckTelp(Request $req) {
        $data = AdminModel::getIGByPhone($req->phone);
        echo json_encode($data);
    }

    function ajaxCheckig(Request $req) {
        $data = AdminModel::getPhoneByIG($req->username);
        echo json_encode($data);
    }

    function deleteStock(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $datastock = AdminModel::getProductById($req->id);
        if (AdminModel::deleteStock($req->id)) {
            AdminModel::addSystemLog('Delete stock '.$datastock->internal_code.' / '.$datastock->variant.' / '.$datastock->size.' (Qty: '.$datastock->qty.')');
            return redirect('stock-report');
        }
    }

    function editStock(Request $req) {
        
        $supplier = AdminModel::getSupplierList();
        $supplier_list = [];
        foreach ($supplier as $row) {
            $supplier_list[$row->supplier_code] = $row->supplier_name;
        }
        $product_data = AdminModel::getProductById($req->id);
        $master_data = AdminModel::getMasterImage(strtoupper($product_data->external_code));
        $data = [
            'stock' => $product_data,
            'supplier' => $supplier_list,
            'category' => AdminModel::getKategori(),
            'master_data' => $master_data,
        ];
        //dd($data);
        return view('stock-edit')->with($data);
    }

    function editStockKode(Request $req) {
        $supplier = AdminModel::getSupplierList();
        $supplier_list = [];
        foreach ($supplier as $row) {
            $supplier_list[$row->supplier_code] = $row->supplier_name;
        }
        $product_data = AdminModel::getProductByIdIntCode($req->internal_code);
        $master_data = AdminModel::getMasterImage(strtoupper($product_data->external_code));
        $data = [
            'stock' => $product_data,
            'supplier' => $supplier_list,
            'category' => AdminModel::getKategori(),
            'master_data' => $master_data,
        ];
        return view('stock-edit')->with($data);
    }

    function saveEditStock(Request $req) {
        //dd($req);
        $path = null;
        if ($req->hasFile('product_photo')) {
            $path = $req->file('product_photo')->store('public/products');
        }
        $updatemaster = AdminModel::insertMasterData($req);
        $updatechild = AdminModel::updateStockProduct($req, $path);
        if ($updatemaster || $updatechild) {
            AdminModel::addSystemLog('Update stock product '.strtoupper($req->internal_code).' / '.ucwords(strtolower($req->variant)).' / '.$req->size);
            return redirect('stock-report')->with(['success' => 'Berhasil Menyimpan Data']);
        } else {
            return redirect('stock-report')->with(['error' => 'Gagal Menyimpan Data']);
        }
    }

    function postAjaxMasterImage(Request $req) {
        $path = null;
        if ($req->hasFile('master_image')) {
            try {
                
                $image = $req->file('master_image');
                $imageName = $image->getClientOriginalName();
                $path = 'public/products/'.md5(time().rand(0,1000)).'.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();            
                });
                $img->insert(storage_path('app/public/watermark.png'), 'top-right', 10, 10);
                $img->encode('jpg', 75);
                $img->stream();
                Storage::disk('local')->put($path, $img, 'public');

            } catch (Exception $e) {
                echo $e->message();
            }
        }
        if (AdminModel::updateMasterImage($req, $path)) {
            AdminModel::addSystemLog('Update photo master external code : '.strtoupper($req->external_code));
            echo $path;
        } else {
            echo "Gagal Menyimpan Foto Master";
        }
    }

    function postAjaxSelling(Request $req) {
        if (AdminModel::saveSingleSelling($req)) {
            echo "ok";
        }
    }

    function showListPendingOrder() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'list_order' => AdminModel::getListPendingOrder(),
            'page_status' => 'new',
        ];
        return view('list-new-order')->with($data);
    }

    function showListPendingOrderReseller() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'list_order' => AdminModel::getListPendingOrderReseller(),
            'page_status' => 'new',
        ];
        return view('list-new-order')->with($data);
    }

    function showListCustomer() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'list_order' => AdminModel::getListCustomer(),
            'page_status' => 'new',
        ];
        //dd($data);
        return view('customer-list')->with($data);
    }

    function showListReseller() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'list_order' => AdminModel::getListReseller(),
            'page_status' => 'new',
        ];
        //dd($data);
        return view('reseller-list')->with($data);
    }

    public function showRegisterdpForm(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'page_title' => "EditData",
            'datauser' => AdminModel::getResellerDataByInstagram($req->username),
        ];
        return view('reseller-edit')->with($data);
    }


    public function editResellerUser(Request $req) {

       
            
        if (ResellerModel::editUser($req)) {
            
            return redirect('reseller-list')->with(['success' => 'Berhasil merubah data']);
        } else {
            return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
        }
    
}

public function editResellerAddress(Request $req) {

   
        
    if (ResellerModel::editAddress($req)) {
        
        return redirect('reseller-list')->with(['success' => 'Berhasil merubah data']);
    } else {
        return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
    }

}

public function editResellerPassword(Request $req) {

   
if(!empty($req->ig) && !empty($req->password)){
    if (ResellerModel::editPassword($req)) {
        
        return redirect('reseller-list')->with(['success' => 'Berhasil merubah data']);
    } else {
        return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
    }
}
}

    function showCustomerBlast(Request $req) {
        //dd($req);
        if (empty(Session::get('username'))) { return redirect('login'); }

        if($req->pilihan=="Vincent"){
            $datakirim= AdminModel::getListCustomerVincent();
        }
        if($req->pilihan=="Paid Customer with Phone & Email"){
            $datakirim= AdminModel::getListCustomerBlastPaidPE();
        }
        if($req->pilihan=="Paid Customer with Email"){
            $datakirim= AdminModel::getListCustomerBlastPaidE();
        }
        if($req->pilihan=="Paid Customer with Phone"){
            $datakirim= AdminModel::getListCustomerBlastPaidP();
        }
       
        if($req->pilihan=="Customer No Order with Phone & Email"){
            $datakirim= AdminModel::getListCustomerBlastNPaidPE();
        }
        if($req->pilihan=="Customer No Order with Email"){
            $datakirim= AdminModel::getListCustomerBlastNPaidE();
        }
        if($req->pilihan=="Customer No Order with Phone"){
            $datakirim= AdminModel::getListCustomerBlastNPaidP();
        }
        if($req->pilihan=="WA OTO Registered ALL"){
            $datakirim= AdminModel::getListCustomerBlastWOAll();
        }
        if($req->pilihan=="WA OTO Registered Paid"){
            $datakirim= AdminModel::getListCustomerBlastWOPaid();
        }
        if($req->pilihan=="WA OTO Registered No Transaction"){
            $datakirim= AdminModel::getListCustomerBlastWONPaid();
        }
        //dd($datakirim);
        $data = [
            'pilihan' => $req->pilihan,
            'list_order' => $datakirim,
            'page_status' => 'new',
        ];
        //dd($data);
        return view('customerblast')->with($data);
        
    
    }

    function sendCustomerBlast(Request $req) {
        //dd($req->pilihan);
        //dd("Vincent");
        if (empty(Session::get('username'))) { return redirect('login'); }

        if($req->pilihan=="Vincent"){
            $datakirims= AdminModel::getListCustomerVincent();
            
        }

        if($req->pilihan=="Paid Customer with Phone & Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidPE();
        }
        if($req->pilihan=="Paid Customer with Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidE();
        }
        if($req->pilihan=="Paid Customer with Phone"){
            $datakirims= AdminModel::getListCustomerBlastPaidP();
        }
       
        if($req->pilihan=="Customer No Order with Phone & Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidPE();
        }
        if($req->pilihan=="Customer No Order with Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidE();
        }
        if($req->pilihan=="Customer No Order with Phone"){
            $datakirims= AdminModel::getListCustomerBlastPaidP();
        }
        // dd($datakirims);
        



        








        if($req->command=="Kirim WA"){
            foreach ($datakirims as $row){
                
          
                $this->sendAWBWhatsappManual($row->phone,$req->pesanwa);
            }
    
        }
        
        if($req->command=="Kirim Email"){
            foreach ($datakirims as $row){
                
                $this->sendEmailManual($row->email, $req->subjectemail, $req->pesanemail);
            }
        }

       
        //dd($data);
        
        return redirect('customerselect')->with(['success' => 'Berhasil Menjalankan Data']);
    }
    

    function showEditCustomer($username) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'paid_order' => AdminModel::getPaidOrderByInstagram($username),
            'addresses' => AdminModel::getShippingAddress($username),
            'userdata' => AdminModel::getCustomerDataByInstagram($username),
            'page_title' => "Akun Saya",
        ];
        return view('customer-edit')->with($data);
    }

    function submitResetPassword(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::submitResetPassword($req)) {
           
            return redirect('customer-list');
        } else {
            return redirect('/')->with(['error' => 'Gagal reset password']);
        }
    }

    function showListHitAndRunOrder() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'list_order' => AdminModel::getListHitAndRunOrder(),
            'page_status' => 'hit and run',
            
        ];
        return view('list-new-order')->with($data);
    }

    function showListPaidOrder() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        file_get_contents(url('auto-recheck-payment'));
        $data = [
            'list_order' => AdminModel::getListPaidOrder(),
            'refund' => AdminModel::getRefundList(),
        ];
        return view('list-order')->with($data);
    }

    function showListUnpaidOrder() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        file_get_contents(url('auto-recheck-payment'));
        $data = [
            'list_order' => AdminModel::getListUnpaidOrder(),
        ];
        return view('list-order')->with($data);
    }

    function submitAddCustomerAddress(Request $req) {
        if (AdminModel::insertCustomerAddress($req)) {
            return redirect($req->callbackURL)->with(['success' => 'Berhasil Menambah Alamat']);
        } else {
            return redirect($req->callbackURL)->with(['error' => 'Gagal Menambah Alamat']);
        }
    }

    function deleteCustomerAddresss(Request $req) {
        if (AdminModel::deleteCustomerAddress($req->id)) {
            return redirect('list-order/new')->with(['success' => 'Berhasil Menghapus Alamat']);
        } else {
            return redirect('list-order/new')->with(['error' => 'Gagal Menghapus Alamat']);
        }
    }

    function showListDeliveredOrder() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'list_order' => AdminModel::getListDeliveredOrder(),
            'refund' => AdminModel::getRefundList(),
        ];
        return view('list-order')->with($data);
    }

    function showPersonalOrder(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'item_order' => AdminModel::getOrderByParameter($req->instagram, $req->date),
            'stock' => AdminModel::getAllStock(),
            'product_main' => AdminModel::getProductMainData(),
            
            'userdata' => AdminModel::getCustomer($req->instagram),
            'instagram' => $req->instagram,
            'tanggal' => $req->date,
        ];
        return view('list-order-item')->with($data);
    }

    function showPaidOrderDetail(Request $req) {
        
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'item_order' => AdminModel::getPaidOrderByPaymentID($req->order_id),
            'stock' => AdminModel::getAllStock(),
            'product_main' => AdminModel::getProductMainData(),
            'order_id' => $req->order_id,
            'payment' => AdminModel::getPaymentById($req->order_id),
        ];
        return view('list-paid-item')->with($data);
    }

    function saveAWB(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::saveAWB($req)) {
            if ($this->sendAWBWhatsapp($req->payment_id))
            if ($this->sendAWBEmail($req->payment_id))
            
            AdminModel::addSystemLog('Save AWB for payment ID '.$req->payment_id.' / AWB No. '.$req->awb);
            return redirect('list-order/paid')->with(['success' => 'Berhasil Menyimpan Data']);
        } else {
            return redirect('list-order/paid')->with(['error' => 'Gagal Menyimpan Data']);
        }
    }

    function deleteDeliveredOrd(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::deleteDelivdOrd($req)) {
            
            return redirect('list-order/delivered')->with(['success' => 'Berhasil Menyimpan Data']);
        } else {
            return redirect('list-order/delivered')->with(['error' => 'Gagal Menyimpan Data']);
        }
    }

    function testEmail() {
        $this->sendAWBEmail('1673278570');
    }

    function sendAWBEmail($payment_id) {
        $userdata = AdminModel::getCustomerDataFromPaymentID($payment_id);
        if (isset($userdata)) {
          if (isset($userdata->email)){
            $data = [
                'data' => AdminModel::getPaymentById($payment_id),
            ];
            \Mail::send('emails.order-shipped', $data, function ($message) use ($userdata) {
                $message->from('info@momavel.id', 'MomAvel.id');
                $message->to($userdata->email);
                $message->subject('Pesanan Anda Telah Dikirim');
            });
          }
        }
    }

    function sendEmailManual($email, $subject, $message) {
        
          if (isset($email) && isset($subject) && isset($message)){
            



            $to_name = $email;
            $to_email = $email;
            $data = [
                'name' => $email,
                'body' => $message,
            ];
              
            \Mail::send('emails.manual-email', $data, function($message) use ($to_name, $to_email, $subject) {
                $message->to($to_email, $to_name);
                $message->subject($subject);
                $message->from('info@momavel.id', 'MomAvel.id');
            });








          }
        
    }

    function sendAWBWhatsappManual($number,$message) {
        
        $data = [
            'number' => $number,
            'message' => $message,
        ];

        $curl = curl_init("https://wa-api.momavel.id/send-message");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT , 1);
        $send_header = array('Content-Type: application/json');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

        $response = curl_exec($curl);
        curl_close($curl);
    }


    function sendemailmedia($req) {
        
        //dd(url('public'). Storage::url(AdminModel::getMarketingStokData()->images));
        //dd( 'https://momavel.id'.'/public'. Storage::url(AdminModel::getMarketingStokData()->images));
        //dd(url('/selling/show-paid/'));

//dd($req->pilihan);
        //dd("Vincent");
        if (empty(Session::get('username'))) { return redirect('login'); }

        if($req=="Vincent"){
            $datakirims= AdminModel::getListCustomerVincent();
            
        }

        if($req=="Paid Customer with Phone & Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidPE();
        }
        if($req=="Paid Customer with Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidE();
        }
        if($req=="Paid Customer with Phone"){
            $datakirims= AdminModel::getListCustomerBlastPaidP();
        }
       
        if($req=="Customer No Order with Phone & Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidPE();
        }
        if($req=="Customer No Order with Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidE();
        }
        if($req=="Customer No Order with Phone"){
            $datakirims= AdminModel::getListCustomerBlastPaidP();
        }
        // dd($datakirims);
        



        








       
            //foreach ($datakirims as $row){
                
          
                
            //    $this->sendWhatsappImage($row->phone, $kirim,'https://momavel.id'.'/public'. Storage::url($product->images));
            //    //$this->sendAWBWhatsappManual($row->phone,$req->pesanwa);
            //}
    
            
                foreach ($datakirims as $row){
                    
                    $product = AdminModel::getMarketingStokData();

                    $kirim = '<img src = "'  . 'https://momavel.id'.'/public'. Storage::url($product->images) . '" width="500"> <br>' .  '<b>Hanya ' . number_format(intval($product->selling_price) * 0.9) .' <s>' . number_format($product->selling_price) . '</s></b><br>' . $product->internal_code . ' - ' . $product->product_name . '<br><i>'. $product->variant . ' - ' . $product->size . '</i><br>' .  $product->keterangan .  '<br><br>Beli Sekarang Klik Link Disini :<br>' . 'https://momavel.id/wa/beli/' . $product->internal_code . '<br><br>MomAvel bertekad memberikan pelayanan terbaik bagi pelanggan dengan menyediakan produk berkualitas dengan harga terjangkau. Kami memberikan diskon dan gratis ongkir apabila melakukan self checkout. Syarat dan ketentuan berlaku. Kami sudah support QRIS sehingga anda bs membayar menggunakan BCA / Mandiri / Gopay / Shopeepay dll<br><br>Apabila anda tidak ingin menerima pesan berisi informasi produk diskon seperti ini anda bisa klik link di bawah ini untuk di keluarkan dari list:<br>https://momavel.id/user/unsub/' . base64_encode($row->username) ;
                    //dd($kirim);
                    $this->sendEmailManual($row->email, 'Produk Sale Momavel', $kirim);
                }
            
        


       
        //dd($data);
        
        return redirect('customerselect')->with(['success' => 'Berhasil Menjalankan Data']);





        //$this->sendWhatsappImage('628119994888','test kirim',url('public'). Storage::url(AdminModel::getMarketingStokData()->images));
    }






    function sendwhatsappmedia($req) {
        
        //dd(url('public'). Storage::url(AdminModel::getMarketingStokData()->images));
        //dd( 'https://momavel.id'.'/public'. Storage::url(AdminModel::getMarketingStokData()->images));
        //dd(url('/selling/show-paid/'));

//dd($req->pilihan);
        //dd("Vincent");
        if (empty(Session::get('username'))) { return redirect('login'); }

        if($req=="Vincent"){
            $datakirims= AdminModel::getListCustomerVincent();
            
        }

        if($req=="Paid Customer with Phone & Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidPE();
        }
        if($req=="Paid Customer with Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidE();
        }
        if($req=="Paid Customer with Phone"){
            $datakirims= AdminModel::getListCustomerBlastPaidP();
        }
       
        if($req=="Customer No Order with Phone & Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidPE();
        }
        if($req=="Customer No Order with Email"){
            $datakirims= AdminModel::getListCustomerBlastPaidE();
        }
        if($req=="Customer No Order with Phone"){
            $datakirims= AdminModel::getListCustomerBlastPaidP();
        }
        // dd($datakirims);
        



        








       
            foreach ($datakirims as $row){
                
          
                $product = AdminModel::getMarketingStokData();

                $kirim = '*Hanya ' . number_format(intval($product->selling_price) * 0.9) .' ~' . number_format($product->selling_price) . '~*'.PHP_EOL . $product->internal_code . ' - ' . $product->product_name . PHP_EOL . '_'. $product->variant . ' - ' . $product->size . '_' . PHP_EOL . $product->keterangan . PHP_EOL . PHP_EOL. 'Beli Sekarang Klik Link Disini :' .PHP_EOL. 'https://momavel.id/wa/beli/' . $product->internal_code . PHP_EOL . PHP_EOL. 'MomAvel bertekad memberikan pelayanan terbaik bagi pelanggan dengan menyediakan produk berkualitas dengan harga terjangkau. Kami memberikan diskon dan gratis ongkir apabila melakukan self checkout. Syarat dan ketentuan berlaku. Kami sudah support QRIS sehingga anda bs membayar menggunakan BCA / Mandiri / Gopay / Shopeepay dll' .PHP_EOL. PHP_EOL. 'Apabila anda tidak ingin menerima pesan berisi informasi produk diskon seperti ini anda bisa klik link di bawah ini untuk di keluarkan dari list:' . PHP_EOL . 'https://momavel.id/user/unsub/' . base64_encode($row->username) ;

                $this->sendWhatsappImage($row->phone, $kirim,'https://momavel.id'.'/public'. Storage::url($product->images));
                //$this->sendAWBWhatsappManual($row->phone,$req->pesanwa);
            }
    
        
        


       
        //dd($data);
        
        return redirect('customerselect')->with(['success' => 'Berhasil Menjalankan Data']);





        //$this->sendWhatsappImage('628119994888','test kirim',url('public'). Storage::url(AdminModel::getMarketingStokData()->images));
    }

    function sendWhatsappImage($number, $caption, $file) {
        
        $data = [
            'number' => $number,
            'caption' => $caption,
            'file' => $file,
        ];
        //dd($data);
        $curl = curl_init("https://wa-api.momavel.id/send-media");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT , 1);
        $send_header = array('Content-Type: application/json');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

        $response = curl_exec($curl);
        curl_close($curl);
    }


    function sendAWBWhatsapp($payment_id) {
        $userdata = AdminModel::getCustomerDataFromPaymentID($payment_id);
        $data = [
            'number' => $userdata->phone,
            'message' => 'Salam dari MomAvel.id, Pesanan anda telah terkirim. Pengiriman kami menggunakan ID Express. Nomor Resi Pengiriman : '.$userdata->awb.'

Nomor Order : '.$payment_id.'

(MOHON UNTUK TIDAK MEMBALAS PESAN INI, Jika ada masalah bisa hub WA 0812 3844 3452)

Link Pengiriman:
https://idexpress.com/lacak-paket/?actionType=delivery&waybillNo='.$userdata->awb,
        ];

        $curl = curl_init("https://wa-api.momavel.id/send-message");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT , 1);
        $send_header = array('Content-Type: application/json');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

        $response = curl_exec($curl);
        curl_close($curl);
    }

    // function showManualPaymentForm(Request $req) {
    //     if (empty(Session::get('username'))) { return redirect('login'); }
    //     $data = [
    //         'cart' => AdminModel::getOrderByParameter($req->instagram, $req->tanggal),
    //         'stock' => AdminModel::getAllStock(),
    //     ];
    //     return view('manual-payment')->with($data);
    // }

    // function submitManualPayment(Request $req) {
    //     if (empty(Session::get('username'))) { return redirect('login'); }
        
    //     if (AdminModel::insertPayment($req) && AdminModel::updateStock($req)) {
    //         AdminModel::addSystemLog('Input manual payment for '.$req->payment_id.' / '.$req->instagram.' / '.$req->order_date);
    //         return redirect('list-order')->with(['success' => 'Berhasil menyimpan pembayaran']);
    //     } else {
    //         return redirect('list-order')->with(['error' => 'Terjadi kesalahan menyimpan']);
    //     }
    // }

    function getMasterDataArray() {
        $master_data = AdminModel::getMasterData();
        $master_array = [];
        foreach ($master_data as $row) {
            // $master_data[strtoupper($row->internal_code)]['supplier_code'] = $row->supplier_code;
            // $master_data[strtoupper($row->internal_code)]['external_code'] = $row->external_code;
            if (isset($row->internal_code)) {
                $master_array[strtoupper($row->internal_code)] = [
                        'supplier_code' => $row->supplier_code,
                        'external_code' => $row->external_code,
                        'product_name' => $row->product_name,
                        'category' => $row->category,
                        'master_image' => $row->master_image,
                        'supplier_price' => $row->supplier_price,
                        'diskon' => $row->diskon,
                        'branded' => $row->branded,
                        'register_time' => $row->register_time,
                        'hargaasli' => $row->hargaasli,
                        'selling_price' => $row->selling_price,
                ];
            }
        }
        return $master_array;
    }

    function checkOverallStock(Request $req) {
        $data = [
            'order' => AdminModel::getAllPendingOrder(),
            'stock' => AdminModel::getAllStock(),
            'supplier' => AdminModel::getSupplierList(),
            'master' => $this->getMasterDataArray(),
            'type' => $req->type,
        ];
        return view('compare-stock')->with($data);
    }

    function compareStockByProduct(Request $req) {
        $data = [
            'order' => AdminModel::getAllPendingOrderByProduct($req),
            'query' => [
                'internal_code' => $req->internal_code,
                'variant' => $req->variant,
                'size' => $req->size,
            ],
            'master' => AdminModel::getMasterDataByInternalCode($req->internal_code),
        ];
        return view('compare-stock-detail')->with($data);
    }

    function ajaxChangeOrder(Request $req) {
        if (AdminModel::changeOrder($req)) {
            echo "ok";
        } else {
            echo "bad";
        }
    }

    function ajaxGetVariant(Request $req) {
        $result = AdminModel::getVariant($req->internal_code);
        echo json_encode($result);
    }

    function ajaxGetSize(Request $req) {
        $result = AdminModel::getSize($req->internal_code, $req->variant);
        echo json_encode($result);
    }

    function submitManualSelling(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::saveSingleSelling($req)) {
            AdminModel::addSystemLog('Add order manual for '.$req->instagram.' '.$req->tanggal);
            return redirect($req->return_url)->with(['success' => 'Order Tersimpan']);
        } else {
            return redirect($req->return_url)->with(['error' => 'Gagal Menyimpan Order']);
        }
    }
    

    function showManualSellingForm(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
            $last_batch = AdminModel::get_last_batch();
        $data = [
            'batch_no' => $last_batch++,
            'product_main' => AdminModel::getProductMainData(),
            'product_mainSimple' => AdminModel::getProductMainDataSimple(),
        ];
        return view('selling-manual')->with($data);
    }

    function deleteBatchNo(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $selling_order = AdminModel::getSellingOrderByBatch($req->batch_no);
        if (AdminModel::deleteBatchNo($req->batch_no)) {
            AdminModel::addSystemLog('Delete selling order batch no '.$req->batch_no.' date '.$selling_order[0]->tanggal.' total '.count($selling_order).' order');
            return redirect('selling-report')->with(['success' => 'Berhasil menghapus data']);
        } else {
            return redirect('selling-report')->with(['error' => 'Gagal menghapus data']);
        }
    }

    function showUserManagement(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if ((Session::get('access_level') != 'superuser') && (Session::get('access_level') != 'supervisor'))  { return abort(403, 'Unauthorized action.'); }
        $data = [
            'user' => AdminModel::getAllUser(),
        ];
        return view('user-management')->with($data);
    }

    function submitUser(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if ((Session::get('access_level') != 'superuser') && (Session::get('access_level') != 'supervisor'))  { return abort(403, 'Unauthorized action.'); }
        if (AdminModel::submitUser($req)) {
            AdminModel::addSystemLog('Add new '.$req->access_level.' user '.$req->username);
            return redirect('user-management')->with(['success' => 'Berhasil menambah user']);
        } else {
            return redirect('user-management')->with(['error' => 'Gagal menambah user']);
        }
    }

    function deleteUser(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if ((Session::get('access_level') != 'superuser') && (Session::get('access_level') != 'supervisor'))  { return abort(403, 'Unauthorized action.'); }
        if (AdminModel::deleteUser($req->username)) {
            AdminModel::addSystemLog('Add new '.$req->access_level.' user '.$req->username);
            return redirect('user-management')->with(['success' => 'Berhasil menambah user']);
        } else {
            return redirect('user-management')->with(['error' => 'Gagal menambah user']);
        }
    }

    

    function showManualPaymentFormV2(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }

        
            $last_batch = AdminModel::get_last_batch();


        $data = [
            'cart' => AdminModel::getOrderByParameterV2($req->instagram, $req->tanggal),
            //'stock' => AdminModel::getAllStock(),
            'supplier' => AdminModel::getSupplierList(),
            'master' => $this->getMasterDataArray(),
            'custdata' => AdminModel::getCustomer($req->instagram),
            'custaddress' => AdminModel::getCustomerAddress($req->instagram),
            'batch_no' => $last_batch++,
            //'product_main' => AdminModel::getProductMainData(),
            'item_order' => AdminModel::getOrderByParameter($req->instagram, $req->tanggal),
            'product_mainSimple' => AdminModel::getProductMainDataSimple(),
            'stock' => AdminModel::getAllStock(),
            
            'userdata' => AdminModel::getCustomer($req->instagram),
            'instagram' => $req->instagram,
            'tanggal' => $req->date,
        ];
        return view('manual-payment-v2')->with($data);
    }

    function submitNamaAdd(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }

        
            $address_gen = "$req->nama_depan $req->nama_belakang \n $req->telepon / $req->email \n $req->address \n $req->subdistrict, $req->city, $req->province";

            if (empty(AdminModel::getCustomer($req->instagram))) {
                AdminModel::registerCustomerManually($req);
            } else {
                AdminModel::updateCustomerManually($req);
            }
            return "WOI";
         
    }

    function deleteAdd(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }

        
            AdminModel::deleteAddress($req->instagram);
            return $req->instagram;  
        
    }


    function submitManualPaymentV2(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }

        if (empty(AdminModel::getCustomer($req->instagram))) {
            AdminModel::registerCustomerManually($req);
        }
        $req->tanggal = $req->tglskrg;
        if (AdminModel::insertPayment($req)) {
            AdminModel::addSystemLog('Input manual payment for '.$req->payment_id.' / '.$req->instagram.' / '.$req->tanggal);
            for ($i=0; $i < count($req->internal_code); $i++) { 
                $item_data = [
                    'instagram' => $req->instagram,
                    'order_date' => $req->tanggal,
                    'payment_id' => $req->order_id,
                    'internal_code' => $req->internal_code[$i],
                    'variant' => $req->variant[$i],
                    'size' => $req->size[$i],
                    'selling_price' => $req->selling_price[$i],
                    'qty' => $req->qty[$i],
                    'comment' => $req->comment,
                ];
                AdminModel::reduceStock($item_data);
                AdminModel::insertPaidOrder($item_data);
                AdminModel::deletePendingOrder($item_data);
                
            }
            $this->sendAWBWhatsappManual($req->telepon,'Pembayaran anda sebesar ' . number_format($req->amount) . ' telah kami proses secara manual. Silahkan menunggu pesanan momavel.id anda. Pada umumnya resi akan di update di website dan juga di kirim secara otomatis menggunakan WA Otomatis. Apabila ada pertanyaan silahkan hubungi WA kami di 081283443452, Terima kasih atas kepercayaan telah berbelanja bersama kami. Sekarang anda bisa berbelanja melalui WA ini dengan mengetik -DAFTAR ');
            
            $sdks = (object) AdminModel::getDailySalesYTD();
            
            $this->sendWAToADMIN($req->instagram. ' ' . $req->nama_depan . ' ' . $req->nama_belakang . ' ' . $req->telepon . ' melakukan pembayaran manual sebesar : '.number_format($req->amount) . PHP_EOL . $req->comment . PHP_EOL. $req->reffer . PHP_EOL . 'Total Hari ini : ' . number_format($sdks->total));
            return redirect('list-order/paid')->with(['success' => 'Berhasil menyimpan pembayaran']);
        } else {
            return back()->with(['error' => 'Terjadi kesalahan menyimpan']);
        }
    }


    function sendWAToADMIN($Text){
        $ss = AdminModel::getAdminWa();
        //dd($ss);
        $this->sendAWBWhatsappManual($ss->wa1,$Text);
        $this->sendAWBWhatsappManual($ss->wa2,$Text);
        $this->sendAWBWhatsappManual($ss->wa3,$Text);
        $this->sendAWBWhatsappManual($ss->wa4,$Text);
        
    }


    function showSupplierBayar() {
        if (empty(Session::get('username'))) { return redirect('login'); }

        $SuppList = AdminModel::getSupplierLoginList();

        $Payment_Array = [];
        
        foreach ($SuppList as $row) {
            $kirim = (object) [
                'supplier1' => $row->supplier1,
                'supplier2' => $row->supplier2,
                'supplier3' => $row->supplier3,
                'supplier4' => $row->supplier4,
                'supplier5' => $row->supplier5,
                'supplier6' => $row->supplier6,
            ];
            
            $Payment_Array[$row->username]['kode'] = $row->supplier1 .' '.$row->supplier2 .' '.$row->supplier3 .' '.$row->supplier4 .' '.$row->supplier5 .' '.$row->supplier6;
            $Payment_Array[$row->username]['stok'] = SupplierModel::getRekapValuasi($kirim);
            $Payment_Array[$row->username]['penjualan'] = SupplierModel::getRekapSalesALLO($kirim);
            $Payment_Array[$row->username]['pembayaran'] = SupplierModel::getRekapPayment($kirim);
            $Payment_Array[$row->username]['nama'] = $row->nama;
            $Payment_Array[$row->username]['demodemode'] = $row->demodemode;
            

            //$Payment_Array[$row->username]
        }

    	$data = [
    		'supplier' => AdminModel::getSupplierLoginList(),
            'paymentarray' => $Payment_Array,
    	];
            //dd($data);
    	return view('supplierbayar')->with($data);
    }

    

    function showSalesAdmin(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $listsup = explode(' ', $req->koder . '      ');

        $sup1='-';
        $sup2='-';
        $sup3='-';
        $sup4='-';
        $sup5='-';
        $sup6='-';
        if ($listsup[0]) { $sup1=strtoupper($listsup[0]);}
        if ($listsup[1]) { $sup2=strtoupper($listsup[1]);}
        if ($listsup[2]) { $sup3=strtoupper($listsup[2]);}
        if ($listsup[3]) { $sup4=strtoupper($listsup[3]);}
        if ($listsup[4]) { $sup5=strtoupper($listsup[4]);}
        if ($listsup[5]) { $sup6=strtoupper($listsup[5]);}


        $kirim = (object) [
            'supplier1' => $sup1,
            'supplier2' => $sup2,
            'supplier3' => $sup3,
            'supplier4' => $sup4,
            'supplier5' => $sup5,
            'supplier6' => $sup6,
            'tanggaldr' => $req->tanggaldr,
            'tanggalsmp' => $req->tanggalsmp,
        ];

        $data = [
            'kode' => $sup1 . ' '.$sup2 . ' '.$sup3 . ' '.$sup4 . ' '.$sup5 . ' '.$sup6 ,
            'periode' => 'Periode Tanggal : ' . $req->tanggaldr .' s/d ' . $req->tanggalsmp,
            'sales' => SupplierModel::getSales($kirim),
            'pembayaran' => SupplierModel::getRekapPayment($kirim),
            'salesbyprice' => SupplierModel::getRekapBySupPrice($kirim),
            'norek' => AdminModel::getNorekBySupp($sup1),
            'valuasi' => SupplierModel::getRekapSales($kirim)
        ];
        //dd($data);
        return view('supsales')->with($data);
    }


    function showRekapStok(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        
        $listsup = explode(' ', $req->suppliers . '      ');

        $sup1='-';
        $sup2='-';
        $sup3='-';
        $sup4='-';
        $sup5='-';
        $sup6='-';
        if ($listsup[0]) { $sup1=strtoupper($listsup[0]);}
        if ($listsup[1]) { $sup2=strtoupper($listsup[1]);}
        if ($listsup[2]) { $sup3=strtoupper($listsup[2]);}
        if ($listsup[3]) { $sup4=strtoupper($listsup[3]);}
        if ($listsup[4]) { $sup5=strtoupper($listsup[4]);}
        if ($listsup[5]) { $sup6=strtoupper($listsup[5]);}


        $kirim = (object) [
            'supplier1' => $sup1,
            'supplier2' => $sup2,
            'supplier3' => $sup3,
            'supplier4' => $sup4,
            'supplier5' => $sup5,
            'supplier6' => $sup6,
        ];

        $data = [
            'kode' => $sup1 . ' '.$sup2 . ' '.$sup3 . ' '.$sup4 . ' '.$sup5 . ' '.$sup6 ,
            
            'supplier' => SupplierModel::getStockHariIni($kirim),
            'valuasi' => SupplierModel::getRekapValuasi($kirim),
            'penjualan' => SupplierModel::getRekapSalesALLO($kirim),
            'pembayaran' => SupplierModel::getRekapPayment($kirim),
        ];
        
        return view('rekapstokadmin')->with($data);
    }



    function showSupplierLogin() {
        if (empty(Session::get('username'))) { return redirect('login'); }
    	$data = [
    		'supplier' => AdminModel::getSupplierLoginList(),
    	];
    	return view('supplierlogin')->with($data);
    }


    
    function showSupplierSales($req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        
        $listsup = explode(' ', strtoupper($req) . '      ');
        
        $sup1='-';
        $sup2='-';
        $sup3='-';
        $sup4='-';
        $sup5='-';
        $sup6='-';
        if ($listsup[0]) { $sup1=strtoupper($listsup[0]);}
        if ($listsup[1]) { $sup2=strtoupper($listsup[1]);}
        if ($listsup[2]) { $sup3=strtoupper($listsup[2]);}
        if ($listsup[3]) { $sup4=strtoupper($listsup[3]);}
        if ($listsup[4]) { $sup5=strtoupper($listsup[4]);}
        if ($listsup[5]) { $sup6=strtoupper($listsup[5]);}


        $kirim = (object) [
            'supplier1' => $sup1,
            'supplier2' => $sup2,
            'supplier3' => $sup3,
            'supplier4' => $sup4,
            'supplier5' => $sup5,
            'supplier6' => $sup6,
        ];

        


    	$data = [
            'kode' => $sup1 . ' '.$sup2 . ' '.$sup3 . ' '.$sup4 . ' '.$sup5 . ' '.$sup6 ,
    		'sales' => AdminModel::getSupplierSales($kirim),
            'pembayaran' => SupplierModel::getRekapPayment($kirim),
    	];

        //dd($data);
    	return view('suppliersales')->with($data);
    }


    function showSupplierStok($req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        
        $listsup = explode(' ', strtoupper($req) . '      ');
        
        $sup1='-';
        $sup2='-';
        $sup3='-';
        $sup4='-';
        $sup5='-';
        $sup6='-';
        if ($listsup[0]) { $sup1=strtoupper($listsup[0]);}
        if ($listsup[1]) { $sup2=strtoupper($listsup[1]);}
        if ($listsup[2]) { $sup3=strtoupper($listsup[2]);}
        if ($listsup[3]) { $sup4=strtoupper($listsup[3]);}
        if ($listsup[4]) { $sup5=strtoupper($listsup[4]);}
        if ($listsup[5]) { $sup6=strtoupper($listsup[5]);}


        $kirim = (object) [
            'supplier1' => $sup1,
            'supplier2' => $sup2,
            'supplier3' => $sup3,
            'supplier4' => $sup4,
            'supplier5' => $sup5,
            'supplier6' => $sup6,
        ];

        



        $data = [
            'kode' => $sup1 . ' '.$sup2 . ' '.$sup3 . ' '.$sup4 . ' '.$sup5 . ' '.$sup6 ,
            'supplier' => SupplierModel::getStockHariIni($kirim),
            'stokbysupcode' => SupplierModel::getRekapValuasiBySupCode($kirim),
            'valuasi' => SupplierModel::getRekapValuasi($kirim),
        ];

        //dd($data);
        return view('supplierstockadmin')->with($data);
    }


    function showSelectSales() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        return view('customerselect');
    }

    

    function showSupplierLoginAddForm() {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'supplier' => AdminModel::getSupplierList(),
        ];
        return view('supplierlogin-add')->with($data);;
    }

    




    function submitSupplierLogin(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::submitSupplierLogin($req)) {
            AdminModel::addSystemLog('Add new supplier login: '.$req->supplier_code.' - '.$req->supplier_name);
            return redirect('supplierlogin')->with(['success' => 'Successfully Add Supplier']);
        } else {
            return redirect('supplierlogin')->with(['error' => 'Failed To Add Supplier']);
        }
    }

    function showCustomerWA(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'idd' =>$req->id,
            'supplier' => AdminModel::getSupplierList(),
        ];


        return view('supplierlogin-edit')->with($data);
    }

   
    function showSupplierLoginEditloginForm(Request $req) {
        
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'supplier_data' => AdminModel::getSupplierLoginByUsername($req->id),
            'supplier' => AdminModel::getSupplierList(),
        ];


        return view('supplierlogin-edit')->with($data);
    }



    function submitEditSupplierLogin(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if (AdminModel::submitEditSupplierLogin($req)) {
            AdminModel::addSystemLog('Edit supplier login: '.$req->supplier_code.' - '.$req->supplier_name);
            return redirect('supplierlogin')->with(['success' => 'Successfully Edit Supplier']);
        } else {
            return redirect('supplierlogin')->with(['error' => 'Failed To Edit Supplier']);
        }
    }

    function deleteSupplierLogin(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        if ((Session::get('access_level') != 'superuser') && (Session::get('access_level') != 'supervisor'))  { return abort(403, 'Unauthorized action.'); }
        if (AdminModel::deleteSupplierLogin($req)) {
            $supplier_data = AdminModel::getSupplierLoginById($req->id);
            
            return redirect('supplierlogin')->with(['success' => 'Successfully Delete Supplier']);
        } else {
            return redirect('supplierlogin')->with(['error' => 'Failed To Delete Supplier']);
        }
    }









    function showSupplierPayment() {
        if (empty(Session::get('username'))) { return redirect('Payment'); }
    	$data = [
    		'supplier' => AdminModel::getSupplierPaymentList(),
    	];
    	return view('supplierPayment')->with($data);
    }

    function showSupplierPaymentAddForm() {
        if (empty(Session::get('username'))) { return redirect('Payment'); }
        $data = [
            'supplier' => AdminModel::getSupplierList(),
        ];
        
        return view('supplierPayment-add')->with($data);
    }

    




    function submitSupplierPayment(Request $req) {
        if (empty(Session::get('username'))) { return redirect('Payment'); }
        if (AdminModel::submitSupplierPayment($req)) {
            AdminModel::addSystemLog('Add new supplier Payment: '.$req->supplier_code.' - '.$req->supplier_name);
            return redirect('supplierPayment')->with(['success' => 'Successfully Add Supplier']);
        } else {
            return redirect('supplierPayment')->with(['error' => 'Failed To Add Supplier']);
        }
    }

    function showSupplierPaymentEditForm(Request $req) {
        if (empty(Session::get('username'))) { return redirect('Payment'); }
        $data = [
            'supplier_data' => AdminModel::getSupplierPaymentById($req->id),
            'supplier' => AdminModel::getSupplierList(),
        ];


        return view('supplierPayment-edit')->with($data);
    }

    function submitEditSupplierPayment(Request $req) {
        if (empty(Session::get('username'))) { return redirect('Payment'); }
        if (AdminModel::submitEditSupplierPayment($req)) {
            AdminModel::addSystemLog('Edit supplier Payment: '.$req->supplier_code.' - '.$req->supplier_name);
            return redirect('supplierPayment')->with(['success' => 'Successfully Edit Supplier']);
        } else {
            return redirect('supplierPayment')->with(['error' => 'Failed To Edit Supplier']);
        }
    }

    function deleteSupplierPayment(Request $req) {
        if (empty(Session::get('username'))) { return redirect('Payment'); }
        if ((Session::get('access_level') != 'superuser') && (Session::get('access_level') != 'supervisor'))  { return abort(403, 'Unauthorized action.'); }
        if (AdminModel::deleteSupplierPayment($req)) {
            $supplier_data = AdminModel::getSupplierPaymentById($req->id);
            
            return redirect('supplierPayment')->with(['success' => 'Successfully Delete Supplier']);
        } else {
            return redirect('supplierPayment')->with(['error' => 'Failed To Delete Supplier']);
        }
    }

















    function ajaxTableProduct(Request $req) {
        $data = AdminModel::getProductByInternalCode(strtoupper($req->internal_code));
        $master = AdminModel::getMasterDataByInternalCode(strtoupper($req->internal_code));
        if (count($data) == 0) :
            $inline = "<table class='table-style'><tr><th>CURRENT STOCK KODE ".strtoupper($req->internal_code)."</th></tr>";
            $inline .= "<tr><th>Tidak Ada Stok</th></tr></table>";
            $table2 = '';
            $table3 = '';
            $images = '';
        else :
            $image = AdminModel::getMasterImage($data[0]->external_code);
            $array_data = [];
            $total_data = [];
            $array_harga = [];
            foreach ($data as $row) {
                $array_data[strtoupper($row->internal_code)][strtoupper($row->variant)][$row->size] = $row->qty;
                $array_harga[strtoupper($row->internal_code)][strtoupper($row->variant)][$row->size] = $row->selling_price;
                if (isset($total_data[$row->size])) {
                    $total_data[$row->size] += $row->qty;
                } else {
                    $total_data[$row->size] = $row->qty;
                }
            }
            $inline = "<table class='table-style'><tr><th colspan='".(count($total_data) + 1)."'>CURRENT STOCK KODE ".strtoupper($req->internal_code)."</th></tr>";
            $inline .= "<tr><th>".strtoupper($req->internal_code)."</th>";
            if (isset($total_data["ALLSIZE"])) { $inline .= "<th>ALLSIZE</th>"; }
            if (isset($total_data["NOSIZE"])) { $inline .= "<th>NOSIZE</th>"; }
            if (isset($total_data["3XS"])) { $inline .= "<th>3XS</th>"; }
            if (isset($total_data["2XS"])) { $inline .= "<th>2XS</th>"; }
            if (isset($total_data["XS"])) { $inline .= "<th>XS</th>"; }
            if (isset($total_data["S"])) { $inline .= "<th>S</th>"; }
            if (isset($total_data["M"])) { $inline .= "<th>M</th>"; }
            if (isset($total_data["L"])) { $inline .= "<th>L</th>"; }
            if (isset($total_data["XL"])) { $inline .= "<th>XL</th>"; }
            if (isset($total_data["2XL"])) { $inline .= "<th>2XL</th>"; }
            if (isset($total_data["3XL"])) { $inline .= "<th>3XL</th>"; }
            if (isset($total_data["4XL"])) { $inline .= "<th>4XL</th>"; }
            if (isset($total_data["5XL"])) { $inline .= "<th>5XL</th>"; }
            if (isset($total_data["6XL"])) { $inline .= "<th>6XL</th>"; }
            for ($i=25; $i <= 45; $i++) { 
                if (isset($total_data[$i])) { $inline .= "<th>".$i."</th>"; }
            }
            
            foreach ($array_data[strtoupper($req->internal_code)] as $key => $value) {
                $inline .= "<tr><td>".$key."</td>";

                if (isset($total_data["ALLSIZE"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["ALLSIZE"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["ALLSIZE"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["NOSIZE"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["NOSIZE"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["NOSIZE"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["3XS"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["3XS"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["3XS"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["2XS"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["2XS"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["2XS"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["XS"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["XS"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["XS"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["S"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["S"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["S"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["M"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["M"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["M"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["L"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["L"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["L"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["XL"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["XL"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["XL"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["2XL"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["2XL"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["2XL"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["3XL"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["3XL"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["3XL"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["4XL"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["4XL"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["4XL"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["5XL"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["5XL"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["5XL"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                if (isset($total_data["6XL"])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key]["6XL"])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key]["6XL"]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }

                for ($i=25; $i <= 45; $i++) { 
                if (isset($total_data[$i])) {
                    if (isset($array_data[strtoupper($row->internal_code)][$key][$i])) {
                        $inline .= "<td>".$array_data[strtoupper($row->internal_code)][$key][$i]."</td>";
                    } else {
                        $inline .= "<td>0</td>";
                    }
                }
                }

                $inline .= "</tr>";
            }
            $inline .= "</table><br>";
            if (($master->hargaasli>0||$master->hargaasli!=null) && $master->hargaasli > $master->selling_price) :
                $table2 = "<table class='table-style'><tr><th colspan='6'>".strtoupper($req->internal_code)."</th></tr><tr><th colspan='3'>HARGA ASLI</th><th colspan='3'>HARGA JUAL</th></tr>";
                $table2 .= "<tr><td colspan='3'>".number_format($master->hargaasli)."</td><td colspan='3'>".number_format($master->selling_price)."</td></tr>";
            else:
                $table2 = "<table class='table-style'><tr><th colspan='3'>".strtoupper($req->internal_code)."</th></tr><tr><th colspan='3'>HARGA JUAL</th></tr>";
                $table2 .= "<tr><td colspan='3'>".number_format($master->selling_price)."</td></tr>";
            endif;
            
            // foreach ($array_harga as $code => $row) {
            //     foreach ($row as $variant => $size) {
            //         foreach ($size as $size => $price) {
            //             $table2 .= "<tr><td>".$variant."</td>";
            //             $table2 .= "<td>".$size."</td>";
            //             $table2 .= "<td>".number_format($price)."</td></tr>";
            //         }
            //     }
            // }
            
            if (($master->hargaasli>0||$master->hargaasli!=null) && $master->hargaasli > $master->selling_price) :
                $table2 .= "<tr><td colspan='6'><h1><p style='color:red'>SALE!!!!</p></h1></td></tr>";
            
            endif;
            $table2 .= "</table><br>";
            $images = "<br><img src='".url('public').\Storage::url($image->master_image)."' style='width:230px'><br><br>";
            $table3 = "<table class='table-style'><tr><th colspan='3'>KETERANGAN</td></tr><tr><th>Variant</th><th>Size</th><th>Keterangan</th></tr>";
            foreach ($data as $row) {
                if (!empty($row->keterangan)) {
                    $table3 .= "<tr><td>".$row->variant."</td><td>".$row->size."</td><td style='text-align: justify'>".nl2br($row->keterangan)."</td></tr>";
                }
            }
            $table3 .= "</table>";
        endif;

        $css = "<style>
        .table-style {
            border-collapse: collapse;
            font-family: sans-serif;
        }
        .table-style td, .table-style th {
            border: 1px solid grey;
            padding: 3px 10px;
            font-size: 14px;
            text-align: center;
        }
        .table-style th {
            background: navy;
            color: white;
        }
        .table-style tr:nth-child(even) td {
            background: aliceblue;
        }
        </style>
        ";
        echo $inline;
        echo $table2;
        echo $table3;
        echo $images;
        echo $css;
    }

    function ajaxGetMasterImage(Request $req) {
        $master_data = AdminModel::getMasterImage(strtoupper($req->external_code));
        if (isset($master_data)) {
            echo $master_data->master_image;
        } else {
            echo '/no_image.png';
        }
    }

    function deleteListOrderPersonal(Request $req) {
        if (AdminModel::deleteListOrderPersonal($req->instagram, $req->tanggal)) {
            return redirect('list-order/new')->with(['success' => 'Berhasil Menghapus Data']);
        } else {
            return redirect('list-order/new')->with(['error' => 'Gagal Menghapus Data']);
        }
    }

    function deleteListOrderPersonal3Days(Request $req) {
        $date = date('Y-m-d', strtotime('-3 day'));
        if (AdminModel::deleteListOrderPersonal3days($date)) {
            return redirect('list-order/new')->with(['success' => 'Berhasil Menghapus Data']);
        } else {
            return redirect('list-order/new')->with(['error' => 'Gagal Menghapus Data']);
        }
    }
    function deleteListOrderPersonal1Days(Request $req) {
        $date = date('Y-m-d', strtotime('-1 day'));
        if (AdminModel::deleteListOrderPersonal3days($date)) {
            return redirect('list-order/new')->with(['success' => 'Berhasil Menghapus Data']);
        } else {
            return redirect('list-order/new')->with(['error' => 'Gagal Menghapus Data']);
        }
    }

    function deletePaidOrd(Request $req) {
        $custdata = AdminModel::getPaymentById($req->payment_id);
        $order_list = AdminModel::getPaidOrderByPaymentID($req->payment_id);
        foreach ($order_list as $row) {
            if (AdminModel::returnToUnpaidOrder($row, $custdata)) {
                if( AdminModel::returnToStock($row)) {
                    AdminModel::deletePaidOrderList($row, $req->payment_id);
                }
            }
        }
        if (AdminModel::deletePayment($req->payment_id)) {
                      $sdks = (object) AdminModel::getDailySalesYTD();
            
            
            $this->sendWAToADMIN($req->instagram. ' pembayaran manual di batalkan. '. PHP_EOL . $req->comment . PHP_EOL. $req->reffer . PHP_EOL . 'Total Hari ini : ' . number_format($sdks->total));

            AdminModel::addSystemLog('Delete Pending Payment '.$req->payment_id);
            return redirect('list-order/paid')->with(['success' => 'Berhasil Menghapus Data']);
        } else {
            return redirect('list-order/paid')->with(['error' => 'Gagal Menghapus Data']);
        }
    }



    function repopulateMasterData() {
        $variant_data = AdminModel::getUniqueStock();
        foreach ($variant_data as $row) {
            AdminModel::insertMasterData($row);
            echo $row->external_code." Updated<br>";
        }
    }

    function cancelPayment(Request $req) {
        $custdata = AdminModel::getPaymentById($req->payment_id);
        $order_list = AdminModel::getPaidOrderByPaymentID($req->payment_id);
        foreach ($order_list as $row) {
            if (AdminModel::returnToUnpaidOrder($row, $custdata)) {
                if( AdminModel::returnToStock($row)) {
                    AdminModel::deletePaidOrderList($row, $req->payment_id);
                }
            }
        }
        if (AdminModel::deletePayment($req->payment_id)) {
            AdminModel::addSystemLog('Delete Pending Payment '.$req->payment_id);
            return redirect('list-order/paid')->with(['success' => 'Berhasil Menghapus Data']);
        } else {
            return redirect('list-order/paid')->with(['error' => 'Gagal Menghapus Data']);
        }
    }

    function changePackingStatus(Request $req) {
        if (AdminModel::changePackingStatus($req->payment_id, $req->status)) {
            return back();
        } else {
            return back()->with(['error' => 'Gagal Mengubah Status Packing']);
        }
    }

    function changeShippingStatus(Request $req) {
        if (AdminModel::changeShippingStatus($req->payment_id, $req->status)) {
            return back();
        } else {
            return back()->with(['error' => 'Gagal Mengubah Status Packing']);
        }
    }

    function showManualConfirmation(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'payment' => AdminModel::getPaymentById($req->payment_id),
            'order' => AdminModel::getPaidOrderByPaymentID($req->payment_id),
        ];
        return view('manual-confirmation')->with($data);
    }

    function updateManualConfirmation(Request $req) {
        if (AdminModel::updateManualConfirmation($req)) {
            return redirect('list-order/paid')->with(['success' => 'Berhasil Menyimpan Data']);
        } else {
            return redirect('list-order/pending')->with(['error' => 'Gagal Menyimpan Data']);
        }
    }

    function showListRefund() {
        $data = [
            'refund_list' => AdminModel::getRefundList(),
        ];
        return view('refund-list')->with($data);
    }

    function showRefundCreationPage(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $data = [
            'payment' => AdminModel::getPaymentById($req->payment_id),
            'item' => AdminModel::getPaidOrderByPaymentID($req->payment_id),
            'master' => $this->getMasterDataArray(),
        ];
        return view('refund-creation')->with($data);
    }

    function showRefundManagementPage(Request $req) {
        if (empty(Session::get('username'))) { return redirect('login'); }
        $refund_master = AdminModel::getRefundMasterByID($req->refund_master_id);
        $step_array = [];
        for ($i=0; $i <= $refund_master->last_step_no; $i++) { 
            $step_array[$i] = AdminModel::getRefundDetailByMasterIDAndStep($req->refund_master_id, $i);
        }

        $data = [
            'refund' => $refund_master,
            'refund_item' => AdminModel::getRefundItemByMasterID($req->refund_master_id),
            'refund_detail' => AdminModel::getRefundDetailByMasterID($req->refund_master_id),
            'refund_detail_steps' => $step_array,
            'master' => $this->getMasterDataArray(),
        ];
        return view('refund-manage')->with($data);
    }

    function updateRefundStatus(Request $req) {
        if ($req->step_no == 0) {
            if (AdminModel::insertNewRefund($req)) {
                AdminModel::addSystemLog('Create new refund with number '.$req->refund_master_id);
                return redirect('refund/manage/'.$req->refund_master_id)->with(['success' => 'Berhasil Menyimpan Data']);
            } else {
                return redirect('refund/manage/'.$req->refund_master_id)->with(['error' => 'Gagal Menyimpan Data']);
            }
        } else if ($req->step_no == 1) {
            if (AdminModel::updateRefundStep1($req)) {
                AdminModel::addSystemLog('Update refund number '.$req->refund_master_id.' => '.$req->step_detail);
                return redirect('refund/manage/'.$req->refund_master_id)->with(['success' => 'Berhasil Menyimpan Data']);
            } else {
                return redirect('refund/manage/'.$req->refund_master_id)->with(['error' => 'Gagal Menyimpan Data']);
            }
        } else if ($req->step_no == 2) {
            $path = '';
            if ($req->hasFile('received_product_photo')) {
                $path = $req->file('received_product_photo')->store('public/retur');

                if (AdminModel::updateRefundStep2($req, $path)) {
                AdminModel::addSystemLog('Update refund number '.$req->refund_master_id.' => '.$req->step_detail);
                    $items = AdminModel::getRefundItemByMasterID($req->refund_master_id);
                    foreach ($items as $row) {
                        AdminModel::returnRefundedItemToStock($row);
                    }
                    return redirect('refund/manage/'.$req->refund_master_id)->with(['success' => 'Berhasil Menyimpan Data']);
                } else {
                    return redirect('refund/manage/'.$req->refund_master_id)->with(['error' => 'Gagal Menyimpan Data']);
                }
            }
        } else if ($req->step_no == 3) {
            $path = '';
            if ($req->hasFile('transfer_proof')) {
                $path = $req->file('transfer_proof')->store('public/transfer_proof');
            }
            if (AdminModel::updateRefundStep3($req, $path)) {
                AdminModel::addSystemLog('Update refund number '.$req->refund_master_id.' => '.$req->step_detail);
                return redirect('refund/manage/'.$req->refund_master_id)->with(['success' => 'Berhasil Menyimpan Data']);
            } else {
                return redirect('refund/manage/'.$req->refund_master_id)->with(['error' => 'Gagal Menyimpan Data']);
            }
        }
    }

    function repopulateCustomerData() {
        $custaddress = AdminModel::getAllShippingAddress();
        foreach ($custaddress as $row) {
            $myarray = explode("\n", $row->shipping_address);
            $name_array = explode(" ", $myarray[0]);
            $firstname = $name_array[0];
            $lastname = "";
            for ($i=1; $i < count($name_array); $i++) { 
                if ($i > 1) {
                    $lastname .= " ";
                }
                $lastname .= $name_array[$i];
            }
            $contact_array = explode(" / ", $myarray[1]);
            $phone = str_replace(" ", "", $contact_array[0]);
            $email = str_replace(" ", "", $contact_array[1]);

            $data = [
                'username' => $row->instagram,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'phone' => $phone,
                'email' => $email,
                'shipping_address' => $row->shipping_address,
            ];
            if (AdminModel::repopulateCustomerData($data)) {
                echo $row->instagram." customer data updated";
            } else {
                echo $row->instagram." gagal";
            }
        }
    }

    function hitAndRunFlag(Request $req) {
        $custdata = AdminModel::getHitAndRunByInstagram($req->instagram);

        if ($req->status == '1') {
            AdminModel::moveOrderToHitAndRun($req);
            if (!isset($custdata)) {
                if (AdminModel::insertHitAndRun($req)) {
                    AdminModel::addSystemLog('Add hit and run flag '.$req->instagram);
                    return redirect('list-order/new')->with(['success' => 'Berhasil menandai hit and run']);
                } else {
                    return redirect('list-order/new')->with(['error' => 'Gagal menandai hit and run']);
                }
            } else {
                return redirect('list-order/new')->with(['success' => 'Berhasil menandai hit and run']);
            }
            AdminModel::addSystemLog("Menandai ".$req->instagram." sebagai Hit and Run");
        } else {
            if (AdminModel::removeHitAndRun($req)) {
                AdminModel::returnHitAndRunToActiveOrder($req);
                return redirect('hit-and-run')->with(['success' => 'Berhasil menghapus hit and run']);
            } else {
                return redirect('hit-and-run')->with(['error' => 'Gagal menghapus hit and run']);
            }
            AdminModel::addSystemLog("Menghapus ".$req->instagram." dari Hit and Run");
        }
    }

    function resizeAllImages() {
        ini_set('max_execution_time', 600);
        $path = storage_path('app/public/products');
        $files = array_diff(scandir($path), array('..', '.'));
        foreach ($files as $key => $file) {
            try {
                $img = Image::make(storage_path('app/public/products').'/'.$file);
                $img->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();                 
                });
                $img->insert(storage_path('app/public/watermark.png'), 'top-right', 10, 10);
                $img->encode('jpg', 75);
                $img->save(storage_path('app/public/products').'/'.$file);
                echo $file." resized<br>";
            } catch (Exception $e) {
                echo $e->message();
            }
        }
    }

    function showSalesReportByPeriode(Request $req) {
        switch ($req->periode) {
            case 'yearly':
                $header = 'Tahun Ini';
                break;
            case 'monthly':
                $header = 'Bulan Ini';
                break;
            case 'weekly':
                $header = 'Minggu Ini';
                break;
            case 'daybefore' :
                    $header = 'H-3';
                    break;
            case 'daybefore' :
                    $header = 'H-2';
                    break;
            case 'yesterday' :
                $header = 'Kemarin';
                break;
            case 'daily':
                $header = 'Hari Ini';
                break;
            case 'all':
            default:
                $header = 'Keseluruhan';
                break;
        }
        $data = [
            'header' => $header,
            'sales_data' => AdminModel::getSalesReportByPeriode($req->periode),
        ];

        return view('sales-report')->with($data);
    }

    function showSalesReportByDateFilter(Request $req) {
        $data = [
            'header' => 'dari '.date('d/m/Y', strtotime($req->start)).' hingga '.date('d/m/Y', strtotime($req->end)),
            'sales_data' => AdminModel::getSalesReportByDateFilter($req),
        ];

        return view('sales-report')->with($data);
    }

    function deleteOrderByID(Request $req) {
        $order_data = AdminModel::getOrderItemByID($req->id);
        if (AdminModel::deleteOrderByID($req->id)) {
            AdminModel::addSystemLog('Delete '.$order_data->internal_code.' / '.$order_data->variant.' / '.$order_data->size.' from order of '.$order_data->instagram);
            return back()->with(['success' => 'Berhasil menghapus data']);
        } else {
            return back()->with(['error' => 'Gagal menghapus data']);
        }
    }

    function updateSupplierPricePaidOrder() {
        $master = AdminModel::getMasterData();
        foreach ($master as $row) {
            AdminModel::updateSupplierPricePaidOrder($row->internal_code, $row->supplier_price);
        }
    }

    function showDiscountPage() {
        $data = [
            'slides' => SlideshowModel::getSlides(),
            'discount' => AdminModel::getDiscount(),
        ];
        return view('discount')->with($data);
    }

    function showStockKode() {
        $data = [
            'product_main' => AdminModel::getProductMainData(),
            
            'discount' => AdminModel::getDiscount(),
        ];
        return view('stock-kodeselect')->with($data);
    }

    function saveDiscount(Request $req) {
        if (AdminModel::saveDiscount($req)) {
            return redirect('discount')->with(['success' => 'Berhasil menyimpan data']);
        } else {
            return back()->with(['error' => 'Gagal menyimpan data']);
        }
    }

    function showLaporanPenjualan() {
        $req = (object) array('report_date' => date('Y-m-d'));
        $data = [
            'report' => AdminModel::getLaporanPenjualanSupplier($req),
            'rekap' => AdminModel::getRekapLaporanPenjualanSupplier($req),
            'supplier_list' => AdminModel::getSupplierList(),
            'report_date' => $req->report_date,
            'supplier_filter' => null,
        ];
        return view('penjualan')->with($data);
    }

    function filterLaporanPenjualan(Request $req) {
        $data = [
            'report' => AdminModel::getLaporanPenjualanSupplier($req),
            'rekap' => AdminModel::getRekapLaporanPenjualanSupplier($req),
            'supplier_list' => AdminModel::getSupplierList(),
            'report_date' => $req->report_date,
            'supplier_filter' => $req->supplier_code,
        ];
        return view('penjualan')->with($data);
    }

    function manageKategori(Request $req) {
        $data = [
            'category' => AdminModel::getKategori(),
        ];
        return view('category')->with($data);
    }

    function addKategori(Request $req) {
        if (AdminModel::addKategori($req)) {
            return redirect('category')->with(['success' => 'Berhasil menyimpan data']);
        } else {
            return redirect('category')->with(['error' => 'Gagal menyimpan data']);
        }
    }

    function deleteKategori(Request $req) {
        if (AdminModel::deleteKategori($req->category)) {
            return redirect('category')->with(['success' => 'Berhasil menghapus data']);
        } else {
            return redirect('category')->with(['error' => 'Gagal menghapus data']);
        }
    }
}
