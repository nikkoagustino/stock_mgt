<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SupplierModel;
use App\Models\SlideshowModel;
use Session;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;


class SupplierController extends Controller
{


    function Suplogin(Request $req) {
        $userdata = SupplierModel::loginAttempt($req);
        if (isset($userdata)) {
            Session::put('supplierlogin', $userdata->username);
            Session::put('supplier1', $userdata->supplier1);
            Session::put('supplier2', $userdata->supplier2);
            Session::put('supplier3', $userdata->supplier3);
            Session::put('supplier4', $userdata->supplier4);
            Session::put('supplier5', $userdata->supplier5);
            Session::put('supplier6', $userdata->supplier6);
            Session::put('demodemode', $userdata->demodemode);
            
            return redirect('supps/dashboard');
        } else {
            return redirect('supps/login');
        }
    }

    function logout() {
        Session::flush();
        Session::regenerate();
        return redirect('supps/login');
    }
   
    function showTemplate() {
        //if (empty(Session::get('username'))) { return redirect('login'); }
        //$data = [
        //    'stock' => AdminModel::getAllStock(),
        //    'master' => $this->getMasterDataArray(),
        //];
        return view('supplier/template');
    }



    function showstockhariini() {
        if (empty(Session::get('supplier1'))) { return redirect('supps/login'); }

        $sup1='-';
        $sup2='-';
        $sup3='-';
        $sup4='-';
        $sup5='-';
        $sup6='-';
        if ((Session::get('supplier1'))) { $sup1=Session::get('supplier1');}
        if ((Session::get('supplier2'))) { $sup2=Session::get('supplier2');}
        if ((Session::get('supplier3'))) { $sup3=Session::get('supplier3');}
        if ((Session::get('supplier4'))) { $sup4=Session::get('supplier4');}
        if ((Session::get('supplier5'))) { $sup5=Session::get('supplier5');}
        if ((Session::get('supplier6'))) { $sup6=Session::get('supplier6');}


        $kirim = (object) [
            'supplier1' => $sup1,
            'supplier2' => $sup2,
            'supplier3' => $sup3,
            'supplier4' => $sup4,
            'supplier5' => $sup5,
            'supplier6' => $sup6,
        ];

        $data = [
            'supplier' => SupplierModel::getStockHariIni($kirim),
            'stokbysupcode' => SupplierModel::getRekapValuasiBySupCode($kirim),
            'valuasi' => SupplierModel::getRekapValuasi($kirim),
        ];
        //dd($data);
        return view('supplier/stockhariini')->with($data);
    }

    function showRekapStok() {
        if (empty(Session::get('supplier1'))) { return redirect('supps/login'); }

        $sup1='-';
        $sup2='-';
        $sup3='-';
        $sup4='-';
        $sup5='-';
        $sup6='-';
        if ((Session::get('supplier1'))) { $sup1=Session::get('supplier1');}
        if ((Session::get('supplier2'))) { $sup2=Session::get('supplier2');}
        if ((Session::get('supplier3'))) { $sup3=Session::get('supplier3');}
        if ((Session::get('supplier4'))) { $sup4=Session::get('supplier4');}
        if ((Session::get('supplier5'))) { $sup5=Session::get('supplier5');}
        if ((Session::get('supplier6'))) { $sup6=Session::get('supplier6');}


        $kirim = (object) [
            'supplier1' => $sup1,
            'supplier2' => $sup2,
            'supplier3' => $sup3,
            'supplier4' => $sup4,
            'supplier5' => $sup5,
            'supplier6' => $sup6,
        ];

        $data = [
            'supplier' => SupplierModel::getStockHariIni($kirim),
            'valuasi' => SupplierModel::getRekapValuasi($kirim),
            'penjualan' => SupplierModel::getRekapSalesALLO($kirim),
            'pembayaran' => SupplierModel::getRekapPayment($kirim),
        ];
        return view('supplier/rekapstoktotal')->with($data);
    }




    function showDashboard(Request $req) {
        if (empty(Session::get('supplier1'))) { return redirect('supps/login'); }

        $sup1='-';
        $sup2='-';
        $sup3='-';
        $sup4='-';
        $sup5='-';
        $sup6='-';
        if ((Session::get('supplier1'))) { $sup1=Session::get('supplier1');}
        if ((Session::get('supplier2'))) { $sup2=Session::get('supplier2');}
        if ((Session::get('supplier3'))) { $sup3=Session::get('supplier3');}
        if ((Session::get('supplier4'))) { $sup4=Session::get('supplier4');}
        if ((Session::get('supplier5'))) { $sup5=Session::get('supplier5');}
        if ((Session::get('supplier6'))) { $sup6=Session::get('supplier6');}


        $kirim = (object) [
            'supplier1' => $sup1,
            'supplier2' => $sup2,
            'supplier3' => $sup3,
            'supplier4' => $sup4,
            'supplier5' => $sup5,
            'supplier6' => $sup6,
            'tanggaldr' => date('Y-m-d'),
            'tanggalsmp' => date('Y-m-d'),
        ];

        

        $daily_sales = SupplierModel::getDailySales($kirim);
        $tanggal_string = null;
        $total_jual_string = null;
        foreach ($daily_sales as $row) {
            if ($tanggal_string == null) {
                $tanggal_string = '"'.$row->tanggal.'"';
            } else {
                $tanggal_string = $tanggal_string.', "'.$row->tanggal.'"';
            }
            if ($total_jual_string == null) {
                $total_jual_string = $row->qty_sold;
            } else {
                $total_jual_string = $total_jual_string.', '.$row->qty_sold;
            }
        }
        $chart_data = [
            'tanggal' => $tanggal_string,
            'total_jual' => $total_jual_string,
        ];

        $data = [
            'chart' => (object) $chart_data,
            'sales' => SupplierModel::getSales($kirim),
            'valuasi' => SupplierModel::getRekapSales($kirim)
        ];
        return view('supplier/dashboard')->with($data);
    }




    function showSales(Request $req) {
        if (empty(Session::get('supplier1'))) { return redirect('supps/login'); }

        $sup1='-';
        $sup2='-';
        $sup3='-';
        $sup4='-';
        $sup5='-';
        $sup6='-';
        if ((Session::get('supplier1'))) { $sup1=Session::get('supplier1');}
        if ((Session::get('supplier2'))) { $sup2=Session::get('supplier2');}
        if ((Session::get('supplier3'))) { $sup3=Session::get('supplier3');}
        if ((Session::get('supplier4'))) { $sup4=Session::get('supplier4');}
        if ((Session::get('supplier5'))) { $sup5=Session::get('supplier5');}
        if ((Session::get('supplier6'))) { $sup6=Session::get('supplier6');}


        $kirim = (object) [
            'supplier1' => $sup1,
            'supplier2' => $sup2,
            'supplier3' => $sup3,
            'supplier4' => $sup4,
            'supplier5' => $sup5,
            'supplier6' => $sup6,
            'tanggaldr' => $req->tanggaldr,
            'tanggalsmp' => $req->tanggalsmp,
        ];

        $data = [
            'sales' => SupplierModel::getSales($kirim),
            'salesbyprice' => SupplierModel::getRekapBySupPrice($kirim),
            'valuasi' => SupplierModel::getRekapSales($kirim)
        ];
        return view('supplier/sales')->with($data);
    }

    function showSelectSales() {
        
        return view('supplier/selectsales');
    }



    function showPayment() {
        if (empty(Session::get('supplier1'))) { return redirect('supps/login'); }
    	$data = [
    		'supplier' => SupplierModel::getSupplierPaymentList(),
    	];
    	return view('supplier/payment')->with($data);
    }

}
