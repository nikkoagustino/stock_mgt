<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\SlideshowModel;

class SlideshowController extends Controller
{
    function addSlide(Request $req) {
        $path = null;
        if ($req->hasFile('slide_image')) {
            try {
                
                $image = $req->file('slide_image');
                $imageName = $image->getClientOriginalName();
                $path = 'public/slideshow/'.md5(time().rand(0,1000)).'.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(1500, null, function ($constraint) {
                    $constraint->aspectRatio();            
                });
                // $img->insert(storage_path('app/public/watermark.png'), 'top-right', 10, 10);
                $img->encode('jpg', 80);
                $img->stream();
                Storage::disk('local')->put($path, $img, 'public');

                if (SlideshowModel::addSlide($path)) {
                    return redirect('discount')->with('success', 'Slide added');
                } else {
                    return redirect('discount')->with('error', 'failed');
                }
            } catch (Exception $e) {
                echo $e->message();
            }
        }
    }

    function deleteSlide(Request $req) {
        if (SlideshowModel::deleteSlide($req->id)) {
            return redirect('discount')->with('success', 'Slide deleted');
        } else {
            return redirect('discount')->with('error', 'failed');
        }
    }
}
