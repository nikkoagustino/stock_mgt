<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\SlideshowModel;
use Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function __construct() {
        // Set your Merchant Server Key
        // \Midtrans\Config::$serverKey = 'SB-Mid-server-MJG-jCRHSTUVT22LBytTT3D9';
        \Midtrans\Config::$serverKey = config('midtrans.SERVER_KEY');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = true;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;
    }

    public function showRegisterForm() {
        $data = [
            'page_title' => "Registrasi",
        ];
        return view('userpage/register')->with($data);
    }
    public function showRegisterFormch(Request $req) {
        $data = [
            'page_title' => "Registrasi",
            'red' => $req->red,
        ];
        return view('userpage/registerch')->with($data);
    }

    public function registerUser(Request $req) {
        if(substr($req->telepon2,0,1)=="0"){
            $req->telepon= $req->kodearea .  substr($req->telepon2,1,strlen($req->telepon2)-1);
        } else {
            $req->telepon= $req->kodearea .  $req->telepon2;
        }
        $custdata = UserModel::getCustomerDataByInstagram($req->instagram);
        if (isset($custdata)) {
            return redirect(url()->back())->with(['error' => 'Username / instagram sudah terdaftar']);
        } else {
            
            
            if (UserModel::registerUser($req)) {
                \Session::put('instagram', $req->instagram);
                return redirect('my-account')->with(['success' => 'Berhasil mendaftarkan user']);
            } else {
                return redirect(url()->back())->with(['error' => 'Gagal Mendaftarkan User']);
            }
        }
    }

    public function registerUserch(Request $req) {
        if(substr($req->telepon2,0,1)=="0"){
            $req->telepon= $req->kodearea .  substr($req->telepon2,1,strlen($req->telepon2)-1);
        } else {
            $req->telepon= $req->kodearea .  $req->telepon2;
        }
        $custdata = UserModel::getCustomerDataByInstagram($req->instagram);
        if (isset($custdata)) {
            return redirect(url()->back())->with(['error' => 'Username / instagram sudah terdaftar']);
        } else {
            
                $blmurl='my-account';
            if($req->red=='rekapcheck'){
                $blmurl='rekap/check/';
            }
            
            
            if (UserModel::registerUser($req)) {
                \Session::put('instagram', $req->instagram);
                return redirect($blmurl)->with(['success' => 'Berhasil mendaftarkan user']);
            } else {
                return redirect(url()->back())->with(['error' => 'Gagal Mendaftarkan User']);
            }
        }
    }

    public function checkOrder(Request $req) {
        
        try {
            
            if (isset($req->user_status) && ($req->user_status == 'register')) {
                UserModel::registerUser($req);
                Session::put('instagram', $req->instagram);
                return redirect('rekap/check');
            } else if (isset($req->user_status) && ($req->user_status == 'newpass')) {
                UserModel::saveUserPassword($req);
                Session::put('instagram', $req->instagram);
                return redirect('rekap/check');
            } else if (isset($req->user_status) && ($req->user_status == 'login')) {
                if (UserModel::login($req) == false) {
                    return redirect('rekap')->with(['error' => 'Password Salah']);
                } else {
                    
                    Session::put('instagram', $req->instagram);
                    return redirect('rekap/check');
                }
            } else {
                return redirect('rekap')->with(['error' => 'Anda Membatalkan Checkout']);
            }
        } catch (\Exception $e) {
            return redirect('rekap')->with(['error' => 'Anda Membatalkan Checkout']);
            
        }
    }

    public function showSlider(Request $req) {
        $data = [
            'stock' => UserModel::getSliderData(),
            
            'page_title' => "Keranjang Belanja"
        ];
        return view('userpage/sliderjalan')->with($data);
    }



    public function showCustomerOrder() {
        if (empty(\Session::get('instagram'))) { return redirect('user/logincarthome/rekapcheck'); }
        $data = [
            'discount' => UserModel::getDiscount(),
            'cart' => UserModel::getOrderByParameterV2(Session::get('instagram'), date('Y-m-d')),
            'stock' => UserModel::getStock(),
            'custdata' => UserModel::getCustomerDataByInstagram(Session::get('instagram')),
            'tanggal' => date('Y-m-d'),
            'addresses' => UserModel::getShippingAddress(Session::get('instagram')),
            'supplier' => UserModel::getSupplier(),
            'master' => $this->getMasterDataArray(),
            'page_title' => "Keranjang Belanja"
        ];
        return view('userpage/cart')->with($data);
    }

    public function cancelOrderItem(Request $req) {
        if (UserModel::cancelOrderItem(Session::get('instagram'), $req->internal_code)) {
            return redirect('rekap/check');
        }
    }

    public function apiGateway(Request $req){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $req->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => strtoupper($req->method),
            CURLOPT_POSTFIELDS => $req->postdata,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: 62ac4db3874b3abdb2128808f02c03fe"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    public function midtrans($req){

        $address = array(
            'first_name'   => $req->nama_depan,
            'last_name'    => $req->nama_belakang,
            'address'      => $req->shipping_address,
            'city'         => '',
            'phone'        => $req->telepon,
            'country_code' => 'IDN'
        );

        $params = array(
            'transaction_details' => array(
                'order_id' => $req->order_id,
                'gross_amount' => $req->amount,
            ),
            "custom_field1" => $req->instagram,
            "custom_field2" => $req->tanggal,
            "custom_field3" => $req->shipping_address,
            'customer_details' => array(
                'first_name' => $req->nama_depan,
                'last_name' => $req->nama_belakang,
                'email' => $req->email,
                'phone' => $req->telepon,
                'billing_address'  => $address,
                'shipping_address' => $address
            ),
            'callbacks' => array(
                'finish' => url('payment/callback'),
            ),
        );

        try {
          // Get Snap Payment Page URL
            $paymentUrl = \Midtrans\Snap::createTransaction($params)->redirect_url;
          // Redirect to Snap Payment Page
            $this->sendAWBWhatsapp($req->telepon,'Anda telah melakukan pembayaran secara mandiri Midtrans Momavel sebesar : ' . number_format($req->amount) . PHP_EOL. 'Pembayaran ini akan di update secara instant dan proses checkout secara mandiri.'  . PHP_EOL. 'Diskon / promo ongkir hanya di berikan kepada yang melakukan pembayaran mandiri tdk bs melalui WA Manual.' . PHP_EOL.'Akan tetapi sering kali tidak mengerti cara transfer untuk pertama kali atau transfer bank terlambat di catat informasinya sehingga pembayaran gagal di lakukan.' . PHP_EOL. 'Apabila itu terjadi, anda bisa melakukan reset ulang pembayaran online untuk mendapatkan informasi tersebut.' . PHP_EOL. 'Apabila pembayaran transfer bank BCA tidak tersedia, silahkan menggunakan Bank Lain / Other, anda akan di berikan Virtual Acc BNI tetap bisa di lakukan melalui m-BCA. Di jam kerja bisa menggunakan 2.500, malam hari bisa gunakan yang 6.500, Saat ini kami masih proses memasukan BCA Virtual, jadi sementara kami baru menerima BNI Virtual ACC, bisa di bayar menggunakan BCA, Transfer ke Bank Lain, Bank BNI masukan no Virtual Acc. Apabila anda tidak mau repot bisa menggunakan QRIS yg jg support BCA  / Kartu Kredit / Gopay. Saat ini QRIS hanya support pada checkout menggunakan komputer saja.' . PHP_EOL. 'Apabila anda gagal melakukan payment bisa klik link di bawah ini untuk mereset:' . PHP_EOL. 'https://momavel.id/user-cancel-payment/' .$req->order_id . PHP_EOL. 'Apabila masih terkendala bisa menghubungi WA kami di : 081283443452');
            $this->sendWAToADMIN('Customer : ' . $req->nama_depan . ' ' . $req->nama_belakang . '(' . $req->telepon . ')' .PHP_EOL. 'Pembelian : ' . number_format($req->amount) . ' ID: ' . $req->instagram . PHP_EOL. 'Melakukan Pembayaran Online Order ID : ' . $req->order_id . PHP_EOL. 'Silahkan Cek Apakah Pending Atau Berhasil.' . PHP_EOL. url('/selling/show-paid/'.$req->order_id). PHP_EOL.$req->reffer);
         
            
            return redirect($paymentUrl);
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }
  
      function sendWAToADMIN($Text){
        $ss = UserModel::getAdminWa();
        
        $this->sendAWBWhatsapp($ss->wa1,$Text);
        $this->sendAWBWhatsapp($ss->wa2,$Text);
        $this->sendAWBWhatsapp($ss->wa3,$Text);
        $this->sendAWBWhatsapp($ss->wa4,$Text);
        
    }

    // function paymentCallback(Request $req) {
    //     $status = \Midtrans\Transaction::status($req->order_id);

    //     // if ($status->transaction_status == 'settlement') {
    //     if (UserModel::insertPayment($status) && UserModel::updateStock($status)) {
    //         return redirect('payment/success')->with(['order_id' => $status->order_id]);
    //     } else {
    //         return redirect('payment/failed');
    //     }
    //     // } else {
    //         // return redirect('payment/pending')->with(['order_id' => $status->order_id]);;
    //     // }
    // }

    function paymentCallbackV2(Request $req) {
        $status = \Midtrans\Transaction::status($req->order_id);
        
        if (($status->transaction_status == 'settlement') || ($status->transaction_status == 'capture')){
            $this->sendAWBWhatsapp(UserModel::getPhoneNumberByOrderID($req->order_id)->phone,'Pembayaran online anda di website momavel.id sudah BERHASIL di proses. Terima kasih atas kepercayaannya telah berbelanja bersama kami.');
            $this->sendWAToADMIN(UserModel::getPhoneNumberByOrderID($req->order_id)->phone . PHP_EOL. 'Pembayaran online ' . UserModel::getPhoneNumberByOrderID($req->order_id)->phone .  ' sudah BERHASIL di proses. Order ID : ' . $req->order_id);
        } else {
            $this->sendAWBWhatsapp(UserModel::getPhoneNumberByOrderID($req->order_id)->phone,'Pembayaran online MomAvel.id anda masih dalam PROSES. Silahkan cek kembali beberapa saat setelah pembayaran di terima.');
            $this->sendWAToADMIN(UserModel::getPhoneNumberByOrderID($req->order_id)->phone . PHP_EOL. 'Pembayaran online ' . UserModel::getPhoneNumberByOrderID($req->order_id)->phone .  ' PENDING di proses Midtrans. Order ID : ' . $req->order_id);
        }

        
        if (UserModel::updateMidtransPayment($status)) {

            return redirect('my-account')->with(['order_id' => $status->order_id]);
            
        } else {
            
            return redirect('payment/failed')->with(['order_id' => $status->order_id]);
            
        }
    }

    function sendAWBWhatsapp($number,$message) {
        
        $data = [
            'number' => $number,
            'message' => $message,
        ];

        $curl = curl_init("https://wa-api.momavel.id/send-message");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT , 1);
        $send_header = array('Content-Type: application/json');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

        $response = curl_exec($curl);
        curl_close($curl);
    }

    function checkCustomer(Request $req) {
        $data = [
            'req' => $req,
            'customer_data' => UserModel::getCustomerData($req),
            'page_title' => "Cek Pengguna",
        ];
        return view('userpage/step-2')->with($data);
    }



    function submitPaymentOrder(Request $req) {
        
        $req->tanggal = $req->tglskrg;




        $data = [
            'payment' => $req,
            'paymentsend' => json_encode($req->all()),
            
        ];
        
        return view('userpage/paynow')->with($data);



    }

    function submitPaymentOrderQris(Request $req) {
        $reqs=json_decode($req->yyy);

       
        $address = array(
            'first_name'   => $reqs->nama_depan,
            'last_name'    => $reqs->nama_belakang,
            'address'      => $reqs->shipping_address,
            'city'         => '',
            'phone'        => $reqs->telepon,
            'country_code' => 'IDN'
        );

        $params = array(
            'payment_type' => 'qris',
            'transaction_details' => array(
                'order_id' => $reqs->order_id,
                'gross_amount' => $reqs->amount,
            ),
            // "custom_field1" => $reqs->instagram,
            // "custom_field2" => $reqs->tanggal,
            // "custom_field3" => $reqs->shipping_address,
            'customer_details' => array(
                'first_name' => $reqs->nama_depan,
                'last_name' => $reqs->nama_belakang,
                'email' => $reqs->email,
                'phone' => $reqs->telepon,
                // 'billing_address'  => $reqs->shipping_address,
                // 'shipping_address' => $reqs->shipping_address
            ),
            // 'callbacks' => array(
            //     'finish' => url('userpage/my-aacount'),
            // ),
            'qris'=> array(
                'acquirer' => 'gopay',
            ),
        );
        $listprod = '';
        if (UserModel::insertPaymentV2($reqs)) {
            for ($i=0; $i < count($reqs->internal_code); $i++) { 
                $listprod = $listprod . '<br>' . $reqs->qty[$i] . 'Pc(s) ' . $reqs->internal_code[$i] . ' - ' . $reqs->variant[$i] . ' - ' . $reqs->size[$i];
                $item_data = [
                    'instagram' => $reqs->instagram,
                    'order_date' => $reqs->tglskrg,
                    'payment_id' => $reqs->order_id,
                    'internal_code' => $reqs->internal_code[$i],
                    'variant' => $reqs->variant[$i],
                    'size' => $reqs->size[$i],
                    'selling_price' => $reqs->selling_price[$i],
                    'qty' => $reqs->qty[$i],
                    'comment' => $reqs->comment[$i],
                    'reffer' => $reqs->reffer,
                ];
                UserModel::insertPaidOrder($item_data);
                    UserModel::deletePendingOrder($item_data);
                    UserModel::reduceStock($item_data);
                
            }
            $curl = curl_init("https://api.midtrans.com/v2/charge");
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $send_header = array('Accept: application/json','Content-Type: application/json','Authorization: Basic '.base64_encode(config('midtrans.SERVER_KEY').':'));
            // dd($send_header);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

            $response = curl_exec($curl);
            curl_close($curl);

            $response_array = json_decode($response);
            $qris_url = $response_array->actions[0]->url;
            
            try {

            $this->sendAWBWhatsapp($reqs->telepon,'Anda telah melakukan pembayaran secara QRIS Midtrans Momavel sebesar : ' . number_format($reqs->amount) . PHP_EOL. 'Pembayaran ini akan di update secara instant dan proses checkout secara mandiri.'  . PHP_EOL. 'Berikut adalah link qris anda :' . PHP_EOL . PHP_EOL . $qris_url . PHP_EOL . PHP_EOL.'Akan tetapi sering kali tidak mengerti cara transfer untuk pertama kali atau transfer bank terlambat di catat informasinya sehingga pembayaran gagal di lakukan.' . PHP_EOL. 'Apabila itu terjadi, anda bisa melakukan reset ulang pembayaran online untuk mendapatkan informasi tersebut.' . PHP_EOL. 'Apabila pembayaran transfer bank BCA tidak tersedia, silahkan menggunakan Bank Lain / Other, anda akan di berikan Virtual Acc BNI tetap bisa di lakukan melalui m-BCA. Di jam kerja bisa menggunakan 2.500, malam hari bisa gunakan yang 6.500, Saat ini kami masih proses memasukan BCA Virtual, jadi sementara kami baru menerima BNI Virtual ACC, bisa di bayar menggunakan BCA, Transfer ke Bank Lain, Bank BNI masukan no Virtual Acc. Apabila anda tidak mau repot bisa menggunakan QRIS yg jg support BCA  / Kartu Kredit / Gopay. Saat ini QRIS hanya support pada checkout menggunakan komputer saja.' . PHP_EOL. 'Apabila anda gagal melakukan payment bisa klik link di bawah ini untuk mereset:' . PHP_EOL. 'https://momavel.id/user-cancel-payment/' .$reqs->order_id . PHP_EOL. 'Apabila masih terkendala bisa menghubungi WA kami di : 081283443452');
            $this->sendWAToADMIN('Customer : ' . $reqs->nama_depan . ' ' . $reqs->nama_belakang . '(' . $reqs->telepon . ')' .PHP_EOL. 'Pembelian : ' . number_format($reqs->amount) . ' ID: ' . $reqs->instagram . PHP_EOL. 'Melakukan Pembayaran QRIS Order ID : ' . $reqs->order_id . PHP_EOL. 'Silahkan Cek Apakah Pending Atau Berhasil.' . PHP_EOL. url('/selling/show-paid/'.$reqs->order_id). PHP_EOL.$reqs->reffer);
            $this->sendEmailManual($reqs->email, 'Link Pembayaran QRIS MomAvel', 'Dear ' . $reqs->nama_depan .',<br><br>Berikut link pembayaran QRIS anda dengan total pembelian : '. number_format($reqs->amount) .'<br><br><img src="' .$qris_url . '" style="max-width: 100%; max-height: 400px;" src="" alt=""><br>' . $qris_url  .'<br><br>Silahkan lakukan pembayaran di token QR tersebut. Apabila anda melakukan kesalahan dan ingin membatalkan pembayaran ini bisa dengan cara klik link di bawah ini: <br>https://momavel.id/user-cancel-payment/' .$reqs->order_id . '<br>Apabila masih terkendala bisa menghubungi WA kami di : 081283443452<br><br>Pesanan Anda :<br>' . $listprod . '<br><br>Pada umumnya anda akan mendapatkan notifikasi Resi melalui Email / Whatsapp H+1 setelah pembayaran berhasil.<br><br>Terima kasih telah berbelanja bersama kami.');
            } catch (Exception $e) {
                // skip
            }
            // dd($response);
            return response()->json($response, 200);
            //return $this->midtrans($reqs);
        } else {
            return response()->json(['status' => 'error'], 400);
            // return redirect(url()->back())->with(['error' => 'Terjadi kesalahan menyimpan']);
        }

        
    }

    function submitPaymentCC(Request $req) {
        
      $reqs=json_decode($req->yyy);

	 
      //dd($reqs); 

        $listprod='';
        if (UserModel::insertPaymentV2($reqs)) {
            for ($i=0; $i < count($reqs->internal_code); $i++) { 
                $listprod = $listprod . '<br>' . $reqs->qty[$i] . 'Pc(s) ' . $reqs->internal_code[$i] . ' - ' . $reqs->variant[$i] . ' - ' . $reqs->size[$i];
                $item_data = [
                    'instagram' => $reqs->instagram,
                    'order_date' => $reqs->tanggal,
                    'payment_id' => $reqs->order_id,
                    'internal_code' => $reqs->internal_code[$i],
                    'variant' => $reqs->variant[$i],
                    'size' => $reqs->size[$i],
                    'selling_price' => $reqs->selling_price[$i],
                    'qty' => $reqs->qty[$i],
					'comment' => $reqs->comment[$i],
                    'reffer' => $reqs->reffer,
                ];
                if (UserModel::insertPaidOrder($item_data)) {
                    UserModel::deletePendingOrder($item_data);
                    UserModel::reduceStock($item_data);
                }
            }
            $this->sendEmailManual($reqs->email, 'Pesanan Anda di MomAvel', 'Dear ' . $reqs->nama_depan .',<br><br>Berikut info pembelian anda di MomAvel dengan total pembelian : '. number_format($reqs->amount) .'<br><br>Apabila anda melakukan kesalahan dan ingin membatalkan pembayaran ini bisa dengan cara klik link di bawah ini: <br>https://momavel.id/user-cancel-payment/' .$reqs->order_id . '<br>Apabila masih terkendala bisa menghubungi WA kami di : 081283443452<br><br>Pesanan Anda :<br>' . $listprod . '<br><br>Pada umumnya anda akan mendapatkan notifikasi Resi melalui Email / Whatsapp H+1 setelah pembayaran berhasil.<br><br>Terima kasih telah berbelanja bersama kami.');
            return $this->midtrans($reqs);
        } else {
            return redirect(url()->back())->with(['error' => 'Terjadi kesalahan menyimpan']);
        }

    }


    function sendEmailManual($email, $subject, $message) {
        
        if (isset($email) && isset($subject) && isset($message)){
          



          $to_name = $email;
          $to_email = $email;
          $data = [
              'name' => $email,
              'body' => $message,
          ];
            
          \Mail::send('emails.manual-email', $data, function($message) use ($to_name, $to_email, $subject) {
              $message->to($to_email, $to_name);
              $message->subject($subject);
              $message->from('info@momavel.id', 'MomAvel.id');
          });








        }
      
  }

    function getMasterDataArray() {
        $master_data = UserModel::getMasterData();
        $master_array = [];
        foreach ($master_data as $row) {
            // $master_data[$row->internal_code]['supplier_code'] = $row->supplier_code;
            // $master_data[$row->internal_code]['external_code'] = $row->external_code;
            $master_array[$row->internal_code] = [
                    'supplier_code' => $row->supplier_code,
                    'external_code' => $row->external_code,
                    'product_name' => $row->product_name,
                    'category' => $row->category,
                    'master_image' => $row->master_image,
                    'supplier_price' => $row->supplier_price,
                    'selling_price' => $row->selling_price,
            ];
        }
        return $master_array;
    }

    function recheckPayment(Request $req) {
        try {
            $status = \Midtrans\Transaction::status($req->payment_id);
            if (UserModel::updateMidtransPayment($status)) {
                $this->sendAWBWhatsapp(UserModel::getPhoneNumberByOrderID($req->payment_id)->phone,'Pembayaran online anda di website momavel.id sudah BERHASIL di proses. Terima kasih atas kepercayaannya telah berbelanja bersama kami. Order ID : ' . $req->payment_id . ' Status : ' . $status->transaction_status);
                $this->sendWAToADMIN(UserModel::getPhoneNumberByOrderID($req->payment_id)->phone,'Pembayaran online ' . UserModel::getPhoneNumberByOrderID($req->payment_id)->phone .  ' sudah BERHASIL di proses. Order ID : ' . $req->payment_id . ' Status : ' . $status->transaction_status);
                return redirect('list-order/paid')->with(['success' => 'Status pembayaran : '.$status->custom_field1.' / '.$status->order_id.' adalah '.$status->transaction_status]);
            } else {
                
                $this->sendWAToADMIN(UserModel::getPhoneNumberByOrderID($req->payment_id)->phone,'Pembayaran online ' . UserModel::getPhoneNumberByOrderID($req->payment_id)->phone .  ' *GAGAL* di proses. Order ID : ' . $req->payment_id . ' Status : ' . $status->transaction_status);
                return redirect('list-order/paid')->with(['error' => 'Status pembayaran : '.$status->custom_field1.' / '.$status->order_id.' adalah '.$status->transaction_status]);
            }
        } catch (\Exception $e) {
            return redirect('list-order/paid')->with(['error' => $e->getMessage()]);
        }
    }
    

    function autoRecheckPayment() {
        $data_pending = UserModel::getPendingPayment();
        foreach ($data_pending as $row) {
            try {
                $status = \Midtrans\Transaction::status($row->id);
                UserModel::updateMidtransPayment($status);
                if ($status->transaction_status == 'expire') {
                    file_get_contents(url('cancel-payment').'/'.$row->id);
                }
                echo $row->id.' '.$status->transaction_status;
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
            echo "<br>";
        }
    }

    function showLD(Request $req) {
        \Session::put('reffer', 'ADS');

        if(empty(UserModel::checkWebVisitAv())){
            UserModel::insertWebVisitAv();
        }
        UserModel::updateWebVisiAds();

        if(Session::get('reffer')=='ADS'){
            $slidesreq = SlideshowModel::getSlidesAds();
        } else {
            $slidesreq = SlideshowModel::getSlides();
        }

            

        $data = [
            
            'slides' => $slidesreq,
            'stock' => UserModel::getLDData(),

        ];
        //dd($data);
        return view('ld/ld')->with($data);
    }

    function showUserStock(Request $req) {
        
        if(Session::get('reffer')!='ADS'){
            
            if(Session::get('reffer')!='WEB'){
               
                \Session::put('reffer', 'WEB');
                if(empty(UserModel::checkWebVisitAv())){
                    UserModel::insertWebVisitAv();
                }
                UserModel::updateWebVisiNormal();
            } else {
                if(empty(UserModel::checkWebVisitAv())){
                    UserModel::insertWebVisitAv();
                }
                UserModel::updateWebVisiNormal();
            }
        } else {
            if(empty(UserModel::checkWebVisitAv())){
                UserModel::insertWebVisitAv();
            }
            UserModel::updateWebVisiAds();
        }

        if(Session::get('reffer')=='ADS'){
            $slidesreq = SlideshowModel::getSlidesAds();
        } else {
            $slidesreq = SlideshowModel::getSlides();
        }
        
        $data = [
            'master' => UserModel::getMasterDataPaginate($req->sort),
            'stock' => UserModel::getAllStockAvailable(),
            'page_title' => "Produk",
            'slides' => $slidesreq,
        ];
        return view('userpage/stock-view')->with($data);
    }

    function showUserStockAds(Request $req) {
        \Session::put('reffer', 'ADS');

        if(empty(UserModel::checkWebVisitAv())){
            UserModel::insertWebVisitAv();
        }
        UserModel::updateWebVisiAds();

        if(Session::get('reffer')=='ADS'){
            $slidesreq = SlideshowModel::getSlidesAds();
        } else {
            $slidesreq = SlideshowModel::getSlides();
        }

        $data = [
            'master' => UserModel::getMasterDataPaginate($req->sort),
            'stock' => UserModel::getAllStockAvailable(),
            'page_title' => "Produk",
            'slides' => $slidesreq,
        ];
        return view('userpage/stock-view')->with($data);
    }

    function showProductDetail(Request $req) {
        $data = [
            'product' => UserModel::getProductByInternalCode($req->internal_code),
            'master' => UserModel::getMasterDataByInternalCode($req->internal_code),
            'page_title' => "Produk"
        ];
        return view('userpage/stock-detail')->with($data);
    }

    

    function userProductSearch(Request $req) {
        $data = [
            'master' => UserModel::getMasterDataSearch($req->search, $req->sort),
            'search_query' => $req->search,
            'stock' => UserModel::getAllStockAvailable(),
            'page_title' => "Hasil Pencarian",
            'slides' => SlideshowModel::getSlides(),
        ];
        return view('userpage/stock-view')->with($data);
    }

    function login(Request $req) {

        if (UserModel::login($req) == false) {
            return redirect('rekap')->with(['error' => 'Password Salah']);
        } else {
            Session::put('instagram', $req->instagram);
        }
    }

    function showUserDashboard() {
        if (empty(\Session::get('instagram'))) { return redirect('user/logincarthome/my-account'); }
        $data = [
            'paid_order' => UserModel::getPaidOrderByInstagram(\Session::get('instagram')),
            'addresses' => UserModel::getShippingAddress(\Session::get('instagram')),
            'userdata' => UserModel::getCustomerDataByInstagram(\Session::get('instagram')),
            'page_title' => "Akun Saya",
        ];
        return view('userpage/my-account')->with($data);
    }

    


    function showLoginCartPage(Request $req){
        \Session::put('reffer', 'WA OTO');
        $data = [
            'page_title' => "Login",
            'instagram' => $req->instagram,
            'password'=> $req->password,
        ];
        return view('userpage/logincart')->with($data);
    }

    function loginUserCart(Request $req) {
        if (UserModel::login($req)) {
            \Session::put('instagram', $req->instagram);
            \Session::put('reffer', 'WA OTO');
            return redirect('rekap/check');
        } else {
            return redirect('user/login')->with(['error' => 'Username / password salah']);
        }
    }


    function loginUser(Request $req) {
        if (UserModel::login($req)) {
            \Session::put('instagram', $req->instagram);
            return redirect('my-account');
        } else {
            return redirect('user/login')->with(['error' => 'Username / password salah']);
        }
    }

    function loginUserLD(Request $req) {
        //dd($req);
        if (UserModel::login($req)) {
            \Session::put('instagram', $req->instagram);
            return redirect('ld/beli/'.$req->internal_code);
        } else {
            return redirect('user/login')->with(['error' => 'Username / password salah']);
        }
    }

    function loginUserCH(Request $req) {
        $blmurl='my-account';
        if($req->red=='rekapcheck'){
            $blmurl='rekap/check/';
        }
        if (UserModel::login($req)) {
            \Session::put('instagram', $req->instagram);
            return redirect($blmurl);
        } else {
            return redirect('user/login')->with(['error' => 'Username / password salah']);
        }
    }

    function showLoginPage(){
        $data = [
            'page_title' => "Login",
        ];
        
        return view('userpage/login');
    }


    function showLoginCartHomePage(Request $req){
        $data = [
            'page_title' => "Login",
            'red' => $req->red,
        ];
        return view('userpage/logincarthome')->with($data);
    }

    function showLDLoginPage(Request $req){
        $data = [
            'page_title' => "Login",
            'internal_code' => $req->internal_code,
        ];
        //dd($data);
        return view('userpage/ldlogin')->with($data);
    }

    function showProductDetailLD(Request $req) {
        \Session::put('reffer', 'ADS');
        if (empty(\Session::get('instagram'))) { return redirect('user/ldlogin/'.$req->internal_code); }

        $data = [
            'product' => UserModel::getProductByInternalCodeHaveStock($req->internal_code),
            'master' => UserModel::getMasterDataByInternalCode($req->internal_code),
            'page_title' => "Produk"
        ];
        
        //dd($data['product']);
        if(count($data['product'])==1){

            $data2 = [
                'internal_code' => $data['product'][0]->internal_code,
                'variant' => $data['product'][0]->variant,
                'size' => $data['product'][0]->size,
                'qty' => '1',
                'instagram' => \Session::get('instagram'),
                'reffer' => \Session::get('reffer'),
                '_token' => $req->session()->get('_token'),
            ];




            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => url('api/add-to-cartld'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => strtoupper('POST'),
                CURLOPT_POSTFIELDS => json_encode($data2),
                CURLOPT_HTTPHEADER => array(
                    //"content-type: application/x-www-form-urlencoded"
                    'Content-Type: application/json'
                ),
            ));
            
            $response = curl_exec($curl);
            
            $err = curl_error($curl);
            
            curl_close($curl);
            
            $jdc = json_decode($response);
            
            if ($jdc->success) {
                
                return redirect('rekap/ldcheck');
            } else {
                return redirect('user/ldlogin/'.$req->internal_code);
            }





        } else {
            return view('userpage/ldstock-detail')->with($data);
        }
        
    }

    public function showRegisterLDForm(Request $req) {
        $data = [
            'page_title' => "Registrasi",
            'internal_code' => $req->internal_code,
        ];
        return view('userpage/ldregister')->with($data);
    }


    public function registerLDUser(Request $req) {
        if(substr($req->telepon2,0,1)=="0"){
            $req->telepon= $req->kodearea .  substr($req->telepon2,1,strlen($req->telepon2)-1);
        } else {
            $req->telepon= $req->kodearea .  $req->telepon2;
        }
        $req->instagram=$req->telepon;
        $custdata = UserModel::getCustomerDataByInstagram($req->instagram);
        
        if (isset($custdata)) {
            return redirect('user/ldregister/'.$req->internal_code)->with(['error' => 'Username / Telepon sudah terdaftar']);
        } else {
            
            
            if (UserModel::registerUserLD($req)) {
                \Session::put('instagram', $req->instagram);
                return redirect('ld/beli/'.$req->internal_code)->with(['success' => 'Berhasil mendaftarkan user']);
            } else {
                return redirect('user/ldregister/'.$req->internal_code)->with(['error' => 'Gagal Mendaftarkan User']);
            }
        }
    }

    public function showLDCustomerOrder() {
        if (empty(\Session::get('instagram'))) { return redirect('user/ldlogin/'.$req->internal_code); }
        $data = [
            'discount' => UserModel::getDiscount(),
            'cart' => UserModel::getOrderByParameterV2(Session::get('instagram'), date('Y-m-d')),
            'stock' => UserModel::getStock(),
            'custdata' => UserModel::getCustomerDataByInstagram(Session::get('instagram')),
            'tanggal' => date('Y-m-d'),
            'addresses' => UserModel::getShippingAddress(Session::get('instagram')),
            'supplier' => UserModel::getSupplier(),
            'master' => $this->getMasterDataArray(),
            'page_title' => "Keranjang Belanja"
        ];
        return view('userpage/ldcart')->with($data);
    }
    
    function showPaidOrderDetail(Request $req) {
        
        if (empty(\Session::get('instagram'))) { return redirect('user/login/'); }
        $data = [
            'item_order' => UserModel::getPaidOrderByPaymentID($req->order_id),
            'stock' => UserModel::getAllStock(),
            'product_main' => UserModel::getProductMainData(),
            'order_id' => $req->order_id,
            'payment' => UserModel::getPaymentById($req->order_id),
        ];
        
        return view('userpage/list-paid-item')->with($data);
    }

    public function cancelLDOrderItem(Request $req) {
        if (UserModel::cancelOrderItem(Session::get('instagram'), $req->internal_code)) {
            return redirect('rekap/ldcheck');
        }
    }

    function payNowToMidtrans(Request $req) {
        $userdata = UserModel::getUser(\Session::get('instagram'));
        $paymentdata = UserModel::getPaymentDataByID($req->payment_id);

        $params = array(
            'transaction_details' => array(
                'order_id' => $paymentdata->id,
                'gross_amount' => $paymentdata->amount,
            ),
            "custom_field1" => $userdata->username,
            "custom_field2" => $paymentdata->order_date,
            "custom_field3" => $paymentdata->shipping_address,
            'customer_details' => array(
                'first_name' => $userdata->firstname,
                'last_name' => $userdata->lastname,
                'email' => $userdata->email,
                'phone' => $userdata->phone,
                'billing_address'  => $paymentdata->shipping_address,
                'shipping_address' => $paymentdata->shipping_address
            ),
            'callbacks' => array(
                'finish' => url('payment/callback'),
            ),
        );

        try {
          // Get Snap Payment Page URL
            $paymentUrl = \Midtrans\Snap::createTransaction($params)->redirect_url;
          // Redirect to Snap Payment Page
            return redirect($paymentUrl);
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    // function postAjaxAddToCart(Request $req) {
    //     if (UserModel::addToCart($req)) {
    //         echo "success";
    //     } else {
    //         echo "failed";
    //     }
    // }

    function postAjaxAddToSessionCart(Request $req) {
        $cartdata = \Session::get('cartdata');
        if (!isset($cartdata)) {
            $cartdata = [];
        }
        $newcartdata = [
            'internal_code' => $req->internal_code,
            'variant' => $req->variant,
            'size' => $req->size,
            'qty' => $req->qty,
        ];
        array_push($cartdata, $newcartdata);
        \Session::put('cartdata', $cartdata);
    }

    function ajaxGetCartQty() {
        $cartdata = \Session::get('cartdata');
        $qty_total = 0;
        if (!empty($cartdata)) {
            foreach ($cartdata as $row) {
                $qty_total += (int) $row['qty'];
            }
        }
        echo $qty_total;
    }

    function updateShippingCost() {
        $addressdata = UserModel::getAllSubdistrictID();
        foreach ($addressdata as $row) {
            $subdistrict_id = $row->subdistrict_id;
            try {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "origin=155&originType=city&destination=".$subdistrict_id."&destinationType=subdistrict&weight=1000&courier=ide",
                    CURLOPT_HTTPHEADER => array(
                        "content-type: application/x-www-form-urlencoded",
                        "key: ".config('rajaongkir.API_KEY'),
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    $result = json_decode($response);
                    $ongkir = $result->rajaongkir->results[0]->costs[0]->cost[0]->value;
                    if (UserModel::updateShippingCost($subdistrict_id, $ongkir)) {
                        echo "Ongkir tujuan : ".$subdistrict_id." -> ".$ongkir;
                    }
                }
            } catch (\Exception $e) {
                
            }
        }
    }

    function showRekapPage() {
        if (!empty(\Session::get('instagram'))) {
            return redirect('rekap/check');
        } else {
            $data = [
                'page_title' => "Cek Pesanan"
            ];
            return view('userpage/step-1')->with($data);
        }
    }

    function showForgotPasswordForm() {
        $data = [
            'page_title' => 'Lupa Password',
        ];
        return view('userpage/forgot-password')->with($data);
    }

    function submitForgotPassword(Request $req) {
        //dd($req->instagram);
        $userdata = UserModel::getCustomerFromInstagram($req->instagram);
        $data = [
            'instagram' => $req->instagram,
            'userdata' => $userdata,
            'page_title' => "Lupa Password",
        ];
        if (isset($userdata)) {
            if ($this->sendResetPasswordEmail($userdata)) {
                //dd($userdata);
                $this->sendAWBWhatsapp($userdata->phone,'Klik disini untuk reset password akun MomAvel.id Anda:' . PHP_EOL . PHP_EOL . url('user/reset-password')."/".base64_encode($userdata->username.'|'.$userdata->password) . PHP_EOL . PHP_EOL . 'Apabila anda tidak meminta reset password ini mohon abaikan pesan ini, dan jangan berikan kepada siapapun.' . PHP_EOL . 'HATI-HATI PENIUPUAN, Momavel tidak akan pernah menanyakan password dan kode anda.');
                return view('userpage/forgot-password-2')->with($data);
            } else {
                //return view('userpage/forgot-password')->with(['error' => 'Gagal Mengirim Email Verifikasi. Silahkan Coba Lagi.']);
                return view('userpage/forgot-password')->with(['error', 'Gagal Mengirim Email Verifikasi. Silahkan Coba Lagi.']);
            }
        }else {
            //return view('userpage/forgot-password')->with(['error' => 'Akun tidak terdaftar. Silahkan lakukan registrasi ulang.']);
            return view('userpage/forgot-password')->with('error' , 'Akun ini tidak terdaftar, silahkan lakukan registrasi.');
        }

        // return view('emails/reset-password')->with($data);
    }

    function sendResetPasswordEmail($userdata) {
        $data = [
            'userdata' => $userdata,
        ];
        \Mail::send('emails.reset-password', $data, function ($message) use ($userdata) {
            $message->from('info@momavel.id', 'MomAvel.id');
            $message->to($userdata->email);
            $message->subject('Konfirmasi Reset Password MomAvel.id');
        });
        if (\Mail::failures()) {
            return false;
        } else {
            return true;
        }
    }

    function showResetPasswordForm(Request $req) {
        $key = explode("|", base64_decode($req->encryptedkey));
        $userdata = UserModel::getCustomerDataFromParam($key[0], $key[1]);
        if (isset($userdata)) {
            $data = [
                'userdata' => $userdata,
                'page_title' => "Reset Password",
            ];
            return view('userpage/reset-password')->with($data);
        } else {
            echo "Encryption key tidak cocok";
        }
    }

    function submitResetPassword(Request $req) {
        if (UserModel::submitResetPassword($req)) {
            \Session::put('instagram', $req->instagram);
            return redirect('my-account')->with(['success' => 'Password berhasil di reset']);
        } else {
            return redirect('/')->with(['error' => 'Gagal reset password']);
        }
    }

    function submitChangePhone(Request $req) {
        if (UserModel::submitPhoneChange($req)) {
            
            return redirect('my-account')->with(['success' => 'Berhasil menyimpan telepon baru.']);
        } else {
            return redirect('/')->with(['error' => 'Gagal simpan telepon.']);
        }
    }
    
    function cancelPayment(Request $req) {
        $custdata = UserModel::getPaymentById($req->payment_id);
        //dd($custdata);
        
        $order_list = UserModel::getPaidOrderByPaymentID($req->payment_id);
        
        foreach ($order_list as $row) {
            
            if (UserModel::returnToUnpaidOrder($row, $custdata)) {
                if( UserModel::returnToStock($row)) {
                    UserModel::deletePaidOrderList($row, $req->payment_id);
                }
            }
        }
        if (UserModel::deletePayment($req->payment_id)) {
            UserModel::addSystemLog('Delete Pending Payment '.$req->payment_id);
            //$this->sendAWBWhatsapp('628119994888','Username : ' . $custdata->instagram . ' Telp : ' .$custdata->phone . PHP_EOL. 'Order ID : ' . $custdata->id . ' Nominal : ' . number_format($custdata->amount) . PHP_EOL . 'MELAKUKAN RESET PEMBAYARAN MIDTRANS PENDING (' . $custdata->reffer . ')');
            $this->sendAWBWhatsapp($custdata->phone,'Pembayaran Momavel Midtrans Order ID : ' . $custdata->id  . ' Nominal : ' . number_format($custdata->amount) . ' anda telah di reset.'. PHP_EOL . 'Silahkan lakukan pembayaran kembali dengan mengetik -BAYAR atau login melalui website https://momavel.id/rekap/check');
            $this->sendWAToADMIN('Username : ' . $custdata->instagram . ' Telp : ' .$custdata->phone . PHP_EOL. 'Order ID : ' . $custdata->id . ' Nominal : ' . number_format($custdata->amount) . PHP_EOL . 'MELAKUKAN RESET PEMBAYARAN MIDTRANS PENDING (' . $custdata->reffer . ')');
            return redirect('rekap/check')->with(['success' => 'Berhasil Menghapus Data']);
        } else {
            return redirect('my-account')->with(['error' => 'Gagal Menghapus Data']);
        }
    }

    function submitAddCustomerAddress(Request $req) {
        if (UserModel::insertCustomerAddress($req)) {
            return redirect($req->callbackURL)->with(['success' => 'Berhasil Menambah Alamat']);
        } else {
            return redirect($req->callbackURL)->with(['error' => 'Gagal Menambah Alamat']);
        }
    }

    function deleteCustomerAddress(Request $req) {
        if (UserModel::deleteCustomerAddress($req->id)) {
            return redirect('my-account')->with(['success' => 'Berhasil Menghapus Alamat']);
        } else {
            return redirect('my-account')->with(['error' => 'Gagal Menghapus Alamat']);
        }
    }

    function showProductInCategory(Request $req) {
        
        if($req->category=="promo"){
            $masterdt = UserModel::getMasterDataByPromo($req->sort);
        } else {
            $masterdt = UserModel::getMasterDataByCategory($req->category, $req->sort);
        }
        //dd($masterdt);
        $data = [
            'master' => $masterdt,
            'stock' => UserModel::getAllStockAvailable(),
            'category_query' => $req->category,
            'page_title' => $req->category,
            'slides' => SlideshowModel::getSlides(),

        ];
        return view('userpage/stock-view')->with($data);
    }

    function showCart() {
        $data = [
            'master_data' => $this->getMasterDataArray(),
        ];
        return view('userpage/temp-cart')->with($data);
    }

    function saveTempCartToCart(Request $req) {
        $cartdata = \Session::get('cartdata');
        foreach ($cartdata as $row) {
            $status = UserModel::addToCartV2($row, $req->instagram);
        }
        if ($status) {
            \Session::forget('cartdata');
            return redirect('products')->with(['success' => 'Berhasil Menyimpan Pesanan']);
        } else {
            return redirect('cart')->with(['error' => 'Gagal Menyimpan Pesanan']);
        }
    }

    function directCart(Request $req) {
        
        Validator::make($req->all(), [
            'internal_code' => 'required|string',
            'variant' => 'required|string',
            'size' => 'required|string',
            'qty' => 'required|int|min:1',
        ])->validate();

        if (empty(Session::get('instagram'))) {
            $response = [
                'success' => false,
                'message' => 'Harap Login Tekan AKUN SAYA / KERANJANG Terlebih Dahulu',
            ];
            return response()->json($response, 403);
        }

        $LastOrder=UserModel::CheckLastOrder(Session::get('instagram'));
        date_default_timezone_set('Asia/Jakarta');
        $tgl=date('Y-m-d H:i:s');
        if(count($LastOrder)>0){
            $req->tgl=$LastOrder[0]->tanggal;
        } else {
            $req->tgl=$tgl;
        }
        
        if (UserModel::addToCartV3($req, Session::get('instagram'), $req->reffer)) {
            $response = [
                'success' => true,
                'message' => 'Add to cart berhasil',
            ];
            return response()->json($response, 200);
        } else {
            $response = [
                'success' => false,
                'message' => 'Error occured',
            ];
            return response()->json($response, 500);
        }
    }

    function directCartld(Request $req) {
        \Session::put('reffer', 'ADS');
        //return response()->json($req->all());
        Validator::make($req->all(), [
            'internal_code' => 'required|string',
            'variant' => 'required|string',
            'size' => 'required|string',
            'qty' => 'required|int|min:1',
        ])->validate();

        
        $LastOrder=UserModel::CheckLastOrder($req->instagram);
        date_default_timezone_set('Asia/Jakarta');
        $tgl=date('Y-m-d H:i:s');
        if(count($LastOrder)>0){
            $req->tgl=$LastOrder[0]->tanggal;
        } else {
            $req->tgl=$tgl;
        }

        if (UserModel::addToCartV3($req, $req->instagram,'ADS')) {
            $response = [
                'success' => true,
                'message' => 'Add to cart berhasil',
            ];
            return response()->json($response, 200);
        } else {
            $response = [
                'success' => false,
                'message' => 'Error occured',
            ];
            return response()->json($response, 500);
        }
    }
}
