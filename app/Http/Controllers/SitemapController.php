<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;

class SitemapController extends Controller
{
    function generateXMLSitemap() {
        date_default_timezone_set('Asia/Jakarta');
        
        // Start building the XML sitemap string
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;
        
        // Fetch products
        $products = UserModel::getMasterDataDesc();
        $xml .= '<url>'.PHP_EOL;
        $xml .= '<loc>'.url('/').'</loc>'.PHP_EOL;
        $xml .= '<priority>1.0</priority>' . PHP_EOL;
        $xml .= '<lastmod>' . date('c', strtotime($products[0]->register_time)) . '</lastmod>' . PHP_EOL;
        $xml .= '</url>' . PHP_EOL;
        $xml .= '<url>'.PHP_EOL;
        $xml .= '<loc>'.url('products').'</loc>'.PHP_EOL;
        $xml .= '<priority>0.9</priority>' . PHP_EOL;
        $xml .= '<lastmod>' . date('c', strtotime($products[0]->register_time)) . '</lastmod>' . PHP_EOL;
        $xml .= '</url>' . PHP_EOL;

        foreach ($products as $product) {
            $xml .= '<url>' . PHP_EOL;
            $xml .= '<loc>'.url('product').'/'.$product->internal_code.'</loc>' . PHP_EOL;
            $xml .= '<priority>0.8</priority>' . PHP_EOL;
            $xml .= '<lastmod>' . date('c', strtotime($product->register_time)) . '</lastmod>' . PHP_EOL;
            $xml .= '</url>' . PHP_EOL;
        }

        // Fetch categories
        $categories = UserModel::getAllCategory();

        foreach ($categories as $category) {
            $xml .= '<url>' . PHP_EOL;
            $xml .= '<loc>'.url('category').'/'.str_replace(' ', '%20', $category->category).'</loc>' . PHP_EOL;
            $xml .= '<priority>0.8</priority>' . PHP_EOL;
            $xml .= '<lastmod>' . date('c', strtotime($category->register_time)) . '</lastmod>' . PHP_EOL;
            $xml .= '</url>' . PHP_EOL;
        }

        // Adding register, login, and cart
        $xml .= '<url>' . PHP_EOL;
        $xml .= '<loc>'.url('user/registerch/my-account').'</loc>' . PHP_EOL;
        $xml .= '<priority>0.2</priority>' . PHP_EOL;
        $xml .= '<lastmod>' . date('c', strtotime($category->register_time)) . '</lastmod>' . PHP_EOL;
        $xml .= '</url>' . PHP_EOL;

        $xml .= '<url>' . PHP_EOL;
        $xml .= '<loc>'.url('user/logincarthome/my-account').'</loc>' . PHP_EOL;
        $xml .= '<priority>0.1</priority>' . PHP_EOL;
        $xml .= '<lastmod>' . date('c', strtotime($category->register_time)) . '</lastmod>' . PHP_EOL;
        $xml .= '</url>' . PHP_EOL;

        $xml .= '<url>' . PHP_EOL;
        $xml .= '<loc>'.url('user/logincarthome/rekapcheck').'</loc>' . PHP_EOL;
        $xml .= '<priority>0.1</priority>' . PHP_EOL;
        $xml .= '<lastmod>' . date('c', strtotime($category->register_time)) . '</lastmod>' . PHP_EOL;
        $xml .= '</url>' . PHP_EOL;

        // Close the XML sitemap string
        $xml .= '</urlset>' . PHP_EOL;
        
        // Set the HTTP response header to indicate that this is an XML file
        $headers = [
            'Content-type' => 'text/xml',
        ];
        
        // Return the XML sitemap as a Laravel Response object
        return response($xml, 200, $headers);
    }

}
