<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WhatsappModel;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;


class WhatsappController extends Controller
{
    public function receiveWebhook(Request $req) {
      $inito = explode('@',$req->sender);

      $data = [
            'sender' => $inito[0],
            'message' => $req->message . ' !',
        ];
      
      
        $validator = Validator::make($data, [
            'sender' => 'required|max:100',
            'message' => 'required',
        ]);

        if ($validator->fails() || $inito[0]=='status') {
            $response = [
                'status' => false,
                'message' => $validator->errors(),
            ];
            return response()->json($response, 400);
        }
		$masterstring = explode(' ', $req->message);
		

      
      $listgabung='';
      
             
      
      
      
      if(substr($req->message,0,1)=="-"){
          $headerdata = strtoupper(substr($masterstring[0],1,strlen($masterstring[0])-1));

          $statuss=WhatsappModel::checkPhoneAdded($inito[0]);
          if(count($statuss)==0){ WhatsappModel::AddPhoneNumber($inito[0]); }
        
 		   if($headerdata=='CARA'){
             
            
              $Informasi2='Selamat datang di momavel.id sistem otomatis, disini anda bisa melakukan pemesanan mandiri dan mendapatkan diskon sesuai dengan panduan. Semua perintah diawali dengan strip -'  .PHP_EOL.PHP_EOL.'Cara memesan melalui WA: ' . PHP_EOL . '   *-ORDER (spasi) Kode* Contoh: -Order D121 <-- Untuk Masukan Produk Ke Keranjang'. PHP_EOL . '   *-LIST* <-- Untuk Melihat Keranjang Anda ' .PHP_EOL .'   *-HAPUS (spasi) Kode* Contoh: -Hapus D121 <-- Untuk Hapus Produk dr Keranjang' .PHP_EOL .'   *-BAYAR* <-- Untuk Proses Checkout' .PHP_EOL .'   *-BAYARULANG* <-- Apabila Pembayaran gagal/ Lupa catat rekening dapat di ulang kembali'.PHP_EOL .'   *-RESETPASS (spasi) Pass Baru* <-- Untuk Merubah Password Login Web'.PHP_EOL .'   *-MASUKWEB* <-- Untuk Masuk Ke Web Login secara otomatis.' .PHP_EOL .'   *-DAFTARULANG* <-- Untuk daftar ulang apabila ada kesalahan data saat pengisian awal.';
              
              $this->sendMessageText( $inito[0],$Informasi2);
              return ;
            }
        
          if($headerdata=='DAFTAR'){
            $statusR1=WhatsappModel::checkPhoneRegistered($inito[0]);
            if(count($statusR1)>=1){ 
              $Informasi2='Selamat datang di momavel.id sistem otomatis, disini anda bisa melakukan pemesanan mandiri dan mendapatkan diskon sesuai dengan panduan. Semua perintah diawali dengan strip -'  .PHP_EOL.PHP_EOL.'Cara memesan melalui WA: ' . PHP_EOL . '   *-ORDER (spasi) Kode* Contoh: -Order D121 <-- Untuk Masukan Produk Ke Keranjang'. PHP_EOL . '   *-LIST* <-- Untuk Melihat Keranjang Anda ' .PHP_EOL .'   *-HAPUS (spasi) Kode* Contoh: -Hapus D121 <-- Untuk Hapus Produk dr Keranjang' .PHP_EOL .'   *-BAYAR* <-- Untuk Proses Checkout' .PHP_EOL .'   *-BAYARULANG* <-- Apabila Pembayaran gagal/ Lupa catat rekening dapat di ulang kembali'.PHP_EOL .'   *-RESETPASS (spasi) Pass Baru* <-- Untuk Merubah Password Login Web'.PHP_EOL .'   *-MASUKWEB* <-- Untuk Masuk Ke Web Login secara otomatis.' .PHP_EOL .'   *-DAFTARULANG* <-- Untuk daftar ulang apabila ada kesalahan data saat pengisian awal.' ;
              $listgabung='Anda Sudah Teregistrasi. '.PHP_EOL.PHP_EOL.$Informasi2;
              $this->sendMessageText( $inito[0],$listgabung);
              return ;
            }
            
            
            WhatsappModel::updateWhatsappBotStatus($inito[0],'ENTERFIRSTNAME');
			$this->sendMessageText( $inito[0],'Masukan Nama Depan Anda:');
			return;
          }
        
        if($headerdata=='DAFTARULANG'){
            $statusR1=WhatsappModel::checkPhoneRegistered($inito[0]);
            if(count($statusR1)>=1){ 
              WhatsappModel::deletePhoneRegistered($inito[0]);
              WhatsappModel::updateWhatsappBotStatus($inito[0],'ENTERFIRSTNAME');
              $this->sendMessageText( $inito[0],'Masukan Nama Depan Anda:');
              return;
              
            }
            
            WhatsappModel::updateWhatsappBotStatus($inito[0],'ENTERFIRSTNAME');
			$this->sendMessageText( $inito[0],'Masukan Nama Depan Anda:');
			return;
          }
     
          $statusR=WhatsappModel::checkPhoneRegistered($inito[0]);
          if(count($statusR)==0){ 
            $listgabung='Nomor Anda Belum Teregistrasi. '.PHP_EOL.'Silahkan ketik -DAFTAR untuk mendaftarkan diri '.PHP_EOL.'Jika ada waktu luang dapat browsing di website kami '.PHP_EOL.'Disana banyak produk yang bagus-bagus dan belum di live-kan.';
            $this->sendMessageText( $inito[0],$listgabung);
            return ;
          }

          if($headerdata=='LIST'){
             $listorderdata=WhatsappModel::getOrderByPhone($inito[0]);

             if(count($listorderdata) >0){
                 $listgabung='Berikut Adalah List Keranjang Anda:'.PHP_EOL.'(Jika ada masalah anda bisa checkout kapan saja dgn WA Ke 081283443452 )'.PHP_EOL;
                 foreach ($listorderdata as $row) {
                      $stockdataf = WhatsappModel::getPriceByParameter($row->internal_code,$row->variant,$row->size);
                          $listgabung = $listgabung . PHP_EOL . '*' . $row->internal_code . ' - ' . $row->variant . ' - ' . $row->size . '* - ' . $row->qty . ' pc(s)' . PHP_EOL .'Harga : ' . number_format($stockdataf->selling_price) . ' - Stock : ' . number_format($stockdataf->qty) . PHP_EOL . 'https://momavel.id/product/'.$row->internal_code .PHP_EOL;

                 }
             } else {
                 $listgabung='Tidak ada order di keranjang anda. '.PHP_EOL.'Anda juga bisa checkout mandiri di website https://momavel.id '.PHP_EOL.'Disana banyak produk yang bagus-bagus dan belum di live-kan.';
             }
             $this->sendMessageText( $inito[0],$listgabung);
            return;
          }
        
        if($headerdata=='BAYAR'){
             $listorderdata=WhatsappModel::getOrderByPhone($inito[0]);

             if(count($listorderdata) >0){
                 $listgabung='Berikut Adalah Perhitungan *BAYAR* Pesanan Anda:'.PHP_EOL.'(Jika ada masalah anda bisa checkout kapan saja dgn WA Ke 081283443452 )'.PHP_EOL.'Free Ongkir Hanya UNK DKI Jakarta sebesar 10rb unk setiap kelipatan 4 item. Di luar jakarta tetap mendapatkan subsidi Rp 10.000 / 4 pcs. *(PERHATIAN PER 1KG HANYA BISA MENAMPUNG 4 PCS)* Diskon Reseller berlaku 15% apabila pembelian lebih dari 1 juta rupiah.'.PHP_EOL;
                 $SubTotot=0;
                 $qtybeli=0;
                 foreach ($listorderdata as $row) {
                      $stockdataf = WhatsappModel::getPriceByParameter($row->internal_code,$row->variant,$row->size);
                      if ($stockdataf->qty>=$row->qty){
                         $Totot=($row->qty* $stockdataf->selling_price);
                        $qtybeli=$qtybeli+$row->qty;
                         $SubTotot=$SubTotot + $Totot;
                         
                          $listgabung = $listgabung . PHP_EOL . '*' . $row->internal_code . ' - ' . $row->variant . ' - ' . $row->size . '* - ' . $row->qty . ' pc(s)' . PHP_EOL .'Harga : ' . number_format($stockdataf->selling_price) . ' - Stock : ' . number_format($stockdataf->qty) . PHP_EOL . 'Sub Total : ' .  number_format($Totot) .' - Total : ' . number_format($SubTotot) .PHP_EOL . 'https://momavel.id/product/'.$row->internal_code .PHP_EOL;
                      } else {
                          $listgabung = $listgabung . PHP_EOL . '~*' . $row->internal_code . ' - ' . $row->variant . ' - ' . $row->size . '* - ' . $row->qty . ' pc(s)~' . PHP_EOL .'~Harga : ' . number_format($stockdataf->selling_price) . ' - Stock : ' . number_format($stockdataf->qty) . '~'. PHP_EOL . '~Sub Total : 0 - Total : ' .number_format($SubTotot) . '~' . PHP_EOL .'https://momavel.id/product/'.$row->internal_code .PHP_EOL . '_{Tidak Bisa Di Checkout Stok Tdk Ada. Pembayaran bs d lakukan tanpa item ini.}_' . PHP_EOL ;
                      }
                 }
               
                 
                 $diskonstring='';
               	 if($SubTotot>=1000000){
                   
                   $diskonwa=WhatsappModel::getDiscountWA();
               	   $potongan=(($diskonwa->percentwa/100)*$SubTotot);
                   $diskonstring = 'Diskon Reseller(Min 1jt): ' . $diskonwa->percentwa . '% : ' . number_format($potongan) .PHP_EOL;
                 } else {
                   $diskonwa=0;
                   $potongan=0;
                   $diskonstring='';
                 }
                 
               	 
               	 $shipcost=WhatsappModel::getShippingCost($inito[0]);
                 $beratpengiriman = ceil($qtybeli / 4);
                 $totalbiayakirim = $beratpengiriman * $shipcost->shipping_cost;
                 $gratis_ongkir = (ceil($qtybeli / 3.5)-1) * 10000;
                 $grandtotalbayar = ($SubTotot-$potongan)+$totalbiayakirim-$gratis_ongkir;
               
                 $listgabung=$listgabung .PHP_EOL . PHP_EOL . 'Total Produk : (' . $qtybeli . ') - ' . number_format($SubTotot) . PHP_EOL . $diskonstring . 'Total - Diskon : ' . number_format($SubTotot - $potongan).PHP_EOL . 'Berat Pengiriman : ' . $beratpengiriman .' kg'  .PHP_EOL . 'Biaya Kirim Per Kg :' . number_format($shipcost->shipping_cost) .PHP_EOL . 'Total Biaya Kirim :' . number_format($totalbiayakirim) .PHP_EOL . '_Free Ongkir / Subsidi Ongkir : ' . number_format($gratis_ongkir) . '_'. PHP_EOL.'*GRAND TOTAL BAYAR : ' . number_format($grandtotalbayar) . '*' . PHP_EOL.PHP_EOL. 'MAU BAYAR SEKARANG? KETIK YA APABILA MAU MELANJUTKAN';
               
                 WhatsappModel::updateWhatsappTotalBayar($inito[0],$grandtotalbayar);
                 $this->sendMessageText( $inito[0],$listgabung);
            	 return;
             } else {
                 $listgabung='Tidak ada order di keranjang anda untuk di bayar. '.PHP_EOL.'Anda juga bisa checkout mandiri di website https://momavel.id '.PHP_EOL.'Disana banyak produk yang bagus-bagus dan belum di live-kan.';
               	 $this->sendMessageText( $inito[0],$listgabung);
            	 return;
             }
             
          }
        
        if($headerdata=='BAYARULANG'){
          	$listpaymentid=WhatsappModel::getPaymentIDList($inito[0]);
			$paymentids='';
            
             if(count($listpaymentid) >0){ 
          			foreach($listpaymentid as $row){
                      $paymentids=$paymentids .' ' .$row->id;
                      
                    }
               		//$this->sendMessageText( $inito[0],'Payment ID : '.strval($paymentids) .' berhasil di request ulang. Silahkan lakukan kembali pembayaran dengan mengetik -BAYAR atau bisa login ke website untuk checkout');
               		foreach($listpaymentid as $row){
                      $filecont=file_get_contents(url('user-cancel-payment').'/'.$row->id);
                    }
             } else {
                $this->sendMessageText( $inito[0],'Tidak ada pending payment di akun anda. Silahkan lakukan pembayaran.');
             }
             //$this->sendMessageText( $inito[0],strval($listpaymentid));
             //$this->sendMessageText( $inito[0],'FITUR INI BELUM TERSEDIA, Anda bisa mengulang pembayaran dari my-account di website momavel.id atau bisa menghubungi CS kami di 081283443452');
            	 return;
          }
        
        if($headerdata=='RESETPASS'){
      		if(WhatsappModel::UpdatePassword($inito[0],$masterstring[1])){ 

          		$this->sendMessageText( $inito[0],'Perubahan Password Berhasil Di Lakukan');
             } else {
                $this->sendMessageText( $inito[0],'Perubahan Password Gagal Di Lakukan');
             }
             //$this->sendMessageText( $inito[0],strval($listpaymentid));
             //$this->sendMessageText( $inito[0],'FITUR INI BELUM TERSEDIA, Anda bisa mengulang pembayaran dari my-account di website momavel.id atau bisa menghubungi CS kami di 081283443452');
            	 return;
       
          }
        
        if($headerdata=='MASUKWEB'){
          
				$WhatsappStatuss=WhatsappModel::CheckWhatsappBotStatus($inito[0]);
      		$infopembayaran ='Silahkan klik link di bawah untuk masuk ke web tekan CEK STOK untuk melihat etalase kami :'.PHP_EOL.PHP_EOL.'https://momavel.id/usercart/login/' .$inito[0]. '/'.$WhatsappStatuss->password.PHP_EOL.'Anda juga bisa melakukan pembelanjaan melalui website, dapat melihat secara jelas semua detail stok kami.';
                   
                  
                 	//WhatsappModel::updateWhatsappBotFirstName($inito[0],$req->message);
                    $this->sendMessageText( $inito[0],$infopembayaran);
            	 return;
          }

          if($headerdata=='HAPUS'){
            WhatsappModel::deleteOrder($inito[0],strtoupper($masterstring[1]));
            
            
            $listorderdata=WhatsappModel::getOrderByPhone($inito[0]);

             if(count($listorderdata) >0){
                 $listgabung='Berikut Adalah List Keranjang Anda:'.PHP_EOL.'(Jika ada masalah anda bisa checkout kapan saja dgn WA Ke 081283443452 )'.PHP_EOL;
                 foreach ($listorderdata as $row) {
                      $stockdataf = WhatsappModel::getPriceByParameter($row->internal_code,$row->variant,$row->size);
                          $listgabung = $listgabung . PHP_EOL . '*' . $row->internal_code . ' - ' . $row->variant . ' - ' . $row->size . '* - ' . $row->qty . ' pc(s)' . PHP_EOL .'Harga : ' . number_format($stockdataf->selling_price) . ' - Stock : ' . number_format($stockdataf->qty) . PHP_EOL . 'https://momavel.id/product/'.$row->internal_code .PHP_EOL;

                 }
             } else {
                 $listgabung='Tidak ada order di keranjang anda. '.PHP_EOL.'Anda juga bisa checkout mandiri di website https://momavel.id '.PHP_EOL.'Disana banyak produk yang bagus-bagus dan belum di live-kan.';
             }
             //$this->sendMessageText( $inito[0],$listgabung);
             $this->sendMessageText( $inito[0],'Perintah Hapus - ' . strtoupper($masterstring[1]) . ' Telah Di Lakukan' . PHP_EOL . PHP_EOL. $listgabung );
            
            return;
          }
		 
        
        if($headerdata=='ORDER'){
             
             $variantlist=WhatsappModel::getVariantbyInternalCode(strtoupper($masterstring[1]));

             if(count($variantlist) >0){
                 $variantjembreng='Berikut Adalah List Variant Yang Tersedia:'.PHP_EOL.'(Jika ada masalah anda bisa checkout kapan saja dgn WA Ke 081283443452 )'.PHP_EOL.'Lihat Variant dan Warna disini : ' . PHP_EOL.   'https://momavel.id/product/'.strtoupper($masterstring[1]).PHP_EOL ;
                 $variantjembreng = $variantjembreng . PHP_EOL . 'PILIH NOMOR VARIANT DI BAWAH INI ';
                 $variantcounter=0;
                 $variantindex='';
                 foreach ($variantlist as $row) {
                      //$stockdataf = WhatsappModel::getPriceByParameter($row->internal_code,$row->variant,$row->size);
                          $variantcounter=$variantcounter+1;
                          $variantjembreng = $variantjembreng . PHP_EOL . $variantcounter .' : ' . $row->variant  ;
                          $variantindex = $variantindex . $variantcounter .'~'. $row->variant . '|' ;

                 }
                //$variantindex= $variantcounter .'%' . $variantindex;
               WhatsappModel::updateWhatsappBotVariant($inito[0],strtoupper($masterstring[1]),$variantindex);
            	//$this->sendMessageText( $inito[0],'Masukan Nama Belakang Anda:');
               $this->sendMessageText( $inito[0],$variantjembreng);
            	return;
             } else {
                 $variantinfo='Kode Produk *' . strtoupper($masterstring[1]) . '* Habis / Tidak Di Temukan ' .PHP_EOL.'Anda juga bisa browsing kode di https://momavel.id '.PHP_EOL.'Lalu bisa memesan sesuai kode yang ready stock';
               $this->sendMessageText( $inito[0],$variantinfo);
               return;
             }
             
          }
       	 
      } 
        
        
        
      
      
      
      
      
      
      $WhatsappStatus=WhatsappModel::CheckWhatsappBotStatus($inito[0]);
          
      	
          if ($WhatsappStatus->status=='READYPAYMENT'){
                if(strtoupper($req->message)=='YA'){
                  
                   $infopembayaran ='Silahkan klik link di bawah dan lakukan pembayaran sesuai dengan metode pembayaran yang di pilih.:'.PHP_EOL.PHP_EOL.'_APABILA ANDA MELAKUKAN KESALAHAN, DAN LUPA ATAU SALAH MEMILIH METODE PEMBAYARAN KETIK_ *-BAYARULANG* _anda akan kembali mendapatkan link pembayaran._'.PHP_EOL.PHP_EOL.'https://momavel.id/usercart/login/' .$inito[0]. '/'.$WhatsappStatus->password;
                   

                  
                 	//WhatsappModel::updateWhatsappBotFirstName($inito[0],$req->message);
                    $this->sendMessageText( $inito[0],$infopembayaran);
                    //$this->sendMessageText( $inito[0],$req->message);
                    return;
                } else {
                  
                    $this->sendMessageText( $inito[0],'Anda Tidak Memasukan Entri yang di minta. Ketik YA jika ingin melanjutkan pembayaran.');
                    //$this->sendMessageText( $inito[0],$req->message);
                    return;
                }
 
          }
          if ($WhatsappStatus->status=='ENTERFIRSTNAME'){
            	WhatsappModel::updateWhatsappBotFirstName($inito[0],$req->message);
            	$this->sendMessageText( $inito[0],'Masukan Nama Belakang Anda:');
            	//$this->sendMessageText( $inito[0],$req->message);
                return;
          }
          if ($WhatsappStatus->status=='ENTERLASTNAME'){
            	WhatsappModel::updateWhatsappBotLastName($inito[0],$req->message);
            	$this->sendMessageText( $inito[0],'Masukan Nama Email Anda:');
            	//$this->sendMessageText( $inito[0],$req->message);
                return;
          }  
        if ($WhatsappStatus->status=='SELECTVARIANT'){
          		$VariantSelect='-';
                $varlist=explode('|',$WhatsappStatus->variant_list);
          		if(count($varlist)-1 == 0){
                  	$this->sendMessageText( $inito[0],'Data Tidak Di Temukan, Silahkan Ulangi Pesanan Anda Dengan Cara Ketik -ORDER (KODE).');
                	return;
                } else {
                   
                   for ($i=0; $i < count($varlist); $i++){
                       $pecahvariant = explode('~',$varlist[$i]);
                       if ($req->message == $pecahvariant[0]){
                       		$VariantSelect = $pecahvariant[1];
                       }
                   }
                }
          		
                if($VariantSelect=='-'){
                	$this->sendMessageText( $inito[0],'Variant Tidak Di Temukan, Silahkan Ulangi Pesanan Anda Dengan Cara Ketik -ORDER (KODE).');
                	return;
                } else {
                      $Selection ='Anda Telah Memilih : ' . $VariantSelect . PHP_EOL . PHP_EOL;

                       $sizelist=WhatsappModel::getSize(strtoupper($WhatsappStatus->internal_code),$VariantSelect);
                  
                       $sizejembreng='Berikut Adalah List Size Yang Tersedia:'.PHP_EOL.'(Jika ada masalah anda bisa checkout kapan saja dgn WA Ke 081283443452 )'.PHP_EOL;
                       $sizejembreng = $sizejembreng . PHP_EOL . 'PILIH NOMOR SIZE DI BAWAH INI ';
                       $sizecounter=0;
                       $sizeindex='';
                  		
                       foreach ($sizelist as $row) {
                            //$stockdataf = WhatsappModel::getPriceByParameter($row->internal_code,$row->variant,$row->size);
                                $sizecounter=$sizecounter+1;
                                $sizejembreng = $sizejembreng . PHP_EOL . $sizecounter .' : ' . $row->size . ' (Sisa Stok : ' . $row->qty . ')' ;
                                $sizeindex = $sizeindex . $sizecounter .'~'. $row->size . '$' . $row->qty . '|' ;

                       }
                  	
                      $Selection = $Selection . $sizejembreng;
						
                      WhatsappModel::updateWhatsappBotSize($inito[0],$sizeindex,$VariantSelect);
                      $this->sendMessageText( $inito[0],$Selection);
                      return;
                }
          }   
      
      
      if ($WhatsappStatus->status=='ENTERSIZE'){
          		$SizeSelect='-';
                $sizelist=explode('|',$WhatsappStatus->size_list);
          		if(count($sizelist)-1 == 0){
                  	$this->sendMessageText( $inito[0],'Data Tidak Di Temukan, Silahkan Ulangi Pesanan Anda Dengan Cara Ketik -ORDER (KODE).');
                	return;
                } else {
                   
                   for ($i=0; $i < count($sizelist)-1; $i++){
                       $pecahsize = explode('~',$sizelist[$i]);
                     
                       $pecahqty = explode('$',$pecahsize[1]);

                       if ($req->message == $pecahsize[0]){
                       		$SizeSelect = $pecahqty[0];
                       }
                                          
                   }

                  
                }
          		
                if($SizeSelect=='-'){
                	$this->sendMessageText( $inito[0],'Size Tidak Di Temukan, Silahkan Ulangi Pesanan Anda Dengan Cara Ketik -ORDER (KODE).');
                	return;
                } else {
               		  
                      WhatsappModel::updateWhatsappBotSizeSelect($inito[0],$SizeSelect,$pecahqty[1]);
                      $this->sendMessageText( $inito[0],'Anda Memilih : ' . $SizeSelect . PHP_EOL .PHP_EOL .'Silahkan masukan Jumlah Pesanan (hanya angka 1-' . $pecahqty[1] .  '):');
                      return;
                }
          }   
      
        if ($WhatsappStatus->status=='ENTERQUANTITY'){
          		          
                if($req->message>0 &&$req->message<=50 ){
                  	  
                  	  if($WhatsappStatus->qtyav>=$req->message){
                        WhatsappModel::updateWhatsappBotQty($inito[0],$req->message);
                      	$this->sendMessageText( $inito[0],'Anda Membeli Sebanyak : ' . $req->message .' pc(s)'.PHP_EOL.PHP_EOL. 'Silahkan konfirmasi pesanan anda:'.PHP_EOL. 'Kode:'.$WhatsappStatus->internal_code.PHP_EOL. 'Variant:'.$WhatsappStatus->variant_select.PHP_EOL. 'Size:'.$WhatsappStatus->size_select.PHP_EOL. 'Jumlah:'.$req->message.PHP_EOL.PHP_EOL.'Ketik *YA* untuk input order.'.PHP_EOL.'Ketik *-ORDER (Kode)* untuk ulangi order.'.PHP_EOL.'Ketik *-LIST* untuk cek keranjang.');
                      	return;
                      } else {
                      	$this->sendMessageText( $inito[0],'Anda memasuki jumlah tidak sesuai stok. Stok saat ini : ' . strval($WhatsappStatus->qtyav) .' pc(s)'.PHP_EOL.PHP_EOL. 'Silahkan masukan ulang jumlah pembelian maksimal adalah jumlah stok tersedia:');
                      	return;
                      }
                } else {
               
                    $this->sendMessageText( $inito[0],'Format jumlah hanya bisa angka 1-50 tanpa spasi. Mohon ulangi Jumlah:');
                	return;
                }
          } 
      
        if ($WhatsappStatus->status=='ENTERORDCONF'){
          		          
                if(strtoupper($req->message)=='YA' ){
                  
                  	  	$LastOrder=WhatsappModel::CheckLastOrder($inito[0]);
                  		date_default_timezone_set('Asia/Jakarta');
                  		$tgl=date('Y-m-d H:i:s');
                  		if(count($LastOrder)>0){
                            //$tgl = $this->sendMessageText( $inito[0],$LastOrder[0]->tanggal);
                          	
                            if(WhatsappModel::insertOrder($inito[0],$WhatsappStatus->internal_code,$WhatsappStatus->qty,$WhatsappStatus->size_select,$WhatsappStatus->variant_select,$LastOrder[0]->tanggal)){
                               //$this->sendMessageText( $inito[0],'Penyimpanan Berhasil. Silahkan cek keranjang anda dengan ketik -LIST');
                              
                              
                              $listorderdata=WhatsappModel::getOrderByPhone($inito[0]);
								
                               if(count($listorderdata) >0){
                                 $listgabung='Penyimpanan Berhasil.'.PHP_EOL.PHP_EOL;
                                   $listgabung=$listgabung.'Berikut Adalah List Keranjang Anda:'.PHP_EOL.'(Jika ada masalah anda bisa checkout kapan saja dgn WA Ke 081283443452 )'.PHP_EOL;
                                   foreach ($listorderdata as $row) {
                                        $stockdataf = WhatsappModel::getPriceByParameter($row->internal_code,$row->variant,$row->size);
                                            $listgabung = $listgabung . PHP_EOL . '*' . $row->internal_code . ' - ' . $row->variant . ' - ' . $row->size . '* - ' . $row->qty . ' pc(s)' . PHP_EOL .'Harga : ' . number_format($stockdataf->selling_price) . ' - Stock : ' . number_format($stockdataf->qty) . PHP_EOL . 'https://momavel.id/product/'.$row->internal_code .PHP_EOL;

                                   }
                               }
                              $this->sendMessageText( $inito[0],$listgabung);
                              WhatsappModel::clearWhatsappBot($inito[0]);
                               return;
								
                            } else {
                               $this->sendMessageText( $inito[0],'Ada Masalah Penyimpanan, silahkan ulangi order dengan cara ketik -ORDER (KODE)');
                               return;
                            }
                            
                        } else {
                          	
                         	 $tgl=date('Y-m-d H:i:s');
                            
                          	if(WhatsappModel::insertOrder($inito[0],$WhatsappStatus->internal_code,$WhatsappStatus->qty,$WhatsappStatus->size_select,$WhatsappStatus->variant_select,$tgl)){
                              //$this->sendMessageText( $inito[0],'Penyimpanan Berhasil. Silahkan cek keranjang anda dengan ketik -LIST');
                               $listorderdata=WhatsappModel::getOrderByPhone($inito[0]);
								
                               if(count($listorderdata) >0){
                                 $listgabung='Penyimpanan Berhasil.'.PHP_EOL.PHP_EOL;
                                   $listgabung=$listgabung.'Berikut Adalah List Keranjang Anda:'.PHP_EOL.'(Jika ada masalah anda bisa checkout kapan saja dgn WA Ke 081283443452 )'.PHP_EOL;
                                   foreach ($listorderdata as $row) {
                                        $stockdataf = WhatsappModel::getPriceByParameter($row->internal_code,$row->variant,$row->size);
                                            $listgabung = $listgabung . PHP_EOL . '*' . $row->internal_code . ' - ' . $row->variant . ' - ' . $row->size . '* - ' . $row->qty . ' pc(s)' . PHP_EOL .'Harga : ' . number_format($stockdataf->selling_price) . ' - Stock : ' . number_format($stockdataf->qty) . PHP_EOL . 'https://momavel.id/product/'.$row->internal_code .PHP_EOL;

                                   }
                               }
                              $this->sendMessageText( $inito[0],$listgabung);
                              WhatsappModel::clearWhatsappBot($inito[0]);
                               return;
                            } else {
                              $this->sendMessageText( $inito[0],'Ada Masalah Penyimpanan, silahkan ulangi order dengan cara ketik -ORDER (KODE)');
                               return;
                            }
                            return;
                        }
                } else {
                		$this->sendMessageText( $inito[0],'Anda tidak memasukan entry yang di minta. Ketik YA atau -ORDER (KODE).');
                      	return;
                }
          } 
      
      
      
      
      
      
      

      
      
      
          if ($WhatsappStatus->status=='ENTEREMAIL'){
            	//WhatsappModel::updateWhatsappBotEmail($inito[0],$req->message);
            	//$dataProvince = $client->get('https://pro.rajaongkir.com/api/province');
            	$dataProvince= $this->apiGateway('https://pro.rajaongkir.com/api/province');
                $dataProvinceList = json_decode($dataProvince)->rajaongkir->results;
                $ProvinceList='Mohon Memasukan Data Yang Akurat Unk Pengiriman Yang Akurat.' . PHP_EOL . '*Silahkan Pilih Provinsi:*';
                foreach ($dataProvinceList as $row){
                    $ProvinceList=$ProvinceList .PHP_EOL.$row->province_id . ' : ' . $row->province;
                }
            
            	WhatsappModel::updateWhatsappBotEmail($inito[0],strtolower($req->message));
            	$this->sendMessageText( $inito[0],$ProvinceList);
            	//$this->sendMessageText( $inito[0],$req->message);
               return;
          }       
      	  if ($WhatsappStatus->status=='ENTERPROVINCE'){
            	//WhatsappModel::updateWhatsappBotEmail($inito[0],$req->message);
            	//$dataProvince = $client->get('https://pro.rajaongkir.com/api/province');
            	$dataProvince= $this->apiGateway('https://pro.rajaongkir.com/api/province');
                $dataProvinceList = json_decode($dataProvince)->rajaongkir->results;
                
            	$ProvinceIDSelected='0';
            	$ProvinceSelected = '-';
            
                $ProvinceList='Anda tidak memilih apa yang di minta, mohon memilih dengan memasukan angka sesuai provinsi anda.' . PHP_EOL . '*Silahkan Pilih Provinsi:*';
                foreach ($dataProvinceList as $row){
                    $ProvinceList=$ProvinceList .PHP_EOL.$row->province_id . ' : ' . $row->province;
                    if( $row->province_id == $req->message) {
                     	 $ProvinceSelected = $row->province;
                      	 $ProvinceIDSelected=$row->province_id;
                    }
                }
                
                if ($ProvinceSelected == '-'){
                	$this->sendMessageText( $inito[0],$ProvinceList);
                    return;
                } else {
                  
                  	$CityList='*Silahkan Pilih Kota/Kabupaten:*';
                    $dataCity= $this->apiGateway('https://pro.rajaongkir.com/api/city?province=' . $ProvinceIDSelected);
                	$dataCityList = json_decode($dataCity)->rajaongkir->results;
                    
                    foreach ($dataCityList as $row){
                         $CityList = $CityList . PHP_EOL . $row->city_id . ' : ' . $row->city_name ;
                    }
                  
                    WhatsappModel::updateWhatsappBotProvinsi($inito[0],$ProvinceSelected,$ProvinceIDSelected);
                	$this->sendMessageText( $inito[0],$CityList);
                    return;
                }
                
          }     
      
      
     
         if ($WhatsappStatus->status=='ENTERCITY'){
            	//WhatsappModel::updateWhatsappBotEmail($inito[0],$req->message);
            	//$dataProvince = $client->get('https://pro.rajaongkir.com/api/province');
            	$dataSubDis= $this->apiGateway('https://pro.rajaongkir.com/api/city?province=' . $WhatsappStatus->provincelist);
                $dataSubDisList = json_decode($dataSubDis)->rajaongkir->results;
                
            	$CityIDSelected='0';
            	$CitySelected = '-';
            
                $CityList='Anda tidak memilih apa yang di minta, mohon memilih dengan memasukan angka sesuai kota/kabupaten anda.' . PHP_EOL . '*Silahkan Pilih Kota/Kabupaten:*';
                foreach ($dataSubDisList as $row){
                    $CityList = $CityList . PHP_EOL . $row->city_id . ' : ' . $row->city_name ;
                    if( $row->city_id == $req->message) {
                     	 $CitySelected = $row->city_name;
                      	 $CityIDSelected=$row->city_id;
                    }
                }
                //$this->sendMessageText( $inito[0],$CitySelected);
                if ($CitySelected == '-'){
                	$this->sendMessageText( $inito[0],$CityList);
                    return;
                } else {
                  
                  	$SubDistList='*Silahkan Pilih Kecamatan:*';
                    $dataSubDist= $this->apiGateway('https://pro.rajaongkir.com/api/subdistrict?city=' . $CityIDSelected);
                	$dataSubDistList = json_decode($dataSubDist)->rajaongkir->results;
                    //$this->sendMessageText( $inito[0],$CitySelected);
                    foreach ($dataSubDistList as $row){
                         $SubDistList = $SubDistList . PHP_EOL . $row->subdistrict_id . ' : ' . $row->subdistrict_name ;
                    }
                  
                    WhatsappModel::updateWhatsappBotCity($inito[0],$CitySelected,$CityIDSelected);
                	$this->sendMessageText( $inito[0],$SubDistList);
                    return;
                }
                
          }  
        if ($WhatsappStatus->status=='ENTERSUBDIST'){
            	//WhatsappModel::updateWhatsappBotEmail($inito[0],$req->message);
            	//$dataProvince = $client->get('https://pro.rajaongkir.com/api/province');
                    $dataSubDist= $this->apiGateway('https://pro.rajaongkir.com/api/subdistrict?city=' . $WhatsappStatus->citylist);
                	$dataSubDistList = json_decode($dataSubDist)->rajaongkir->results;
                
            	$SubDistIDSelected='0';
            	$SubDistSelected = '-';
            
                $SubDistList='Anda tidak memilih apa yang di minta, mohon memilih dengan memasukan angka sesuai kecamatan anda.' . PHP_EOL . '*Silahkan Pilih Kecamatan:*';
                foreach ($dataSubDistList as $row){
                    $SubDistList = $SubDistList . PHP_EOL . $row->subdistrict_id . ' : ' . $row->subdistrict_name ;
                    if( $row->subdistrict_id == $req->message) {
                     	 $SubDistSelected = $row->subdistrict_name;
                      	 $SubDistIDSelected=$row->subdistrict_id;
                    }
                }
                //$this->sendMessageText( $inito[0],$SubDistSelected);
                if ($SubDistSelected == '-'){
                	$this->sendMessageText( $inito[0],$SubDistList);
                    return;
                } else {
                  
                    $dataOngkir= $this->apiGatewayOngkir('https://pro.rajaongkir.com/api/cost',$req->message);
                    //$OngkirList = json_decode($dataOngkir)->rajaongkir->results[0]->costs[0]->costs[0]->value;
                  	$OngkirList = json_decode($dataOngkir)->rajaongkir->results[0]->costs[0]->cost[0]->value;
                  
                  	//$this->sendMessageText( $inito[0],$dataOngkir);
                   //$this->sendMessageText( $inito[0],strval($OngkirList));
                    WhatsappModel::updateWhatsappBotSubDist($inito[0],$SubDistSelected,$SubDistIDSelected,strval($OngkirList));
                	$this->sendMessageText( $inito[0],'Masukan Alamat Lengkap Anda Berikut Kode Pos:');
                    return;
                }
                
          }  
      
          if ($WhatsappStatus->status=='ENTERADDRESS'){
            	WhatsappModel::updateWhatsappBotAddress($inito[0],$req->message);
            	$this->sendMessageText( $inito[0],'Masukan Password Anda (Bisa di gunakan unk login website):');
            	//$this->sendMessageText( $inito[0],$req->message);
                return;
          }   
          if ($WhatsappStatus->status=='ENTERPASSWORD'){
            	
                $Addrs=$WhatsappStatus->firstname . ' ' . $WhatsappStatus->lastname .PHP_EOL. '+'. $inito[0] .' / '. $WhatsappStatus->email .PHP_EOL. $WhatsappStatus->address . PHP_EOL . $WhatsappStatus->subdisselect . ', ' . $WhatsappStatus->cityselect . ', ' . $WhatsappStatus->provinceselect;
            	WhatsappModel::updateWhatsappBotPassword($inito[0],$req->message,$Addrs);
                $konfirmasi='Konfirmasi Akun Anda' .PHP_EOL.'*Mohon Periksa Dengan Baik Apakah Sudah Benar*'.PHP_EOL.PHP_EOL.'Password : ' . $req->message . $WhatsappStatus->password.PHP_EOL.$Addrs.PHP_EOL.PHP_EOL.'Ongkir : ' . number_format($WhatsappStatus->shipping_cost) . ' / kg (estimasi 3-4 baju per kg atau 2 celana)'.PHP_EOL.PHP_EOL.'SILAHKAN ' . PHP_EOL . '*KETIK YA* apabila data tersebut di atas sudah benar, atau ' .PHP_EOL .'*KETIK -DAFTAR* untuk mengulang pendaftaran.*';
            	//$this->sendMessageText( $inito[0],'Masukan Password Anda (Bisa di gunakan unk login website):');
            	$this->sendMessageText( $inito[0],$konfirmasi);
                return;
          } 
      
          if ($WhatsappStatus->status=='ENTERKONFIRMASI'){
            
             if(strtoupper($req->message)=="YA"){
                WhatsappModel::RegisterID($inito[0],$WhatsappStatus->password,$WhatsappStatus->firstname,$WhatsappStatus->lastname,$inito[0],$WhatsappStatus->email);
                WhatsappModel::RegisterAddress($inito[0],$WhatsappStatus->shipping_address,$WhatsappStatus->subdislist,$WhatsappStatus->shipping_cost);
                WhatsappModel::updateWhatsappBotStatus($inito[0],'');
            	$Informasi='*Simpan Data Ini Dan Jangan Informasikan Kepada Siapapun.*'.PHP_EOL .PHP_EOL . 'Username : ' . $inito[0] . PHP_EOL . 'Password : ' . $WhatsappStatus->password .PHP_EOL .PHP_EOL . 'Anda dapat memesan melalui WA maupun Website.' .PHP_EOL. 'User ID di atas dapat masuk login ke website https://momavel.id/user/login dan bisa mendapatkan benefit mendapatkan barang terupdate sebelum di live kan.'; 
                $Informasi2=$Informasi .PHP_EOL .PHP_EOL . 'Selamat datang di momavel.id sistem otomatis, disini anda bisa melakukan pemesanan mandiri dan mendapatkan diskon sesuai dengan panduan. Semua perintah diawali dengan strip -'  .PHP_EOL.PHP_EOL.'Cara memesan melalui WA: ' . PHP_EOL . '   *-ORDER (spasi) Kode* Contoh: -Order D121 <-- Untuk Masukan Produk Ke Keranjang'. PHP_EOL . '   *-LIST* <-- Untuk Melihat Keranjang Anda ' .PHP_EOL .'   *-HAPUS (spasi) Kode* Contoh: -Hapus D121 <-- Untuk Hapus Produk dr Keranjang' .PHP_EOL .'   *-BAYAR* <-- Untuk Proses Checkout' .PHP_EOL .'   *-BAYARULANG* <-- Apabila Pembayaran gagal/ Lupa catat rekening dapat di ulang kembali'.PHP_EOL .'   *-RESETPASS (spasi) Pass Baru* <-- Untuk Merubah Password Login Web'.PHP_EOL .'   *-MASUKWEB* <-- Untuk Masuk Ke Web Login secara otomatis.' .PHP_EOL .'   *-DAFTARULANG* <-- Untuk daftar ulang apabila ada kesalahan data saat pengisian awal.' ;
            	
            	//$this->sendMessageText( $inito[0],'Masukan Password Anda (Bisa di gunakan unk login website):');
            	$this->sendMessageText( $inito[0],$Informasi2);
               
                return;
             }
          } 
      
      
      
      $statusR=WhatsappModel::checkPhoneRegistered($inito[0]);
          if(count($statusR)==0){ 
            $listgabung='Selamat datang di momavel.id. Di WA ini anda bisa melakukan pemesanan secara mandiri.' .PHP_EOL.PHP_EOL. 'Nomor Anda Belum Teregistrasi. '.PHP_EOL.'Silahkan ketik -DAFTAR untuk mendaftarkan diri '.PHP_EOL.'Jika ada waktu luang dapat browsing di website kami '.PHP_EOL.'Disana banyak produk yang bagus-bagus dan belum di live-kan.';
            $this->sendMessageText( $inito[0],$listgabung);
            return ;
          } else {
            $statusR1=WhatsappModel::checkPhoneRegistered($inito[0]);
            if(count($statusR1)>=1){ 
              $Informasi2='Cara memesan melalui WA: ' . PHP_EOL . '   *-ORDER (Kode)* Contoh: -Order D121 <-- Untuk Masukan Produk Ke Keranjang'. PHP_EOL . '   *-LIST* <-- Untuk Melihat Keranjang Anda ' .PHP_EOL .'   *-HAPUS (Kode)* Contoh: -Hapus D121 <-- Untuk Hapus Produk dr Keranjang' .PHP_EOL .'   *-BAYAR* <-- Untuk Proses Checkout' ;
              $listgabung='Anda Sudah Teregistrasi. '.PHP_EOL.PHP_EOL.$Informasi2;
              $this->sendMessageText( $inito[0],$listgabung);
              return ;
            } else {
              $listgabung='Perintah tidak di kenal. ';
              $this->sendMessageText( $inito[0],$listgabung);
            }
            
          } 
      
      
          if ($WhatsappStatus->status==''){
            	
              $Informasi2='Selamat datang di momavel.id sistem otomatis, disini anda bisa melakukan pemesanan mandiri dan mendapatkan diskon sesuai dengan panduan.'  .PHP_EOL.PHP_EOL.'Cara memesan melalui WA: ' . PHP_EOL . '   *-ORDER (Kode)* Contoh: -Order D121 <-- Untuk Masukan Produk Ke Keranjang'. PHP_EOL . '   *-LIST* <-- Untuk Melihat Keranjang Anda ' .PHP_EOL .'   *-HAPUS (Kode)* Contoh: -Hapus D121 <-- Untuk Hapus Produk dr Keranjang' .PHP_EOL .'   *-BAYAR* <-- Untuk Proses Checkout' ;
              
              $this->sendMessageText( $inito[0],$Informasi2);
              return ;

          }       
      
       
    }

    public function readAll() {
        $data = WhatsappModel::readAll();
        
        $response = [
            'status' => true,
            'data' => $data,
        ];
        return response()->json($response, 200);
    }
  
  public function sendMessageObject($req) {
        

        $curl = curl_init("https://wa-api.momavel.id/send-message");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($req));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $send_header = array('Content-Type: application/json');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

        $response = curl_exec($curl);
        curl_close($curl);
        return response($response, 200)->header('Content-Type', 'application/json');
    }
  
   public function sendMessageText($number,$message) {
        
		$data3 = [
            'number' => $number,
            'message' => $message,
        ];
        $curl = curl_init("https://wa-api.momavel.id/send-message");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data3));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $send_header = array('Content-Type: application/json');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

        $response = curl_exec($curl);
        curl_close($curl);
        return response($response, 200)->header('Content-Type', 'application/json');
    }
  

    public function sendMessage(Request $req) {
        $validator = Validator::make($req->all(), [
            'number' => 'required|max:100',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'message' => $validator->errors(),
            ];
            return response()->json($response, 400);
        }

        $curl = curl_init("https://wa-api.momavel.id/send-message");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($validator->validated()));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $send_header = array('Content-Type: application/json');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

        $response = curl_exec($curl);
        curl_close($curl);
        return response($response, 200)->header('Content-Type', 'application/json');
    }
  
   public function apiGatewayOngkir($url,$subdist_id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => strtoupper('POST'),
			CURLOPT_POSTFIELDS => 'origin=155&originType=city&destination=' . $subdist_id . '&destinationType=subdistrict&weight=1000&courier=ide',
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: 62ac4db3874b3abdb2128808f02c03fe"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
		
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
  
  
  
  
  public function apiGateway($url){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => strtoupper('GET'),

            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: 62ac4db3874b3abdb2128808f02c03fe"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
		
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
  
  
  
  
}
