<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ResellerModel;
use App\Models\UserModel;
use App\Models\SlideshowModel;
use Session;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;


class ResellerController extends Controller
{

    function deleteListOrder(Request $req) {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }
        if (ResellerModel::deleteListOrder($req->list_id)) {

            return back();
        }
    }

    public function showRegisterForm() {
        $data = [
            'page_title' => "Registrasi",
        ];
        return view('reseller/resregister')->with($data);
    }
    public function showRegisterdpForm() {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }
        $data = [
            'page_title' => "EditData",
            'datauser' => ResellerModel::getCustomerDataByInstagram(Session::get('reseller')),
        ];
        return view('reseller/datapribadi')->with($data);
    }

    public function registerUser(Request $req) {
        //dd($req->hasFile('slide_image'));

        if(substr($req->telepon2,0,1)=="0"){
            $req->telepon= $req->kodearea .  substr($req->telepon2,1,strlen($req->telepon2)-1);
        } else {
            $req->telepon= $req->kodearea .  $req->telepon2;
        }
        $custdata = ResellerModel::getCustomerDataByInstagram($req->instagram);
        $custdata2 = ResellerModel::getCustomerDataByInstagram2($req->instagram);
        if (isset($custdata) || isset($custdata2)) {
            return redirect('resellerlogin')->with(['error' => 'Nama Toko sudah terdaftar, Silahkan Gunakan Nama Lain']);
        } else {
            

            $path = null;

            if ($req->hasFile('slide_image')) {
                
                    
                    $image = $req->file('slide_image');
                    $imageName = $image->getClientOriginalName();
                    $path = 'public/reseller/'.md5(time().rand(0,1000)).'.jpg';
    
                    $img = Image::make($image->getRealPath());
                    $img->resize(400, null, function ($constraint) {
                        $constraint->aspectRatio();            
                    });
                    // $img->insert(storage_path('app/public/watermark.png'), 'top-right', 10, 10);
                    $img->encode('jpg', 80);
                    $img->stream();
                    
                    //dd($img);
                    Storage::disk('local')->put($path, $img, 'public');
    
                    $req->sd=$path;
                    

            }

            
            
            if (ResellerModel::registerUser($req)) {
                \Session::put('reseller', $req->instagram);
                //dd($req);
                return redirect('reseller/dashboard')->with(['success' => 'Berhasil mendaftarkan user']);
            } else {
                return redirect(url()->back())->with(['error' => 'Gagal Mendaftarkan User']);
            }
        }
    }



    function editPhoto(Request $req) {
        //dd($req);
        $path = null;
        if ($req->hasFile('slide_image')) {
            try {
                //dd(ResellerModel::getCustomerDataByInstagram($req->igs)->logo_toko);
                $image = $req->file('slide_image');
                $imageName = $image->getClientOriginalName();
                $path = 'public/reseller/'.md5(time().rand(0,1000)).'.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();            
                });
                // $img->insert(storage_path('app/public/watermark.png'), 'top-right', 10, 10);
                $img->encode('jpg', 80);
                $img->stream();
                Storage::disk('local')->put($path, $img, 'public');

                Storage::delete(ResellerModel::getCustomerDataByInstagram($req->igs)->logo_toko);

                if (ResellerModel::EditPhotos($req->igs,$path)) {
                    return redirect('reseller/datapribadi')->with('success', 'Photo Edited added');
                } else {
                    return redirect('reseller/datapribadi')->with('error', 'Photo Edit failed');
                }
            } catch (Exception $e) {
                echo $e->message();
            }
        }
    }
        
    
    function cancelPayment(Request $req) {
        $custdata = UserModel::getPaymentById($req->payment_id);
        //dd($custdata);
        
        $order_list = UserModel::getPaidOrderByPaymentID($req->payment_id);
        
        foreach ($order_list as $row) {
            
            if (ResellerModel::returnToUnpaidOrder($row, $custdata)) {
                if( UserModel::returnToStock($row)) {
                    UserModel::deletePaidOrderList($row, $req->payment_id);
                }
            }
        }
        if (UserModel::deletePayment($req->payment_id)) {
            UserModel::addSystemLog('Delete Pending Payment '.$req->payment_id);
            //$this->sendAWBWhatsapp('628119994888','Username : ' . $custdata->instagram . ' Telp : ' .$custdata->phone . PHP_EOL. 'Order ID : ' . $custdata->id . ' Nominal : ' . number_format($custdata->amount) . PHP_EOL . 'MELAKUKAN RESET PEMBAYARAN MIDTRANS PENDING (' . $custdata->reffer . ')');
            $this->sendAWBWhatsapp($custdata->phone,'Pembayaran Momavel Midtrans Order ID : ' . $custdata->id  . ' Nominal : ' . number_format($custdata->amount) . ' anda telah di reset.'. PHP_EOL . 'Silahkan lakukan pembayaran kembali dengan mengetik -BAYAR atau login melalui website https://momavel.id/rekap/check');
            $this->sendWAToADMIN('Username : ' . $custdata->instagram . ' Telp : ' .$custdata->phone . PHP_EOL. 'Order ID : ' . $custdata->id . ' Nominal : ' . number_format($custdata->amount) . PHP_EOL . 'MELAKUKAN RESET PEMBAYARAN MIDTRANS PENDING (' . $custdata->reffer . ')');
            return redirect('reseller/prosesorder')->with(['success' => 'Berhasil Menghapus Data']);
        } else {
            return redirect('reseller/myacc')->with(['error' => 'Gagal Menghapus Data']);
        }
    }

    function sendWAToADMIN($Text){
        $ss = UserModel::getAdminWa();
        
        $this->sendAWBWhatsapp($ss->wa1,$Text);
        $this->sendAWBWhatsapp($ss->wa2,$Text);
        $this->sendAWBWhatsapp($ss->wa3,$Text);
        $this->sendAWBWhatsapp($ss->wa4,$Text);
        
    }

    function sendAWBWhatsapp($number,$message) {
        
        $data = [
            'number' => $number,
            'message' => $message,
        ];

        $curl = curl_init("https://wa-api.momavel.id/send-message");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT , 1);
        $send_header = array('Content-Type: application/json');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $send_header);

        $response = curl_exec($curl);
        curl_close($curl);
    }



    public function editUser(Request $req) {

       
            
            if (ResellerModel::editUser($req)) {
                
                return redirect('reseller/dashboard')->with(['success' => 'Berhasil merubah data']);
            } else {
                return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
            }
        
    }

    public function editAddress(Request $req) {

       
            
        if (ResellerModel::editAddress($req)) {
            
            return redirect('reseller/dashboard')->with(['success' => 'Berhasil merubah data']);
        } else {
            return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
        }
    
}

public function editPassword(Request $req) {

       
    if(!empty($req->ig) && !empty($req->password)){
        if (ResellerModel::editPassword($req)) {
            
            return redirect('reseller/dashboard')->with(['success' => 'Berhasil merubah data']);
        } else {
            return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
        }
    }
}

    function showDashboard(Request $req) {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }
        $purchasedata = ResellerModel::getPurchaseDataMonthly();
        $chart_tgl = '';
        $chart_qty = '';
        $chart_tx = '';
        $chart_amount = '';
        $monthly_qty = 0;
        $monthly_tx = 0;
        $monthly_amount = 0;

        foreach ($purchasedata as $row) {
            $monthly_amount = $monthly_amount + $row->amount_tx;
            $monthly_tx = $monthly_tx + $row->tx_count;
            $monthly_qty = $monthly_qty + $row->total_item_qty;

            if ($chart_tgl == '') {
                $chart_tgl = "'".date('d-m', strtotime($row->tanggal))."'";
            } else {
                $chart_tgl = $chart_tgl.", '".date('d-m', strtotime($row->tanggal))."'";
            }
            if ($chart_qty == '') {
                $chart_qty = $row->total_item_qty;
            } else {
                $chart_qty = $chart_qty.', '.$row->total_item_qty;
            }
            if ($chart_tx == '') {
                $chart_tx = $row->tx_count;
            } else {
                $chart_tx = $chart_tx.', '.$row->tx_count;
            }
            if ($chart_amount == '') {
                $chart_amount = ($row->amount_tx / 10000);
            } else {
                $chart_amount = $chart_amount.', '.($row->amount_tx / 10000);
            }
        }

        $todaydata = ResellerModel::getPurchaseDataDaily();

        $data = [
            'chart_tgl' => '['.$chart_tgl.']',
            'chart_tx' => '['.$chart_tx.']',
            'chart_qty' => '['.$chart_qty.']',
            'chart_amount' => '['.$chart_amount.']',
            'monthly_qty' => $monthly_qty,
            'monthly_tx' => $monthly_tx,
            'monthly_amount' => $monthly_amount,
            'daily_qty' => $todaydata->total_item_qty,
            'daily_tx' => $todaydata->tx_count,
            'daily_amount' => $todaydata->amount_tx,
        ];
        // dd($data);
        return view('reseller/dashboard')->with($data);
    }

    function loginUser(Request $req) {
        if (ResellerModel::login($req)) {
            \Session::put('reseller', $req->instagram);
            return redirect('reseller/dashboard');
        } else {
            return redirect('resellerlogin')->with(['error' => 'Username / password salah']);
        }
    }

    function showOrderNow(Request $req) {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }

        

        $data = [
            
            'batch_no' => -3,
            'product_mainSimple' => ResellerModel::getProductMainDataSimple(),
            
            
            'instagram' => $req->instagram,
            'tanggal' => $req->date,
        ];
        return view('reseller/ordercmd')->with($data);
    }


    function resellerpayment(Request $req) {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }
        

        //dd($req);
        //$req->tanggal = $req->tglskrg;




        $data = [
            'payment' => $req,
            'paymentsend' => json_encode($req->all()),
            
        ];
        
        return view('userpage/paynow')->with($data);



    
    }


    function showProsesOrder(Request $req) {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }

        //dd(ResellerModel::getOrderByIG(Session::get('reseller')));
        $listorderdata=ResellerModel::getOrderByIG(Session::get('reseller'));
		
        //dd(strval(count($listorderdata)));
        
        if(count($listorderdata) >0){
            $cat = ResellerModel::getOrderByParameterV2($listorderdata[0]->instagram, $listorderdata[0]->tanggal);
            $data = [
                'cart' => $cat,
                'stock' => ResellerModel::getAllStock(),
                'supplier' => ResellerModel::getSupplierList(),
                'master' => $this->getMasterDataArray(),
                'custdata' => ResellerModel::getCustomer(Session::get('reseller')),
                
                'batch_no' => -3,
                
                'product_mainSimple' => ResellerModel::getProductMainDataSimple(),
                'stock' => ResellerModel::getAllStock(),
                
                'userdata' => ResellerModel::getCustomer($req->instagram),
                'instagram' => $req->instagram,
                'tanggal' => $req->date,
            ];
            //dd($data);
            return view('reseller/prosesorder')->with($data);
        } else {
            
            $data = [
            
                'batch_no' => -3,
                'product_mainSimple' => ResellerModel::getProductMainDataSimple(),
                
                
                'instagram' => Session::get('reseller'),
                'tanggal' => $req->date,
            ];
            return view('reseller/ordercmd')->with($data);
        }
        //dd($cat);

        
    }

    function submitManualSelling(Request $req) {
        
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }
        if (ResellerModel::saveSingleSelling($req)) {
            
            return redirect($req->return_url)->with(['success' => 'Order Tersimpan']);
        } else {
            return redirect($req->return_url)->with(['error' => 'Gagal Menyimpan Order']);
        }
    }

    function showmyyacc() {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }
        $data = [
            'paid_order' => ResellerModel::getPaidOrderByInstagram(\Session::get('reseller')),
            'addresses' => ResellerModel::getShippingAddress(\Session::get('reseller')),
            'userdata' => ResellerModel::getCustomerDataByInstagram(\Session::get('reseller')),
            'page_title' => "Akun Saya",
        ];
        return view('reseller/suppacc')->with($data);
    }

    function getMasterDataArray() {
        $master_data = ResellerModel::getMasterData();
        $master_array = [];
        foreach ($master_data as $row) {
            // $master_data[strtoupper($row->internal_code)]['supplier_code'] = $row->supplier_code;
            // $master_data[strtoupper($row->internal_code)]['external_code'] = $row->external_code;
            if (isset($row->internal_code)) {
                $master_array[strtoupper($row->internal_code)] = [
                        'supplier_code' => $row->supplier_code,
                        'external_code' => $row->external_code,
                        'product_name' => $row->product_name,
                        'category' => $row->category,
                        'master_image' => $row->master_image,
                        'supplier_price' => $row->supplier_price,
                        'diskon' => $row->diskon,
                        'hargaasli' => $row->hargaasli,
                        'selling_price' => $row->selling_price,
                ];
            }
        }
        return $master_array;
    }

    function getMasterPhoto(Request $req) {
        $masterdata = ResellerModel::getMasterDataFromInternalCode($req->internal_code);
        $data = [
            'path' => $masterdata->master_image,
            'photo_url' => url('public').\Storage::disk('local')->url($masterdata->master_image),
        ];
        return response()->json($data, 200);
    }

    function getVariantPhoto(Request $req) {
        $variantdata = ResellerModel::getVariantDataFromInternalCode($req);
        $data = [
            'path' => $variantdata->images,
            'photo_url' => url('public').\Storage::disk('local')->url($variantdata->images),
        ];
        return response()->json($data, 200);
    }
}
