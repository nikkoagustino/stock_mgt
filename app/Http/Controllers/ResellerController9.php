<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ResellerModel;
use App\Models\SlideshowModel;
use Session;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;


class ResellerController extends Controller
{


    public function showRegisterForm() {
        $data = [
            'page_title' => "Registrasi",
        ];
        return view('reseller/resregister')->with($data);
    }
    public function showRegisterdpForm() {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }
        $data = [
            'page_title' => "EditData",
            'datauser' => ResellerModel::getCustomerDataByInstagram(Session::get('reseller')),
        ];
        return view('reseller/datapribadi')->with($data);
    }

    public function registerUser(Request $req) {
        //dd($req->hasFile('slide_image'));

        if(substr($req->telepon2,0,1)=="0"){
            $req->telepon= $req->kodearea .  substr($req->telepon2,1,strlen($req->telepon2)-1);
        } else {
            $req->telepon= $req->kodearea .  $req->telepon2;
        }
        $custdata = ResellerModel::getCustomerDataByInstagram($req->instagram);
        $custdata2 = ResellerModel::getCustomerDataByInstagram2($req->instagram);
        if (isset($custdata) || isset($custdata2)) {
            return redirect('resellerlogin')->with(['error' => 'Nama Toko sudah terdaftar, Silahkan Gunakan Nama Lain']);
        } else {
            

            $path = null;

            if ($req->hasFile('slide_image')) {
                
                    
                    $image = $req->file('slide_image');
                    $imageName = $image->getClientOriginalName();
                    $path = 'public/reseller/'.md5(time().rand(0,1000)).'.jpg';
    
                    $img = Image::make($image->getRealPath());
                    $img->resize(400, null, function ($constraint) {
                        $constraint->aspectRatio();            
                    });
                    // $img->insert(storage_path('app/public/watermark.png'), 'top-right', 10, 10);
                    $img->encode('jpg', 80);
                    $img->stream();
                    
                    //dd($img);
                    Storage::disk('local')->put($path, $img, 'public');
    
                    $req->sd=$path;
                    

            }

            
            
            if (ResellerModel::registerUser($req)) {
                \Session::put('reseller', $req->instagram);
                //dd($req);
                return redirect('reseller/dashboard')->with(['success' => 'Berhasil mendaftarkan user']);
            } else {
                return redirect(url()->back())->with(['error' => 'Gagal Mendaftarkan User']);
            }
        }
    }



    function editPhoto(Request $req) {
        //dd($req);
        $path = null;
        if ($req->hasFile('slide_image')) {
            try {
                //dd(ResellerModel::getCustomerDataByInstagram($req->igs)->logo_toko);
                $image = $req->file('slide_image');
                $imageName = $image->getClientOriginalName();
                $path = 'public/reseller/'.md5(time().rand(0,1000)).'.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();            
                });
                // $img->insert(storage_path('app/public/watermark.png'), 'top-right', 10, 10);
                $img->encode('jpg', 80);
                $img->stream();
                Storage::disk('local')->put($path, $img, 'public');

                Storage::delete(ResellerModel::getCustomerDataByInstagram($req->igs)->logo_toko);

                if (ResellerModel::EditPhotos($req->igs,$path)) {
                    return redirect('reseller/datapribadi')->with('success', 'Photo Edited added');
                } else {
                    return redirect('reseller/datapribadi')->with('error', 'Photo Edit failed');
                }
            } catch (Exception $e) {
                echo $e->message();
            }
        }
    }
        
    




    public function editUser(Request $req) {

       
            
            if (ResellerModel::editUser($req)) {
                
                return redirect('reseller/dashboard')->with(['success' => 'Berhasil merubah data']);
            } else {
                return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
            }
        
    }

    public function editAddress(Request $req) {

       
            
        if (ResellerModel::editAddress($req)) {
            
            return redirect('reseller/dashboard')->with(['success' => 'Berhasil merubah data']);
        } else {
            return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
        }
    
}

public function editPassword(Request $req) {

       
    if(!empty($req->ig) && !empty($req->password)){
        if (ResellerModel::editPassword($req)) {
            
            return redirect('reseller/dashboard')->with(['success' => 'Berhasil merubah data']);
        } else {
            return redirect(url()->back())->with(['error' => 'Gagal merubah data']);
        }
    }
}

    function showDashboard(Request $req) {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }
        return view('reseller/dashboard');
    }

    function loginUser(Request $req) {
        if (ResellerModel::login($req)) {
            \Session::put('reseller', $req->instagram);
            return redirect('reseller/dashboard');
        } else {
            return redirect('resellerlogin')->with(['error' => 'Username / password salah']);
        }
    }

    function showOrderNow(Request $req) {
        if (empty(Session::get('reseller'))) { return redirect('resellerlogin'); }

        

        $data = [
            'stock' => ResellerModel::getAllStock(),
            'supplier' => ResellerModel::getSupplierList(),
            'master' => $this->getMasterDataArray(),
            'custdata' => ResellerModel::getCustomer($req->instagram),
            'custaddress' => ResellerModel::getCustomerAddress($req->instagram),
            'batch_no' => -3,
            'product_main' => ResellerModel::getProductMainData(),
            'product_mainSimple' => ResellerModel::getProductMainDataSimple(),
            'stock' => ResellerModel::getAllStock(),
            
            'userdata' => ResellerModel::getCustomer($req->instagram),
            'instagram' => $req->instagram,
            'tanggal' => $req->date,
        ];
        return view('reseller/ordernow')->with($data);
    }

    function getMasterDataArray() {
        $master_data = ResellerModel::getMasterData();
        $master_array = [];
        foreach ($master_data as $row) {
            // $master_data[strtoupper($row->internal_code)]['supplier_code'] = $row->supplier_code;
            // $master_data[strtoupper($row->internal_code)]['external_code'] = $row->external_code;
            if (isset($row->internal_code)) {
                $master_array[strtoupper($row->internal_code)] = [
                        'supplier_code' => $row->supplier_code,
                        'external_code' => $row->external_code,
                        'product_name' => $row->product_name,
                        'category' => $row->category,
                        'master_image' => $row->master_image,
                        'supplier_price' => $row->supplier_price,
                        'diskon' => $row->diskon,
                        'hargaasli' => $row->hargaasli,
                        'selling_price' => $row->selling_price,
                ];
            }
        }
        return $master_array;
    }

}
