<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use App\Models\UserModel;
use App\Models\AdminModel;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        View::share('product_category', UserModel::getAllCategory());
        View::share('total_order_new', count(AdminModel::getListPendingOrder()));
        View::share('total_order_pending', count(AdminModel::getListUnpaidOrder()));
        View::share('total_order_paid', count(AdminModel::getListPaidOrder()));
        View::share('total_order_delivered', count(AdminModel::getListDeliveredOrder()));
        View::share('total_order_refund', count(AdminModel::getRefundListOnProgress()));
    }
}
