<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WhatsappController;
use App\Http\Controllers\SlideshowController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\ResellerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('.env', function(){
    return 403;
});
Route::get('dashboard', [AdminController::class, 'showDashboard']);

Route::get('landingpage', function(){
    return view('landingpage/landingpage');
});

Route::get('ld', [UserController::class, 'showLD']);
Route::get('buynow', [UserController::class, 'showLD']);


Route::get('login', function(){
    return view('login');
});
Route::post('login', [AdminController::class, 'login']);
Route::get('logout', [AdminController::class, 'logout']);

Route::get('system-log', [AdminController::class, 'showSystemLog']);

Route::get('resellerlogin', function(){
    return view('reseller/login');
});
Route::get('resellerregister', function(){
    return view('reseller/register');
});

Route::get('customerselect', [AdminController::class, 'showSelectSales']);
Route::get('whatsappsendmedia/{bank}', [AdminController::class, 'sendwhatsappmedia']);
Route::get('emailsendmedia/{bank}', [AdminController::class, 'sendemailmedia']);
Route::post('customerselect', [AdminController::class, 'showCustomerBlast']);
Route::post('customerblastsend', [AdminController::class, 'sendCustomerBlast']);
Route::get('customerwa/{id}', [AdminController::class, 'showCustomerWA']);
Route::get('customeremail/{id}', [AdminController::class, 'showSupplierEditForm']);

Route::get('supps', [SupplierController::class, 'showTemplate']);
Route::get('supps/dashboard', [SupplierController::class, 'showDashboard']);
Route::get('supps/stockhariini', [SupplierController::class, 'showstockhariini']);
Route::get('supps/rekapstoktotal', [SupplierController::class, 'showRekapStok']);
Route::get('supps/selectsales', [SupplierController::class, 'showSelectSales']);
Route::post('supps/selectsales', [SupplierController::class, 'showSales']);
Route::get('supps/payment', [SupplierController::class, 'showPayment']);

Route::get('supps/login', function(){
    return view('supplier/login');
});
Route::post('supps/login', [SupplierController::class, 'Suplogin']);
Route::get('supps/logout', [SupplierController::class, 'logout']);


Route::get('supplier', [AdminController::class, 'showSupplier']);
Route::get('supplier/add', [AdminController::class, 'showSupplierAddForm']);
Route::post('supplier/add', [AdminController::class, 'submitSupplier']);
Route::get('supplier/edit/{id}', [AdminController::class, 'showSupplierEditForm']);
Route::post('supplier/edit/{id}', [AdminController::class, 'submitEditSupplier']);
Route::get('supplier/delete/{id}', [AdminController::class, 'deleteSupplier']);

Route::get('selling', [AdminController::class, 'showSellingForm']);
Route::post('selling/process', [AdminController::class, 'processSelling']);
Route::get('selling-report', [AdminController::class, 'showSellingReport']);
// Route::get('selling/show/{batch_no}', [AdminController::class, 'showSellingOrder']);
Route::get('selling/show/{instagram}/{date}', [AdminController::class, 'showPersonalOrder']);
Route::get('selling/show-paid/{order_id}', [AdminController::class, 'showPaidOrderDetail']);
Route::get('selling/user-paid/{order_id}', [UserController::class, 'showPaidOrderDetail']);
// Route::get('selling/show-paid/{order_id}', function(){
//     return view('coming-soon');
// });
Route::get('selling/order/delete/{list_id}', [AdminController::class, 'deleteListOrder']);
Route::get('selling/order/deletereseller/{list_id}', [ResellerController::class, 'deleteListOrder']);
Route::get('selling/compare-stock/{batch_no}', [AdminController::class, 'showCompareStock']);
Route::post('selling/add', [AdminController::class, 'submitManualSelling']);
Route::post('reseller/selling/add', [ResellerController::class, 'submitManualSelling']);

Route::get('selling/manual', [AdminController::class, 'showManualSellingForm']);
Route::get('selling/delete/{instagram}/{tanggal}', [AdminController::class, 'deleteListOrderPersonal']);
Route::get('selling/delete3days', [AdminController::class, 'deleteListOrderPersonal3Days']);
Route::get('selling/delete1days', [AdminController::class, 'deleteListOrderPersonal1Days']);

Route::get('paid/delete/{instagram}/{tanggal}/{payment_id}', [AdminController::class, 'deletePaidOrd']);
Route::get('delivered/delete/{payment_id}', [AdminController::class, 'deleteDeliveredOrd']);

// Route::get('selling/delete/{batch_no}', [AdminController::class, 'deleteBatchNo']);

Route::get('stock-in', [AdminController::class, 'showStockInForm']);
Route::post('stock-in', [AdminController::class, 'submitStockIn']);
// Route::get('stock-report', [AdminController::class, 'showStockReport']);
Route::get('stock-report-branded-instock', [AdminController::class, 'showStockReportBrandedInStock']);
Route::get('stock-report-nonbranded', [AdminController::class, 'showStockReportNonBranded']);
Route::get('stock-report', [AdminController::class, 'showStockReportPaginated']);
Route::get('stock-report/search', [AdminController::class, 'showStockReportPaginatedSearch']);
Route::get('stock-reportld', [AdminController::class, 'showStockReportld']);
Route::get('stock-reportav', [AdminController::class, 'showStockReportav']);
Route::get('stock-reportemp', [AdminController::class, 'showStockReportemp']);
Route::get('stock-report/pin/{internal_code}/{status}', [AdminController::class, 'pinStockReport']);
Route::get('stock-photo-variant', [AdminController::class, 'showNoPhotoVariant']);
Route::get('stock-reportkode/pin/{internal_code}/{status}', [AdminController::class, 'pinStockReportKode']);
Route::get('stock/edit/{id}', [AdminController::class, 'editStock']);
Route::get('stock/editkode/{internal_code}', [AdminController::class, 'editStockKode']);
Route::get('stock/delete/{id}', [AdminController::class, 'deleteStock']);
Route::post('stock/save-edit', [AdminController::class, 'saveEditStock']);
Route::get('stock-check/{type}', [AdminController::class, 'checkOverallStock']);
Route::get('delete-order/{id}', [AdminController::class, 'deleteOrderByID']);
Route::get('stock-check/detail/{internal_code}/{variant}/{size}', [AdminController::class, 'compareStockByProduct']);
// Route::get('products', [AdminController::class, 'showProducts']);
// Route::get('products/add', [AdminController::class, 'showAddProductsForm']);
// Route::post('products/add', [AdminController::class, 'submitAddProducts']);

Route::get('discount', [AdminController::class, 'showDiscountPage']);
Route::post('discount', [AdminController::class, 'saveDiscount']);
Route::get('stock-kodeselect', [AdminController::class, 'showStockKode']);


Route::get('api/documentation', function(){
    return view('api-docs');
});

Route::get('api/allinstock', [UserController::class, 'returnProductList']);
Route::get('api/product/{internal_code}', [AdminController::class, 'ajaxSingleProduct']);
Route::get('api/master-product/{internal_code}', [AdminController::class, 'ajaxMasterProduct']);
Route::get('api/product/{internal_code}/{variant}/{size}', [AdminController::class, 'ajaxSingleProductDetail']);
Route::get('api/product-inline/{internal_code}', [AdminController::class, 'ajaxInlineProduct']);
Route::get('api/product-table/{internal_code}', [AdminController::class, 'ajaxTableProduct']);
Route::get('api/generate/{supplier_code}', [AdminController::class, 'ajaxGenerateCode']);
Route::post('api/save_product', [AdminController::class, 'postAjaxSaveProduct']);
Route::get('api/check-external/{external_code}', [AdminController::class, 'ajaxCheckExternal']);
Route::get('api/check-telp/{phone}', [AdminController::class, 'ajaxCheckTelp']);
Route::get('api/check-ig/{username}', [AdminController::class, 'ajaxCheckig']);
Route::post('api/save_master_image', [AdminController::class, 'postAjaxMasterImage']);
Route::post('api/save_selling', [AdminController::class, 'postAjaxSelling']);
Route::post('api/update-order', [AdminController::class, 'ajaxChangeOrder']);
Route::get('api/get-variant/{internal_code}', [AdminController::class, 'ajaxGetVariant']);
Route::get('api/get-size/{internal_code}/{variant}', [AdminController::class, 'ajaxGetSize']);
Route::get('api/get-master-image/{external_code}', [AdminController::class, 'ajaxGetMasterImage']);
Route::post('api/add-to-cart', [UserController::class, 'directCart']);
Route::post('api/add-to-cartrs', [UserController::class, 'directCartrs']);
Route::post('api/add-to-cartld', [UserController::class, 'directCartld']);
Route::post('api/add-to-session-cart', [UserController::class, 'postAjaxAddToSessionCart']);
Route::get('api/cart-qty', [UserController::class, 'ajaxGetCartQty']);
Route::get('api/whatsapp', [WhatsappController::class, 'readAll']);
Route::post('api/whatsapp', [WhatsappController::class, 'receiveWebhook']);
Route::post('api/whatsapp/send-message', [WhatsappController::class, 'sendMessage']);
Route::get('api/get-photo/{internal_code}', [ResellerController::class, 'getMasterPhoto']);
Route::get('api/get-photo/{internal_code}/{variant}', [ResellerController::class, 'getVariantPhoto']);

// cron this
Route::get('api/update-shipping-cost', [UserController::class, 'updateShippingCost']);

Route::get('print/sticker/{param}', [AdminController::class, 'printSticker']);
Route::get('print/shipping/{param}', [AdminController::class, 'printShippingLabel']);

Route::get('coming-soon', function() {
    return view('coming-soon');
});

// Route::post('order/check', [UserController::class, 'checkOrder']);
// Route::post('order/checkout', [UserController::class, 'midtrans']);
Route::get('payment/callback', [UserController::class, 'paymentCallbackV2']);
Route::get('freeshippingflush', function(){
    \Session::forget('fs');
    return redirect('/');
});
Route::get('freeshipping', function(){
    \Session::put('fs','y');
    return redirect('rekap/check');
});
Route::get('freeshippingld', function(){
    \Session::put('fs','y');
    return redirect('rekap/ldcheck');
});
Route::get('payment/success', function() {
    return view('userpage/payment-success');
});
Route::get('payment/failed', function() {
    return view('userpage/payment-failed');
});
Route::get('payment/pending', function() {
    return view('userpage/payment-pending');
});

Route::get('api-gateway', [UserController::class, 'apiGateway']);
Route::post('api-gateway', [UserController::class, 'apiGateway']);

Route::get('payment-report', [AdminController::class, 'showPaymentReport']);
Route::get('list-order/new', [AdminController::class, 'showListPendingOrder']);
Route::get('list-order/newreseller', [AdminController::class, 'showListPendingOrderReseller']);
Route::get('customer-list', [AdminController::class, 'showListCustomer']);
Route::get('customer-edit/{username}', [AdminController::class, 'showEditCustomer']);
Route::get('reseller-list', [AdminController::class, 'showListReseller']);
Route::get('reseller-edit/{username}', [AdminController::class, 'showRegisterdpForm']);
Route::post('adminreseller/editdata', [AdminController::class, 'editResellerUser']);
Route::post('adminreseller/editaddress', [AdminController::class, 'editResellerAddress']);
Route::post('adminreseller/editpassword', [AdminController::class, 'editResellerPassword']);



Route::get('list-order/paid', [AdminController::class, 'showListPaidOrder']);
Route::get('list-order/pending', [AdminController::class, 'showListUnpaidOrder']);
Route::post('order/save-awb', [AdminController::class, 'saveAWB']);

// Route::get('payment-manual/{instagram}/{tanggal}', [AdminController::class, 'showManualPaymentForm']);
// Route::post('payment-manual', [AdminController::class, 'submitManualPayment']);

Route::get('user-management', [AdminController::class, 'showUserManagement']);
Route::post('user-management', [AdminController::class, 'submitUser']);
Route::get('user-management/delete/{username}', [AdminController::class, 'deleteUser']);

Route::get('v2/manual-payment/{instagram}/{tanggal}', [AdminController::class, 'showManualPaymentFormV2']);
Route::post('v2/manual-payment', [AdminController::class, 'submitManualPaymentV2']);
Route::post('resellerpayment', [ResellerController::class, 'resellerpayment']);
Route::post('v2/manual-saveadd', [AdminController::class, 'submitNamaAdd']);
Route::post('v2/manual-deleteadd', [AdminController::class, 'deleteAdd']);
Route::get('manual-confirmation/{payment_id}', [AdminController::class, 'showManualConfirmation']);
Route::post('manual-confirmation', [AdminController::class, 'updateManualConfirmation']);

Route::get('test', [UserController::class, 'autoRecheckPayment']);

Route::get('auto-recheck-payment', [UserController::class, 'autoRecheckPayment']);
Route::get('packing/{payment_id}/{status}', [AdminController::class, 'changePackingStatus']);
Route::get('shipping/{payment_id}/{status}', [AdminController::class, 'changeShippingStatus']);


// Route::get('rekap', function(){
//     return view('userpage/order');
// });
Route::post('bayar/qris', [UserController::class, 'submitPaymentOrderQris']);
Route::post('bayar/cc', [UserController::class, 'submitPaymentCC']);

Route::get('sliderjalan', [UserController::class, 'showSlider']);
Route::get('rekap', [UserController::class, 'showRekapPage']);
Route::post('rekap/user', [UserController::class, 'checkCustomer']);
Route::post('rekap/check', [UserController::class, 'checkOrder']);
Route::post('rekap/checkout', [UserController::class, 'submitPaymentOrder']);

Route::get('rekap/check', [UserController::class, 'showCustomerOrder']);
Route::get('rekap/cancel/{internal_code}', [UserController::class, 'cancelOrderItem']);

Route::get('usercart/login/{instagram}/{password}', [UserController::class, 'showLoginCartPage']);
Route::post('usercart/login', [UserController::class, 'loginUserCart']);

Route::get('ld/beli/{internal_code}', [UserController::class, 'showProductDetailLD']);
Route::get('wa/beli/{internal_code}', [UserController::class, 'showProductDetailWA']);
Route::get('user/logincarthome/{red}', [UserController::class, 'showLoginCartHomePage']);
Route::post('user/logincarthome', [UserController::class, 'loginUserCH']);
Route::get('user/ldlogin/{internal_code}', [UserController::class, 'showLDLoginPage']);
Route::post('user/ldlogin', [UserController::class, 'loginUserLD']);
Route::get('user/ldregister/{internal_code}', [UserController::class, 'showRegisterLDForm']);
Route::post('user/ldregister', [UserController::class, 'registerLDUser']);
Route::get('rekap/ldcheck', [UserController::class, 'showLDCustomerOrder']);
Route::post('rekap/ldcheck', [UserController::class, 'checkLDOrder']);
Route::get('rekap/ldcancel/{internal_code}', [UserController::class, 'cancelLDOrderItem']);
Route::get('reseller/register', [ResellerController::class, 'showRegisterForm']);
Route::get('reseller/datapribadi', [ResellerController::class, 'showRegisterdpForm']);
Route::post('reseller/register', [ResellerController::class, 'registerUser']);
Route::post('reseller/editdata', [ResellerController::class, 'editUser']);
Route::post('reseller/editaddress', [ResellerController::class, 'editAddress']);
Route::post('reseller/editpassword', [ResellerController::class, 'editPassword']);
Route::post('reseller/editphoto', [ResellerController::class, 'editPhoto']);
Route::get('reseller/dashboard', [ResellerController::class, 'showDashboard']);
Route::post('reseller/login', [ResellerController::class, 'loginUser']);
Route::get('reseller/ordernow', [ResellerController::class, 'showOrderNow']);
Route::get('reseller/prosesorder', [ResellerController::class, 'showProsesOrder']);
Route::post('reseller/ordernow', [ResellerController::class, 'submitManualPaymentV2']);
Route::get('reseller/myacc', [ResellerController::class, 'showmyyacc']);

Route::get('refresh', function(){
    \Session::put('reffer', 'WEB');
    return redirect('rekap/check');
});
Route::get('diskon', function(){
    \Session::put('reffer', 'WEB');
    return redirect('rekap/check');
});
Route::get('user/login', [UserController::class, 'showLoginPage']);
Route::post('user/login', [UserController::class, 'loginUser']);
Route::post('user/loginulang', [UserController::class, 'loginUserulang']);
Route::get('user/logout', function(){
    \Session::forget('instagram');
    return redirect('/');
});

Route::get('reseller/logout', function(){
    \Session::forget('reseller');
    return redirect('/');
});
Route::get('user/registerch/{red}', [UserController::class, 'showRegisterFormch']);
Route::post('user/registerch', [UserController::class, 'registerUserch']);
Route::get('user/register', [UserController::class, 'showRegisterForm']);
Route::post('user/register', [UserController::class, 'registerUser']);
Route::get('user/forgot-password', [UserController::class, 'showForgotPasswordForm']);
Route::post('user/forgot-password', [UserController::class, 'submitForgotPassword']);
Route::get('user/reset-password/{encryptedkey}', [UserController::class, 'showResetPasswordForm']);
Route::get('user/unsub/{encryptedkey}', [UserController::class, 'unsubUser']);
Route::post('user/reset-password', [UserController::class, 'submitResetPassword']);
Route::post('admin/reset-password', [AdminController::class, 'submitResetPassword']);

Route::get('list-order/delivered', [AdminController::class, 'showListDeliveredOrder']);

Route::get('repopulate-master', [AdminController::class, 'repopulateMasterData']);
Route::get('repopulate-customer', [AdminController::class, 'repopulateCustomerData']);

Route::get('recheck-payment/{payment_id}', [UserController::class, 'recheckPayment']);
Route::get('cancel-payment/{payment_id}', [AdminController::class, 'cancelPayment']);
Route::get('user-cancel-payment/{payment_id}', [UserController::class, 'cancelPayment']);
Route::get('reseller-cancel-payment/{payment_id}', [ResellerController::class, 'cancelPayment']);


Route::get('allproducts', [UserController::class, 'showUserStockAds']);

Route::get('products', [UserController::class, 'showUserStock']);
Route::get('/', function() {
    return redirect('products');
});
Route::get('product/{internal_code}', [UserController::class, 'showProductDetail']);


Route::get('my-account', [UserController::class, 'showUserDashboard']);
Route::post('my-account/savephone', [UserController::class, 'submitChangePhone']);
Route::get('pay-now/{payment_id}', [UserController::class, 'payNowToMidtrans']);
Route::get('cart', [UserController::class, 'showCart']);
Route::post('save-cart', [UserController::class, 'saveTempCartToCart']);

Route::get('refund/create/{payment_id}', [AdminController::class, 'showRefundCreationPage']);
Route::get('refund/manage/{refund_master_id}', [AdminController::class, 'showRefundManagementPage']);
Route::get('refund/show/{payment_id}', [AdminController::class, 'showRefundManagementPage']);
Route::get('refund/list', [AdminController::class, 'showListRefund']);
Route::post('refund/update', [AdminController::class, 'updateRefundStatus']);

Route::get('flush', function(){
    \Session::flush();
    return redirect('/');
});

Route::get('product-search', [UserController::class, 'userProductSearch']);
Route::post('admin/add-address', [AdminController::class, 'submitAddCustomerAddress']);
Route::post('user/add-address', [UserController::class, 'submitAddCustomerAddress']);
Route::get('user/delete-address/{id}', [UserController::class, 'deleteCustomerAddress']);
Route::get('admin/delete-address/{id}', [AdminController::class, 'deleteCustomerAddresss']);

Route::get('hit-and-run/{instagram}/{status}', [AdminController::class, 'hitAndRunFlag']);
Route::get('hit-and-run', [AdminController::class, 'showListHitAndRunOrder']);

Route::get('category/{category}', [UserController::class, 'showProductInCategory']);

Route::get('resize-product-photo', [AdminController::class, 'resizeAllImages']);
Route::get('sales-report/{periode}', [AdminController::class, 'showSalesReportByPeriode']);
Route::get('sales-report/filter/{start}/{end}', [AdminController::class, 'showSalesReportByDateFilter']);

Route::get('update-supp', [AdminController::class, 'updateSupplierPricePaidOrder']);

Route::get('laporan-penjualan', [AdminController::class, 'showLaporanPenjualan']);
Route::post('laporan-penjualan', [AdminController::class, 'filterLaporanPenjualan']);

Route::get('category', [AdminController::class, 'manageKategori']);
Route::post('category', [AdminController::class, 'addKategori']);
Route::get('category/delete/{category}', [AdminController::class, 'deleteKategori']);

Route::post('slide/add', [SlideshowController::class, 'addSlide']);
Route::get('slide/delete/{id}', [SlideshowController::class, 'deleteSlide']);

Route::get('test-email', [AdminController::class, 'testEmail']);

Route::get('supplierbayar', [AdminController::class, 'showSupplierBayar']);
Route::get('rekapstokadmin/{suppliers}', [AdminController::class, 'showRekapStok']);

Route::get('supplierlogin', [AdminController::class, 'showSupplierLogin']);
Route::get('supplierlogin/add', [AdminController::class, 'showSupplierLoginAddForm']);
Route::post('supplierlogin/add', [AdminController::class, 'submitSupplierLogin']);
Route::get('supplierlogin/editlogin/{id}', [AdminController::class, 'showSupplierLoginEditloginForm']);
Route::get('supplierlogin/edit/{id}', [AdminController::class, 'showSupplierLoginEditForm']);
Route::post('supplierlogin/edit/{id}', [AdminController::class, 'submitEditSupplierLogin']);
Route::get('supplierlogin/delete/{id}', [AdminController::class, 'deleteSupplierLogin']);
Route::get('suppliersales/{id}', [AdminController::class, 'showSupplierSales']);
Route::post('suppliersales', [AdminController::class, 'showSalesAdmin']);
Route::get('supplierstok/{id}', [AdminController::class, 'showSupplierStok']);

Route::get('supplierPayment', [AdminController::class, 'showSupplierPayment']);
Route::get('supplierPayment/add', [AdminController::class, 'showSupplierPaymentAddForm']);
Route::post('supplierPayment/add', [AdminController::class, 'submitSupplierPayment']);
Route::get('supplierPayment/edit/{id}', [AdminController::class, 'showSupplierPaymentEditForm']);
Route::post('supplierPayment/edit/{id}', [AdminController::class, 'submitEditSupplierPayment']);
Route::get('supplierPayment/delete/{id}', [AdminController::class, 'deleteSupplierPayment']);

Route::get('phpinfo.php', function() {
  phpinfo();
});
